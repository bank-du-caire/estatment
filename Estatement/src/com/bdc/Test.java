package com.bdc;

import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import com.bdc.estatement.model.Account;

import java.io.File;
import java.util.Arrays;


public class Test {
//	static <T> void exportToExcel(List<T> objects) {
//		try {
//			HSSFWorkbook workbook = new HSSFWorkbook();
//			CreationHelper createHelper = workbook.getCreationHelper();
//			Sheet sheet = workbook.createSheet();
//			// Create a Font for styling header cells
//			Font headerFont = workbook.createFont();
//			headerFont.setFontHeightInPoints((short) 14);
//			headerFont.setColor(IndexedColors.RED.getIndex());
//			// Create Cell Style for formatting Date
//			CellStyle dateCellStyle = workbook.createCellStyle();
//			dateCellStyle.setDataFormat(createHelper.createDataFormat()
//					.getFormat("dd-MM-yyyy"));
//			// Create a CellStyle with the font
//			CellStyle headerCellStyle = workbook.createCellStyle();
//			headerCellStyle.setFont(headerFont);
//			int rowNum = 1;
//			for (Object object : objects) {
//				Field[] fields = object.getClass().getDeclaredFields();
//				Row headerRow = sheet.createRow(0);
//				for (int i = 0; i < fields.length; i++) {
//					Cell cell = headerRow.createCell(i);
//					cell.setCellValue(fields[i].getName());
//					cell.setCellStyle(headerCellStyle);
//				}
//				Row row = sheet.createRow(rowNum++);
//				for (int i = 0; i < fields.length; i++) {
//					Field field = fields[i];
//					field.setAccessible(true);
//					row.createCell(i).setCellValue(String.valueOf(field.get(object)));
//				}
//
//				for (int i = 0; i < fields.length; i++) {
//					sheet.autoSizeColumn(i);
//				}
//			}
//			FileOutputStream fileOut;
//
//			fileOut = new FileOutputStream("reports test.xls");
//			workbook.write(fileOut);
//			fileOut.close();
//			workbook.close();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}

//	public static void main(String args[]) {
//		Account acc = new Account("0555", "555555", 1, "a@a.com", new Date(),
//				"", new Date(), 0, "", "s", "212", "2", "", "", "", "Ahmed");
//
//		Account acc1 = new Account("0666", "555555", 1, "a@a.com", new Date(),
//				"", new Date(), 0, "", "s", "212", "2", "", "", "", "Ahmed");
//
//		List<Account> accounts = new ArrayList<Account>();
//		accounts.add(acc);
//		accounts.add(acc1);
//
//		exportToExcel(accounts);
//
//		// Account acc = new Account("0555", "555555", 1, "a@a.com", new Date(),
//		// "", new Date(), 0, "", "s", "212", "2", "", "", "", "Ahmed");
//		// Field[] fs = acc.getClass().getDeclaredFields();
//		// System.out.println(fs[0].getName());
//		// fs[0].setAccessible(true);
//		// try {
//		// System.out.println(fs[0].get(acc));
//		// } catch (IllegalArgumentException e) {
//		// // TODO Auto-generated catch block
//		// e.printStackTrace();
//		// } catch (IllegalAccessException e) {
//		// // TODO Auto-generated catch block
//		// e.printStackTrace();
//		// }
//
//		// List<Double> list= new ArrayList<Double>();
//		// list.add(1d);
//		// list.add(1d);
//		// list.add(1d);
//		// list.add(1d);
//		// list.add(1d);
//		// list.add(1d);
//		//
//		// double totalDepit = list.stream()
//		// .collect(Collectors.summingDouble(Double::doubleValue));
//		//
//		// System.out.println(totalDepit);
//		// long credit = list.stream()
//		// .filter(e->e.equalsIgnoreCase("c"))
//		// .count();
//		// long depit = list.stream().filter(e->e.equals("d")).count();
//		// System.out.println("Credit: "+credit);
//		// System.out.println("Depit: "+depit);
//		//
//		// .collect(Collectors.toList());
//
//		// System.out.println(Utils.getStatementFrequencyString(0));
//
//		// LogUtil.initialize();
//		// Account account = new Account();
//		// account.setAccountNumber("00455700000013");
//		// TransactionsDao.getOpeningBalace(account,"2018-02-02","2018-02-02");
//
//		// System.out.println( TransactionsDao.getTransacationPackage("", "",
//		// "").size());
//		// System.out.println(TransactionsDao.getAccountsPackage().size());
//		// System.out.println(Utils.getStatementFrequencyString(1));
//		// System.out.println(TransactionsDao.getAccountByAccountNoAndFrequency(
//		// "00455700000013", 1).toString());
//	}
//	
//	

	  public static void main(String[] args) {

		  // Create a File Object and pass directory/folder name
		  // as a string to it.  
		  String name="Nada Mohey";
		  File fileStructure = new File("F:\\"+name);

		  // File object has a method called as exists() which
		  // Tests whether the file or directory denoted by 
		  // this abstract pathname exists. 
		  if(! fileStructure.exists()) {

		   // File object has a method called as mkdir() which
		   // Creates the directory named by this abstract pathname. 
		   if (fileStructure.mkdir()) {

		    System.out.println("New Folder/Directory created .... ");

		   }
		   else {

		    System.out.println("Oops!!! Something blown up file creation...");

		   }

		  } else {

		   System.out.println("File already exists !!! ...");

		  }

		  // Create a File object and pass as a string full file structure
		  // you want to create. 
		  File subFiles = new File("F:\\Directory\\Sub-Folder\\Sub-Folder2");

		  // File object has a method by name mkdirs() which
		  // Creates the directory named by this abstract pathname,
		  // it creates full file structure as it is given in as string
		  // Note that if this operation fails it may have succeeded 
		  // in creating some of the necessary parent directories. 

		  if(subFiles.mkdirs()) {

		   System.out.println("Full directory structure created ... ");

		  }
		  else {

		   System.out.println("Oops!!! Something blown up files creation...");

		  }

		 }
	  }
	
