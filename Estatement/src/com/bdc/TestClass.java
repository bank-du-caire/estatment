package com.bdc;

import java.net.URI;
import java.net.URISyntaxException;

import microsoft.exchange.webservices.data.core.ExchangeService;
import microsoft.exchange.webservices.data.core.enumeration.misc.ExchangeVersion;
import microsoft.exchange.webservices.data.core.service.item.EmailMessage;
import microsoft.exchange.webservices.data.credential.ExchangeCredentials;
import microsoft.exchange.webservices.data.credential.WebCredentials;
import microsoft.exchange.webservices.data.property.complex.MessageBody;


public class TestClass {

    public static void main(String[] args) {
        TestClass obj = new TestClass();
        obj.testMethod();
    }

    public void testMethod() {
        ExchangeService service = new ExchangeService(
                ExchangeVersion.Exchange2007_SP1);
        ExchangeCredentials credentials = new WebCredentials("soa",
                "BdcS0@2016");

        service.setCredentials(credentials);

        try {
            service.setUrl(new URI("https://owa.banqueducaire.com/EWS/Exchange.asmx"));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        EmailMessage msg;
        try {
            msg = new EmailMessage(service);
            //msg.getFrom().setAddress("SOA@banqueducaire.com") ;
            msg.setSubject("hello world");
            msg.setBody(MessageBody
                    .getMessageBodyFromText("Sent using the EWS API"));
           msg.getToRecipients().add("afaisal2016@gmail.com");
            
            msg.getAttachments().addFileAttachment("test.pdf");
            msg.send();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}