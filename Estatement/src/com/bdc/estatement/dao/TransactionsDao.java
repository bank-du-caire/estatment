package com.bdc.estatement.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.bdc.estatement.connection.ConnectionManager;
import com.bdc.estatement.model.Account;
import com.bdc.estatement.model.AccountPackage;
import com.bdc.estatement.model.Balance;
import com.bdc.estatement.model.Customer;
import com.bdc.estatement.model.EndOffDay;
import com.bdc.estatement.model.EstatementHistory;
import com.bdc.estatement.model.MeezaRegCustomers;
import com.bdc.estatement.model.MeezaTransactions;
import com.bdc.estatement.model.SwiftMsg;
import com.bdc.estatement.model.Transaction;
import com.bdc.estatement.util.Defines;
import com.bdc.estatement.util.LogUtil;

import oracle.jdbc.OracleTypes;

public class TransactionsDao {

	public static List<Account> getUnsendStatements() {
		Connection connection = ConnectionManager.getBDCConnection();
//		String sqlStr = "SELECT * from account a where a.STMT_NEXT_DATE < trunc (sysdate) and a.ESTATEMENT = 1 and a.FAUILERFLAG = 0";
		String sqlStr = "SELECT * from account a where a.STMT_NEXT_DATE < trunc (sysdate) and a.ESTATEMENT = 1 and a.IS_APPROVED=1 and a.FAUILERFLAG = 0 and (a.NONEPERFORMING_ACCOUNT is null or "
				+ " a.NONEPERFORMING_ACCOUNT ='N') and (a.NONEPERFORMING_CUSTOMER is null or a.NONEPERFORMING_CUSTOMER not in (1008,10081,10082,10083,1009,10091,10092,10093,1010,10101,10102,10103))";
		LogUtil.info("Get Acounts from Database.");
		LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		Account account = null;
		List<Account> accounts = new ArrayList<Account>();
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sqlStr);
			if (resultSet.isBeforeFirst()) {
				LogUtil.info("Data retrieved successfully and will deal with it.");
				while (resultSet.next()) {
					String customerCode = resultSet.getString(1);
					String accountNumber = resultSet.getString(2);
					int frequancyStmt = resultSet.getInt(3);
					String email = resultSet.getString(4);
					Date nextDate = resultSet.getDate(5);
					String description = "";
					if (resultSet.getString(6) != null) {
						description = resultSet.getString(6);
					}
					Date lastSentDate = resultSet.getDate(7);
					int noOfFaildAttemps = resultSet.getInt(8);
					String password = resultSet.getString(9);
					String status = resultSet.getString(10);
					String mobile = resultSet.getString(11);
					String address = resultSet.getString(12);
					String accountType = resultSet.getString(13);
					String branchName = resultSet.getString(14);
					String currencyName = resultSet.getString(15);
					String customerName = resultSet.getString(16);
					int createdBy = resultSet.getInt(19);
					int isApproved = resultSet.getInt(22);
					int smsFlag = resultSet.getInt(23);
					int estatementFlag = resultSet.getInt(24);
					int isDeleted = resultSet.getInt(25);

					account = new Account(customerCode, accountNumber, frequancyStmt, email, nextDate, description,
							lastSentDate, noOfFaildAttemps, password, status, mobile, address, accountType, branchName,
							currencyName, customerName);
					account.setIsApproved(isApproved);
					account.setCreatedBy(createdBy);
					account.setIsDeleted(isDeleted);
					account.setSmsFlag(smsFlag);
					account.setEstatementFlag(estatementFlag);
					accounts.add(account);
				}
			} else {
				LogUtil.info("There no data retrieved");
			}
		} catch (SQLException e) {
			LogUtil.error("SQL Error.", e);
		} finally {
			try {
				// if (statement != null && !statement.isClosed()) {
				// statement.close();
				// }
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
			}

		}
		LogUtil.info("Number of Acounts is: " + accounts.size());
		return accounts;
	}

	public static List<String> getHostToHostAccountNumbers(String excludedAccountNumber) {
		Connection connection = ConnectionManager.getBDCConnection();
		String sqlStr = "";
		if (excludedAccountNumber.isEmpty())
			sqlStr = "(SELECT unique(REFERENCE_NO) FROM BDCEFCCP.MSTB_DLY_MSG_OUT@FCC.WORLD "
					+ "where media = 'SWIFT' AND swift_msg_type = '940') minus "
					+ "select cod_acct_no from host_to_host_account";
		else
			sqlStr = "(SELECT unique(REFERENCE_NO) FROM BDCEFCCP.MSTB_DLY_MSG_OUT@FCC.WORLD "
					+ "where media = 'SWIFT' AND swift_msg_type = '940') minus "
					+ "select cod_acct_no from host_to_host_account where cod_acct_no <> '" + excludedAccountNumber
					+ "'";
		LogUtil.info("Get Acounts from Database.");
		LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		List<String> accountNumbers = new ArrayList<String>();
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sqlStr);
			if (resultSet.isBeforeFirst()) {
				LogUtil.info("Data retrieved successfully and will deal with it.");
				while (resultSet.next()) {
					String accountNumber = resultSet.getString(1);
					accountNumbers.add(accountNumber);
				}
			} else {
				LogUtil.info("There no data retrieved");
			}
		} catch (SQLException e) {
			LogUtil.error("SQL Error.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
			}

		}
		LogUtil.info("Number of Acounts is: " + accountNumbers.size());
		return accountNumbers;
	}

	public static List<String> getUserNameAndCode(String accountNumber) {
		Connection connection = ConnectionManager.getBDCConnection();
		String sqlStr = " select ch.NAM_CUST_SHRT,ch.COD_CUST from BDCEFCRP.CH_ACCT_MAST@ARMY.BDC ch"
				+ " where ch.cod_Acct_no='" + accountNumber + "'";
		LogUtil.info("Get Acounts from Database.");
		LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		List<String> info = new ArrayList<String>();
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sqlStr);
			if (resultSet.isBeforeFirst()) {
				LogUtil.info("Data retrieved successfully and will deal with it.");
				while (resultSet.next()) {
					String customerName = resultSet.getString(1);
					String accountNum = resultSet.getString(2);
					info.add(customerName);
					info.add(accountNum);
				}
			} else {
				LogUtil.info("There no data retrieved");
			}
		} catch (SQLException e) {
			LogUtil.error("SQL Error.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
			}

		}
		LogUtil.info("Number of Acounts is: " + info.size());
		return info;
	}

	public static int insertHostToHostAccounts(String userName, String codCust, String accountNumber, String sftpFolder,
			String createdByUserName, String createdByUserID, String sftpPaymentFolder) {
		Connection connection = ConnectionManager.getBDCConnection();
		String sqlStr = "INSERT INTO ESTATEMENT.HOST_TO_HOST_ACCOUNT (COD_CUST, COD_ACCT_NO, "
				+ "FREQ_STMNT, IS_APPROVED, SFTP_FOLDER,CUSTOMER_NAME,CREATEDBYUSER,CREATEDBYUSERID,sftpPaymentFolder) "
				+ "VALUES ('" + codCust + "','" + accountNumber + "', 1, 0, '" + sftpFolder + "','" + userName + "' ,'"
				+ createdByUserName + "','" + createdByUserID + "','" + sftpPaymentFolder + "')";
		LogUtil.info("Get Acounts from Database.");
		LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		Statement statement = null;
		ResultSet resultSet = null;
		int result = 0;
		try {
			statement = connection.createStatement();
			result = statement.executeUpdate(sqlStr);
		} catch (SQLException e) {
			LogUtil.error("SQL Error.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
			}

		}
		return result;
	}

	public static int deleteHostToHostAccounts(String accountNumber) {
		Connection connection = ConnectionManager.getBDCConnection();
		String sqlStr = "DELETE ESTATEMENT.HOST_TO_HOST_ACCOUNT where COD_ACCT_NO = '" + accountNumber + "'";
		LogUtil.info("Get Acounts from Database.");
		LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		Statement statement = null;
		int result = 0;
		try {
			statement = connection.createStatement();
			result = statement.executeUpdate(sqlStr);
		} catch (SQLException e) {
			LogUtil.error("SQL Error.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
			}
		}
		return result;
	}

	public static int updateHostToHostAccount(String accountNumber, String sftpFolder, String sftpPaymentFolder) {
		Connection connection = ConnectionManager.getBDCConnection();
		String sqlStr = "UPDATE ESTATEMENT.HOST_TO_HOST_ACCOUNT set COD_ACCT_NO='" + accountNumber + "', SFTP_FOLDER='"
				+ sftpFolder + "',sftpPaymentFolder='" + sftpPaymentFolder
				+ "', IS_APPROVED = 0 where COD_ACCT_NO like '%" + accountNumber + "%'";
		LogUtil.info("Get Acounts from Database.");
//		LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		Statement statement = null;
		ResultSet resultSet = null;
		int result = 0;
		try {
			statement = connection.createStatement();
			result = statement.executeUpdate(sqlStr);
		} catch (SQLException e) {
			LogUtil.error("SQL Error.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
			}

		}
		return result;
	}

	public static int approveHostToHostAccounts(String accountNumber, int approveFlag, String approvedByUserID) {
		Connection connection = ConnectionManager.getBDCConnection();
		String sqlStr = "update ESTATEMENT.HOST_TO_HOST_ACCOUNT set IS_APPROVED='" + approveFlag
				+ "',STMT_NEXT_DATE = sysdate,APPROVEDBYUSERID='" + approvedByUserID + "' " + "where cod_acct_no='"
				+ accountNumber + "'";
		LogUtil.info("Get Acounts from Database.");
		LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		Statement statement = null;
		ResultSet resultSet = null;
		int result = 0;
		try {
			statement = connection.createStatement();
			result = statement.executeUpdate(sqlStr);
		} catch (SQLException e) {
			LogUtil.error("SQL Error.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
			}

		}
		return result;
	}

	public static List<Account> getAccounts() {
		Connection connection = ConnectionManager.getBDCConnection();
//		String sqlStr = "SELECT * from account where FREQ_STMNT != '0' order by CREATION_DATE desc";
		String sqlStr = "SELECT * from account order by CREATION_DATE desc";
		LogUtil.info("Get Acounts from Database.");
		LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		Account account = null;
		List<Account> accounts = new ArrayList<Account>();
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sqlStr);
			if (resultSet.isBeforeFirst()) {
				LogUtil.info("Data retrieved successfully and will deal with it.");
				while (resultSet.next()) {
					String customerCode = resultSet.getString(1);
					String accountNumber = resultSet.getString(2);
					int frequancyStmt = resultSet.getInt(3);
					String email = resultSet.getString(4);
					Date nextDate = resultSet.getDate(5);
					String description = "";
					if (resultSet.getString(6) != null) {
						description = resultSet.getString(6);
					}
					Date lastSentDate = resultSet.getDate(7);
					int noOfFaildAttemps = resultSet.getInt(8);
					String password = resultSet.getString(9);
					String status = resultSet.getString(10);
					String mobile = resultSet.getString(11);
					String address = resultSet.getString(12);
					String accountType = resultSet.getString(13);
					String branchName = resultSet.getString(14);
					String currencyName = resultSet.getString(15);
					String customerName = resultSet.getString(16);
					int createdBy = resultSet.getInt(19);
					Date creationDate = resultSet.getDate(20);
					int isApproved = resultSet.getInt(22);
					int smsFlag = resultSet.getInt(23);
					int estatementFlag = resultSet.getInt(24);
					int isDeleted = resultSet.getInt(25);
					String nonePerformingAccount = resultSet.getString(29);
					String nonePerformingCustomer = resultSet.getString(30);
					account = new Account(customerCode, accountNumber, frequancyStmt, email, nextDate, description,
							lastSentDate, noOfFaildAttemps, password, status, mobile, address, accountType, branchName,
							currencyName, customerName);
					account.setIsApproved(isApproved);
					account.setCreatedBy(createdBy);
					account.setIsDeleted(isDeleted);
					account.setSmsFlag(smsFlag);
					account.setEstatementFlag(estatementFlag);
					account.setCreationDate(creationDate);
					account.setNonePerformingAccount(nonePerformingAccount);
					account.setNonePerformingCustomer(nonePerformingCustomer);
					accounts.add(account);
				}
			} else {
				LogUtil.info("There no data retrieved");
			}
		} catch (SQLException e) {
			LogUtil.error("SQL Error.", e);
		} finally {
			try {
				// if (statement != null && !statement.isClosed()) {
				// statement.close();
				// }
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
			}

		}
		LogUtil.info("Number of Acounts is: " + accounts.size());
		return accounts;
	}

	public static void updateAccountFauilerFlag(String accountNumber) {
		Connection connection = ConnectionManager.getBDCConnection();
		String sqlStr = "Update account set FAUILERFLAG=? where COD_ACCT_NO = ?";
		LogUtil.info("Update Acount with account number = " + accountNumber);

		LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		// Statement statement = null;
		PreparedStatement statement = null;
		int resultSet = 0;
		try {
			statement = connection.prepareStatement(sqlStr);
			statement.setInt(1, 1);
			statement.setString(2, accountNumber);
			resultSet = statement.executeUpdate();
			if (resultSet > 0) {
				LogUtil.info("Account with account number = " + accountNumber + " has updated successfully");
			}
		} catch (Exception e) {
			try {
				connection.rollback();
				e.printStackTrace();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public static List<SwiftMsg> getSwiftPackage(String accountNo, String FromDate, String ToDate) {
		Connection connection = ConnectionManager.getBDCConnection();
		SwiftMsg swiftmsg = null;
		List<SwiftMsg> msgs_list = new ArrayList<SwiftMsg>();
		CallableStatement cstmt = null;
		ResultSet resultSet = null;
		try {
			cstmt = connection.prepareCall("{? = call PK_INQUIRY.GET_SWIFT(?,?,?)}");
			cstmt.registerOutParameter(1, OracleTypes.CURSOR);
			cstmt.setString(2, accountNo.trim());
			cstmt.setDate(3, java.sql.Date.valueOf(FromDate));
			cstmt.setDate(4, java.sql.Date.valueOf(ToDate));
			cstmt.executeQuery();
			resultSet = (ResultSet) cstmt.getObject(1);

			while (resultSet.next()) {

				String MSG = resultSet.getString(3);
				String REF_NO = resultSet.getString(4);

				swiftmsg = new SwiftMsg(MSG, REF_NO);

				msgs_list.add(swiftmsg);
			}

		} catch (SQLException e) {
			LogUtil.error("SQL Error {}.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
			}

		}
		LogUtil.info("Number of transactions is: " + msgs_list.size());
		return msgs_list;
	}

	public static List<Transaction> getTransacation(String accountNo, String startDate, String endDate) {
		Connection connection = ConnectionManager.getFlexConnection();
		String sqlStr = "SELECT to_clob(c.nam_cust_full) as CUST_FULL_NAME,\r\n"
				+ "				c.cod_cust_id CUSTOMER_ID,B.COD_ACCT_NO ACCOUNT_NUMBER,\r\n"
				+ "				to_clob(B.COD_ACCT_TITLE) as ACCOUNT_TITLE,to_clob(F.nam_product) as PRODUCT_NAME, \r\n"
				+ "				to_clob(D.NAM_BRANCH) as BRANCH_NAME,A.DAT_TXN DATE_OF_TXN, A.DAT_VALUE DATE_VALUE, \r\n"
				+ "				A.amt_txn TOTAL_BALANCE,B.BAL_BOOK BOOK_BALANCE, \r\n"
				+ "				TO_CHAR (B.BAL_AVAILABLE) AVAILABLE_BALANCE, to_clob(E.NAM_CCY_SHORT) as CURRENCY, \r\n"
				+ "				to_clob(c.TXT_CUSTADR_ADD1 || ' ' || c.TXT_CUSTADR_ADD2 || ' ' || c.TXT_CUSTADR_ADD3) as ADDRESS, \r\n"
				+ "				b.AMT_LAST_CR_INT_ACCR LAST_CREDIT, b.AMT_LAST_DR_INT_ACCR LAST_DEBIT, \r\n"
				+ "				to_clob(A.TXT_TXN_DESC) as TXN_DESCRIPTION, A.COD_DRCR CRDR_INDICATOR, A.AMT_TXN TXN_AMOUNT\r\n"
				+ "				FROM (SELECT COD_ACCT_NO,DAT_TXN,AMT_TXN,COD_DRCR,TXT_TXN_DESC,DAT_VALUE \r\n"
				+ "				FROM BDCEFCRP.VW_CH_NOBOOK AA WHERE (AA.COD_ACCT_NO = '" + accountNo + "' \r\n"
				+ "				     AND AA.DAT_TXN >= TO_DATE('" + startDate
				+ " 12:00:00 AM','MM/DD/YYYY HH:MI:SS AM')  \r\n" + "                 AND AA.DAT_TXN <= TO_DATE('"
				+ endDate + " 12:00:00 AM','MM/DD/YYYY HH:MI:SS AM')  \r\n"
				+ "				     )) A, ch_acct_mast B, ci_custmast C, ba_cc_brn_mast D, \r\n"
				+ "				ba_ccy_code E,CH_PROD_MAST F WHERE B.COD_ACCT_NO = A.COD_ACCT_NO \r\n"
				+ "				AND B.COD_CC_BRN = D.COD_CC_BRN AND b.COD_CCY = E.COD_CCY \r\n"
				+ "				AND B.FLG_MNT_STATUS = 'A' AND b.cod_cust = c.cod_cust_id \r\n"
				+ "				AND C.FLG_MNT_STATUS = 'A' AND B.COD_PROD = F.COD_PROD";
		LogUtil.info("Get Transaction from Flex Cube.");

		Transaction transacation = null;
		List<Transaction> transacations = new ArrayList<Transaction>();
		Statement statement = null;
		ResultSet resultSet = null;
		// int running =start ;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sqlStr);
			if (resultSet.isBeforeFirst()) {
				LogUtil.info("Data retrieved successfully and will deal with it.");
				while (resultSet.next()) {
					String customerName = resultSet.getString(2);
					String customerId = resultSet.getString(3);
					String accountNumber = resultSet.getString(4);
					String accountTitle = resultSet.getString(5);
					String productName = resultSet.getString(6);
					String branchName = resultSet.getString(7);
					Date transactionDate = resultSet.getDate(8);
					Date valueDate = resultSet.getDate(9);
					String currecy = resultSet.getString(10);
					String address = resultSet.getString(11);
					String desc = resultSet.getString(12);
					String indicator = resultSet.getString(13);
					double amount = resultSet.getDouble(14);
					// if (indicator == 'c')
					// {
					// running = running + amount;
					// transacation.setTotalBalance(running);
					// }
					// else
					// {
					// running = running + amount;
					//
					// transacation.setTotalBalance(start - amount);
					// }
					transacation = new Transaction(customerName, customerId, accountNumber, accountTitle, productName,
							branchName, transactionDate, valueDate, currecy, address, desc, indicator, amount);
					transacations.add(transacation);
				}
			} else {
				LogUtil.info("There no data retrieved");
			}
			LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		} catch (SQLException e) {
			LogUtil.error("SQL Error {}.", e);
		} finally {
			try {
				// if (statement != null && !statement.isClosed()) {
				// statement.close();
				// }
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
			}

		}
		LogUtil.info("Number of transactions is: " + transacations.size());
		return transacations;
	}

	public static List<Transaction> getTransacationPackage(String accountNo, String startDate, String endDate,
			double startBalance) {
		Connection connection = ConnectionManager.getBDCConnection();
		Transaction transacation = null;
		List<Transaction> transacations = new ArrayList<Transaction>();
		CallableStatement cstmt = null;
		ResultSet resultSet = null;
		try {
			if (ConnectionManager.bdcUrl.contains("172.17.63.178"))
				cstmt = connection.prepareCall("{? = call PK_INQUIRY.GET_TXN_STMT(?,?,?)}");
			else
				cstmt = connection.prepareCall("{? = call FCCTCIB.PK_INQUIRY.GET_TXN_STMT(?,?,?)}");
			cstmt.registerOutParameter(1, OracleTypes.CURSOR);
			cstmt.setString(2, accountNo);
			cstmt.setDate(3, java.sql.Date.valueOf(startDate));
			cstmt.setDate(4, java.sql.Date.valueOf(endDate));
			cstmt.executeQuery();
			resultSet = (ResultSet) cstmt.getObject(1);

			while (resultSet.next()) {
				String customerName = resultSet.getString(3);
				String customerId = resultSet.getString(4);
				String accountNumber = resultSet.getString(5);
				String accountTitle = resultSet.getString(6);
				String productName = resultSet.getString(7);
				String branchName = resultSet.getString(8);
				Date transactionDate = resultSet.getDate(9);
				Date valueDate = resultSet.getDate(10);
				String currecy = resultSet.getString(11);
				String address = resultSet.getString(12);
				String desc = resultSet.getString(13);
				String indicator = resultSet.getString(14);
				double amount = resultSet.getDouble(15);

				transacation = new Transaction(customerName, customerId, accountNumber, accountTitle, productName,
						branchName, transactionDate, valueDate, currecy, address, desc, indicator, amount);

				if (indicator.equalsIgnoreCase("c")) {
					startBalance = startBalance + amount;
					transacation.setTotalBalance(startBalance);
					transacation.setCredit(amount);
				} else {
					startBalance = startBalance - amount;
					transacation.setTotalBalance(startBalance);
					transacation.setDebit(amount);
				}
				transacations.add(transacation);
			}

		} catch (SQLException e) {
			LogUtil.error("SQL Error {}.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
			}

		}
		LogUtil.info("Number of transactions is: " + transacations.size());
		return transacations;
	}

	public static AccountPackage getAccountsPackage(String AccountNo) {
		Connection connection = ConnectionManager.getBDCConnection();
		AccountPackage account = null;
		// List<AccountPackage> accounts = new ArrayList<AccountPackage>();
		CallableStatement cstmt = null;
		ResultSet resultSet = null;
		try {
			cstmt = connection.prepareCall("{? = call ESTATEMENT.PK_INQUIRY.GET_ACCOUNT(?)}");
			cstmt.registerOutParameter(1, OracleTypes.CURSOR);
			cstmt.setString(2, AccountNo);
			cstmt.executeQuery();
			resultSet = (ResultSet) cstmt.getObject(1);

			while (resultSet.next()) {
				LogUtil.info("Data retrieved successfully and will deal with it.");
				String fullName = resultSet.getString(3);
				String customerId = resultSet.getString(4);
				String accountNumber = resultSet.getString(5);
				String accountTitle = resultSet.getString(6);
				String productName = resultSet.getString(7);
				String branchName = resultSet.getString(8);
				double bookBalance = resultSet.getDouble(9);
				double availableBalance = resultSet.getDouble(10);
				double netBalance = resultSet.getDouble(11);
				String currecy = resultSet.getString(12);
				String address = resultSet.getString(13);
				String lastCredit = resultSet.getString(14);
				String lastDebit = resultSet.getString(15);

				account = new AccountPackage(fullName, customerId, accountNumber, accountTitle, productName, branchName,
						bookBalance, availableBalance, netBalance, currecy, address, lastCredit, lastDebit);
			}
		} catch (SQLException e) {
			LogUtil.error("SQL Error.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
			}
		}
		return account;
	}

	public static AccountPackage getCorpAccountsPackage(String AccountNo) {
		Connection connection = ConnectionManager.getBDCConnection();
		AccountPackage account = null;
		//// List<AccountPackage> accounts = new ArrayList<AccountPackage>();
		CallableStatement cstmt = null;
		ResultSet resultSet = null;
		try {
			if (ConnectionManager.bdcUrl.contains("85.199"))
				cstmt = connection.prepareCall("{? = call GEFUPLUS.PK_INQUIRY.GET_ACCOUNT(?)}");
			else if (ConnectionManager.bdcUrl.contains("60.185"))
				cstmt = connection.prepareCall("{? = call FCCTCIB.PK_INQUIRY.GET_ACCOUNT(?)}");
			cstmt.registerOutParameter(1, OracleTypes.CURSOR);
			cstmt.setString(2, AccountNo);
			cstmt.executeQuery();
			resultSet = (ResultSet) cstmt.getObject(1);

			while (resultSet.next()) {
				LogUtil.info("Data retrieved successfully and will deal with it.");
				String fullName = resultSet.getString(3);
				String customerId = resultSet.getString(4);
				String accountNumber = resultSet.getString(5);
				String accountTitle = resultSet.getString(6);
				String productName = resultSet.getString(7);
				String branchName = resultSet.getString(8);
				double bookBalance = resultSet.getDouble(9);
				double availableBalance = resultSet.getDouble(10);
				double netBalance = resultSet.getDouble(11);
				String currecy = resultSet.getString(12);
				String address = resultSet.getString(13);
				String lastCredit = resultSet.getString(14);
				String lastDebit = resultSet.getString(15);

				account = new AccountPackage(fullName, customerId, accountNumber, accountTitle, productName, branchName,
						bookBalance, availableBalance, netBalance, currecy, address, lastCredit, lastDebit);
			}
		} catch (SQLException e) {
			LogUtil.error("SQL Error.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
			}
		}
		return account;
	}

	public static EndOffDay getEndOffDayDateProcess() {
		Connection conn = ConnectionManager.getFlexConnection();
//		Connection conn = ConnectionManager.getBDCConnection();
		List<EstatementHistory> estatementHistories = new ArrayList<EstatementHistory>();
		EstatementHistory estatementHistory = null;
		Date dateProcess = null, dateLastProcess = null, dateNextProcess = null;
		EndOffDay endOffDay = null;
		try {
			String query = "select Dat_last_process,DAT_PROCESS,Dat_next_process from bdcefcrp.BA_BANK_MAST";
//			String query = "select Dat_last_process,DAT_PROCESS,Dat_next_process from estatement.BA_BANK_MAST where TRUNC(CREATION_DATE) = TRUNC(SYSDATE)";
			Statement state = conn.createStatement();
			ResultSet rs = state.executeQuery(query);
			while (rs.next()) {
				dateLastProcess = rs.getDate(1);
				dateProcess = rs.getDate(2);
				dateNextProcess = rs.getDate(3);
				endOffDay = new EndOffDay(dateLastProcess, dateProcess, dateNextProcess);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return endOffDay;
	}

	public static int insertEndOfDay() {
		int status = 0;
		Connection conn = ConnectionManager.getBDCConnection();
		try {
			EndOffDay endOffDay = getEndOffDayDateProcess();
			if (!getEndOffDayDateProcessTest(endOffDay)) {
				PreparedStatement ps = conn.prepareStatement(
						"insert into BA_BANK_MAST (Dat_last_process,DAT_PROCESS,Dat_next_process) VALUES (?,?,?)");
				ps.setDate(1, java.sql.Date
						.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(endOffDay.getDateLastProcess())));
				ps.setDate(2,
						java.sql.Date.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(endOffDay.getDateProcess())));
				ps.setDate(3, java.sql.Date
						.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(endOffDay.getDateNextProcess())));
				status = ps.executeUpdate();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return status;
	}

	public static boolean getEndOffDayDateProcessTest(EndOffDay endOffDay) {
		Connection conn = ConnectionManager.getBDCConnection();
		try {
			java.sql.Date.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(endOffDay.getDateLastProcess()));
			String query = "select * from ba_bank_mast where Dat_last_process = to_date('"
					+ java.sql.Date.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(endOffDay.getDateLastProcess()))
					+ "','yyyy-mm-dd') and DAT_PROCESS = to_date('"
					+ java.sql.Date.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(endOffDay.getDateProcess()))
					+ "','yyyy-mm-dd') and Dat_next_process = to_date('"
					+ java.sql.Date.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(endOffDay.getDateNextProcess()))
					+ "','yyyy-mm-dd')";
			Statement state = conn.createStatement();
			ResultSet rs = state.executeQuery(query);
			while (rs.next()) {
				return true;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}

	// it takes account number and date post then it get back closing balance for
	// all transactions since that day.
	public static double getClosingBalance(String AccountNo, Date datePost) {
		Connection conn = ConnectionManager.getFlexConnection();
		List<EstatementHistory> estatementHistories = new ArrayList<EstatementHistory>();
		EstatementHistory estatementHistory = null;
		double closingBalance = 0.0;
		try {
			String query = " select sum(decode(cod_drcr,'C',amt_txn,-amt_txn)) "
					+ " from bdcefcrp.vw_ch_nobook where cod_acct_no='" + AccountNo.trim() + "' and dat_post < '"
					+ new SimpleDateFormat("dd-MMM-yyyy").format(datePost) + "'";
			Statement state = conn.createStatement();
			ResultSet rs = state.executeQuery(query);
			while (rs.next()) {
				closingBalance = rs.getDouble(1);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return closingBalance;
	}

	public static void updateCorpAccountDates(Account account) {
		Connection connection = ConnectionManager.getBDCConnection();
		String sqlStr = "Update FCCTCIB.IB_ACCOUNT set LAST_SENT_DATE = ? ,STMT_NEXT_DATE = ? where COD_ACCT_NO = ? and FREQ_STMNT = ?";

		LogUtil.info("Update Acount with account number = " + account.getAccountNumber());

		LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		// Statement statement = null;
		PreparedStatement statement = null;
		int resultSet = 0;
		try {
			statement = connection.prepareStatement(sqlStr);
			statement.setDate(1,
					java.sql.Date.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(account.getLastSentDate())));
			statement.setDate(2,
					java.sql.Date.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(account.getNextDate())));
			statement.setString(3, account.getAccountNumber());
			statement.setInt(4, account.getFrequancyStmt());

			resultSet = statement.executeUpdate();
			if (resultSet > 0) {
				LogUtil.info("Account with account number = " + account.getAccountNumber() + " and frequancy = "
						+ account.getFrequancyStmt() + " has updated successfully");
			}
		} catch (Exception e) {
			try {
				connection.rollback();
				e.printStackTrace();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public static void updateAccountDates(Account account) {
		Connection connection = ConnectionManager.getBDCConnection();

		String sqlStr = "Update account set LAST_SENT_DATE = ? ,STMT_NEXT_DATE = ? where COD_ACCT_NO = ? and FREQ_STMNT = ?";

		LogUtil.info("Update Acount with account number = " + account.getAccountNumber());

		LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		// Statement statement = null;
		PreparedStatement statement = null;
		int resultSet = 0;
		try {
			statement = connection.prepareStatement(sqlStr);
			statement.setDate(1,
					java.sql.Date.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(account.getLastSentDate())));
			statement.setDate(2,
					java.sql.Date.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(account.getNextDate())));
			statement.setString(3, account.getAccountNumber());
			statement.setInt(4, account.getFrequancyStmt());

			resultSet = statement.executeUpdate();
			if (resultSet > 0) {
				LogUtil.info("Account with account number = " + account.getAccountNumber() + " and frequancy = "
						+ account.getFrequancyStmt() + " has updated successfully");
			}
		} catch (Exception e) {
			try {
				connection.rollback();
				e.printStackTrace();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public static void updateAccountWelcomeMail(Account account) {
		Connection connection = ConnectionManager.getBDCConnection();

		String sqlStr = "Update account set WELCOMEMAIL = ? where COD_ACCT_NO = ? and FREQ_STMNT = ?";

		LogUtil.info("Update Acount with account number = " + account.getAccountNumber());

		LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		// Statement statement = null;
		PreparedStatement statement = null;
		int resultSet = 0;
		try {
			statement = connection.prepareStatement(sqlStr);
			statement.setInt(1, account.getWelcomeMail());
			statement.setString(2, account.getAccountNumber());
			statement.setInt(3, account.getFrequancyStmt());
			resultSet = statement.executeUpdate();
			if (resultSet > 0) {
				LogUtil.info("Account with account number = " + account.getAccountNumber() + " and frequancy = "
						+ account.getFrequancyStmt() + " has updated successfully");
			}
		} catch (Exception e) {
			try {
				connection.rollback();
				e.printStackTrace();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public static List<Account> getAccountsByToday() {
		Connection connection = ConnectionManager.getBDCConnection();
		String sqlStr = "select * from ACCOUNT where STMT_NEXT_DATE = trunc(sysdate) and ESTATEMENT = 1 and IS_APPROVED = 1 ";
		// String sqlStr = "select * from ACCOUNT where STMT_NEXT_DATE = trunc(sysdate)
		// and ESTATEMENT = 1 and IS_APPROVED = 1 ";
		LogUtil.info("Get Acounts from Database.");
		LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		Account account = null;
		List<Account> accounts = new ArrayList<Account>();
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sqlStr);
			if (resultSet.isBeforeFirst()) {
				LogUtil.info("Data retrieved successfully and will deal with it.");
				while (resultSet.next()) {
					String customerCode = resultSet.getString(1);
					String accountNumber = resultSet.getString(2);
					int frequancyStmt = resultSet.getInt(3);
					String email = resultSet.getString(4);
					Date nextDate = resultSet.getDate(5);
					String description = resultSet.getString(6);
					Date lastSentDate = resultSet.getDate(7);
					int noOfFaildAttemps = resultSet.getInt(8);
					String password = resultSet.getString(9);
					String status = resultSet.getString(10);
					String mobile = resultSet.getString(11);
					String address = resultSet.getString(12);
					String accountType = resultSet.getString(13);
					String branchName = resultSet.getString(14);
					String currencyName = resultSet.getString(15);
					String customerName = resultSet.getString(16);
					int smsFlag = resultSet.getInt(23);
					int estatementFlag = resultSet.getInt(24);
					int welcomeMail = resultSet.getInt(31);

					String nonePerformingAccount = resultSet.getString(29);
					String nonePerformingCustomer = resultSet.getString(30);
					account = new Account(customerCode, accountNumber, frequancyStmt, email, nextDate, description,
							lastSentDate, noOfFaildAttemps, password, status, mobile, address, accountType, branchName,
							currencyName, customerName);
					account.setSmsFlag(smsFlag);
					account.setEstatementFlag(estatementFlag);
					account.setNonePerformingAccount(nonePerformingAccount);
					account.setNonePerformingCustomer(nonePerformingCustomer);
					account.setWelcomeMail(welcomeMail);
					accounts.add(account);
				}
			} else {
				LogUtil.info("There no data retrieved");
			}
		} catch (SQLException e) {
			LogUtil.error("SQL Error.", e);
		} finally {
			try {
				// if (statement != null && !statement.isClosed()) {
				// statement.close();
				// }
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
			}

		}
		LogUtil.info("Number of Acounts is: " + accounts.size());
		return accounts;

	}

	public static List<Account> getApprovedHotToHostAccounts() {
		Connection connection = ConnectionManager.getBDCConnection();
		String sqlStr = "SELECT COD_CUST, COD_ACCT_NO, FREQ_STMNT,STMT_NEXT_DATE, LAST_SENT_DATE,"
				+ " CREATION_DATE,IS_APPROVED, APPROVEDBYUSERID,"
				+ " SFTP_FOLDER,Email,Customer_NAME FROM ESTATEMENT.HOST_TO_HOST_ACCOUNT where IS_APPROVED=1 "
				+ " and trunc(STMT_NEXT_DATE) = trunc(sysdate)";
		LogUtil.info("Get Acounts from Database.");
		LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		Account account = null;
		List<Account> accounts = new ArrayList<Account>();
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sqlStr);
			if (resultSet.isBeforeFirst()) {
				LogUtil.info("Data retrieved successfully and will deal with it.");
				while (resultSet.next()) {
					String customerCode = resultSet.getString(1);
					String accountNumber = resultSet.getString(2);
					int frequancyStmt = resultSet.getInt(3);
					Date nextDate = resultSet.getDate(4);
					Date lastSentDate = resultSet.getDate(5);
					Date creationDate = resultSet.getDate(6);
					int isApproved = resultSet.getInt(7);
					String sftpFolder = resultSet.getString(9);
					String email = resultSet.getString(10);
					String customerName = resultSet.getString(11);
					account = new Account();
					account.setAccountNumber(accountNumber);
					account.setCustomerCode(customerCode);
					account.setFrequancyStmt(frequancyStmt);
					account.setNextDate(nextDate);
					account.setLastSentDate(lastSentDate);
					account.setCreationDate(creationDate);
					account.setIsApproved(isApproved);
					account.setSftpFolder(sftpFolder);
					account.setEmail(email);
					account.setCustomerName(customerName);
					accounts.add(account);
				}
			} else {
				LogUtil.info("There no data retrieved");
			}
		} catch (SQLException e) {
			LogUtil.error("SQL Error.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
			}

		}
		LogUtil.info("Number of Acounts is: " + accounts.size());
		return accounts;

	}

	public static List<String> getApprovedHotToHostPaymentFolders() {
		Connection connection = ConnectionManager.getBDCConnection();
		String sqlStr = "SELECT SFTPPAYMENTFOLDER FROM ESTATEMENT.HOST_TO_HOST_ACCOUNT where IS_APPROVED=1";
		LogUtil.info("Get Acounts from Database.");
		LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		Account account = null;
		List<String> paymentFolders = new ArrayList<String>();
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sqlStr);
			if (resultSet.isBeforeFirst()) {
				LogUtil.info("Data retrieved successfully and will deal with it.");
				while (resultSet.next()) {
					String paymentFolder = resultSet.getString(1);
					paymentFolders.add(paymentFolder);
				}
			} else {
				LogUtil.info("There no data retrieved");
			}
		} catch (SQLException e) {
			LogUtil.error("SQL Error.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
			}

		}
		LogUtil.info("Number of Acounts is: " + paymentFolders.size());
		return paymentFolders;

	}

	public static List<Account> getAllHotToHostAccounts() {
		Connection connection = ConnectionManager.getBDCConnection();
		String sqlStr = "SELECT COD_CUST, COD_ACCT_NO, FREQ_STMNT,STMT_NEXT_DATE, LAST_SENT_DATE,"
				+ " CREATION_DATE,IS_APPROVED, APPROVEDBYUSERID, "
				+ " SFTP_FOLDER,Email,Customer_NAME,createdbyuserid,SFTPPAYMENTFOLDER FROM ESTATEMENT.HOST_TO_HOST_ACCOUNT";
		LogUtil.info("Get Acounts from Database.");
		LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		Account account = null;
		List<Account> accounts = new ArrayList<Account>();
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sqlStr);
			if (resultSet.isBeforeFirst()) {
				LogUtil.info("Data retrieved successfully and will deal with it.");
				while (resultSet.next()) {
					String customerCode = resultSet.getString(1);
					String accountNumber = resultSet.getString(2);
					int frequancyStmt = resultSet.getInt(3);
					Date nextDate = resultSet.getDate(4);
					Date lastSentDate = resultSet.getDate(5);
					Date creationDate = resultSet.getDate(6);
					int isApproved = resultSet.getInt(7);
					String sftpFolder = resultSet.getString(9);
					String email = resultSet.getString(10);
					String customerName = resultSet.getString(11);
					int createdByUserID = resultSet.getInt(12);
					String sftpPaymentFolder = resultSet.getString(13);
					account = new Account();
					account.setAccountNumber(accountNumber);
					account.setCustomerCode(customerCode);
					account.setFrequancyStmt(frequancyStmt);
					account.setNextDate(nextDate);
					account.setLastSentDate(lastSentDate);
					account.setCreationDate(creationDate);
					account.setIsApproved(isApproved);
					account.setSftpFolder(sftpFolder);
					account.setEmail(email);
					account.setCustomerName(customerName);
					account.setCreatedBy(createdByUserID);
					account.setSftpPaymentFolder(sftpPaymentFolder);
					accounts.add(account);
				}
			} else {
				LogUtil.info("There no data retrieved");
			}
		} catch (SQLException e) {
			LogUtil.error("SQL Error.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
			}

		}
		LogUtil.info("Number of Acounts is: " + accounts.size());
		return accounts;

	}

	public static void updateAccountDatesHostToHost(Account account) {
		Connection connection = ConnectionManager.getBDCConnection();

		String sqlStr = "UPDATE ESTATEMENT.HOST_TO_HOST_ACCOUNT SET LAST_SENT_DATE = ?, STMT_NEXT_DATE = ? where COD_ACCT_NO = ? and FREQ_STMNT = ?";

		LogUtil.info("Update Acount with account number = " + account.getAccountNumber());

		LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		// Statement statement = null;
		PreparedStatement statement = null;
		int resultSet = 0;
		try {
			statement = connection.prepareStatement(sqlStr);
			statement.setDate(1,
					java.sql.Date.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(account.getLastSentDate())));
			statement.setDate(2,
					java.sql.Date.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(account.getNextDate())));
			statement.setString(3, account.getAccountNumber());
			statement.setInt(4, account.getFrequancyStmt());

			resultSet = statement.executeUpdate();
			if (resultSet > 0) {
				LogUtil.info("Account with account number = " + account.getAccountNumber() + " and frequancy = "
						+ account.getFrequancyStmt() + " has updated successfully");
			}
		} catch (Exception e) {
			try {
				connection.rollback();
				e.printStackTrace();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public static List<Account> getCorpAccounts(String date) {
		Connection connection = ConnectionManager.getBDCConnection();
		String sqlStr = "select * from FCCTCIB.IB_ACCOUNT where COD_ACCT_NO not in (select c.COD_ACCT_NO from bdcefcrp.ch_acct_mast c where c.COD_ACCT_STAT in (1,5)) "
				+ " and  STMT_NEXT_DATE is null or STMT_NEXT_DATE = " + date;
		// String sqlStr = "select * from ACCOUNT where STMT_NEXT_DATE = trunc(sysdate)
		LogUtil.info("Get Acounts from Database.");
		LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		Account account = null;
		List<Account> accounts = new ArrayList<Account>();
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sqlStr);
			if (resultSet.isBeforeFirst()) {
				LogUtil.info("Data retrieved successfully and will deal with it.");
				while (resultSet.next()) {

					String customerCode = resultSet.getString(1);
					String accountNumber = resultSet.getString(2);
					int frequancyStmt = resultSet.getInt(3);
					Date nextDate = resultSet.getDate(4);
					Date lastSentDate = resultSet.getDate(5);
					String address = resultSet.getString(6);
					String accountType = resultSet.getString(7);
					String branchName = resultSet.getString(8);
					String currencyName = resultSet.getString(9);
					String customerName = resultSet.getString(10);
					account = new Account();
					account.setCustomerCode(customerCode);
					account.setAccountNumber(accountNumber);
					account.setFrequancyStmt(frequancyStmt);
					account.setNextDate(nextDate);
					account.setLastSentDate(lastSentDate);
					account.setAddress(address);
					account.setAccountType(accountType);
					account.setBranchName(branchName);
					account.setCurrencyName(currencyName);
					account.setCustomerName(customerName);
					accounts.add(account);

				}
			} else {
				LogUtil.info("There no data retrieved");
			}
		} catch (SQLException e) {
			LogUtil.error("SQL Error.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
			}

		}
		LogUtil.info("Number of Acounts is: " + accounts.size());
		return accounts;

	}

	public static List<Account> getSMSAccountsByToday() {
		Connection connection = ConnectionManager.getBDCConnection();
		String sqlStr = "select * from ACCOUNT where SMS = 1 and IS_APPROVED = 1 ";
		LogUtil.info("Get Acounts from Database.");
		LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		Account account = null;
		List<Account> accounts = new ArrayList<Account>();
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sqlStr);
			if (resultSet.isBeforeFirst()) {
				LogUtil.info("Data retrieved successfully and will deal with it.");
				while (resultSet.next()) {
					String customerCode = resultSet.getString(1);
					String accountNumber = resultSet.getString(2);
					int frequancyStmt = resultSet.getInt(3);
					String email = resultSet.getString(4);
					Date nextDate = resultSet.getDate(5);
					String description = resultSet.getString(6);
					Date lastSentDate = resultSet.getDate(7);
					int noOfFaildAttemps = resultSet.getInt(8);
					String password = resultSet.getString(9);
					String status = resultSet.getString(10);
					String mobile = resultSet.getString(11);
					String address = resultSet.getString(12);
					String accountType = resultSet.getString(13);
					String branchName = resultSet.getString(14);
					String currencyName = resultSet.getString(15);
					String customerName = resultSet.getString(16);
					Date smsActivationDate = resultSet.getDate(26);
					account = new Account(customerCode, accountNumber, frequancyStmt, email, nextDate, description,
							lastSentDate, noOfFaildAttemps, password, status, mobile, address, accountType, branchName,
							currencyName, customerName);
					String nonePerformingAccount = resultSet.getString(29);
					String nonePerformingCustomer = resultSet.getString(30);
					account.setNonePerformingAccount(nonePerformingAccount);
					account.setNonePerformingCustomer(nonePerformingCustomer);
					account.setSmsActivationDate(smsActivationDate);
					accounts.add(account);
				}
			} else {
				LogUtil.info("There no data retrieved");
			}
		} catch (SQLException e) {
			LogUtil.error("SQL Error.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
			}

		}
		LogUtil.info("Number of Acounts is: " + accounts.size());
		return accounts;

	}

	public static Balance getOpeningBalace(Account account, String fromDate, String toDate) {

		Connection connection = ConnectionManager.getBDCConnection();
		// List<Account> accounts = new ArrayList<Account>();
		CallableStatement cstmt = null;
		// ResultSet resultSet = null;
		Balance balance = new Balance();
		try {
			if (ConnectionManager.bdcUrl.contains("172.17.63.178"))
				cstmt = connection.prepareCall("{? = call PK_INQUIRY.GET_START_CLOSE_BALANCE(?,?,?,?,?)}");
			else
				cstmt = connection.prepareCall("{? = call FCCTCIB.PK_INQUIRY.GET_START_CLOSE_BALANCE(?,?,?,?,?)}");
			cstmt.registerOutParameter(1, Types.DOUBLE);
			cstmt.setString(2, account.getAccountNumber());
			cstmt.setDate(3, java.sql.Date.valueOf(fromDate));
			cstmt.setDate(4, java.sql.Date.valueOf(toDate));
			cstmt.registerOutParameter(5, Types.DOUBLE);
			cstmt.registerOutParameter(6, Types.DOUBLE);

			cstmt.executeQuery();
			double bd1 = 0.0, bd2 = 0.0;

			// double bd = (Double) cstmt.getObject(1);
			if (cstmt != null && cstmt.getObject(5) != null)
				bd1 = (Double) cstmt.getObject(5);
			if (cstmt != null && cstmt.getObject(6) != null)
				bd2 = (Double) cstmt.getObject(6);

			balance.setStartBalance(bd1);
			balance.setEndBalance(bd2);

			LogUtil.info("Data retrieved successfully and will deal with it.");
		} catch (SQLException e) {
			LogUtil.error("SQL Error.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
			}
		}
		return balance;
	}

	public static Account getAccountByAccountNoAndFrequency(String accountNo, int freq) {
		Account account = null;
		String sql = "select * from Account where COD_ACCT_NO = ? and FREQ_STMNT = ?";
		Connection connection = ConnectionManager.getBDCConnection();
		ResultSet resultSet = null;
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, accountNo);
			statement.setInt(2, freq);
			resultSet = statement.executeQuery();
			if (resultSet.isBeforeFirst()) {
				while (resultSet.next()) {
					String customerCode = resultSet.getString(1);
					String accountNumber = resultSet.getString(2);
					int frequancyStmt = resultSet.getInt(3);
					String email = resultSet.getString(4);
					Date nextDate = resultSet.getDate(5);
					String description = resultSet.getString(6);
					Date lastSentDate = resultSet.getDate(7);
					int noOfFaildAttemps = resultSet.getInt(8);
					String password = resultSet.getString(9);
					String status = resultSet.getString(10);
					String mobile = resultSet.getString(11);
					String address = resultSet.getString(12);
					String accountType = resultSet.getString(13);
					String branchName = resultSet.getString(14);
					String currencyName = resultSet.getString(15);
					String customerName = resultSet.getString(16);
					account = new Account(customerCode, accountNumber, frequancyStmt, email, nextDate, description,
							lastSentDate, noOfFaildAttemps, password, status, mobile, address, accountType, branchName,
							currencyName, customerName);
					account.setSmsFlag(resultSet.getInt(23));
					account.setEstatementFlag(resultSet.getInt(24));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return account;
	}

	public static Account getAccountByAccountNo(String accountNo) {
		Account account = null;
		String sql = "select * from Account where COD_ACCT_NO like '" + accountNo.trim() + "%'";
		Connection connection = ConnectionManager.getBDCConnection();
		ResultSet resultSet = null;
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
//			statement.setString(1, accountNo.trim());
			resultSet = statement.executeQuery();
			if (resultSet.isBeforeFirst()) {
				while (resultSet.next()) {
					String customerCode = resultSet.getString(1);
					String accountNumber = resultSet.getString(2);
					int frequancyStmt = resultSet.getInt(3);
					String email = resultSet.getString(4);
					Date nextDate = resultSet.getDate(5);
					String description = resultSet.getString(6);
					Date lastSentDate = resultSet.getDate(7);
					int noOfFaildAttemps = resultSet.getInt(8);
					String password = resultSet.getString(9);
					String status = resultSet.getString(10);
					String mobile = resultSet.getString(11);
					String address = resultSet.getString(12);
					String accountType = resultSet.getString(13);
					String branchName = resultSet.getString(14);
					String currencyName = resultSet.getString(15);
					String customerName = resultSet.getString(16);
					account = new Account(customerCode, accountNumber, frequancyStmt, email, nextDate, description,
							lastSentDate, noOfFaildAttemps, password, status, mobile, address, accountType, branchName,
							currencyName, customerName);
					account.setSmsFlag(resultSet.getInt(23));
					account.setEstatementFlag(resultSet.getInt(24));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return account;
	}

	// Shady Update
	public static int updateAccount(Account account) {
		Connection connection = ConnectionManager.getBDCConnection();

		String sqlStr = "Update account set  FREQ_STMNT = ? ,EMAIL = ?, STMT_NEXT_DATE = ? , MOBILE_NO = ?,CREATED_BY_USER_ID = ?,IS_APPROVED = ?,STATUS=?,APPROVEDBYUSERID=? ,SMS=?,ESTATEMENT=?,FAUILERFLAG=? where COD_CUST = ? and COD_ACCT_NO = ? ";

		LogUtil.info("Update Acount with account number = " + account.getAccountNumber());

		LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		// Statement statement = null;
		PreparedStatement statement = null;
		int resultSet = 0;

		String nextDate = "";
		if (account.getNextDate() != null) {
			nextDate = new SimpleDateFormat("yyyy-MM-dd").format(account.getNextDate());
		}
		try {
			statement = connection.prepareStatement(sqlStr);
			statement.setInt(1, account.getFrequancyStmt());
			statement.setString(2, account.getEmail());
			if (nextDate != null && !nextDate.isEmpty())
				statement.setDate(3, java.sql.Date.valueOf(nextDate));
			else
				statement.setDate(3, null);
			statement.setString(4, account.getMobile());
			statement.setInt(5, account.getCreatedBy());
			if (account.getIsApproved() == 1)
				statement.setInt(6, account.getIsApproved());
			else
				statement.setInt(6, 0);

			statement.setString(7, Defines.EDIT_STATUS);
			statement.setInt(8, 0);
			statement.setInt(9, account.getSmsFlag());
			statement.setInt(10, account.getEstatementFlag());
			statement.setInt(11, account.getFailureMail());

			statement.setString(12, account.getCustomerCode());
			statement.setString(13, account.getAccountNumber());

			resultSet = statement.executeUpdate();
			if (resultSet > 0) {
				LogUtil.info("Account with account number = " + account.getAccountNumber() + " and frequancy = "
						+ account.getFrequancyStmt() + " has updated successfully");
			}
		} catch (Exception e) {
			try {
				connection.rollback();
				e.printStackTrace();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return resultSet;
	}

	// Ahmed Update code edit query where Frequent
	public static int updateAccountByFrequency(Account account) {
		Connection connection = ConnectionManager.getBDCConnection();

		String sqlStr = "Update account set  FREQ_STMNT = ? ,EMAIL = ?, STMT_NEXT_DATE = ? , MOBILE_NO = ?,CREATED_BY_USER_ID = ?,IS_APPROVED = ?,STATUS=?,APPROVEDBYUSERID=? ,SMS=?,ESTATEMENT=?,FAUILERFLAG=? where COD_CUST = ? and COD_ACCT_NO = ? and FREQ_STMNT = ? ";

		LogUtil.info("Update Acount with account number = " + account.getAccountNumber());

		LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		// Statement statement = null;
		PreparedStatement statement = null;
		int resultSet = 0;

		String nextDate = "";
		if (account.getNextDate() != null) {
			nextDate = new SimpleDateFormat("yyyy-MM-dd").format(account.getNextDate());
		}
		try {
			statement = connection.prepareStatement(sqlStr);
			statement.setInt(1, account.getFrequancyStmt());
			statement.setString(2, account.getEmail());
			if (nextDate != null && !nextDate.isEmpty())
				statement.setDate(3, java.sql.Date.valueOf(nextDate));
			else
				statement.setDate(3, null);
			statement.setString(4, account.getMobile());
			statement.setInt(5, account.getCreatedBy());
			if (account.getIsApproved() == 1)
				statement.setInt(6, account.getIsApproved());
			else
				statement.setInt(6, 0);

			statement.setString(7, Defines.EDIT_STATUS);
			statement.setInt(8, 0);
			statement.setInt(9, account.getSmsFlag());
			statement.setInt(10, account.getEstatementFlag());
			statement.setInt(11, account.getFailureMail());

			statement.setString(12, account.getCustomerCode());
			statement.setString(13, account.getAccountNumber());
			statement.setInt(14, account.getFrequancyStmt());

			resultSet = statement.executeUpdate();
			if (resultSet > 0) {
				LogUtil.info("Account with account number = " + account.getAccountNumber() + " and frequancy = "
						+ account.getFrequancyStmt() + " has updated successfully");
			}
		} catch (Exception e) {
			try {
				connection.rollback();
				e.printStackTrace();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return resultSet;
	}

	public static int updateAccount(Account account, String status, int isApproved) {
		Connection connection = ConnectionManager.getBDCConnection();

		String sqlStr = "Update account set  FREQ_STMNT = ? ,EMAIL = ?, STMT_NEXT_DATE = ? , MOBILE_NO = ?,CREATED_BY_USER_ID = ?,IS_APPROVED = ?,STATUS=?,APPROVEDBYUSERID=? ,SMS=?,ESTATEMENT=?,FAUILERFLAG=? where COD_CUST = ? and COD_ACCT_NO = ? ";

		LogUtil.info("Update Acount with account number = " + account.getAccountNumber());

		LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		// Statement statement = null;
		PreparedStatement statement = null;
		int resultSet = 0;

		String nextDate = "";
		if (account.getNextDate() != null) {
			nextDate = new SimpleDateFormat("yyyy-MM-dd").format(account.getNextDate());
		}
		try {
			statement = connection.prepareStatement(sqlStr);
			statement.setInt(1, account.getFrequancyStmt());
			statement.setString(2, account.getEmail());
			if (nextDate != null && !nextDate.isEmpty())
				statement.setDate(3, java.sql.Date.valueOf(nextDate));
			else
				statement.setDate(3, null);
			statement.setString(4, account.getMobile());
			statement.setInt(5, account.getCreatedBy());
			statement.setInt(6, isApproved);
			statement.setString(7, status);
			statement.setInt(8, 0);
			statement.setInt(9, account.getSmsFlag());
			statement.setInt(10, account.getEstatementFlag());
			statement.setInt(11, account.getFailureMail());

			statement.setString(12, account.getCustomerCode());
			statement.setString(13, account.getAccountNumber());

			resultSet = statement.executeUpdate();
			if (resultSet > 0) {
				LogUtil.info("Account with account number = " + account.getAccountNumber() + " and frequancy = "
						+ account.getFrequancyStmt() + " has updated successfully");
			}
		} catch (Exception e) {
			try {
				connection.rollback();
				e.printStackTrace();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return resultSet;
	}

	public static int updateAccountWithSMSandEstatementFlags(Account account) {
		Connection connection = ConnectionManager.getBDCConnection();

		String sqlStr = "Update account set  FREQ_STMNT = ? ,EMAIL = ?, STMT_NEXT_DATE = ? , MOBILE_NO = ?,CREATED_BY_USER_ID = ?,IS_APPROVED = ?,STATUS=?,SMS=?,ESTATEMENT=? where COD_CUST = ? and COD_ACCT_NO = ? ";

		LogUtil.info("Update Acount with account number = " + account.getAccountNumber());

		LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		// Statement statement = null;
		PreparedStatement statement = null;
		int resultSet = 0;

		String nextDate = "";
		if (account.getNextDate() != null) {
			nextDate = new SimpleDateFormat("yyyy-MM-dd").format(account.getNextDate());
		}
		try {
			statement = connection.prepareStatement(sqlStr);
			statement.setInt(1, account.getFrequancyStmt());
			statement.setString(2, account.getEmail());
			if (account.getNextDate() != null) {
				statement.setDate(3, java.sql.Date.valueOf(nextDate));
			} else
				statement.setDate(3, null);
			statement.setString(4, account.getMobile());
			statement.setInt(5, account.getCreatedBy());
			statement.setInt(6, 0);
			statement.setString(7, Defines.EDIT_STATUS);
			statement.setInt(8, account.getSmsFlag());
			statement.setInt(9, account.getEstatementFlag());
			statement.setString(10, account.getCustomerCode());
			statement.setString(11, account.getAccountNumber());

			resultSet = statement.executeUpdate();
			if (resultSet > 0) {
				LogUtil.info("Account with account number = " + account.getAccountNumber() + " and frequancy = "
						+ account.getFrequancyStmt() + " has updated successfully");
			}
		} catch (Exception e) {
			try {
				connection.rollback();
				e.printStackTrace();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return resultSet;
	}

	public static int deleteAccount(String accountNumber) {
		int status = 0;
		try {
			Connection con = ConnectionManager.getBDCConnection();
			PreparedStatement ps = con.prepareStatement("delete from Account where COD_ACCT_NO = ?");
			ps.setString(1, accountNumber);
			status = ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	// Ahmed Saleh Code
	public static int deleteAccountWithFreq(String accountNumber, int freq) {
		int status = 0;
		try {
			Connection con = ConnectionManager.getBDCConnection();
			PreparedStatement ps = con.prepareStatement("delete from Account where COD_ACCT_NO = ? and FREQ_STMNT = ?");
			ps.setString(1, accountNumber);
			ps.setInt(2, freq);
			status = ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	public static int saveAccount(Account account) {
		int status = 0;
		String nextDate = "";
		if (account.getNextDate() != null) {
			nextDate = new SimpleDateFormat("yyyy-MM-dd").format(account.getNextDate());
		}
		String creationDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		try {
			Connection con = ConnectionManager.getBDCConnection();
			PreparedStatement ps = con.prepareStatement(
					"insert into account(COD_CUST,COD_ACCT_NO,FREQ_STMNT,EMAIL,MOBILE_NO,CUSTOMER_NAME,STMT_NEXT_DATE,CREATED_BY_USER_ID,BRANCH_NAME,CREATION_DATE,STATUS,SMS,ESTATEMENT) values (?,?,?,?,?,?,?,?,?,?,?,?,?)");
			ps.setString(1, account.getCustomerCode());
			ps.setString(2, account.getAccountNumber());
			ps.setInt(3, account.getFrequancyStmt());
			ps.setString(4, account.getEmail());
			ps.setString(5, account.getMobile());
			ps.setString(6, account.getCustomerName());
			if (nextDate.isEmpty())
				ps.setDate(7, null);
			else
				ps.setDate(7, java.sql.Date.valueOf(nextDate));
			ps.setInt(8, account.getCreatedBy());
			ps.setString(9, account.getBranchName());
			ps.setDate(10, java.sql.Date.valueOf(creationDate));
			ps.setString(11, Defines.NEW_REQUEST_STATUS);
			ps.setInt(12, account.getSmsFlag());
			ps.setInt(13, account.getEstatementFlag());

			status = ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	public static void syncNonePerformingAccounts() {
		String query;
		PreparedStatement pstmt;
		Connection conn = ConnectionManager.getBDCConnection();
		try {
			query = ("update Account ac set (ac.NONEPERFORMING_ACCOUNT,ac.NONEPERFORMING_CUSTOMER) ="
					+ "(SELECT B.FLG_ACCR_STATUS ,C.COD_MIS_CUST_CODE_7"
					+ " FROM ch_acct_mast@iflex  B,ci_custmast@iflex C,ba_cc_brn_mast@iflex  D"
					+ " WHERE b.cod_cust = c.cod_cust_id" + " and B.COD_CC_BRN = D.COD_CC_BRN"
					+ " AND (B.FLG_VAT='Y' or B.FLG_TDS = 'Y' )" + " AND B.FLG_MNT_STATUS = 'A'"
					+ " AND B.COD_ACCT_STAT NOT IN ('1', '5')" + " and trim(B.COD_ACCT_NO)  = trim(ac.COD_ACCT_NO))");
			pstmt = conn.prepareStatement(query);
			pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static List<Account> getAccountTypeList() {

//		List<String> result = getAccounts().stream().map(item -> item.getAccountNumber().trim())
//				.collect(Collectors.toList());
		List<Account> allAccounts = getAccounts();
		Connection connection = ConnectionManager.getBDCConnection();
		Account account = new Account();
		List<Account> accounts = new ArrayList<Account>();
		CallableStatement cstmt = null;
		ResultSet resultSet = null;
		List<String> accNum = new ArrayList<String>();
		// Ahmed comment
//		try {
//			
//			cstmt = connection.prepareCall("{? = call ESTATEMENT.PK_INQUIRY.GET_ESTATEMENT_ACCOUNTS()}");
//			cstmt.registerOutParameter(1, OracleTypes.CURSOR);
//			cstmt.executeQuery();
//			resultSet = (ResultSet) cstmt.getObject(1);
//			while (resultSet.next()) {
//
//				LogUtil.info("Data retrieved successfully and will deal with it.");
//				int smsFlag = resultSet.getString("FLAG_SMS") != null
//						? resultSet.getString("FLAG_SMS").equalsIgnoreCase("y") ? 1 : 0
//						: 0;
//				int EstatementFlag = resultSet.getString("FLAG_STAT") != null
//						? resultSet.getString("FLAG_STAT").equalsIgnoreCase("y") ? 1 : 0
//						: 0;
//				int flag = 0;
//				for (Account acc : allAccounts) {
//					flag = 0;
//					if (acc.getAccountNumber().trim().equals(resultSet.getString("ACCOUNT_NUMBER").trim())) {
//						if (EstatementFlag != acc.getEstatementFlag() || smsFlag != acc.getSmsFlag()) {
//							account = new Account();
//							account.setCustomerName(resultSet.getString("CUST_FULL_NAME"));
//							account.setCustomerCode(resultSet.getString("CUSTOMER_ID"));
//							account.setSmsFlag(smsFlag);
//							account.setEstatementFlag(EstatementFlag);
//							account.setAccountNumber(resultSet.getString("ACCOUNT_NUMBER"));
//							account.setBranchName(resultSet.getString("BRANCH_NAME"));
//							accounts.add(account);
//						}
//						flag = 1;
//						break;
//					}
//				}
//				if (flag == 0) {
//					account = new Account();
//					account.setCustomerName(resultSet.getString("CUST_FULL_NAME"));
//					account.setCustomerCode(resultSet.getString("CUSTOMER_ID"));
//					account.setSmsFlag(smsFlag);
//					account.setEstatementFlag(EstatementFlag);
//					account.setAccountNumber(resultSet.getString("ACCOUNT_NUMBER"));
//					account.setBranchName(resultSet.getString("BRANCH_NAME"));
//					if (!accounts.contains(account))
//						accounts.add(account);
//				}

		// Start shady comment
//				if (!result.contains(resultSet.getString("ACCOUNT_NUMBER").trim())) {
//
//					account = new Account();
//					// account.setAccountTypeFlag(resultSet.getString(3));
//					account.setCustomerName(resultSet.getString("CUST_FULL_NAME"));
//					account.setCustomerCode(resultSet.getString("CUSTOMER_ID"));
//					String smsFlag = resultSet.getString("FLAG_SMS");
//					if (smsFlag != null && smsFlag.equalsIgnoreCase("y")) {
//						account.setSmsFlag(1);
//					} else {
//						account.setSmsFlag(0);
//					}
//					String EstatementFlag = resultSet.getString("FLAG_STAT");
//					if (EstatementFlag != null && EstatementFlag.equalsIgnoreCase("y")) {
//						account.setEstatementFlag(1);
//					} else {
//						account.setEstatementFlag(0);
//					}
//					account.setAccountNumber(resultSet.getString("ACCOUNT_NUMBER"));
//					account.setBranchName(resultSet.getString("BRANCH_NAME"));
//					accounts.add(account);
//				}
		// }

		// End Shady Comment

//		} catch (SQLException e) {
//			LogUtil.error("SQL Error.", e);
//		} finally {
//			try {
//				if (connection != null && !connection.isClosed()) {
//					connection.close();
//				}
//			} catch (SQLException e) {
//				LogUtil.error("Error While close the connection. {}", e);
//			}
//		}
//		return accounts;
		return allAccounts;
	}

	public static List<Account> getCorpAccountTypeList() {

//		List<String> result = getAccounts().stream().map(item -> item.getAccountNumber().trim())
//				.collect(Collectors.toList());
		List<Account> allAccounts = getAccounts();
		Connection connection = ConnectionManager.getBDCConnection();
		Account account = new Account();
		List<Account> accounts = new ArrayList<Account>();
		CallableStatement cstmt = null;
		ResultSet resultSet = null;
		List<String> accNum = new ArrayList<String>();
		try {
			if (ConnectionManager.bdcUrl.contains("85.199"))
				cstmt = connection.prepareCall("{? = call GEFUPLUS.PK_INQUIRY.GET_ESTATEMENT_ACCOUNTS()}");
			else if (ConnectionManager.bdcUrl.contains("60.185"))
				cstmt = connection.prepareCall("{? = call FCCTCIB.PK_INQUIRY.GET_ESTATEMENT_ACCOUNTS()}");
			cstmt.registerOutParameter(1, OracleTypes.CURSOR);
			cstmt.executeQuery();
			resultSet = (ResultSet) cstmt.getObject(1);
			while (resultSet.next()) {

				LogUtil.info("Data retrieved successfully and will deal with it.");
				int smsFlag = resultSet.getString("FLAG_SMS") != null
						? resultSet.getString("FLAG_SMS").equalsIgnoreCase("y") ? 1 : 0
						: 0;
				int EstatementFlag = resultSet.getString("FLAG_STAT") != null
						? resultSet.getString("FLAG_STAT").equalsIgnoreCase("y") ? 1 : 0
						: 0;
				int flag = 0;
				for (Account acc : allAccounts) {
					flag = 0;
					if (acc.getAccountNumber().trim().equals(resultSet.getString("ACCOUNT_NUMBER").trim())) {
						if (EstatementFlag != acc.getEstatementFlag() || smsFlag != acc.getSmsFlag()) {
							account = new Account();
							account.setCustomerName(resultSet.getString("CUST_FULL_NAME"));
							account.setCustomerCode(resultSet.getString("CUSTOMER_ID"));
							account.setSmsFlag(smsFlag);
							account.setEstatementFlag(EstatementFlag);
							account.setAccountNumber(resultSet.getString("ACCOUNT_NUMBER"));
							account.setBranchName(resultSet.getString("BRANCH_NAME"));
							accounts.add(account);
						}
						flag = 1;
						break;
					}
				}
				if (flag == 0) {
					account = new Account();
					account.setCustomerName(resultSet.getString("CUST_FULL_NAME"));
					account.setCustomerCode(resultSet.getString("CUSTOMER_ID"));
					account.setSmsFlag(smsFlag);
					account.setEstatementFlag(EstatementFlag);
					account.setAccountNumber(resultSet.getString("ACCOUNT_NUMBER"));
					account.setBranchName(resultSet.getString("BRANCH_NAME"));
					if (!accounts.contains(account))
						accounts.add(account);
				}
//				if (!result.contains(resultSet.getString("ACCOUNT_NUMBER").trim())) {
//
//					account = new Account();
//					// account.setAccountTypeFlag(resultSet.getString(3));
//					account.setCustomerName(resultSet.getString("CUST_FULL_NAME"));
//					account.setCustomerCode(resultSet.getString("CUSTOMER_ID"));
//					String smsFlag = resultSet.getString("FLAG_SMS");
//					if (smsFlag != null && smsFlag.equalsIgnoreCase("y")) {
//						account.setSmsFlag(1);
//					} else {
//						account.setSmsFlag(0);
//					}
//					String EstatementFlag = resultSet.getString("FLAG_STAT");
//					if (EstatementFlag != null && EstatementFlag.equalsIgnoreCase("y")) {
//						account.setEstatementFlag(1);
//					} else {
//						account.setEstatementFlag(0);
//					}
//					account.setAccountNumber(resultSet.getString("ACCOUNT_NUMBER"));
//					account.setBranchName(resultSet.getString("BRANCH_NAME"));
//					accounts.add(account);
//				}
			}
		} catch (SQLException e) {
			LogUtil.error("SQL Error.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
			}
		}
		return accounts;
	}

	public static void addToEstatementHistory(Account account, String pdfPath, String excelPath, String swiftPath,
			String from, String to) {
		String query;
		PreparedStatement pstmt;
		Connection conn = ConnectionManager.getBDCConnection();
		try {
			query = ("insert into ESTATEMENT_HISTORY(CUSTOMER_NAME,CUSTOMER_ACCOUNTNO,CUSTOMER_CODE,FREQ_STMT,DATE_FROM,DATE_TO,PDF_PATH,EXCEL_PATH,SWIFT_PATH,EMAIL) VALUES(?,?,?,?,?,?,?,?,?,?)");
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, account.getCustomerName());
			pstmt.setString(2, account.getAccountNumber());
			pstmt.setString(3, account.getCustomerCode());
			pstmt.setInt(4, account.getFrequancyStmt());
			pstmt.setDate(5, java.sql.Date.valueOf(from));
			pstmt.setDate(6, java.sql.Date.valueOf(to));
			pstmt.setString(7, pdfPath);
			pstmt.setString(8, excelPath);
			pstmt.setString(9, swiftPath);
			pstmt.setString(10, account.getEmail());
			pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static List<EstatementHistory> getEstatementHistory() {
		String query;
		Connection conn = ConnectionManager.getBDCConnection();
		List<EstatementHistory> estatementHistories = new ArrayList<EstatementHistory>();
		EstatementHistory estatementHistory = null;
		try {
			query = "select * from ESTATEMENT_HISTORY order by ID DESC";
			Statement state = conn.createStatement();
			ResultSet rs = state.executeQuery(query);
			while (rs.next()) {
				String customerName = rs.getString(2);
				String accountNo = rs.getString(3);
				String customerCode = rs.getString(4);
				int freqStmt = rs.getInt(5);
				Date from = rs.getDate(6);
				Date to = rs.getDate(7);
				String pdfPath = rs.getString(8);
				String excelPath = rs.getString(9);
				String swiftPath = rs.getString(10);
				estatementHistory = new EstatementHistory(customerName, accountNo, customerCode, freqStmt, from, to,
						pdfPath, excelPath, swiftPath);
				estatementHistory.setMail(rs.getString(11));
				estatementHistories.add(estatementHistory);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return estatementHistories;
	}

	public static List<EstatementHistory> getEstatementHistory(String fromDate, String toDate) {
		String query;
		Connection conn = ConnectionManager.getBDCConnection();
		List<EstatementHistory> estatementHistories = new ArrayList<EstatementHistory>();
		EstatementHistory estatementHistory = null;
		try {
			query = "select * from ESTATEMENT_HISTORY his where his.DATE_TO >= to_date('" + fromDate
					+ "','yyyy-mm-dd') and his.DATE_TO  <= to_date('" + toDate + "','yyyy-mm-dd') order by ID DESC";
			Statement state = conn.createStatement();
			ResultSet rs = state.executeQuery(query);
			while (rs.next()) {
				String customerName = rs.getString(2);
				String accountNo = rs.getString(3);
				String customerCode = rs.getString(4);
				int freqStmt = rs.getInt(5);
				Date from = rs.getDate(6);
				Date to = rs.getDate(7);
				String pdfPath = rs.getString(8);
				String excelPath = rs.getString(9);
				String swiftPath = rs.getString(10);
				estatementHistory = new EstatementHistory(customerName, accountNo, customerCode, freqStmt, from, to,
						pdfPath, excelPath, swiftPath);
				estatementHistory.setMail(rs.getString(11));
				estatementHistories.add(estatementHistory);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return estatementHistories;
	}

	public static int approveAccount(String accountNO, String userID) {
		Connection connection = ConnectionManager.getBDCConnection();
		Date d = new Date();
		String sqlStr = "";
		Account account = getAccountByAccountNo(accountNO);
		// && account.getNextDate()
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date today = Calendar.getInstance().getTime();
		if (account != null && account.getNextDate().after(today))
			sqlStr = "Update account set  IS_APPROVED = 1,STATUS = '" + Defines.APPROVED_STATUS
					+ "',SMSACTIVATIONDATE = to_date('"
					+ java.sql.Date.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(new Date()))
					+ "','yyyy-mm-dd') ,APPROVEDBYUSERID = '" + userID + "',FAUILERFLAG = 0 where COD_ACCT_NO like '%"
					+ accountNO.trim() + "%'";
		else
			sqlStr = "Update account set  IS_APPROVED = 1,STATUS = '" + Defines.APPROVED_STATUS
					+ "',SMSACTIVATIONDATE = to_date('"
					+ java.sql.Date.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(new Date()))
					+ "','yyyy-mm-dd') ,APPROVEDBYUSERID = '" + userID + "',FAUILERFLAG = 0,STMT_NEXT_DATE = to_date('"
					+ java.sql.Date.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(new Date()))
					+ "','yyyy-mm-dd') where COD_ACCT_NO like '%" + accountNO.trim() + "%'";
		LogUtil.info("Update Acount with account Number = " + accountNO);
		System.out.println(sqlStr);
		LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		// Statement statement = null;
		PreparedStatement statement = null;
		int resultSet = 0;

		try {
			statement = connection.prepareStatement(sqlStr);
//			statement.setInt(1, 1);
//			statement.setString(2, Defines.APPROVED_STATUS);
//
//			statement.setDate(3, java.sql.Date.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(new Date())));
//			statement.setInt(4, Integer.valueOf(userID));
//			statement.setInt(5, 0);
//			statement.setDate(6, java.sql.Date.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(new Date())));
//			statement.setString(7, "%" + accountNO.trim() + "%");

			resultSet = statement.executeUpdate();
			if (resultSet > 0) {
				LogUtil.info("Account with customer code = " + accountNO + " has updated successfully");
			}
		} catch (Exception e) {
			try {
				connection.rollback();
				e.printStackTrace();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return resultSet;

	}

	public static int updateDeletedAccount(String accountNo, int userId) {
		Connection connection = ConnectionManager.getBDCConnection();
//		String sqlStr = "Update account set STATUS = ?,IS_APPROVED = ? ,IS_DELETED = ? where COD_ACCT_NO = ?";
		String sqlStr = "Update account set STATUS = '" + Defines.DELETE_STATUS + "',IS_APPROVED = " + 0
				+ " ,IS_DELETED = " + 1 + ",CREATED_BY_USER_ID=" + userId + " where COD_ACCT_NO =" + accountNo;
		Statement s = null;
		int resultSet = 0;
		try {
			s = connection.createStatement();
			resultSet = s.executeUpdate(sqlStr);
		} catch (Exception e) {
			try {
				connection.rollback();
				e.printStackTrace();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return resultSet;
	}

	public static int insertCorporateIBAcct() {
		int status = 0;
		Connection conn = ConnectionManager.getBDCConnection();
		try {
			String sql = "";
			if (ConnectionManager.bdcUrl.contains("85.199")) {
				sql = "INSERT INTO IB_ACCOUNT (" + "   COD_CUST, COD_ACCT_NO,  " + "   ADDRESS, BRANCH_NAME, "
						+ "   CURRENCY_NAME, CUSTOMER_NAME) " + " ( select  "
						+ "c.COD_CUST,c.COD_ACCT_NO,concat(concat( cst.TXT_CUSTADR_ADD1,cst.TXT_CUSTADR_ADD2),cst.TXT_CUSTADR_ADD3) address"
						+ ", B.NAM_BRANCH, cur.NAM_CCY_SHORT, cst.NAM_CUST_FULL "
						+ " from bdcefcrp.ch_acct_mast c,bdcefcrp.ci_custmast cst,"
						+ "  BDCEFCRP.BA_CC_BRN_MAST B, BDCEFCRP.BA_CCY_CODE cur"
						+ "  where c.COD_ACCT_STAT NOT IN (1, 5) AND c.FLG_MNT_STATUS = 'A' AND cst.COD_CUST_ID=c.COD_CUST and c.COD_CCY = cur.COD_CCY and cst.COD_CC_HOMEBRN = B.COD_CC_BRN and c.COD_CUST in (select unique(COU_CUSTOMER_ID) from army.CORP_USERS ) "
						+ "minus " + "select COD_CUST, COD_ACCT_NO,  " + "   ADDRESS, BRANCH_NAME, "
						+ "   CURRENCY_NAME, CUSTOMER_NAME from IB_ACCOUNT" + ") ";
			} else if (ConnectionManager.bdcUrl.contains("60.185")) {
//				sql = "INSERT INTO IB_ACCOUNT (" + "   COD_CUST, COD_ACCT_NO,  " + "   ADDRESS, BRANCH_NAME, "
//						+ "   CURRENCY_NAME, CUSTOMER_NAME) " + " ( select  "
//						+ "c.COD_CUST,c.COD_ACCT_NO,concat(concat( cst.TXT_CUSTADR_ADD1,cst.TXT_CUSTADR_ADD2),cst.TXT_CUSTADR_ADD3) address"
//						+ ", B.NAM_BRANCH, cur.NAM_CCY_SHORT, cst.NAM_CUST_FULL "
//						+ " from bdcefcrp.ch_acct_mast c,bdcefcrp.ci_custmast cst,"
//						+ "  BDCEFCRP.BA_CC_BRN_MAST B, BDCEFCRP.BA_CCY_CODE cur"
//						+ "  where c.COD_ACCT_STAT NOT IN (1, 5) AND c.FLG_MNT_STATUS = 'A' AND cst.COD_CUST_ID=c.COD_CUST and c.COD_CCY = cur.COD_CCY and cst.COD_CC_HOMEBRN = B.COD_CC_BRN and c.COD_CUST in (select unique(COU_CUSTOMER_ID) from CORP_USERS ) "
//						+ "minus " + "select COD_CUST, COD_ACCT_NO,  " + "   ADDRESS, BRANCH_NAME, "
//						+ "   CURRENCY_NAME, CUSTOMER_NAME from IB_ACCOUNT" + ") ";
				sql = "INSERT INTO FCCTCIB.IB_ACCOUNT (COD_CUST," + "                        COD_ACCT_NO, "
						+ "                        ADDRESS, " + "                        BRANCH_NAME, "
						+ "                        CURRENCY_NAME, " + "                        CUSTOMER_NAME) "
						+ "    (SELECT c.COD_CUST, " + "            c.COD_ACCT_NO, "
						+ "            CONCAT (CONCAT (cst.TXT_CUSTADR_ADD1, cst.TXT_CUSTADR_ADD2), "
						+ "                    cst.TXT_CUSTADR_ADD3) " + "                address, "
						+ "            B.NAM_BRANCH, " + "            cur.NAM_CCY_SHORT, "
						+ "            cst.NAM_CUST_FULL " + "       FROM bdcefcrp.ch_acct_mast    c, "
						+ "            bdcefcrp.ci_custmast     cst, " + "            BDCEFCRP.BA_CC_BRN_MAST  B, "
						+ "            BDCEFCRP.BA_CCY_CODE     cur " + "      WHERE     c.COD_ACCT_STAT NOT IN (1, 5) "
						+ "            AND c.FLG_MNT_STATUS = 'A' " + "            AND cst.COD_CUST_ID = c.COD_CUST "
						+ "            AND c.COD_CCY = cur.COD_CCY "
						+ "            AND cst.COD_CC_HOMEBRN = B.COD_CC_BRN "
						+ "            AND c.COD_CUST IN (SELECT UNIQUE (COU_CUSTOMER_ID) "
						+ "                                 FROM FCCTCIB.CORP_USERS) " + "     MINUS "
						+ "     SELECT COD_CUST, " + "            COD_ACCT_NO, " + "            ADDRESS, "
						+ "            BRANCH_NAME, " + "            CURRENCY_NAME, " + "            CUSTOMER_NAME "
						+ "       FROM FCCTCIB.IB_ACCOUNT)";

			}
			PreparedStatement ps = conn.prepareStatement(sql);
			status = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return status;
	}

	public static List<MeezaRegCustomers> getAllMeezaRegCustomer() {
		Connection connection = ConnectionManager.getBDCConnection();
//		String sqlStr = "SELECT COD_CUST, COD_ACCT_NO, FREQ_STMNT,STMT_NEXT_DATE, LAST_SENT_DATE,"
//				+ " CREATION_DATE,IS_APPROVED, APPROVEDBYUSERID, "
//				+ " SFTP_FOLDER,Email,Customer_NAME,createdbyuserid,SFTPPAYMENTFOLDER FROM ESTATEMENT.HOST_TO_HOST_ACCOUNT";

		String sqlStr = "SELECT COD_CUST, COD_ACCT_NO,FREQ_STMNT,STMT_NEXT_DATE,LAST_SENT_DATE"
				+ ",CREATION_DATE,CREATION_DATE,IS_APPROVED,EMAIL,CUSTOMER_NAME,CREATED_BY_USER_ID  FROM MEEZA_REG_CUSTOMERS ";
		System.out.println(sqlStr);
		LogUtil.info("Get Acounts from Database.");
		LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));

		List<MeezaRegCustomers> meezaRegCustomerList = new ArrayList<MeezaRegCustomers>();
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sqlStr);
			if (resultSet.isBeforeFirst()) {
				LogUtil.info("Data retrieved successfully and will deal with it.");
				System.out.println("Data retrieved successfully and will deal with it.");
				while (resultSet.next()) {
					int customerCode = resultSet.getInt("COD_CUST");
					String accountNumber = resultSet.getString("COD_ACCT_NO");
					int frequancyStmt = resultSet.getInt("FREQ_STMNT");
					Date nextDate = resultSet.getDate("STMT_NEXT_DATE");
					Date lastSentDate = resultSet.getDate("LAST_SENT_DATE");
					Date creationDate = resultSet.getDate("CREATION_DATE");
					int isApproved = resultSet.getInt("IS_APPROVED");
					String email = resultSet.getString("EMAIL");
					String customerName = resultSet.getString("CUSTOMER_NAME");
					int createdByUserID = resultSet.getInt("CREATED_BY_USER_ID");
					MeezaRegCustomers meezaRegCustomers = new MeezaRegCustomers();
					meezaRegCustomers.setCOD_CUST(customerCode);
					meezaRegCustomers.setCOD_ACCT_NO(accountNumber);
					meezaRegCustomers.setFREQ_STMNT(frequancyStmt);
					meezaRegCustomers.setSTMT_NEXT_DATE(nextDate);
					meezaRegCustomers.setLAST_SENT_DATE(lastSentDate);
					meezaRegCustomers.setCREATION_DATE(creationDate);
					meezaRegCustomers.setIS_APPROVED(isApproved);
					meezaRegCustomers.setEMAIL(email);
					meezaRegCustomers.setCUSTOMER_NAME(customerName);
					meezaRegCustomers.setCREATED_BY_USER_ID(createdByUserID);
					meezaRegCustomerList.add(meezaRegCustomers);
				}
			} else {
				LogUtil.info("There no data retrieved");
			}
		} catch (SQLException e) {
			System.out.println("SQL Error." + e.toString());
			LogUtil.error("SQL Error.", e);

		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
			}

		}
		LogUtil.info("Number of Acounts is: " + meezaRegCustomerList.size());
		return meezaRegCustomerList;

	}

	public static Customer getCustomerById(int id) {
		Connection connection = ConnectionManager.getBDCConnection();
		String sqlStr = "select c.NAM_CUST_FULL,concat(concat(c.TXT_CUSTADR_ADD1,' '),c.NAM_PERMADR_CITY) ADDRESS from bdcefcrp.ci_custmast@IFLEX.BDC c where c.COD_CUST_ID="
				+ id;
		LogUtil.info("Get Acounts from Database.");
		LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		Customer customer = new Customer();
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sqlStr);
			if (resultSet.isBeforeFirst()) {
				LogUtil.info("Data retrieved successfully and will deal with it.");
				while (resultSet.next()) {
					String customername = resultSet.getString("NAM_CUST_FULL");
					String address = resultSet.getString("ADDRESS");
					customer.setId(id);
					customer.setName(customername);
					customer.setAddress(address);
				}
			} else {
				LogUtil.info("There no data retrieved");
			}
		} catch (SQLException e) {
			LogUtil.error("SQL Error.", e);
		} finally {
			try {
				// if (statement != null && !statement.isClosed()) {
				// statement.close();
				// }
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
			}

		}
//		LogUtil.info("Number of Acounts is: " + customerList.size());
		return customer;
	}

	public static int insertIntoMeezaRegCustomer(Customer customer) {
		int status = 0;
		Connection conn = ConnectionManager.getBDCConnection();
		try {

			PreparedStatement ps = conn.prepareStatement(
					"insert into MEEZA_REG_CUSTOMERS (COD_CUST,CUSTOMER_NAME,EMAIL,COD_ACCT_NO,FREQ_STMNT,ADDRESS) VALUES (?,?,?,?,?,?)");
			ps.setInt(1, customer.getId());
			ps.setString(2, customer.getName());
			ps.setString(3, customer.getEmail());
			ps.setString(4, " ");
			ps.setInt(5, 0);
			ps.setString(6, customer.getAddress());
			status = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return status;
	}

//	public static List<MeezaTransactions> getAllMeezaTransactions(int custId, String fromDate, String toDate) {
////		Connection conn = ConnectionManager.getFlexConnection();
//		Connection conn = ConnectionManager.getFCTConnection();
//		List<MeezaTransactions> meezaTransactionList = new ArrayList<MeezaTransactions>();
//		MeezaTransactions meezaTransaction = null;
//		try {
//			String query = "select * from STG.FCT_MEEZA_TRANSACTION where COD_CUST_ID =? "
//					+ "and TXN_DATE between to_Date(?,'YYYY-MM-DD') and  to_Date(?,'YYYY-MM-DD')";
//
////			String query = "select Dat_last_process,DAT_PROCESS,Dat_next_process from estatement.BA_BANK_MAST where TRUNC(CREATION_DATE) = TRUNC(SYSDATE)";
//			PreparedStatement state = conn.prepareStatement(query);
//
//			state.setInt(1, custId);
//			state.setString(2, fromDate);
//			state.setString(3, toDate);
//			ResultSet rs = state.executeQuery(query);
//			while (rs.next()) {
//				int PROCESS_DATE_ID = rs.getInt(1);
//				String COD_CUST_ID = rs.getString(2);
//				String CUST_EMAIL = rs.getString(3);
//				String CUST_NAME = rs.getString(4);
//				String TXN_DESC = rs.getString(5);
//				String TXN_STATUS = rs.getString(6);
//				String CARD_PAN = rs.getString(7);
//				String CARD_NUMBER = rs.getString(8);
//				String TXN_LOCATION = rs.getString(9);
//				String TXN_DATE = rs.getDate(10).toString();
//				int TXN_AMOUNT = rs.getInt(11);
//				int TXN_ID = rs.getInt(12);
//
//				meezaTransaction.setPROCESS_DATE_ID(PROCESS_DATE_ID);
//				meezaTransaction.setCOD_CUST_ID(COD_CUST_ID);
//				meezaTransaction.setCUST_EMAIL(CUST_EMAIL);
//				meezaTransaction.setCUST_NAME(CUST_NAME);
//				meezaTransaction.setTXN_DESC(TXN_DESC);
//				meezaTransaction.setTXN_STATUS(TXN_STATUS);
//				meezaTransaction.setCARD_PAN(CARD_PAN);
//				meezaTransaction.setCARD_NUMBER(CARD_NUMBER);
//				meezaTransaction.setTXN_LOCATION(TXN_LOCATION);
//				meezaTransaction.setTXN_DATE(TXN_DATE);
//				meezaTransaction.setTXN_AMOUNT(TXN_AMOUNT);
//				meezaTransaction.setTXN_ID(TXN_ID);
//				meezaTransactionList.add(meezaTransaction);
//			}
//
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		return meezaTransactionList;
//	}

	public static List<Integer> getMezaCustomerIds() {

		Connection conn = ConnectionManager.getBDCConnection();
		List<Integer> customerIds = new ArrayList<Integer>();
		try {
//			String query = "SELECT unique(ac.COD_CUST) FROM ACCOUNT ac WHERE ac.ESTATEMENT = 1 AND ac.IS_APPROVED = 1 and ac.COD_CUST = 6113296";
			String query = "SELECT unique(ac.COD_CUST) FROM ACCOUNT ac WHERE ac.ESTATEMENT = 1 AND ac.IS_APPROVED = 1 ";
			PreparedStatement state = conn.prepareStatement(query);
			System.out.println();

			ResultSet rs = state.executeQuery(query);
			while (rs.next()) {
				System.out.println(rs.getInt(1));
				int id = (rs.getInt(1));

				customerIds.add(id);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return customerIds;

	}

	public static List<MeezaTransactions> getAllMeezaTransactions(int custId) {
//		Connection conn = ConnectionManager.getFlexConnection();
		Connection conn = ConnectionManager.getFCTConnection();
		List<MeezaTransactions> meezaTransactionList = new ArrayList<MeezaTransactions>();
		try {
			String query = "select PROCESS_DATE_ID,COD_CUST_ID,CUST_EMAIL,CUST_NAME"
					+ ",TXN_DESC,TXN_STATUS,CARD_PAN,CARD_NUMBER,TXN_LOCATION,to_char(TXN_DATE,'dd/mm/yyyy hh12:mi:ss pm'),TXN_AMOUNT,TXN_ID"
					+ " from STG.FCT_MEEZA_TRANSACTION "
					+ "where PROCESS_DATE_ID = to_char(trunc(SYSDATE)-1,'YYYYMMDD') " + "and to_number(COD_CUST_ID) = "
					+ custId;

//			String query = "select PROCESS_DATE_ID,COD_CUST_ID,CUST_EMAIL,CUST_NAME"
//					+ ",TXN_DESC,TXN_STATUS,CARD_PAN,CARD_NUMBER,TXN_LOCATION,to_char(TXN_DATE,'dd/mm/yyyy hh12:mi:ss pm'),TXN_AMOUNT,TXN_ID"
//					+ " from STG.FCT_MEEZA_TRANSACTION "
//					+ "where PROCESS_DATE_ID = '20210303' and to_number(COD_CUST_ID) = "
//					+ custId;

			System.out.println("SQL Quer =>>>>>>>" + query);
			PreparedStatement state = conn.prepareStatement(query);

			ResultSet rs = state.executeQuery(query);
			while (rs.next()) {
				MeezaTransactions meezaTransaction = new MeezaTransactions();
				int PROCESS_DATE_ID = rs.getInt(1);
				String COD_CUST_ID = rs.getString(2);
				String CUST_EMAIL = rs.getString(3);
				String CUST_NAME = rs.getString(4) == null ? "" : rs.getString(4);
				String TXN_DESC = rs.getString(5) == null ? "" : rs.getString(5);
				String TXN_STATUS = rs.getString(6) == null ? "" : rs.getString(6);
				String CARD_PAN = rs.getString(7);
				String CARD_NUMBER = rs.getString(8);
				String TXN_LOCATION = rs.getString(9);
				String TXN_DATE = rs.getString(10);

				int TXN_AMOUNT = rs.getInt(11);
				int TXN_ID = rs.getInt(12);
				meezaTransaction.setPROCESS_DATE_ID(PROCESS_DATE_ID);
				meezaTransaction.setCOD_CUST_ID(COD_CUST_ID);
				meezaTransaction.setCUST_EMAIL(CUST_EMAIL);
				meezaTransaction.setCUST_NAME(CUST_NAME);
				meezaTransaction.setTXN_DESC(TXN_DESC);
				meezaTransaction.setTXN_STATUS(TXN_STATUS);
				meezaTransaction.setCARD_PAN(CARD_PAN);
				meezaTransaction.setCARD_NUMBER(CARD_NUMBER);
				if (TXN_LOCATION == null)
					meezaTransaction.setTXN_LOCATION("");
				else
					meezaTransaction.setTXN_LOCATION(TXN_LOCATION);
				meezaTransaction.setTXN_DATE(TXN_DATE);
				meezaTransaction.setTXN_AMOUNT(TXN_AMOUNT);
				meezaTransaction.setTXN_ID(TXN_ID);
				meezaTransactionList.add(meezaTransaction);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return meezaTransactionList;
	}

	public static boolean checkExistingMeezaCust(int custId) {
		Connection conn = ConnectionManager.getFCTConnection();
		try {
			String query = "select distinct(cus.COD_CUST) from STG.MEEZA_REG_CUSTOMERS cus where cus.COD_CUST =  "
					+ custId;
			System.out.println("SQL Quer =>>>>>>>" + query);
			PreparedStatement state = conn.prepareStatement(query);

			ResultSet rs = state.executeQuery(query);
			while (rs.next()) {
				conn.close();
				return true;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}

	public static MeezaRegCustomers getMeezaRegCustomerById(int custId) {
//		Connection conn = ConnectionManager.getFlexConnection();
		Connection conn = ConnectionManager.getBDCConnection();
		List<MeezaTransactions> meezaTransactionList = new ArrayList<MeezaTransactions>();
		MeezaRegCustomers meezaRegCustomers = new MeezaRegCustomers();
		try {
			String query = "select COD_CUST,COD_ACCT_NO,EMAIL from MEEZA_REG_CUSTOMERS " + "where  COD_CUST =" + custId;

			System.out.println("SQL Quer =>>>>>>>" + query);
			PreparedStatement state = conn.prepareStatement(query);

			ResultSet rs = state.executeQuery(query);
			while (rs.next()) {
				String COD_ACCT_NO = rs.getString(2);
				int COD_CUST_ID = rs.getInt(1);
				String CUST_EMAIL = rs.getString(3);
				meezaRegCustomers.setCOD_CUST(COD_CUST_ID);
				meezaRegCustomers.setEMAIL(CUST_EMAIL);
				meezaRegCustomers.setCOD_ACCT_NO(COD_ACCT_NO);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return meezaRegCustomers;
	}

	public static boolean updateMeezaRegCust(int status, int custID) {
		Connection connection = ConnectionManager.getBDCConnection();
//		String sqlStr = "SELECT * from account a where a.STMT_NEXT_DATE < trunc (sysdate) and a.ESTATEMENT = 1 and a.FAUILERFLAG = 0";
		String sqlStr = "UPDATE MEEZA_REG_CUSTOMERS SET IS_APPROVED =" + status + " WHERE COD_CUST =" + custID;
		PreparedStatement statement = null;
		int updated = 0;
		try {

			statement = connection.prepareStatement(sqlStr);

			// statement.setInt(1,status);
			// statement.setInt(2, custID);
			System.out.println("SQL Quer =>>>>>>>" + sqlStr);
			System.out.println("custID Quer =>>>>>>>" + custID);
			System.out.println("status=>>>>>>>>" + status);
			updated = statement.executeUpdate(sqlStr);

		} catch (SQLException e) {
			LogUtil.error("SQL Error.", e);
		} finally {
			try {
				// if (statement != null && !statement.isClosed()) {
				// statement.close();
				// }
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
			}

		}

		return (updated != 0);
	}

	public static List<String> getCustomerEmailsById(int custId) {
//		Connection conn = ConnectionManager.getFlexConnection();
		Connection conn = ConnectionManager.getBDCConnection();
		List<String> emailsList = new ArrayList<String>();
		MeezaRegCustomers meezaRegCustomers = new MeezaRegCustomers();
		try {//
			String query = "select distinct(t.EMAIL), t.COD_CUST  from ACCOUNT t inner join ACCOUNT s on t.COD_CUST = s.COD_CUST  "
					+ "where t.COD_CUST =" + custId + " and t.IS_APPROVED =1 and t.ESTATEMENT=1 ";

			System.out.println("SQL Quer =>>>>>>>" + query);
			PreparedStatement state = conn.prepareStatement(query);

			ResultSet rs = state.executeQuery(query);
			while (rs.next()) {
				emailsList.add(rs.getString(1));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return emailsList;
	}

	public static String getCodeAccountNumberById(int custId) {
//		Connection conn = ConnectionManager.getFlexConnection();
		Connection conn = ConnectionManager.getBDCConnection();
		String cod_acc_num = "";
		MeezaRegCustomers meezaRegCustomers = new MeezaRegCustomers();
		try {//
			String query = "select distinct(COD_ACCT_NO)  from ACCOUNT " + " where COD_CUST =" + custId
					+ " and IS_APPROVED =1 and ESTATEMENT=1 ";

			System.out.println("SQL Quer =>>>>>>>" + query);
			PreparedStatement state = conn.prepareStatement(query);

			ResultSet rs = state.executeQuery(query);
			while (rs.next()) {
				cod_acc_num = rs.getString(1);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return cod_acc_num;
	}

	public static void main(String args[]) {
		getAccountTypeList();
	}

}
