package com.bdc.estatement.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.bdc.estatement.connection.ConnectionManager;
import com.bdc.estatement.model.Account;
import com.bdc.estatement.model.SMSHistory;
import com.bdc.estatement.model.SMSModel;
import com.bdc.estatement.model.TermDepositSMS;
import com.bdc.estatement.model.RetailTransaction;
import com.bdc.estatement.util.LogUtil;

public class SMSDao {

//	public static List<RetailTransaction> getRetailSMSsTransactions(String from, String to) {
////		Connection connection = ConnectionManager.getFrsprodConnection();
//		Connection connection = ConnectionManager.getBDCConnection();
//		List<RetailTransaction> retailTransactions = new ArrayList<RetailTransaction>();
////		String sqlStr = "SELECT AA.COD_ACCT_NO, AA.DAT_TXN_STR, AA.AMT_TXN_TCY, AA.COD_TXN_MNEMONIC, AA.TXN_NRRTV, AA.FLG_DRCR, AA.COD_MSG_TYP TRANSACTIONTYPE, CU.NAM_CCY_SHORT, AA.COD_BAL_AVAIL,"
////				+ " AA.COD_CUST, CI.REF_CUST_PHONE, CI.REF_CUST_PHONE_OFF, CI.REF_CUST_TELEX FROM BDCEFCRP.XF_OL_ST_TXNLOG_CURRENT  AA"
////				+ " INNER JOIN (SELECT * FROM BDCEFCRP.CI_CUSTMAST WHERE FLG_MNT_STATUS = 'A'"
////				+ " AND (   (   (    LENGTH (REF_CUST_PHONE) = 11 AND (SUBSTR (REF_CUST_PHONE, 1, 2) = '01') )"
////				+ " OR (    LENGTH (REF_CUST_PHONE) = 12 AND (SUBSTR (REF_CUST_PHONE, 1, 3) = '201')))"
////				+ " OR (   (    LENGTH (REF_CUST_PHONE_OFF) = 11 AND (SUBSTR (REF_CUST_PHONE_OFF, 1, 2) = '01'))"
////				+ " OR (    LENGTH (REF_CUST_PHONE_OFF) = 12 AND (SUBSTR (REF_CUST_PHONE_OFF, 1, 3) = '201')))"
////				+ " OR (   (    LENGTH (REF_CUST_TELEX) = 11 AND (SUBSTR (REF_CUST_TELEX, 1, 2) = '01'))"
////				+ " OR (    LENGTH (REF_CUST_TELEX) = 12 AND (SUBSTR (REF_CUST_TELEX, 1, 3) = '201'))))) CI"
////				+ " ON CI.COD_CUST_ID = AA.COD_CUST INNER JOIN BDCEFCRP.BA_CCY_CODE CU ON CU.COD_CCY = AA.COD_TXN_CCY AND CU.FLG_MNT_STATUS = 'A'"
////				+ " WHERE AA.DAT_TXN_STR >= TO_DATE ('" + from + "', 'YYYY-MM-DD HH-MI-SS')"
////				+ " AND AA.DAT_TXN_STR < TO_DATE ('" + to + "', 'YYYY-MM-DD HH-MI-SS')";
//
////		String sqlStr = "SELECT AA.COD_ACCT_NO,AA.DAT_TXN_STR,AA.AMT_TXN_TCY,AA.COD_TXN_MNEMONIC, AA.TXN_NRRTV, AA.FLG_DRCR, AA.COD_MSG_TYP     TRANSACTIONTYPE,AA.COD_BAL_AVAIL,AA.COD_CUST"
////				+ "        FROM BDCEFCRP.XF_OL_ST_TXNLOG_CURRENT  AA WHERE     AA.DAT_TXN_STR >= ? AND AA.DAT_TXN_STR < ?";
//		String sqlStr = "SELECT AA.COD_ACCT_NO,    AA.DAT_TXN_STR,      AA.AMT_TXN_TCY,       AA.COD_TXN_MNEMONIC,       AA.TXN_NRRTV,       AA.FLG_DRCR,       AA.COD_MSG_TYP     TRANSACTIONTYPE,       AA.COD_BAL_AVAIL,       AA.COD_CUST  FROM BDCEFCRP.XF_OL_ST_TXNLOG_CURRENT@iflex.bdc AA WHERE AA.DAT_TXN_STR BETWEEN TO_DATE ('2020-11-23 12:51:35','yyyy-MM-dd hh:mi:ss') AND TO_DATE ('2020-11-23 12:51:45','yyyy-MM-dd hh:mi:ss')";
//		System.out.println(from + "    " + to);
//		PreparedStatement statement = null;
//		System.out.println(sqlStr);
//		try {
////			Thread.sleep(1500);
//			statement = connection.prepareStatement(sqlStr);
////			statement.setTimestamp(1, Timestamp.valueOf(from));
////			statement.setTimestamp(2, Timestamp.valueOf(to));
//			ResultSet resultSet = statement.executeQuery();
//			if (resultSet != null && resultSet.isBeforeFirst()) {
//				LogUtil.info("Data retrieved successfully and will deal with it.");
//				while (resultSet.next()) {
//					RetailTransaction retailTransaction = new RetailTransaction();
//					retailTransaction.setAccountNumber(resultSet.getString("COD_ACCT_NO"));
//					retailTransaction.setTransactionDate(resultSet.getDate("DAT_TXN_STR"));
//					retailTransaction.setTransactionAmount(resultSet.getDouble("AMT_TXN_TCY"));
//					retailTransaction.setTransactionMenemonic(resultSet.getString("COD_TXN_MNEMONIC"));
//					retailTransaction.setTransactionDescription(resultSet.getString("TXN_NRRTV"));
//					retailTransaction.setTransactionType(resultSet.getString("FLG_DRCR"));
//					retailTransaction.setTransactionCode(resultSet.getString("transactionType"));
////					retailTransaction.setTransactionCurrency(resultSet.getString("NAM_CCY_SHORT"));
//					retailTransaction.setBalanceAvaliable(resultSet.getDouble("COD_BAL_AVAIL"));
//					retailTransaction.setCustomerID(resultSet.getString("COD_CUST"));
////					retailTransaction.setPhoneNum1(resultSet.getString("REF_CUST_PHONE"));
////					retailTransaction.setPhoneNum2(resultSet.getString("REF_CUST_PHONE_OFF"));
////					retailTransaction.setPhoneNum3(resultSet.getString("REF_CUST_TELEX"));
//					retailTransactions.add(retailTransaction);
//				}
//			} else {
//				System.out.println("There no data retrieved");
//				LogUtil.info("There no data retrieved");
//			}
//
//		} catch (SQLException e) {
//			System.out.println(e);
//			LogUtil.error("SQL Error.", e);
//		}
////		catch (InterruptedException e) {
////			e.printStackTrace();
////		}
//		finally {
//			try {
//				if (connection != null && !connection.isClosed()) {
//					connection.close();
//				}
//				if (statement != null)
//					statement.close();
//			} catch (SQLException e) {
//				System.out.println("Error While close the connection. {}" + e);
//				LogUtil.error("Error While close the connection. {}", e);
//			}
//		}
//		return retailTransactions;
//	}

	public static int getRetailSMSsTransactions() {
		Connection connection = ConnectionManager.getBDCConnection();
		List<RetailTransaction> retailTransactions = new ArrayList<RetailTransaction>();
		String sqlStr = " INSERT INTO ESTATEMENT.SMS_RETAIL_TRANSACTIONS (COD_ACCT_NO,DAT_TXN_STR,AMOUNT_TXN,COD_TXN_MNEMONIC,TXN_DESC,FLG_DRCR,TRANSACTION_TYPE,TXN_CUR,BAL_AVAIL,"
				+ " COD_CUUST,REF_CUST_PHONE,REF_CUST_PHONE_OFF,REF_CUST_TELEX)  (SELECT AA.COD_ACCT_NO,AA.DAT_TXN_STR,AA.AMT_TXN_TCY,AA.COD_TXN_MNEMONIC,AA.TXN_NRRTV,"
				+ " AA.FLG_DRCR,AA.COD_MSG_TYP TRANSACTIONTYPE,CU.NAM_CCY_SHORT,AA.COD_BAL_AVAIL,AA.COD_CUST,CI.REF_CUST_PHONE,CI.REF_CUST_PHONE_OFF,CI.REF_CUST_TELEX"
				+ " FROM BDCEFCRP.XF_OL_ST_TXNLOG_CURRENT@iflex.bdc  AA INNER JOIN (SELECT * FROM BDCEFCRP.CI_CUSTMAST@iflex.bdc WHERE     FLG_MNT_STATUS = 'A' "
				+ " AND(((LENGTH (REF_CUST_PHONE) = 11 AND (SUBSTR (REF_CUST_PHONE, 1, 2) = '01')) OR (LENGTH(REF_CUST_PHONE) = 12 "
				+ " AND (SUBSTR (REF_CUST_PHONE, 1, 3) = '201'))) OR((LENGTH(REF_CUST_PHONE_OFF) = 11 AND (SUBSTR (REF_CUST_PHONE_OFF, 1, 2) ='01')) "
				+ " OR(LENGTH(REF_CUST_PHONE_OFF) = 12 AND (SUBSTR (REF_CUST_PHONE_OFF, 1, 3) ='201'))) OR((LENGTH (REF_CUST_TELEX) = 11 AND (SUBSTR (REF_CUST_TELEX, 1, 2) = '01')) "
				+ " OR(LENGTH (REF_CUST_TELEX) = 12 AND (SUBSTR (REF_CUST_TELEX, 1, 3) = '201'))))) CI ON CI.COD_CUST_ID = AA.COD_CUST INNER JOIN BDCEFCRP.BA_CCY_CODE@iflex.bdc CU "
				+ " ON CU.COD_CCY = AA.COD_TXN_CCY AND CU.FLG_MNT_STATUS = 'A' MINUS SELECT COD_ACCT_NO,DAT_TXN_STR,AMOUNT_TXN,COD_TXN_MNEMONIC,TXN_DESC,FLG_DRCR,TRANSACTION_TYPE, "
				+ " TXN_CUR,BAL_AVAIL,COD_CUUST,REF_CUST_PHONE,REF_CUST_PHONE_OFF,REF_CUST_TELEX FROM ESTATEMENT.SMS_RETAIL_TRANSACTIONS)";

		Statement statement = null;
		int result = 0;
		try {
			statement = connection.createStatement();
			result = statement.executeUpdate(sqlStr);
		} catch (SQLException e) {
			LogUtil.error("SQL Error.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
				if (statement != null)
					statement.close();
			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
			}
		}
		return result;
	}

	public static int insertNewReturnedChequesToLocalDB() {
		Connection connection = ConnectionManager.getBDCConnection();
		String sqlStr = "insert into RETURNED_CHEQUES (REF_NO,CHQ_NO,AMOUNT,NAM_BANK,NAM_BRANCH,COD_ACCT_NO,"
				+ "DAT_VALUE_CUST,NAM_CUST_FULL) "
				+ "(select REF_NO,CHQ_NO,AMOUNT,NAM_BANK,NAM_BRANCH,COD_ACCT_NO,DAT_VALUE_CUST,NAM_CUST_FULL "
				+ "from V_RETURNED_CHEQUES@army) minus (select REF_NO,CHQ_NO,AMOUNT,NAM_BANK,"
				+ "NAM_BRANCH,COD_ACCT_NO,DAT_VALUE_CUST,NAM_CUST_FULL from RETURNED_CHEQUES)";
		Statement statement = null;
		int resultSet = 0;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeUpdate(sqlStr);
		} catch (SQLException e) {
			LogUtil.error("SQL Error.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
				if (statement != null)
					statement.close();
			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
			}
		}
		return resultSet;
	}

	public static int insertNewCollectedChequesToLocalDB() {
		Connection connection = ConnectionManager.getBDCConnection();
		String sqlStr = "insert into COLLECTED_CHEQUES (REF_NO,CHQ_NO,AMOUNT,NAM_BANK,NAM_BRANCH,COD_ACCT_NO,"
				+ "DAT_VALUE_CUST,NAM_CUST_FULL) (select REF_NO,CHQ_NO,AMOUNT,NAM_BANK,NAM_BRANCH,COD_ACCT_NO,"
				+ "DAT_VALUE_CUST,NAM_CUST_FULL from V_COLLECTED_CHEQUES@army) minus (select REF_NO,"
				+ "CHQ_NO,AMOUNT,NAM_BANK,NAM_BRANCH,COD_ACCT_NO,DAT_VALUE_CUST,NAM_CUST_FULL"
				+ " from COLLECTED_CHEQUES)";
		Statement statement = null;
		int resultSet = 0;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeUpdate(sqlStr);
		} catch (SQLException e) {
			LogUtil.error("SQL Error.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
				if (statement != null)
					statement.close();
			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
			}
		}
		return resultSet;
	}

	public static int insertNewCustomerTransactionsSMS() {
		System.out.println("Testing");
		int insertedRowCount = 0;
		long startTime = System.nanoTime();
		Connection armyConnection = ConnectionManager.getArmyBDCConnection();
		Connection estatementConnection = ConnectionManager.getBDCConnection();
		String sqlStr = "Truncate Table xf_ol_st_txnlog_current";
		Statement statement = null;
		ResultSet resultSet = null, resultSet_ = null;
		Statement statement_ = null;
		try {
			statement = armyConnection.createStatement();
			resultSet = statement.executeQuery(sqlStr);
			long endTime = System.nanoTime();
			long timeElapsed = endTime - startTime;

			System.out.println("Execution time in Seconds  Truncate : " + (timeElapsed / 1000000000));
			startTime = System.nanoTime();
			sqlStr = "Insert into xf_ol_st_txnlog_current " + "(Select * from bdcefcrp.xf_ol_st_txnlog_current  "
					+ "Where COD_TXN_MNEMONIC IN ('1401','1006','1702','1704','1703') and COD_MSG_TYP='200')";
			statement.executeQuery(sqlStr);

			endTime = System.nanoTime();
			timeElapsed = endTime - startTime;
			System.out.println(
					"Execution time in Seconds  Insert into xf_ol_st_txnlog_current : " + (timeElapsed / 1000000000));
			startTime = System.nanoTime();
			LogUtil.info("Data retrieved successfully and will deal with it.");
			statement_ = estatementConnection.createStatement();
			startTime = System.nanoTime();
//			sqlStr = " INSERT INTO CUSTOMERS_TXNS_SMS (TRANSACTION_CODE, ACCOUNT_NUMBER,ACCOUNT_CURRENCY,ACCOUNT_CURRENCY_CODE, COD_ORG_BRN,"
//					+ " TRANSACTION_DATE, TRANSACTION_VALUE_DATE, TRANSACTION_AMOUNT,REVERSED_TRANSACTION_CODE, TRANSACTION_REMARK,"
//					+ " TRANSACTION_TYPE) "
//					+ " (select TRANSACTION_CODE, ACCOUNT_NUMBER,ACCOUNT_CURRENCY,ACCOUNT_CURRENCY_CODE, COD_ORG_BRN,"
//					+ " TRANSACTION_DATE, TRANSACTION_VALUE_DATE, TRANSACTION_AMOUNT,REVERSED_TRANSACTION_CODE, TRANSACTION_REMARK,"
//					+ " TRANSACTION_TYPE from V_Bdc_Customers_Txns_Sms@army )" + " minus "
//					+ "	(select TRANSACTION_CODE, ACCOUNT_NUMBER,ACCOUNT_CURRENCY,ACCOUNT_CURRENCY_CODE, COD_ORG_BRN,"
//					+ " TRANSACTION_DATE,TRANSACTION_VALUE_DATE, TRANSACTION_AMOUNT,REVERSED_TRANSACTION_CODE, TRANSACTION_REMARK,"
//					+ "	TRANSACTION_TYPE" + "	from  CUSTOMERS_TXNS_SMS )";
			sqlStr = " INSERT INTO CUSTOMERS_TXNS_SMS (TRANSACTION_CODE, ACCOUNT_NUMBER,ACCOUNT_CURRENCY,ACCOUNT_CURRENCY_CODE, COD_ORG_BRN,"
					+ " TRANSACTION_DATE, TRANSACTION_VALUE_DATE, TRANSACTION_AMOUNT,REVERSED_TRANSACTION_CODE, TRANSACTION_REMARK,"
					+ " TRANSACTION_TYPE) "
					+ " (select TRANSACTION_CODE, ACCOUNT_NUMBER,ACCOUNT_CURRENCY,ACCOUNT_CURRENCY_CODE, COD_ORG_BRN,"
					+ " TRANSACTION_DATE, TRANSACTION_VALUE_DATE, TRANSACTION_AMOUNT,REVERSED_TRANSACTION_CODE, TRANSACTION_REMARK,"
					+ " TRANSACTION_TYPE from V_Bdc_Customers_Txns_Sms@army where TRANSACTION_CODE not in (SELECT TRANSACTION_CODE from CUSTOMERS_TXNS_SMS))";

//					+ " minus "
//					+ "	(select TRANSACTION_CODE, ACCOUNT_NUMBER,ACCOUNT_CURRENCY,ACCOUNT_CURRENCY_CODE, COD_ORG_BRN,"
//					+ " TRANSACTION_DATE,TRANSACTION_VALUE_DATE, TRANSACTION_AMOUNT,REVERSED_TRANSACTION_CODE, TRANSACTION_REMARK,"
//					+ "	TRANSACTION_TYPE" + "	from  CUSTOMERS_TXNS_SMS )";
			int res = statement_.executeUpdate(sqlStr);
			insertedRowCount += res;
			endTime = System.nanoTime();
			timeElapsed = endTime - startTime;

			System.out.println("Execution time in Seconds inserting : " + (timeElapsed / 1000000000));
		} catch (SQLException e) {
			LogUtil.error("SQL Error.", e);
			System.out.println(e);
			System.out.println(e.getMessage());
		} finally {
			System.out.println("number of inserted rows : " + insertedRowCount);
			try {
				if (armyConnection != null && !armyConnection.isClosed()) {
					armyConnection.close();
				}
				if (estatementConnection != null && !estatementConnection.isClosed()) {
					estatementConnection.close();
				}
				if (statement_ != null)
					statement_.close();
				if (statement != null)
					statement.close();

			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
				System.out.println(e);
				System.out.println(e.getMessage());
			}
		}
		return insertedRowCount;
	}

	public static List<SMSModel> getNewCustomerTransactionsSMS(String accountNumber, String smsActivationDate) {
		List<SMSModel> smsList = new ArrayList<SMSModel>();
		Connection connection = ConnectionManager.getBDCConnection();
		String sqlStr = "select * from Estatement.CUSTOMERS_TXNS_SMS where ACCOUNT_NUMBER like '%"
				+ accountNumber.trim() + "%' and sent = '0' " + " and TRANSACTION_DATE >= to_date('" + smsActivationDate
				+ "', 'yyyy-mm-dd') ";
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			if (connection != null)
				statement = connection.createStatement();
			if (statement != null)
				resultSet = statement.executeQuery(sqlStr);
			if (resultSet != null && resultSet.isBeforeFirst()) {
				LogUtil.info("Data retrieved successfully and will deal with it.");
				while (resultSet.next()) {
					SMSModel smsModel = new SMSModel();
					smsModel.setRefrenceNumber(resultSet.getString("TRANSACTION_CODE"));
					smsModel.setAccountNumber(resultSet.getString("ACCOUNT_NUMBER"));
					smsModel.setAmount(resultSet.getInt("TRANSACTION_AMOUNT"));
					smsModel.setCurrency(resultSet.getString("ACCOUNT_CURRENCY"));
					smsModel.setDescription(resultSet.getString("TRANSACTION_REMARK"));
//					Date valueDate = resultSet.getDate("TRANSACTION_VALUE_DATE");
					Date valueDate = resultSet.getDate("TRANSACTION_DATE");
					smsModel.setValueDate(valueDate);
					smsModel.setIsSent(resultSet.getInt("SENT"));
					smsModel.setIsReversed(resultSet.getInt("REVERSED_TRANSACTION_CODE"));
					smsModel.setTransfer(resultSet.getString("TRANSACTION_TYPE"));
					smsList.add(smsModel);
				}
			} else {
				LogUtil.info("There no data retrieved");
			}
		} catch (SQLException e) {
			LogUtil.error("SQL Error.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
				if (statement != null)
					statement.close();
			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
			}
		}

		return smsList;
	}

	public static List<SMSModel> getNewReturnedChequesTransactions(String accountNumber, String smsActivationDate) {
		List<SMSModel> smsTransactions = new ArrayList<SMSModel>();
		Connection connection = ConnectionManager.getBDCConnection();
		String sqlStr = "select * from RETURNED_CHEQUES where COD_ACCT_NO like '" + accountNumber.trim()
				+ "%' and sent = 0 " + " and DAT_VALUE_CUST >= to_date('" + smsActivationDate + "', 'yyyy-mm-dd')";
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			if (connection != null)
				statement = connection.createStatement();
			if (statement != null)
				resultSet = statement.executeQuery(sqlStr);
			if (resultSet != null && resultSet.isBeforeFirst()) {
				LogUtil.info("Data retrieved successfully and will deal with it.");
				while (resultSet.next()) {
					String refrenceNumber = resultSet.getString("REF_NO");
					String chequeNumber = resultSet.getString("CHQ_NO");
					double amount = resultSet.getDouble("AMOUNT");
					String bankName = resultSet.getString("NAM_BANK");
					String branchName = resultSet.getString("NAM_BRANCH");
					String accountNum = resultSet.getString("COD_ACCT_NO");
					Date valueDate = resultSet.getDate("DAT_VALUE_CUST");
					String customerName = resultSet.getString("NAM_CUST_FULL");
					int isSent = resultSet.getInt("SENT");
					SMSModel model = new SMSModel(refrenceNumber, accountNum, chequeNumber, bankName, branchName, "",
							"", amount, valueDate, isSent);
					smsTransactions.add(model);
				}
			} else {
				LogUtil.info("There no data retrieved");
			}
		} catch (SQLException e) {
			LogUtil.error("SQL Error.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
				if (statement != null)
					statement.close();
			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
			}
		}

		return smsTransactions;
	}

	public static List<SMSModel> getNewCollectedChequesTransactions(String accountNumber, String smsActivationDate) {
		List<SMSModel> smsTransactions = new ArrayList<SMSModel>();
		Connection connection = ConnectionManager.getBDCConnection();
		String sqlStr = "select * from COLLECTED_CHEQUES where COD_ACCT_NO = " + accountNumber.trim() + " and sent = 0 "
				+ " and DAT_VALUE_CUST >= to_date('" + smsActivationDate + "', 'yyyy-mm-dd')";
		LogUtil.info("executing query : " + sqlStr);
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			if (connection != null)
				statement = connection.createStatement();
			if (statement != null)
				resultSet = statement.executeQuery(sqlStr);
			if (resultSet != null && resultSet.isBeforeFirst()) {
				LogUtil.info("Data retrieved successfully and will deal with it.");
				while (resultSet.next()) {
					String refrenceNumber = resultSet.getString("REF_NO");
					String chequeNumber = resultSet.getString("CHQ_NO");
					double amount = resultSet.getDouble("AMOUNT");
					String bankName = resultSet.getString("NAM_BANK");
					String branchName = resultSet.getString("NAM_BRANCH");
					String accountNum = resultSet.getString("COD_ACCT_NO");
					Date valueDate = resultSet.getDate("DAT_VALUE_CUST");
					String customerName = resultSet.getString("NAM_CUST_FULL");
					int isSent = resultSet.getInt("SENT");
					SMSModel model = new SMSModel(refrenceNumber, accountNum, chequeNumber, bankName, branchName, "",
							"", amount, valueDate, isSent);
					smsTransactions.add(model);
				}
			} else {
				LogUtil.info("There no data retrieved");
			}
		} catch (SQLException e) {
			LogUtil.error("SQL Error.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
				if (statement != null)
					statement.close();
			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
			}
		}

		return smsTransactions;
	}

	public static int updateCustomerTransaction(SMSModel smsModel) {
		Connection connection = ConnectionManager.getBDCConnection();
		String sqlStr = "Update CUSTOMERS_TXNS_SMS set SENT = 1 where TRANSACTION_CODE = '"
				+ smsModel.getRefrenceNumber() + "'";
		PreparedStatement statement = null;
		System.out.println("performing query::: " + sqlStr);
		int resultSet = 0;
		String valueDate = "";
		if (smsModel.getValueDate() != null) {
			valueDate = new SimpleDateFormat("yyyy-MM-dd").format(smsModel.getValueDate());
		}
		try {
			statement = connection.prepareStatement(sqlStr);
			resultSet = statement.executeUpdate(sqlStr);
		} catch (SQLException e) {
			LogUtil.error("SQL Error.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
				if (statement != null)
					statement.close();
			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
			}
		}
		return resultSet;
	}

	public static int updateReturnedChequesTransaction(SMSModel smsModel) {
		Connection connection = ConnectionManager.getBDCConnection();
		String valueDate = "";
		if (smsModel.getValueDate() != null) {
			valueDate = new SimpleDateFormat("dd-MM-yyyy").format(smsModel.getValueDate());
		}
		String sqlStr = "Update RETURNED_CHEQUES set SENT = 1 where REF_NO = '" + smsModel.getRefrenceNumber()
				+ "' and CHQ_NO = '" + smsModel.getChequeNumber() + "'" + " and COD_ACCT_NO = '"
				+ smsModel.getAccountNumber() + "' and DAT_VALUE_CUST = to_date('" + valueDate + "', 'dd-mm-yyyy')";
		PreparedStatement statement = null;
		int resultSet = 0;

		try {
			statement = connection.prepareStatement(sqlStr);
			resultSet = statement.executeUpdate(sqlStr);
		} catch (SQLException e) {
			LogUtil.error("SQL Error.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
				if (statement != null)
					statement.close();
			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
			}
		}
		return resultSet;
	}

	public static int updateCollectedChequesTransaction(SMSModel smsModel) {
		Connection connection = ConnectionManager.getBDCConnection();
		String valueDate = "";
		if (smsModel.getValueDate() != null) {
			valueDate = new SimpleDateFormat("dd-MM-yyyy").format(smsModel.getValueDate());
		}
		String sqlStr = "Update COLLECTED_CHEQUES set SENT = 1 where REF_NO = '" + smsModel.getRefrenceNumber()
				+ "' and CHQ_NO = '" + smsModel.getChequeNumber() + "'" + " and COD_ACCT_NO = '"
				+ smsModel.getAccountNumber() + "' and DAT_VALUE_CUST = to_date('" + valueDate + "', 'dd-mm-yyyy')";
		PreparedStatement statement = null;
		int resultSet = 0;
		try {
			statement = connection.prepareStatement(sqlStr);
			resultSet = statement.executeUpdate(sqlStr);
		} catch (SQLException e) {
			LogUtil.error("SQL Error.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
				if (statement != null)
					statement.close();
			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
			}
		}
		return resultSet;
	}

	public static String getSmsMessageTemplate(String messageName) {
		String message = "";
		Connection connection = ConnectionManager.getBDCConnection();
		String sqlStr = "select * from SMS_MESSAGE_TEMPLATE where name = ?";
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(sqlStr);
			statement.setString(1, messageName);
			resultSet = statement.executeQuery();
			if (resultSet.isBeforeFirst()) {
				LogUtil.info("Data retrieved successfully and will deal with it.");
				while (resultSet.next()) {
					message = resultSet.getString("CONTENT");
				}
			} else {
				LogUtil.info("There no data retrieved");
			}
		} catch (SQLException e) {
			LogUtil.error("SQL Error.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
				if (statement != null)
					statement.close();
			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
			}
		}
		return message;
	}

	public static List<SMSHistory> getSmsIds() {
		String query;
		Connection conn = ConnectionManager.getBDCConnection();
		List<SMSHistory> smsHistories = new ArrayList<SMSHistory>();
		Statement state = null;
		try {
//			query = "select * from SMS_HISTORY smsH where smsH.SMS_ID is not null and (smsH.SMS_STATUS is null or smsH.SMS_STATUS != 1)";
			query = "select * from SMS_HISTORY smsH where smsH.SMS_ID is not null and smsH.FAILURE_MAIL = 0 and (smsH.SMS_STATUS is null or smsH.SMS_STATUS != 1)";
			state = conn.createStatement();
			ResultSet rs = state.executeQuery(query);
			while (rs.next()) {
				String customerName = rs.getString(2);
				String accountNumber = rs.getString(3);
				Date creationDate = rs.getDate(4);
				String message = rs.getString(5);
				String mobile = rs.getString(6);
				String smsID = rs.getString(7);
				SMSHistory smsH = new SMSHistory(customerName, accountNumber, creationDate, message, mobile);
				smsH.setSmsID(smsID);
				smsHistories.add(smsH);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
				if (state != null)
					state.close();
			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
			}
		}
		return smsHistories;
	}

	public static List<String> getSmsIdsForCBE_6_months() {
		String query;
		Connection conn = ConnectionManager.getArmyBDCConnection();
		List<String> smsIds = new ArrayList<String>();
		Statement state = null;
		try {
			query = "select FREE4 smsID from ARMY.LN_POSTPONE where FREE4!='null' and FREE5 is null ";
			state = conn.createStatement();
			ResultSet rs = state.executeQuery(query);
			while (rs.next()) {
				String smsId = rs.getString(1);
				if (smsId != null)
					smsIds.add(smsId);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
				if (state != null)
					state.close();
			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
			}
		}
		return smsIds;
	}

	public static int updateSmsHistory(String smsID, String smsStatus, int failureMail) {
		Connection connection = ConnectionManager.getBDCConnection();
		String sqlStr = "update SMS_HISTORY set SMS_STATUS = " + smsStatus + ",FAILURE_MAIL = " + failureMail
				+ " where SMS_ID = '" + smsID + "'";
		PreparedStatement statement = null;
		int resultSet = 0;
		try {
			statement = connection.prepareStatement(sqlStr);
			resultSet = statement.executeUpdate();
		} catch (SQLException e) {
			LogUtil.error("SQL Error.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
				if (statement != null)
					statement.close();

			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
			}
		}
		return resultSet;
	}

	public static int updateSmsCBE_6_months(String smsId, String smsStatus) {
		Connection connection = ConnectionManager.getArmyBDCConnection();
		String sqlStr = "update ARMY.LN_POSTPONE set FREE5='" + smsStatus + "' where FREE4='" + smsId + "'";
		PreparedStatement statement = null;
		int resultSet = 0;
		try {
			statement = connection.prepareStatement(sqlStr);
			resultSet = statement.executeUpdate();
		} catch (SQLException e) {
			LogUtil.error("SQL Error.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
				if (statement != null)
					statement.close();

			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
			}
		}
		return resultSet;
	}

	public static int insertToSMSHistory(SMSModel smsObj, String CustomerName, String msg, String mobile) {
		Connection connection = ConnectionManager.getBDCConnection();
		String sqlStr = "insert into SMS_HISTORY (ACCOUNT_NO,CREATION_DATE,MESSAGE,MOBILE,CUSTOMER_NAME,SMS_ID) values (?,?,?,?,?,?)";
		PreparedStatement statement = null;
		int resultSet = 0;
		String creationDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		try {
			statement = connection.prepareStatement(sqlStr);
			statement.setString(1, smsObj.getAccountNumber());
			statement.setDate(2, java.sql.Date.valueOf(creationDate));
			statement.setString(3, msg);
			statement.setString(4, mobile);
			statement.setString(5, CustomerName);
			statement.setString(6, smsObj.getSmsID());
			resultSet = statement.executeUpdate();
		} catch (SQLException e) {
			LogUtil.error("SQL Error.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
				if (statement != null)
					statement.close();

			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
			}
		}
		return resultSet;
	}

	public static List<SMSHistory> getSMSHistory() {
		String query;
		Connection conn = ConnectionManager.getBDCConnection();
		List<SMSHistory> smsHistories = new ArrayList<SMSHistory>();
		SMSHistory smsHistory = null;
		Statement state = null;
		try {
			query = "select * from SMS_HISTORY order by CREATION_DATE desc";
			state = conn.createStatement();
			ResultSet rs = state.executeQuery(query);
			while (rs.next()) {
				String customerName = rs.getString(2);
				String accountNo = rs.getString(3);
				Date creationDate = rs.getDate(4);
				String message = rs.getString(5);
				String mobile = rs.getString(6);
				String status = rs.getString(8);
				smsHistory = new SMSHistory(customerName, accountNo, creationDate, message, mobile);
				smsHistory.setStatus(status);
				smsHistories.add(smsHistory);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
//				if (state != null && !state.isClosed()) {
				if (state != null)
					state.close();

			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
			}
		}
		return smsHistories;
	}

	public static int insertTermDepositeForSms() {
		Connection connection = ConnectionManager.getBDCConnection();
		String sqlStr = "INSERT INTO ESTATEMENT.TERM_DEPOSIT_SMS (COD_CUST,REF_CUST_PHONE,REF_CUST_PHONE_OFF,REF_CUST_TELEX,"
				+ "NAM_PRODUCT,RAT_DEP_INT,BAL_PRINCIPAL,NAM_CCY_SHORT,DAT_DEP_DATE,DAT_VALUE_DATE,COD_DEP_STAT,COD_DEP_NO"
				+ ",COD_ACCT_NO) (SELECT dep.COD_CUST,cust.REF_CUST_PHONE,cust.REF_CUST_PHONE_OFF,cust.REF_CUST_TELEX,"
				+ "prod.NAM_PRODUCT,dep.RAT_DEP_INT,dep.BAL_PRINCIPAL,cur.NAM_CCY_SHORT,dep.DAT_DEP_DATE,dep.DAT_VALUE_DATE,"
				+ "dep.COD_DEP_STAT,dep.COD_DEP_NO,dep.COD_ACCT_NO FROM bdcefcrp.td_dep_mast@ARMY  dep LEFT OUTER JOIN "
				+ "bdcefcrp.td_prod_mast@ARMY prod ON prod.COD_PROD = dep.COD_PROD LEFT OUTER JOIN "
				+ "bdcefcrp.BA_CCY_CODE@ARMY cur ON cur.COD_CCY = dep.COD_CCY LEFT OUTER JOIN bdcefcrp.ci_custmast@army cust "
				+ " ON cust.COD_CUST_ID = dep.COD_CUST WHERE dep.COD_DEP_STAT = 6 AND dep.FLG_MNT_STATUS = 'A') MINUS"
				+ " (SELECT COD_CUST,REF_CUST_PHONE,REF_CUST_PHONE_OFF,REF_CUST_TELEX," + " NAM_PRODUCT, RAT_DEP_INT, "
				+ " BAL_PRINCIPAL, " + " NAM_CCY_SHORT, DAT_DEP_DATE, DAT_VALUE_DATE, COD_DEP_STAT, COD_DEP_NO, "
				+ " COD_ACCT_NO " + " FROM ESTATEMENT.TERM_DEPOSIT_SMS)";
		Statement statement = null;
		int resultSet = 0;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeUpdate(sqlStr);
		} catch (SQLException e) {
			LogUtil.error("SQL Error.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
				if (statement != null)
					statement.close();
			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
			}
		}
		return resultSet;
	}

	public static List<TermDepositSMS> getUnsendTermDepositSMS() {
		List<TermDepositSMS> allTermDepositSMS = new ArrayList<TermDepositSMS>();

		Connection connection = ConnectionManager.getBDCConnection();
		String sqlStr = " select trmdep.REF_CUST_PHONE,trmdep.REF_CUST_PHONE_OFF,trmdep.REF_CUST_TELEX,trmdep.NAM_PRODUCT,"
				+ " trmdep.RAT_DEP_INT,trmdep.BAL_PRINCIPAL,trmdep.NAM_CCY_SHORT,trmdep.DAT_DEP_DATE,"
				+ " trmdep.DAT_VALUE_DATE from TERM_DEPOSIT_SMS trmdep where trmdep.SEND_STATUS = 0";
		LogUtil.info("executing query : " + sqlStr);
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			if (connection != null)
				statement = connection.createStatement();
			if (statement != null)
				resultSet = statement.executeQuery(sqlStr);
			if (resultSet != null && resultSet.isBeforeFirst()) {
				LogUtil.info("Data retrieved successfully and will deal with it.");
				while (resultSet.next()) {

					String refCustPhone = resultSet.getString("REF_CUST_PHONE");
					String refCustPhoneOff = resultSet.getString("REF_CUST_PHONE_OFF");
					String refCustTelex = resultSet.getString("REF_CUST_TELEX");
					String nameProduct = resultSet.getString("NAM_PRODUCT");
					float rateDep = resultSet.getFloat("RAT_DEP_INT");
					int balPrincipal = resultSet.getInt("BAL_PRINCIPAL");
					String currancy = resultSet.getString("NAM_CCY_SHORT");

					Date depDate = resultSet.getDate("DAT_DEP_DATE");
					Date valueDate = resultSet.getDate("DAT_VALUE_DATE");
					TermDepositSMS termDepositSMS = new TermDepositSMS(refCustPhoneOff, refCustTelex, refCustPhone,
							nameProduct, rateDep, balPrincipal, currancy, depDate, valueDate);
					allTermDepositSMS.add(termDepositSMS);
				}
			} else {
				LogUtil.info("There no data retrieved");
			}
		} catch (SQLException e) {
			LogUtil.error("SQL Error.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
				if (statement != null)
					statement.close();
			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
			}
		}

		return allTermDepositSMS;
	}

	public static List<SMSModel> getSMSAccountsFor6MonthsAllownce() {
		List<SMSModel> smsTransactions = new ArrayList<SMSModel>();
		Connection connection = ConnectionManager.getArmyBDCConnection();
		String sqlStr = "select distinct(usr.COD_CUST_ID),usr.REF_CUST_PHONE,usr.REF_CUST_PHONE_OFF,usr.REF_CUST_TELEX,u.mobile1 from Army.ln_postpone po "
				+ "left outer join  bdcefcrp.ci_custmast usr "
				+ "on usr.COD_CUST_ID = po.CUST_ID left outer join  OASIS.USMSCARD@oasis u "
				+ "on u.CUSTOMERID = po.CUST_ID " + "where po.FREE6 is null";
		LogUtil.info("executing query : " + sqlStr);
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			if (connection != null)
				statement = connection.createStatement();
			if (statement != null)
				resultSet = statement.executeQuery(sqlStr);
			if (resultSet != null && resultSet.isBeforeFirst()) {
				LogUtil.info("Data retrieved successfully and will deal with it.");
				while (resultSet.next()) {
					String customerID = resultSet.getString(1);
					String mobile = resultSet.getString(2);
					String mobile1 = resultSet.getString(3);
					String mobile2 = resultSet.getString(4);
					String mobile3 = resultSet.getString(5);
					SMSModel model = new SMSModel();
					model.setCustomerID(customerID);
					model.setMobile(mobile);
					model.setMobile2(mobile1);
					model.setMobile3(mobile2);
					model.setMobile4(mobile3);
					smsTransactions.add(model);
				}
			} else {
				LogUtil.info("There no data retrieved");
			}
		} catch (SQLException e) {
			LogUtil.error("SQL Error.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
				if (statement != null)
					statement.close();
			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
			}
		}

		return smsTransactions;
	}

	public static int updateCbe6Months(SMSModel smsModel, String mobile, String msg, String status) {
		Connection connection = ConnectionManager.getArmyBDCConnection();
		String sqlStr = "UPDATE ARMY.LN_POSTPONE" + "   SET FREE2 ='" + mobile + "'," + "       FREE3 ='" + msg + "',"
				+ " FREE4 ='" + smsModel.getSmsID() + "', FREE6 = '" + status + "'" + " WHERE CUST_ID = '"
				+ smsModel.getCustomerID() + "'";
		PreparedStatement statement = null;
		System.out.println("performing query::: " + sqlStr);
		int resultSet = 0;
		try {
			statement = connection.prepareStatement(sqlStr);
			resultSet = statement.executeUpdate(sqlStr);
		} catch (SQLException e) {
			LogUtil.error("SQL Error.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
				if (statement != null)
					statement.close();
			} catch (SQLException e) {
				LogUtil.error("Error While close the connection. {}", e);
			}
		}
		return resultSet;
	}

}
