package com.bdc.estatement.model;

import java.util.Date;

public class SmsCustomerTransactions {

	public String transactionCode;
	public String accountNumer;
	public String accountCurrency;
	public int accountCurrencyCode;
	public int codeORG_BRN;
	public String transactionDate;
	public String transactionValueDate;
	public int transactionAmount;
	public int reversedTransactionCode;
	public String transactionRemark;
	public String transactionType;
	public int sent;

	public String getTransactionCode() {
		return transactionCode;
	}

	public void setTransactionCode(String transactionCode) {
		this.transactionCode = transactionCode;
	}

	public String getAccountNumer() {
		return accountNumer;
	}

	public void setAccountNumer(String accountNumer) {
		this.accountNumer = accountNumer;
	}

	public String getAccountCurrency() {
		return accountCurrency;
	}

	public void setAccountCurrency(String accountCurrency) {
		this.accountCurrency = accountCurrency;
	}

	public int getAccountCurrencyCode() {
		return accountCurrencyCode;
	}

	public void setAccountCurrencyCode(int accountCurrencyCode) {
		this.accountCurrencyCode = accountCurrencyCode;
	}

	public int getCodeORG_BRN() {
		return codeORG_BRN;
	}

	public void setCodeORG_BRN(int codeORG_BRN) {
		this.codeORG_BRN = codeORG_BRN;
	}

	

	public int getTransactionAmount() {
		return transactionAmount;
	}

	public void setTransactionAmount(int transactionAmount) {
		this.transactionAmount = transactionAmount;
	}

	public int getReversedTransactionCode() {
		return reversedTransactionCode;
	}

	public void setReversedTransactionCode(int reversedTransactionCode) {
		this.reversedTransactionCode = reversedTransactionCode;
	}

	public String getTransactionRemark() {
		return transactionRemark;
	}

	public void setTransactionRemark(String transactionRemark) {
		this.transactionRemark = transactionRemark;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public int getSent() {
		return sent;
	}

	public void setSent(int sent) {
		this.sent = sent;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getTransactionValueDate() {
		return transactionValueDate;
	}

	public void setTransactionValueDate(String transactionValueDate) {
		this.transactionValueDate = transactionValueDate;
	}

}
