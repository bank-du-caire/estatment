package com.bdc.estatement.model;

import java.util.Date;

public class MeezaTransactions {
	private int PROCESS_DATE_ID;
	private String COD_CUST_ID;
	private String CUST_EMAIL;
	private String CUST_NAME;
	private String TXN_DESC;
	private String TXN_STATUS;
	private String TXN_LOCATION;
	private String TXN_DATE;
	private int TXN_AMOUNT;
	private int TXN_ID;
	private String CARD_PAN;
	private String CARD_NUMBER;
	private String CARDNAMEHOLDER;
	
	
	public String getCARDNAMEHOLDER() {
		return CARDNAMEHOLDER;
	}
	public void setCARDNAMEHOLDER(String cARDNAMEHOLDER) {
		CARDNAMEHOLDER = cARDNAMEHOLDER;
	}
	public int getPROCESS_DATE_ID() {
		return PROCESS_DATE_ID;
	}
	public void setPROCESS_DATE_ID(int pROCESS_DATE_ID) {
		PROCESS_DATE_ID = pROCESS_DATE_ID;
	}
	public String getCOD_CUST_ID() {
		return COD_CUST_ID;
	}
	public void setCOD_CUST_ID(String cOD_CUST_ID) {
		COD_CUST_ID = cOD_CUST_ID;
	}
	public String getCUST_EMAIL() {
		return CUST_EMAIL;
	}
	public void setCUST_EMAIL(String cUST_EMAIL) {
		CUST_EMAIL = cUST_EMAIL;
	}
	public String getCUST_NAME() {
		return CUST_NAME;
	}
	public void setCUST_NAME(String cUST_NAME) {
		CUST_NAME = cUST_NAME;
	}
	public String getTXN_DESC() {
		return TXN_DESC;
	}
	public void setTXN_DESC(String tXN_DESC) {
		TXN_DESC = tXN_DESC;
	}
	public String getTXN_STATUS() {
		return TXN_STATUS;
	}
	public void setTXN_STATUS(String tXN_STATUS) {
		TXN_STATUS = tXN_STATUS;
	}
	public String getTXN_LOCATION() {
		return TXN_LOCATION;
	}
	public void setTXN_LOCATION(String tXN_LOCATION) {
		TXN_LOCATION = tXN_LOCATION;
	}
	public String getTXN_DATE() {
		return TXN_DATE;
	}
	public void setTXN_DATE(String tXN_DATE) {
		TXN_DATE = tXN_DATE;
	}
	public int getTXN_AMOUNT() {
		return TXN_AMOUNT;
	}
	public void setTXN_AMOUNT(int tXN_AMOUNT) {
		TXN_AMOUNT = tXN_AMOUNT;
	}
	public int getTXN_ID() {
		return TXN_ID;
	}
	public void setTXN_ID(int tXN_ID) {
		TXN_ID = tXN_ID;
	}
	public String getCARD_PAN() {
		return CARD_PAN;
	}
	public void setCARD_PAN(String cARD_PAN) {
		CARD_PAN = cARD_PAN;
	}
	public String getCARD_NUMBER() {
		return CARD_NUMBER;
	}
	public void setCARD_NUMBER(String cARD_NUMBER) {
		CARD_NUMBER = cARD_NUMBER;
	}
}
