package com.bdc.estatement.model;

import java.sql.Date;

public class RetailTransaction {

	private String accountNumber;
	private Date transactionDate;
	private double transactionAmount;
	private String transactionMenemonic;
	private String transactionDescription;
	private String transactionType;// Credit or Depit
	private String transactionCode;
	private String transactionCurrency;
	private double balanceAvaliable;
	private String customerID;
	private String phoneNum1;
	private String phoneNum2;
	private String phoneNum3;

	public RetailTransaction() {

	}

	public RetailTransaction(String accountNumber, Date transactionDate, double transactionAmount,
			String transactionMenemonic, String transactionDescription, String transactionType, String transactionCode,
			String transactionCurrency, double balanceAvaliable, String customerID, String phoneNum1, String phoneNum2,
			String phoneNum3) {
		this.accountNumber = accountNumber;
		this.transactionDate = transactionDate;
		this.transactionAmount = transactionAmount;
		this.transactionMenemonic = transactionMenemonic;
		this.transactionDescription = transactionDescription;
		this.transactionType = transactionType;
		this.transactionCode = transactionCode;
		this.transactionCurrency = transactionCurrency;
		this.balanceAvaliable = balanceAvaliable;
		this.customerID = customerID;
		this.phoneNum1 = phoneNum1;
		this.phoneNum2 = phoneNum2;
		this.phoneNum3 = phoneNum3;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public double getTransactionAmount() {
		return transactionAmount;
	}

	public void setTransactionAmount(double transactionAmount) {
		this.transactionAmount = transactionAmount;
	}

	public String getTransactionMenemonic() {
		return transactionMenemonic;
	}

	public void setTransactionMenemonic(String transactionMenemonic) {
		this.transactionMenemonic = transactionMenemonic;
	}

	public String getTransactionDescription() {
		return transactionDescription;
	}

	public void setTransactionDescription(String transactionDescription) {
		this.transactionDescription = transactionDescription;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getTransactionCode() {
		return transactionCode;
	}

	public void setTransactionCode(String transactionCode) {
		this.transactionCode = transactionCode;
	}

	public String getTransactionCurrency() {
		return transactionCurrency;
	}

	public void setTransactionCurrency(String transactionCurrency) {
		this.transactionCurrency = transactionCurrency;
	}

	public double getBalanceAvaliable() {
		return balanceAvaliable;
	}

	public void setBalanceAvaliable(double balanceAvaliable) {
		this.balanceAvaliable = balanceAvaliable;
	}

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public String getPhoneNum1() {
		return phoneNum1;
	}

	public void setPhoneNum1(String phoneNum1) {
		this.phoneNum1 = phoneNum1;
	}

	public String getPhoneNum2() {
		return phoneNum2;
	}

	public void setPhoneNum2(String phoneNum2) {
		this.phoneNum2 = phoneNum2;
	}

	public String getPhoneNum3() {
		return phoneNum3;
	}

	public void setPhoneNum3(String phoneNum3) {
		this.phoneNum3 = phoneNum3;
	}

}
