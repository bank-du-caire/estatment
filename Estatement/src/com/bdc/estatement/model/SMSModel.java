package com.bdc.estatement.model;

import java.util.Date;

public class SMSModel {

	public String refrenceNumber;
	public String accountNumber;
	public String chequeNumber;
	public String bankName;
	public String branchName;
	public String currency;
	public String description;
	public double amount;
	public Date valueDate;
	public int isSent;
	public int isReversed;
	public String transfer;
	private String smsID;
	private String customerID;
	private String mobile;
	private String mobile2;
	private String mobile3;
	private String mobile4;

	public SMSModel() {

	}

	public SMSModel(String refrenceNumber, String accountNumber, String chequeNumber, String bankName,
			String branchName, String currency, String description, double amount, Date valueDate, int isSent) {
		this.refrenceNumber = refrenceNumber;
		this.accountNumber = accountNumber;
		this.chequeNumber = chequeNumber;
		this.bankName = bankName;
		this.branchName = branchName;
		this.currency = currency;
		this.description = description;
		this.amount = amount;
		this.valueDate = valueDate;
		this.isSent = isSent;
	}

	public String getRefrenceNumber() {
		return refrenceNumber;
	}

	public void setRefrenceNumber(String refrenceNumber) {
		this.refrenceNumber = refrenceNumber;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getChequeNumber() {
		return chequeNumber;
	}

	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Date getValueDate() {
		return valueDate;
	}

	public void setValueDate(Date valueDate) {
		this.valueDate = valueDate;
	}

	public int getIsSent() {
		return isSent;
	}

	public void setIsSent(int isSent) {
		this.isSent = isSent;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getIsReversed() {
		return isReversed;
	}

	public void setIsReversed(int isReversed) {
		this.isReversed = isReversed;
	}

	public String getTransfer() {
		return transfer;
	}

	public void setTransfer(String transfer) {
		this.transfer = transfer;
	}

	public String getSmsID() {
		return smsID;
	}

	public void setSmsID(String smsID) {
		this.smsID = smsID;
	}

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getMobile2() {
		return mobile2;
	}

	public void setMobile2(String mobile2) {
		this.mobile2 = mobile2;
	}

	public String getMobile3() {
		return mobile3;
	}

	public void setMobile3(String mobile3) {
		this.mobile3 = mobile3;
	}

	public String getMobile4() {
		return mobile4;
	}

	public void setMobile4(String mobile4) {
		this.mobile4 = mobile4;
	}

}
