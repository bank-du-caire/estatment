package com.bdc.estatement.model;

import java.util.Date;

public class EndOffDay {
	private Date dateProcess;
	private Date dateLastProcess;
	private Date dateNextProcess;

	// dateLastProcess,dateProcess, dateNextProcess
	public EndOffDay(Date dateLastProcess, Date dateProcess, Date dateNextProcess) {
		this.dateLastProcess = dateLastProcess;
		this.dateProcess = dateProcess;
		this.dateNextProcess = dateNextProcess;
	}

	public Date getDateProcess() {
		return dateProcess;
	}

	public void setDateProcess(Date dateProcess) {
		this.dateProcess = dateProcess;
	}

	public Date getDateLastProcess() {
		return dateLastProcess;
	}

	public void setDateLastProcess(Date dateLastProcess) {
		this.dateLastProcess = dateLastProcess;
	}

	public Date getDateNextProcess() {
		return dateNextProcess;
	}

	public void setDateNextProcess(Date dateNextProcess) {
		this.dateNextProcess = dateNextProcess;
	}

}
