package com.bdc.estatement.model;

public class SwiftMsg {
	private String MSG;
	private String REF_NO;
	public String getMSG() {
		return MSG;
	}
	public void setMSG(String mSG) {
		MSG = mSG;
	}
	public String getREF_NO() {
		return REF_NO;
	}
	public void setREF_NO(String rEF_NO) {
		REF_NO = rEF_NO;
	}
	public SwiftMsg(String mSG, String rEF_NO) {
		super();
		MSG = mSG;
		REF_NO = rEF_NO;
	}
	


}
