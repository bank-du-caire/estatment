package com.bdc.estatement.model;

import java.util.Date;

public class MeezaRegCustomers {
	
	private int COD_CUST;
	private int FREQ_STMNT;               
	private String EMAIL;                    
	private Date STMT_NEXT_DATE;           
	private String DESCRIPTION ;           
	private Date LAST_SENT_DATE;
	private int NO_OF_FAILED_ATTEMPTS  ;
	private String STATUS;
	private String ADDRESS;
	private String ACCOUNT_TYPE;
	private String BRANCH_NAME;
	private String CURRENCY_NAME;
	private String CUSTOMER_NAME;
	private int CREATED_BY_USER_ID;
	private Date CREATION_DATE;
	private int IS_APPROVED;
	private int IS_DELETED;
	private int APPROVEDBYUSERID;
	private String NONEPERFORMING_ACCOUNT;
	private String NONEPERFORMING_CUSTOMER;	
	private String COD_ACCT_NO ;   
	
	public MeezaRegCustomers(int cOD_CUST, int fREQ_STMNT, String eMAIL, Date sTMT_NEXT_DATE, String dESCRIPTION,
			Date lAST_SENT_DATE, int nO_OF_FAILED_ATTEMPTS, String sTATUS, String aDDRESS, String aCCOUNT_TYPE,
			String bRANCH_NAME, String cURRENCY_NAME, String cUSTOMER_NAME, int cREATED_BY_USER_ID, Date cREATION_DATE,
			int iS_APPROVED, int iS_DELETED, int aPPROVEDBYUSERID, String nONEPERFORMING_ACCOUNT,
			String nONEPERFORMING_CUSTOMER, String cOD_ACCT_NO) {
		
		this.COD_CUST = cOD_CUST;
		this.FREQ_STMNT = fREQ_STMNT;
		this.EMAIL = eMAIL;
		this.STMT_NEXT_DATE = sTMT_NEXT_DATE;
		this.DESCRIPTION = dESCRIPTION;
		this.LAST_SENT_DATE = lAST_SENT_DATE;
		this.NO_OF_FAILED_ATTEMPTS = nO_OF_FAILED_ATTEMPTS;
		this.STATUS = sTATUS;
		this.ADDRESS = aDDRESS;
		this.ACCOUNT_TYPE = aCCOUNT_TYPE;
		this.BRANCH_NAME = bRANCH_NAME;
		this.CURRENCY_NAME = cURRENCY_NAME;
		this.CUSTOMER_NAME = cUSTOMER_NAME;
		this.CREATED_BY_USER_ID = cREATED_BY_USER_ID;
		this.CREATION_DATE = cREATION_DATE;
		this.IS_APPROVED = iS_APPROVED;
		this.IS_DELETED = iS_DELETED;
		this.APPROVEDBYUSERID = aPPROVEDBYUSERID;
		this.NONEPERFORMING_ACCOUNT = nONEPERFORMING_ACCOUNT;
		this.NONEPERFORMING_CUSTOMER = nONEPERFORMING_CUSTOMER;
		this.COD_ACCT_NO = cOD_ACCT_NO;
	}
	
	public MeezaRegCustomers() {
		// TODO Auto-generated constructor stub
	}

	public String getCOD_ACCT_NO() {
		return COD_ACCT_NO;
	}
	public void setCOD_ACCT_NO(String cOD_ACCT_NO) {
		COD_ACCT_NO = cOD_ACCT_NO;
	}
	public int getFREQ_STMNT() {
		return FREQ_STMNT;
	}
	public void setFREQ_STMNT(int fREQ_STMNT) {
		FREQ_STMNT = fREQ_STMNT;
	}
	public String getEMAIL() {
		return EMAIL;
	}
	public void setEMAIL(String eMAIL) {
		EMAIL = eMAIL;
	}
	public Date getSTMT_NEXT_DATE() {
		return STMT_NEXT_DATE;
	}
	public void setSTMT_NEXT_DATE(Date sTMT_NEXT_DATE) {
		STMT_NEXT_DATE = sTMT_NEXT_DATE;
	}
	public String getDESCRIPTION() {
		return DESCRIPTION;
	}
	public void setDESCRIPTION(String dESCRIPTION) {
		DESCRIPTION = dESCRIPTION;
	}
	public Date getLAST_SENT_DATE() {
		return LAST_SENT_DATE;
	}
	public void setLAST_SENT_DATE(Date lAST_SENT_DATE) {
		LAST_SENT_DATE = lAST_SENT_DATE;
	}
	public int getNO_OF_FAILED_ATTEMPTS() {
		return NO_OF_FAILED_ATTEMPTS;
	}
	public void setNO_OF_FAILED_ATTEMPTS(int nO_OF_FAILED_ATTEMPTS) {
		NO_OF_FAILED_ATTEMPTS = nO_OF_FAILED_ATTEMPTS;
	}
	public String getSTATUS() {
		return STATUS;
	}
	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}
	public String getADDRESS() {
		return ADDRESS;
	}
	public void setADDRESS(String aDDRESS) {
		ADDRESS = aDDRESS;
	}
	public String getACCOUNT_TYPE() {
		return ACCOUNT_TYPE;
	}
	public void setACCOUNT_TYPE(String aCCOUNT_TYPE) {
		ACCOUNT_TYPE = aCCOUNT_TYPE;
	}
	public String getBRANCH_NAME() {
		return BRANCH_NAME;
	}
	public void setBRANCH_NAME(String bRANCH_NAME) {
		BRANCH_NAME = bRANCH_NAME;
	}
	public String getCURRENCY_NAME() {
		return CURRENCY_NAME;
	}
	public void setCURRENCY_NAME(String cURRENCY_NAME) {
		CURRENCY_NAME = cURRENCY_NAME;
	}
	public String getCUSTOMER_NAME() {
		return CUSTOMER_NAME;
	}
	public void setCUSTOMER_NAME(String cUSTOMER_NAME) {
		CUSTOMER_NAME = cUSTOMER_NAME;
	}
	public int getCREATED_BY_USER_ID() {
		return CREATED_BY_USER_ID;
	}
	public void setCREATED_BY_USER_ID(int cREATED_BY_USER_ID) {
		CREATED_BY_USER_ID = cREATED_BY_USER_ID;
	}
	public Date getCREATION_DATE() {
		return CREATION_DATE;
	}
	public void setCREATION_DATE(Date cREATION_DATE) {
		CREATION_DATE = cREATION_DATE;
	}
	public int getIS_APPROVED() {
		return IS_APPROVED;
	}
	public void setIS_APPROVED(int iS_APPROVED) {
		IS_APPROVED = iS_APPROVED;
	}
	public int getIS_DELETED() {
		return IS_DELETED;
	}
	public void setIS_DELETED(int iS_DELETED) {
		IS_DELETED = iS_DELETED;
	}
	public int getAPPROVEDBYUSERID() {
		return APPROVEDBYUSERID;
	}
	public void setAPPROVEDBYUSERID(int aPPROVEDBYUSERID) {
		APPROVEDBYUSERID = aPPROVEDBYUSERID;
	}
	public String getNONEPERFORMING_ACCOUNT() {
		return NONEPERFORMING_ACCOUNT;
	}
	public void setNONEPERFORMING_ACCOUNT(String nONEPERFORMING_ACCOUNT) {
		NONEPERFORMING_ACCOUNT = nONEPERFORMING_ACCOUNT;
	}
	public String getNONEPERFORMING_CUSTOMER() {
		return NONEPERFORMING_CUSTOMER;
	}
	public void setNONEPERFORMING_CUSTOMER(String nONEPERFORMING_CUSTOMER) {
		NONEPERFORMING_CUSTOMER = nONEPERFORMING_CUSTOMER;
	}
	public int getCOD_CUST() {
		return COD_CUST;
	}
	public void setCOD_CUST(int cOD_CUST) {
		COD_CUST = cOD_CUST;
	}
	
}
