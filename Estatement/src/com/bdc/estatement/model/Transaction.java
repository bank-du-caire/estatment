package com.bdc.estatement.model;

import java.util.Date;

public class Transaction {
	private String customerName;
	private String customerId;
	private String accountNumber;
	private String accountTitle;
	private String productName;
	private String branchName;
	private Date transactionDate;
	private Date valueDate;
	private String currency;
	private String address;
	private String desc;
	private String indicator;
	private double amount;
	private double totalBalance;
	private double debit = 0;
	private double credit = 0;

	public Transaction(String customerName, String customerId,
			String accountNumber, String accountTitle, String productName,
			String branchName, Date transactionDate, Date valueDate,
			String currecy, String address, String desc, String indicator,
			double amount) {
		this.customerName = customerName;
		this.customerId = customerId;
		this.accountNumber = accountNumber;
		this.accountTitle = accountTitle;
		this.productName = productName;
		this.branchName = branchName;
		this.transactionDate = transactionDate;
		this.valueDate = valueDate;
		this.currency = currecy;
		this.address = address;
		this.desc = desc;
		this.indicator = indicator;
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public double getDebit() {
		return debit;
	}

	public void setDebit(double debit) {
		this.debit = debit;
	}

	public double getCredit() {
		return credit;
	}

	public void setCredit(double credit) {
		this.credit = credit;
	}


	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountTitle() {
		return accountTitle;
	}

	public void setAccountTitle(String accountTitle) {
		this.accountTitle = accountTitle;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Date getValueDate() {
		return valueDate;
	}

	public void setValueDate(Date valueDate) {
		this.valueDate = valueDate;
	}

	public String getCurrecy() {
		return currency;
	}

	public void setCurrecy(String currecy) {
		this.currency = currecy;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getIndicator() {
		return indicator;
	}

	public void setIndicator(String indicator) {
		this.indicator = indicator;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getTotalBalance() {
		return totalBalance;
	}

	public void setTotalBalance(double toatalBalance) {
		this.totalBalance = toatalBalance;
	}

	@Override
	public String toString() {
		return "Transaction [customerName=" + customerName + ", customerId="
				+ customerId + ", accountNumber=" + accountNumber
				+ ", accountTitle=" + accountTitle + ", productName="
				+ productName + ", branchName=" + branchName
				+ ", transactionDate=" + transactionDate + ", valueDate="
				+ valueDate + ", currency=" + currency + ", address=" + address
				+ ", desc=" + desc + ", indicator=" + indicator + ", amount="
				+ amount + "]";
	}

}
