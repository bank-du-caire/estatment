package com.bdc.estatement.model;

import java.util.Date;

public class AccountPackage {
	String fullName;
	String customerId;
	String accountNumber;
	String accountTitle;
	String productName;
	String branchName;
	double bookBalance;
	double availableBalance;
	double netBalance;
	String currecy;
	String address;
	String lastCredit;
	String lastDebit;
	double startBalance;
	double endBalance;
	private String fromDate;
	private String toDate;
	private Date lastSentDate;
	private Date nextDate;
	private long debitCount = 0;
	private long creditCount = 0;
	private double totalDebit;
	private double totalCredit;

	public AccountPackage(String fullName, String customerId,
			String accountNumber, String accountTitle, String productName,
			String branchName, double bookBalance, double availableBalance,
			double netBalance, String currecy, String address,
			String lastCredit, String lastDebit) {
		this.fullName = fullName;
		this.customerId = customerId;
		this.accountNumber = accountNumber;
		this.accountTitle = accountTitle;
		this.productName = productName;
		this.branchName = branchName;
		this.bookBalance = bookBalance;
		this.availableBalance = availableBalance;
		this.netBalance = netBalance;
		this.currecy = currecy;
		this.address = address;
		this.lastCredit = lastCredit;
		this.lastDebit = lastDebit;
	}

	public AccountPackage() {
	}

	
	public double getTotalDebit() {
		return totalDebit;
	}

	public void setTotalDebit(double totalDebit) {
		this.totalDebit = totalDebit;
	}

	public double getTotalCredit() {
		return totalCredit;
	}

	public void setTotalCredit(double totalCredit) {
		this.totalCredit = totalCredit;
	}

	public Date getLastSentDate() {
		return lastSentDate;
	}

	public void setLastSentDate(Date lastSentDate) {
		this.lastSentDate = lastSentDate;
	}

	public Date getNextDate() {
		return nextDate;
	}

	public void setNextDate(Date nextDate) {
		this.nextDate = nextDate;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public double getStartBalance() {
		return startBalance;
	}

	public void setStartBalance(double startBalance) {
		this.startBalance = startBalance;
	}

	public double getEndBalance() {
		return endBalance;
	}

	public void setEndBalance(double endBalance) {
		this.endBalance = endBalance;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountTitle() {
		return accountTitle;
	}

	public void setAccountTitle(String accountTitle) {
		this.accountTitle = accountTitle;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public double getBookBalance() {
		return bookBalance;
	}

	public void setBookBalance(double bookBalance) {
		this.bookBalance = bookBalance;
	}

	public double getAvailableBalance() {
		return availableBalance;
	}

	public void setAvailableBalance(double availableBalance) {
		this.availableBalance = availableBalance;
	}

	public double getNetBalance() {
		return netBalance;
	}

	public void setNetBalance(double netBalance) {
		this.netBalance = netBalance;
	}

	public String getCurrecy() {
		return currecy;
	}

	public void setCurrecy(String currecy) {
		this.currecy = currecy;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLastCredit() {
		return lastCredit;
	}

	public void setLastCredit(String lastCredit) {
		this.lastCredit = lastCredit;
	}

	public String getLastDebit() {
		return lastDebit;
	}

	public void setLastDebit(String lastDebit) {
		this.lastDebit = lastDebit;
	}

	public long getDebitCount() {
		return debitCount;
	}

	public void setDebitCount(long debitCount) {
		this.debitCount = debitCount;
	}

	public long getCreditCount() {
		return creditCount;
	}

	public void setCreditCount(long creditCount) {
		this.creditCount = creditCount;
	}
}
