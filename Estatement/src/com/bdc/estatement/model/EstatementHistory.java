package com.bdc.estatement.model;

import java.util.Date;

public class EstatementHistory {
	private String customerName;
	private String AccountNo;
	private String customerCode;
	private int freqStmt;
	private Date from;
	private Date to;
	private String pdfPath;
	private String excelPath;
	private String swiftPath;
	private String mail;

	public EstatementHistory(String customerName, String accountNo, String customerCode, int freqStmt, Date from,
			Date to, String pdfPath, String excelPath, String swiftPath) {
		this.customerName = customerName;
		AccountNo = accountNo;
		this.customerCode = customerCode;
		this.freqStmt = freqStmt;
		this.from = from;
		this.to = to;
		this.pdfPath = pdfPath;
		this.excelPath = excelPath;
		this.swiftPath = swiftPath;
	}

	public EstatementHistory() {
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getAccountNo() {
		return AccountNo;
	}

	public void setAccountNo(String accountNo) {
		AccountNo = accountNo;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public int getFreqStmt() {
		return freqStmt;
	}

	public void setFreqStmt(int freqStmt) {
		this.freqStmt = freqStmt;
	}

	public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
	}

	public Date getTo() {
		return to;
	}

	public void setTo(Date to) {
		this.to = to;
	}

	public String getPdfPath() {
		return pdfPath;
	}

	public void setPdfPath(String pdfPath) {
		this.pdfPath = pdfPath;
	}

	public String getExcelPath() {
		return excelPath;
	}

	public void setExcelPath(String excelPath) {
		this.excelPath = excelPath;
	}

	public String getSwiftPath() {
		return swiftPath;
	}

	public void setSwiftPath(String swiftPath) {
		this.swiftPath = swiftPath;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}
}
