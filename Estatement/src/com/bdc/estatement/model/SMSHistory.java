package com.bdc.estatement.model;

import java.util.Date;

public class SMSHistory {
	private String customerName;
	private String accountNumber;
	private Date creationDate;
	private String message;
	private String mobile;
	private String smsID;
	private String status;

	public SMSHistory() {
	}

	public SMSHistory(String customerName, String accountNumber, Date creationDate, String message, String mobile) {
		this.customerName = customerName;
		this.accountNumber = accountNumber;
		this.creationDate = creationDate;
		this.message = message;
		this.mobile = mobile;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getSmsID() {
		return smsID;
	}

	public void setSmsID(String smsID) {
		this.smsID = smsID;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
