package com.bdc.estatement.model;

import java.util.Date;

public class TermDepositSMS {
	private int codeCust;
	private String refCustPhoneOff;
	private String refCustTelex;
	private String refCustPhone;
	private String nameProduct;
	private float rateDep;
	private int balPrincipal;
	private String currancy;
	private Date depDate;
	private Date valueDate;

	public TermDepositSMS( String refCustPhoneOff, String refCustTelex, String refCustPhone,
			String nameProduct, float rateDep, int balPrincipal, String currancy, Date depDate2, Date valueDate2) {
		
		this.refCustPhoneOff = refCustPhoneOff;
		this.refCustTelex = refCustTelex;
		this.refCustPhone = refCustPhone;
		this.nameProduct = nameProduct;
		this.rateDep = rateDep;
		this.balPrincipal = balPrincipal;
		this.currancy = currancy;
		this.depDate = depDate2;
		this.valueDate = valueDate2;
	}

	public int getCodeCust() {
		return codeCust;
	}

	public void setCodeCust(int codeCust) {
		this.codeCust = codeCust;
	}

	public String getRefCustPhoneOff() {
		return refCustPhoneOff;
	}

	public void setRefCustPhoneOff(String refCustPhoneOff) {
		this.refCustPhoneOff = refCustPhoneOff;
	}

	public String getRefCustTelex() {
		return refCustTelex;
	}

	public void setRefCustTelex(String refCustTelex) {
		this.refCustTelex = refCustTelex;
	}

	public String getRefCustPhone() {
		return refCustPhone;
	}

	public void setRefCustPhone(String refCustPhone) {
		this.refCustPhone = refCustPhone;
	}

	public String getNameProduct() {
		return nameProduct;
	}

	public void setNameProduct(String nameProduct) {
		this.nameProduct = nameProduct;
	}

	
	public float getRateDep() {
		return rateDep;
	}

	public void setRateDep(float rateDep) {
		this.rateDep = rateDep;
	}

	public int getBalPrincipal() {
		return balPrincipal;
	}

	public void setBalPrincipal(int balPrincipal) {
		this.balPrincipal = balPrincipal;
	}

	public String getCurrancy() {
		return currancy;
	}

	public void setCurrancy(String currancy) {
		this.currancy = currancy;
	}

	public Date getDepDate() {
		return depDate;
	}

	public void setDepDate(Date depDate) {
		this.depDate = depDate;
	}

	public Date getValueDate() {
		return valueDate;
	}

	public void setValueDate(Date valueDate) {
		this.valueDate = valueDate;
	}

	@Override
	public String toString() {
		return "TermDepositSMS [codeCust=" + codeCust + ", refCustPhoneOff=" + refCustPhoneOff + ", refCustTelex="
				+ refCustTelex + ", refCustPhone=" + refCustPhone + ", nameProduct=" + nameProduct + ", rateDep="
				+ rateDep + ", balPrincipal=" + balPrincipal + ", currancy=" + currancy + ", depDate=" + depDate
				+ ", valueDate=" + valueDate + "]";
	}

}
