package com.bdc.estatement.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Account {
	private String sftpFolder;
	private String sftpPaymentFolder;
	private String customerCode;
	private String accountNumber;
	private int frequancyStmt;
	private String email;
	private Date nextDate;
	private String description;
	private Date lastSentDate;
	private int noOfFaildAttemps;
	private String password;
	private String status;
	private String mobile;
	private String address;
	private String accountType;
	private String branchName;
	private String currencyName;
	private String customerName;
	private String fromDate;
	private String toDate;
	private String accountTypeFlag;
	private int createdBy;
	private int failureMail;
	private int isApproved;
	private int smsFlag;
	private int estatementFlag;
	private int isDeleted;
	private Date smsActivationDate;
	private Date creationDate;
	private String nonePerformingAccount;
	private String nonePerformingCustomer;
	private int welcomeMail;

	public Account(String customerCode, String accountNumber, int frequancyStmt, String email, Date nextDate,
			String description, Date lastSentDate, int noOfFaildAttemps, String password, String status, String mobile,
			String address, String accountType, String branchName, String currencyName, String customerName) {
		this.customerCode = customerCode;
		this.accountNumber = accountNumber;
		this.frequancyStmt = frequancyStmt;
		this.email = email;
		this.nextDate = nextDate;
		this.description = description;
		this.lastSentDate = lastSentDate;
		this.noOfFaildAttemps = noOfFaildAttemps;
		this.password = password;
		this.status = status;
		this.mobile = mobile;
		this.address = address;
		this.accountType = accountType;
		this.branchName = branchName;
		this.currencyName = currencyName;
		this.customerName = customerName;
		
	}

	public Account() {

	}

	public int getSmsFlag() {
		return smsFlag;
	}

	public void setSmsFlag(int smsFlag) {
		this.smsFlag = smsFlag;
	}

	public int getEstatementFlag() {
		return estatementFlag;
	}

	public void setEstatementFlag(int estatementFlag) {
		this.estatementFlag = estatementFlag;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public int getFrequancyStmt() {
		return frequancyStmt;
	}

	public void setFrequancyStmt(int frequancyStmt) {
		this.frequancyStmt = frequancyStmt;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getNextDate() {
		return nextDate;
	}

	public void setNextDate(Date nextDate) {
		this.nextDate = nextDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getLastSentDate() {
		return lastSentDate;
	}

	public void setLastSentDate(Date lastSentDate) {
		this.lastSentDate = lastSentDate;
	}

	public int getNoOfFaildAttemps() {
		return noOfFaildAttemps;
	}

	public void setNoOfFaildAttemps(int noOfFaildAttemps) {
		this.noOfFaildAttemps = noOfFaildAttemps;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCurrencyName() {
		return currencyName;
	}

	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}

	public String getAccountTypeFlag() {
		return accountTypeFlag;
	}

	public void setAccountTypeFlag(String accountTypeFlag) {
		this.accountTypeFlag = accountTypeFlag;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public int getFailureMail() {
		return failureMail;
	}

	public void setFailureMail(int failureMail) {
		this.failureMail = failureMail;
	}

	public int getIsApproved() {
		return isApproved;
	}

	public void setIsApproved(int isApproved) {
		this.isApproved = isApproved;
	}

	public int getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Date getSmsActivationDate() {
		return smsActivationDate;
	}

	public void setSmsActivationDate(Date smsActivationDate) {
		this.smsActivationDate = smsActivationDate;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	@Override
	public String toString() {
		return "Account [customerCode=" + customerCode + ", accountNumber=" + accountNumber + ", frequancyStmt="
				+ frequancyStmt + ", email=" + email + ", nextDate=" + nextDate + ", description=" + description
				+ ", lastSentDate=" + lastSentDate + ", noOfFaildAttemps=" + noOfFaildAttemps + ", password=" + password
				+ ", status=" + status + ", mobile=" + mobile + ", address=" + address + ", accountType=" + accountType
				+ ", branchName=" + branchName + ", currencyName=" + currencyName + ", customerName=" + customerName
				+ "]";
	}

	public String getNonePerformingAccount() {
		return nonePerformingAccount;
	}

	public void setNonePerformingAccount(String nonePerformingAccount) {
		this.nonePerformingAccount = nonePerformingAccount;
	}

	public String getNonePerformingCustomer() {
		return nonePerformingCustomer;
	}

	public void setNonePerformingCustomer(String nonePerformingCustomer) {
		this.nonePerformingCustomer = nonePerformingCustomer;
	}

	public int getWelcomeMail() {
		return welcomeMail;
	}

	public void setWelcomeMail(int welcomeMail) {
		this.welcomeMail = welcomeMail;
	}

	public String getSftpFolder() {
		return sftpFolder;
	}

	public void setSftpFolder(String sftpFolder) {
		this.sftpFolder = sftpFolder;
	}

	public String getSftpPaymentFolder() {
		return sftpPaymentFolder;
	}

	public void setSftpPaymentFolder(String sftpPaymentFolder) {
		this.sftpPaymentFolder = sftpPaymentFolder;
	}
	

}
