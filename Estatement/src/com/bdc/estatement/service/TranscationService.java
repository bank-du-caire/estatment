package com.bdc.estatement.service;

import java.util.List;

import com.bdc.estatement.model.Account;
import com.bdc.estatement.model.EstatementHistory;
import com.bdc.estatement.model.Transaction;



public interface TranscationService {
	public List<Account> getAllNewRequests();
	public List<Account> getAllAccounts();
	public List<Account> searchAccountsByName(String searchParam);
	public List<EstatementHistory> getAllEstamentHistories();

	

}
