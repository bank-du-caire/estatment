package com.bdc.estatement.hostothost;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

//import com.itextpdf.io.IOException;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;

public class SftpServer {

	public static void uploadToSftpServer(String filename, String workingDirectory) {
		try {
			JSch jsch = new JSch();
			int port = 22;
			HashMap properties = readPropertiesFileInfo("D:\\BDC_APP\\HostToHost\\resources\\properties.txt");
			String username = (String) properties.get(1), host = (String) properties.get(2),
					password = (String) properties.get(3);
			com.jcraft.jsch.Session session = jsch.getSession(username.trim(), host.trim(), port);
			session.setPassword(password.trim());

			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.connect();
			
			Channel channel = session.openChannel("sftp");
			channel.connect();
			ChannelSftp channelSftp = (ChannelSftp) channel;
			channelSftp.cd(workingDirectory);
			File f = new File(filename);

			channelSftp.put(new FileInputStream(f), f.getName());

			f.delete();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static List<String> getFilesFromSftpServer(String workingDirectory) {
		List<String> allFiles = new ArrayList<String>();
		try {
			JSch jsch = new JSch();
			int port = 22;
			HashMap properties = readPropertiesFileInfo("D:\\BDC_APP\\HostToHost\\resources\\properties.txt");
			String username = (String) properties.get(1), host = (String) properties.get(2),
					password = (String) properties.get(3);
			com.jcraft.jsch.Session session = jsch.getSession(username.trim(), host.trim(), port);
			session.setPassword(password.trim());

			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.connect();

			Channel channel = session.openChannel("sftp");
			channel.connect();
			ChannelSftp channelSftp = (ChannelSftp) channel;
			channelSftp.cd(workingDirectory);
			//
//			File file = new File("D:\\BDC_APP\\HostToHost\\Temp\\" + workingDirectory);
			File file = new File("\\\\BDC.bank.local\\BDC\\Host to host\\" + workingDirectory);
			boolean dirCreated = file.mkdir();
			Vector<ChannelSftp.LsEntry> list = channelSftp.ls("*.*");
//			Vector<ChannelSftp.LsEntry> list = channelSftp.ls("*.pdf");
			for (ChannelSftp.LsEntry entry : list) {
				System.out.println(entry.getFilename());
//				allFiles.add("\\\\BDC.bank.local\\BDC\\Host to host\\" + workingDirectory + "\\" + entry.getFilename());
				allFiles.add(entry.getFilename());
				byte[] buffer = new byte[1024];
				BufferedInputStream bis = new BufferedInputStream(channelSftp.get(entry.getFilename()));

				File newFile = new File(
						"\\\\BDC.bank.local\\BDC\\Host to host\\" + workingDirectory + "\\" + entry.getFilename());
				OutputStream os = new FileOutputStream(newFile);
				BufferedOutputStream bos = new BufferedOutputStream(os);
				int readCount;
				// System.out.println("Getting: " + theLine);
				while ((readCount = bis.read(buffer)) > 0) {
					System.out.println("Writing: " + entry.getFilename());
					bos.write(buffer, 0, readCount);
				}
				bis.close();
				bos.close();
				channelSftp.rm(entry.getFilename());

			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return allFiles;
	}

	public static HashMap readPropertiesFileInfo(String resourcesPath) {
		HashMap properties = new HashMap();
		try {
			File file = new File(resourcesPath);
			FileReader fr = new FileReader(file); // reads the file
			BufferedReader br = new BufferedReader(fr); // creates a buffering character input stream
			StringBuffer sb = new StringBuffer(); // constructs a string buffer with no characters
			String line;
			int key = 1;
			while ((line = br.readLine()) != null) {
				String[] arr = line.split("=");
				String value = arr[1];
				properties.put(key++, value);
				sb.append(line); // appends line to string buffer
				sb.append("\n"); // line feed
			}
			fr.close(); // closes the stream and release the resources
//			System.out.println("Contents of File: ");
//			System.out.println(sb.toString()); // returns a string that textually represents the object
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return properties;
	}
}
