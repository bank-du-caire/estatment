package com.bdc.estatement.controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.bdc.estatement.dao.SMSDao;
import com.bdc.estatement.dao.TransactionsDao;
import com.bdc.estatement.model.Account;
import com.bdc.estatement.model.SMSHistory;
import com.bdc.estatement.model.SMSModel;
import com.bdc.estatement.model.TermDepositSMS;
import com.bdc.estatement.util.LogUtil;
import com.bdc.estatement.util.SMSUtils;
import com.bdc.estatement.util.Utils;

import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;
import okhttp3.Request;
import okhttp3.Response;

public class SMSCollatoralsController {

	public static void main(String[] args) {
		LogUtil.initialize();
		LogUtil.info("Get New Term Deposit and insert into SMS DB");
		int result = SMSDao.insertTermDepositeForSms();
		LogUtil.info("counter of new term deposites:: " + result);

		List<TermDepositSMS> allTermDepositSMS = new ArrayList<TermDepositSMS>();
		allTermDepositSMS = SMSDao.getUnsendTermDepositSMS();
		for (TermDepositSMS termDeposit : allTermDepositSMS) {
			LogUtil.info("start initiating messages for Term Deposit");
			initiateMessages(termDeposit);
		}
	}

	private static void initiateMessages(TermDepositSMS termDeposit) {
		try {
			String smsMessage = SMSUtils.prepareMessage(termDeposit);
			System.out.println("SMS body ::: " + smsMessage);
//			LogUtil.info(smsMessage);
//			String smsID = "";
//			String mobile = account.getMobile();
//			if (mobile.contains(",")) {
//				String[] arr = mobile.split(",");
//				for (int i = 0; i < arr.length; i++) {
//					smsID = SMSUtils.sendMessage(smsMessage, arr[i].trim());
//					if (smsID != null) {
//						smsModel.setSmsID(smsID);
//						int res = SMSDao.insertToSMSHistory(smsModel, account.getCustomerName(), smsMessage,
//								arr[i].trim());
//						if (res == 1)
//							LogUtil.info("History Added Successfully.");
//					}
//				}
//			} else {
//				smsID = SMSUtils.sendMessage(smsMessage, mobile.trim());
//				smsModel.setSmsID(smsID);
//				if (smsID != null) {
//					int res = SMSDao.insertToSMSHistory(smsModel, account.getCustomerName(), smsMessage, mobile.trim());
//					if (res == 1)
//						LogUtil.info("History Added Successfully.");
//				}
//			}
////				String smsID = SMSUtils.sendMessage(smsMessage, account.getMobile());
////				smsID = SMSUtils.sendMessage(smsMessage, account.getMobile());
////				smsModel.setSmsID(smsID);
//			if (!smsID.isEmpty() && smsID != "") {
//				LogUtil.info("SMS Sent Successfully");
//				switch (msgType) {
//				case "Deposit":
//					SMSDao.updateCustomerTransaction(smsModel);
//					break;
//				case "Cheques collected":
//					SMSDao.updateCollectedChequesTransaction(smsModel);
//					break;
//				case "Cheques returned":
//					SMSDao.updateReturnedChequesTransaction(smsModel);
//					break;
//				}
//				LogUtil.info("Transaction Flagged As sent in DB.");
//
//			} else
//				LogUtil.info("SMS dose not Sent");
//
		} catch (Exception e) {
			LogUtil.error(e.getMessage());
		}
	}
}
