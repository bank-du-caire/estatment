package com.bdc.estatement.controller;

import java.io.File;
import java.util.List;

import com.bdc.estatement.dao.TransactionsDao;
import com.bdc.estatement.hostothost.SftpServer;
import com.bdc.estatement.util.LogUtil;
import com.bdc.estatement.util.Utils;
import com.itextpdf.io.IOException;
import java.util.*;

public class SftpPaymentHostToHost {
	/*
	 * this job will do below steps 1- read all approved host to host requests get
	 * only sftp payment folder names 2- get all new requests (all documents excel
	 * or pdf files from sftp folder add them to temp folder) 3- delete those files
	 * from sftp payment folder 4- send mail contains attachements for those
	 * requests to mail group 5- move those requests to archiving folder.
	 * 
	 */
	public static void main(String[] args) {

		LogUtil.initialize();
		List<String> sftpPaymentFolders = TransactionsDao.getApprovedHotToHostPaymentFolders();
		for (String sftpPaymentFolder : sftpPaymentFolders) {
			List<String> allAttachements = SftpServer.getFilesFromSftpServer(sftpPaymentFolder);
//			Utils.sendHostToHostMail(allAttachements);
			if (!allAttachements.isEmpty()) {
				String subject = "Host to host-new Request " + sftpPaymentFolder;
				for (String fileName : allAttachements) {
					String msgBody = "Dear Sir/Madam,<br><br>";
					msgBody += "A new request has been received via host to host.<br><br>";
					msgBody += "<b>Company name:</b> " + sftpPaymentFolder + "<br/>";
					msgBody += "<b>Request:</b> " + fileName + "<br/>";
					msgBody += "<b><p style='background-color: #ff9933;'> *** This is system generated mail. Please do not reply.***</p></b>";
					System.out.println(msgBody);
					Utils.sendMail("HostToHost", "P@$$w0rd", "shady.ashour@bdc.com.eg;Engy.AbdelMageed@bdc.com.eg",
							msgBody, subject);
				}
			}
		}

	}

}
