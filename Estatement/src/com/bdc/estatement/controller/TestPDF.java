package com.bdc.estatement.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class TestPDF {

	public static void main(String[] args) {
		try {
			OutputStream file = new FileOutputStream(new File("D:\\Test.pdf"));

			Document document = new Document();
			PdfWriter writer = PdfWriter.getInstance(document, file);
			writer.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);

//			String encoding = "Identity-H";
//			Font fontNormal = FontFactory.getFont(("D:\\arialuni.ttf"),
//					encoding, BaseFont.EMBEDDED, 8, Font.NORMAL);

//			BaseFont bf = BaseFont.createFont("D:\\arialuni.ttf",
//					BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			BaseFont bf2 = BaseFont.createFont("D:\\arial.ttf",
					BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
//			Font font = new Font(bf, 20);
			Font font2 = new Font(bf2, 20);
			// ColumnText column = new ColumnText(writer.getDirectContent());
			// column.setSimpleColumn(36, 730, 569, 36);
			// column.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
			// column.addElement(new Paragraph(
			// "\u0627\u0644\u0645\u0648\u0642\u0639 \u0627\u0644\u0625\u0644\u0643\u062a\u0631\u0648\u0646\u064a",
			// font));
			// column.go();

			document.open();
			PdfPTable pTable = new PdfPTable(1);
			Paragraph paragraph = getCellParagraph();
			paragraph.setFont(font2);
			paragraph.add(" شادى عاشور");
			PdfPCell cell = getPdfPCellNoBorder(paragraph);
			pTable.addCell(cell);

			// after add all your content
			document.add(pTable);
			document.close();
			file.close();

		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	private static Paragraph getCellParagraph() {
		Paragraph paragraph = new Paragraph();
		paragraph.setAlignment(Paragraph.ALIGN_JUSTIFIED);
		// set other styles you need like custom font
		return paragraph;
	}

	private static PdfPCell getPdfPCellNoBorder(Paragraph paragraph) {
		PdfPCell cell = new PdfPCell();
		cell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
		cell.setPaddingBottom(8);
		cell.setBorder(Rectangle.NO_BORDER);
		cell.addElement(paragraph);

		return cell;
	}

}
