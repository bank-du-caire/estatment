package com.bdc.estatement.controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.bdc.estatement.dao.SMSDao;
import com.bdc.estatement.dao.TransactionsDao;
import com.bdc.estatement.model.Account;
import com.bdc.estatement.model.SMSHistory;
import com.bdc.estatement.model.SMSModel;
import com.bdc.estatement.util.LogUtil;
import com.bdc.estatement.util.SMSUtils;
import com.bdc.estatement.util.Utils;

import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;
import okhttp3.Request;
import okhttp3.Response;

public class SMS_CBE_6_MonthsController2 {

	public static void checkSmsStatus(String smsId) {
//		List<String> smsIDs = SMSDao.getSmsIdsForCBE_6_months();
//		for (String smsId : smsIDs) {
		String accessToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6IjY3MGVkMjUwZGIxMmFmOGQwMTk0YTU4NzU3MzVmOGY3MTc3YjlkMmM1M2E2YjY3OGJkYTAwOWViMGQwNWU3MmEyNWRkMTFiY2I3MGYwNmQ1YzUzNDc3MmM2M2MzZWI1MzAwYzc2OTI5ZTVlZGMzNDk3OTQyNmNmZjc4MWViN2I2YzY4MmU0ZTQ2NmUzNjI3NDc2MmJjMzI0ZGIwNDcyMzMiLCJpYXQiOjE1NjEyODQ5NTEsImV4cCI6MzEzOTE2NDk1MX0.tNlyEkSB1cAkKWGqhrr8o_sWbwrE9S7GNoIuGBfc2Y0";
		OkHttpClient client = new OkHttpClient();
		String url = "http://api.cequens.com/cequens/api/v1/messaging/" + smsId;
		Builder builder = client.newBuilder();

		// for development on local machines un comment it and dont forget before
		// publiush to comment proxy 2 lines of code
//			Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.248.4.21", 9090));
//			builder.proxy(proxy);
		LogUtil.info("Calling Cequence Get Message Details URL : " + url);
		Request request = new Request.Builder().header("Accept", "application/json")
				.addHeader("Content-Type", "application/json")
				.addHeader("Authorization", String.format("Bearer %s", accessToken)).url(url).build();
		Response response = null;
		JSONObject jsonObject = null;
		try {
			response = builder.build().newCall(request).execute();
			System.out.println(response);
			LogUtil.info("getting response of message ID: " + smsId + " : " + response.toString());
			String jsonString = response.body().string();
			jsonObject = new JSONObject(jsonString);
			String data = jsonObject.getString("data");
			System.out.println("data:: " + data);

			data = data.replace("[", "");
			data = data.replace("]", "");
			if (!data.isEmpty() && data.length() > 0) {
				JSONObject dataJason = new JSONObject(data);
				String status = dataJason.getString("Status");
				System.out.println("status:: [" + status + "]");
				System.out.println(response);
				if (!status.isEmpty() && status != "") {
					if (status.equals("1"))
						SMSDao.updateSmsCBE_6_months(smsId, "Delivered");
					else if (status.equals("2"))
						SMSDao.updateSmsCBE_6_months(smsId, "Undelivered");
					else if (status.equals("3"))
						SMSDao.updateSmsCBE_6_months(smsId, "Sent");
					else if (status.equals("4"))
						SMSDao.updateSmsCBE_6_months(smsId, "Buffered");
					else if (status.equals("5"))
						SMSDao.updateSmsCBE_6_months(smsId, "Blocked");
					else if (status.equals("16"))
						SMSDao.updateSmsCBE_6_months(smsId, "Rejected by SMS Center");
					else if (status.equals("17"))
						SMSDao.updateSmsCBE_6_months(smsId, "In Progress");
					else
						SMSDao.updateSmsCBE_6_months(smsId, "Unknown");
				}
			}
		} catch (IOException | JSONException e) {
			LogUtil.error(e);
		}
//		}
	}

	public static void checkSmsStatus() {
		List<String> smsIDs = SMSDao.getSmsIdsForCBE_6_months();
		for (String smsId : smsIDs) {
			if (smsId != null && !smsId.isEmpty()) {
				String accessToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6IjY3MGVkMjUwZGIxMmFmOGQwMTk0YTU4NzU3MzVmOGY3MTc3YjlkMmM1M2E2YjY3OGJkYTAwOWViMGQwNWU3MmEyNWRkMTFiY2I3MGYwNmQ1YzUzNDc3MmM2M2MzZWI1MzAwYzc2OTI5ZTVlZGMzNDk3OTQyNmNmZjc4MWViN2I2YzY4MmU0ZTQ2NmUzNjI3NDc2MmJjMzI0ZGIwNDcyMzMiLCJpYXQiOjE1NjEyODQ5NTEsImV4cCI6MzEzOTE2NDk1MX0.tNlyEkSB1cAkKWGqhrr8o_sWbwrE9S7GNoIuGBfc2Y0";
				OkHttpClient client = new OkHttpClient();
				String url = "http://api.cequens.com/cequens/api/v1/messaging/" + smsId;
				Builder builder = client.newBuilder();

				// for development on local machines un comment it and dont forget before
				// publiush to comment proxy 2 lines of code
//			Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.248.4.21", 9090));
//			builder.proxy(proxy);
				LogUtil.info("Calling Cequence Get Message Details URL : " + url);
				Request request = new Request.Builder().header("Accept", "application/json")
						.addHeader("Content-Type", "application/json")
						.addHeader("Authorization", String.format("Bearer %s", accessToken)).url(url).build();
				Response response = null;
				JSONObject jsonObject = null;
				try {
					response = builder.build().newCall(request).execute();
					System.out.println(response);
					LogUtil.info("getting response of message ID: " + smsId + " : " + response.toString());
					String jsonString = response.body().string();
					jsonObject = new JSONObject(jsonString);
					String data = jsonObject.getString("data");
					System.out.println("data:: " + data);

					data = data.replace("[", "");
					data = data.replace("]", "");
					if (!data.isEmpty() && data.length() > 0) {
						JSONObject dataJason = new JSONObject(data);
						String status = dataJason.getString("Status");
						System.out.println("status:: [" + status + "]");
						System.out.println(response);
						if (!status.isEmpty() && status != "") {
							if (status.equals("1"))
								SMSDao.updateSmsCBE_6_months(smsId, "Delivered");
							else if (status.equals("2"))
								SMSDao.updateSmsCBE_6_months(smsId, "Undelivered");
							else if (status.equals("3"))
								SMSDao.updateSmsCBE_6_months(smsId, "Sent");
							else if (status.equals("4"))
								SMSDao.updateSmsCBE_6_months(smsId, "Buffered");
							else if (status.equals("5"))
								SMSDao.updateSmsCBE_6_months(smsId, "Blocked");
							else if (status.equals("16"))
								SMSDao.updateSmsCBE_6_months(smsId, "Rejected by SMS Center");
							else if (status.equals("17"))
								SMSDao.updateSmsCBE_6_months(smsId, "In Progress");
							else
								SMSDao.updateSmsCBE_6_months(smsId, "Unknown");
						}
					}
				} catch (IOException | JSONException e) {
					LogUtil.error(e);
				}
			}
		}
	}

	/*
	 * this job sends SMS for customers who reject CBE 6 month allowance of loans
	 * for corona virus.
	 * 
	 */
	public static void main(String[] args) throws ParseException {
		
		LogUtil.initialize();
		LogUtil.info("Get New SMSs from DB");
		List<SMSModel> smsModels = SMSDao.getSMSAccountsFor6MonthsAllownce();

		for (SMSModel smsModel : smsModels) {
			LogUtil.info("start initiating messages for Customer Transactions");
			String smsID = initiateCBEMessages(smsModel);
			checkSmsStatus(smsID);
		}
		SMSModel smsModel = new SMSModel();
		String s = initiateCBEMessages(smsModel);
		checkSmsStatus();
	}

	private static String initiateCBEMessages(SMSModel smsModel) {
		String msg = "عميلنا العزيز تم تنفيذ طلبكم بعدم تأجيل الأقساط";
		String smsID = "", mobile = "";
		if (smsModel.getMobile4() != null
				&& (smsModel.getMobile4().length() == 11 || smsModel.getMobile4().length() == 12)) {
			mobile = smsModel.getMobile4().trim();
		} else if (smsModel.getMobile() != null
				&& (smsModel.getMobile().length() == 11 || smsModel.getMobile().length() == 12)) {
			mobile = smsModel.getMobile().trim();
		} else if (smsModel.getMobile2() != null
				&& (smsModel.getMobile2().length() == 11 || smsModel.getMobile2().length() == 12)) {
			mobile = smsModel.getMobile2().trim();
		} else if (smsModel.getMobile3() != null
				&& (smsModel.getMobile3().length() == 11 || smsModel.getMobile3().length() == 12)) {
			mobile = smsModel.getMobile3().trim();
		}

		mobile = "01115424896";// 01112738279//01553510869//01011434199
		if (mobile.length() == 11)
			mobile = "2" + mobile;
		smsID = SMSUtils.sendMessage(msg, mobile);
		// here must update now;
		if (smsID != null && !smsID.isEmpty()) {
			smsModel.setSmsID(smsID);
			int res = SMSDao.updateCbe6Months(smsModel, mobile, msg, "Done");
			System.out.println(res);
		} else {
			smsModel.setSmsID(smsID);
			int res = SMSDao.updateCbe6Months(smsModel, mobile, msg, "wrong number");
			System.out.println(res);
		}
		return smsID;
	}

//	private static void initiateMessages(List<SMSModel> sms, Account account, String msgType) {
//		for (SMSModel smsModel : sms) {
//			try {
//				String smsMessage = SMSUtils.prepareMessage(msgType, smsModel);
//				// LogUtil.info(smsMessage);
//				String smsID = "";
//				String mobile = account.getMobile();
//				if (mobile.contains(",")) {
//					String[] arr = mobile.split(",");
//					for (int i = 0; i < arr.length; i++) {
//						smsID = SMSUtils.sendMessage(smsMessage, arr[i].trim());
//						if (smsID != null) {
//							smsModel.setSmsID(smsID);
//							int res = SMSDao.insertToSMSHistory(smsModel, account.getCustomerName(), smsMessage,
//									arr[i].trim());
//							if (res == 1)
//								LogUtil.info("History Added Successfully.");
//						}
//					}
//				} else {
//					smsID = SMSUtils.sendMessage(smsMessage, mobile.trim());
//					smsModel.setSmsID(smsID);
//					if (smsID != null) {
//						int res = SMSDao.insertToSMSHistory(smsModel, account.getCustomerName(), smsMessage,
//								mobile.trim());
//						if (res == 1)
//							LogUtil.info("History Added Successfully.");
//					}
//				}
////				String smsID = SMSUtils.sendMessage(smsMessage, account.getMobile());
////				smsID = SMSUtils.sendMessage(smsMessage, account.getMobile());
////				smsModel.setSmsID(smsID);
//				if (!smsID.isEmpty() && smsID != "") {
//					LogUtil.info("SMS Sent Successfully");
//					switch (msgType) {
//					case "Deposit":
//						SMSDao.updateCustomerTransaction(smsModel);
//						break;
//					case "Cheques collected":
//						SMSDao.updateCollectedChequesTransaction(smsModel);
//						break;
//					case "Cheques returned":
//						SMSDao.updateReturnedChequesTransaction(smsModel);
//						break;
//					}
//					LogUtil.info("Transaction Flagged As sent in DB.");
//
//				} else
//					LogUtil.info("SMS dose not Sent");
//
//			} catch (Exception e) {
//				LogUtil.error(e.getMessage());
//			}
//		}
//	}
}
