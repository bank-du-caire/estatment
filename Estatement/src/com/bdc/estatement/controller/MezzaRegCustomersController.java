package com.bdc.estatement.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import com.bdc.estatement.dao.TransactionsDao;
import com.bdc.estatement.model.Account;
import com.bdc.estatement.model.MeezaTransactions;
import com.bdc.estatement.util.LogUtil;
import com.bdc.estatement.util.MeezaPdfTemplate;
import com.bdc.estatement.util.Utils;

public class MezzaRegCustomersController {

	/*
	 * 
	 * 1- get all data from transaction table mezza reg customers and insert it into
	 * mezza object (you will create it as accounts class) 2- loop on this mezza obj
	 * list and get transactions from Ahmed salah 3- generate PDF and Excel same as
	 * below E-statment and save those 2 files on below path
	 * D://BDC_APP//MezzaRegCustomers//Attachements// 4- prepare the mail and send
	 * it to the customer
	 *
	 */

	public static void main(String[] args) throws ParseException {

//		Utils.sendMail("Nil transactions for the accounting period", "Meeza Estatement",
//				"shady.ashour@bdc.com.eg");

		LogUtil.initialize();

		String sendDate = "";
		Date d = new Date();
		sendDate = new SimpleDateFormat("dd/MM/YYYY").format(d);
		List<Integer> meezaCustomerIds = TransactionsDao.getMezaCustomerIds();
		for (Integer customerId : meezaCustomerIds) {
			List<MeezaTransactions> meezaTransactionList = TransactionsDao.getAllMeezaTransactions(customerId);
			System.out.println("meezaTransactionList size************************* = " + meezaTransactionList.size());
			boolean existingCustomer = TransactionsDao.checkExistingMeezaCust(customerId);
			if (existingCustomer) {
				if (!meezaTransactionList.isEmpty()) {
					boolean isGenerateExcel = false;
					isGenerateExcel = Utils.exportToExcelForMeeza(meezaTransactionList, customerId.toString());
					boolean isGeneratePDF = false;
					MeezaPdfTemplate pdfTest = new MeezaPdfTemplate(meezaTransactionList);
					try {
						if (pdfTest.openPdf(String.valueOf(customerId))) {
							pdfTest.generatePdf(0);
							pdfTest.closePdf();
							isGeneratePDF = true;
							System.out.println("isGeneratePDF = " + isGeneratePDF);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					List<String> emailsList = TransactionsDao.getCustomerEmailsById(customerId);
					for (String email : emailsList)
						if (isGenerateExcel && isGeneratePDF) {

							Utils.sendingMeezaMail(email, customerId.toString(), Integer.toString(customerId),
									sendDate);
						}
					new SimpleDateFormat("yyyy-MM-dd").format(new Date());
				} else {
					List<String> emailsList = TransactionsDao.getCustomerEmailsById(customerId);
					for (String email : emailsList) {
						// Utils.sendingMeezaMail(email, customerId.toString(),
						// Integer.toString(customerId), sendDate);

						if (email.contains(";")) {
							String[] emails = email.split(";");
							for (String mal : emails) {
								if (!mal.isEmpty()) {
									Utils.sendMail("Nil transactions for the accounting period", "Meeza Estatement",
											mal.trim());
								}
							}
						} else {
							Utils.sendMail("Nil transactions for the accounting period", "Meeza Estatement",
									email.trim());
						}

					}
				}
			}
			System.out.println("Done");
		}
	}

	private static void sendWelcomeMail(Account account) {
		StringBuilder message = new StringBuilder();
		message.append("Welcome on board to Banque Du Caire e-Statement notification service.");
		message.append("<p>");
		message.append("For your own security all our generated e-statement emails will be password protected.");
		message.append("<p>");
		message.append("kindly use your Company  ID number as your password to open all future attachments ");
		message.append("<p>");
		message.append(
				"For any assistance regarding your ID no, kindly contact Corporate Service Division , e-mail address:   CorporateServiceDivision@bdc.com.eg”");
		boolean res = Utils.sendMail("eStatement.BDC", "Password1", account.getEmail(), message.toString(),
				"E-Statement notification service");
		if (res) {
			account.setWelcomeMail(1);
			TransactionsDao.updateAccountWelcomeMail(account);
		}
	}

}
