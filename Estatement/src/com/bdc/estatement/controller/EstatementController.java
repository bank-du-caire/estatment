package com.bdc.estatement.controller;

import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
/////////////////////
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.mail.BodyPart;
import javax.mail.internet.MimeBodyPart;

import com.bdc.estatement.dao.TransactionsDao;
import com.bdc.estatement.model.Account;
import com.bdc.estatement.model.AccountPackage;
import com.bdc.estatement.model.Balance;
import com.bdc.estatement.model.EndOffDay;
import com.bdc.estatement.model.SwiftMsg;
import com.bdc.estatement.model.Transaction;
import com.bdc.estatement.util.Defines;
import com.bdc.estatement.util.EstatmentPdfTemplate;
import com.bdc.estatement.util.LogUtil;
import com.bdc.estatement.util.Utils;

public class EstatementController {

	private static void sendWelcomeMail(Account account) {
		StringBuilder message = new StringBuilder();
		message.append("Welcome on board to Banque Du Caire e-Statement notification service.");
		message.append("<p>");
		message.append("For your own security all our generated e-statement emails will be password protected.");
		message.append("<p>");
		message.append("kindly use your Company  ID number as your password to open all future attachments ");
		message.append("<p>");
		message.append(
				"For any assistance regarding your ID no, kindly contact Corporate Service Division , e-mail address:   CorporateServiceDivision@bdc.com.eg”");
//		boolean res = Utils.sendWelcomeMail(account.getEmail(), message.toString(), "E-statement");
		boolean res = Utils.sendMail("eStatement.BDC", "Password1", account.getEmail(), message.toString(),
				"E-Statement notification service");
		if (res) {
			account.setWelcomeMail(1);
			TransactionsDao.updateAccountWelcomeMail(account);
		}
	}

	private static void sendMail() {
		String to = "shady.ashour@bdc.com.eg";

		// Sender's email ID needs to be mentioned
		String from = "eStatement.BDC@bdc.com.eg";
//		final String username = "eStatement.BDC";// change accordingly
//		final String password = "Password1";// change accordingly
		final String username = "shady.ashour";// change accordingly
		final String password = "Isa0109255906";// change accordingly

		// Assuming you are sending email through relay.jangosmtp.net
//		String host = "https://mail.bdc.com.eg/EWS/Exchange.asmx";
		String host = "mail.bdc.com.eg";

		Properties props = new Properties();
//		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", host);
//		props.put("mail.smtp.port", "25");
		props.put("mail.smtp.port", "25");

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {

			// Create a default MimeMessage object.
			Message message = new MimeMessage(session);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress(from));

			// Set To: header field of the header.
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));

			// Set Subject: header field
			message.setSubject("Testing Subject");

			// This mail has 2 part, the BODY and the embedded image
			MimeMultipart multipart = new MimeMultipart("related");

			// first part (the html)
			BodyPart messageBodyPart = new MimeBodyPart();
			String htmlText = "<H1>Hello</H1><img src=\"cid:image\">";
			messageBodyPart.setContent(htmlText, "text/html");
			// add it
			multipart.addBodyPart(messageBodyPart);

			// second part (the image)
			messageBodyPart = new MimeBodyPart();
			DataSource fds = new FileDataSource("D:\\BDC_APP\\EStatement\\resources\\bdc-logo.jpg");

			messageBodyPart.setDataHandler(new DataHandler(fds));
			messageBodyPart.setHeader("Content-ID", "<image>");

			// add image to the multipart
			multipart.addBodyPart(messageBodyPart);

			// put everything together
			message.setContent(multipart);
			// Send message
			Transport.send(message);

			System.out.println("Sent message successfully....");

		} catch (MessagingException e) {
			System.out.println(e);
			throw new RuntimeException(e);

		}
	}

	public static void main(String[] args) throws ParseException {
		// sendMail();
		// TransactionsDao.insertEndOfDay();/
		LogUtil.initialize();
		long startTime = System.nanoTime();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

//		Calendar xmas1 = new GregorianCalendar(2019, Calendar.DECEMBER, 27);
//		Date today = xmas1.getTime();
		Date today = Calendar.getInstance().getTime();
//		EndOffDay endOffDay = TransactionsDao.getEndOffDayDateProcess();
//		if (endOffDay != null && endOffDay.getDateProcess() != null
//				&& (simpleDateFormat.format(endOffDay.getDateProcess()).equals(simpleDateFormat.format(today))))
//		{
//		LogUtil.info("End off Day Finished :)");
		List<Account> accounts = TransactionsDao.getAccountsByToday();
//		List<Account> accounts = Utils.syncFlexCubeChanges(2);

		String fromDate = "";
		String fromDateSQLFormat = "";
		String toDate = "";
		String toDateSQLFormat = "";
		String pdfPath = "";
		String excelPath = "";
		String swiftPath = "";
		for (Account account : accounts) {
			System.out.println("account.getAccountNumber()::::" + account.getAccountNumber());
			Date nextSend = account.getNextDate();
			// here i need to add the values off friday.
			EndOffDay endOffDay = TransactionsDao.getEndOffDayDateProcess();
//			Date lastProcessDate = Utils.addDays(endOffDay.getDateLastProcess(), 1);
			Date lastProcessDate = Utils.addingDays(endOffDay.getDateLastProcess(), 1);
			long diffInMillies = Math
					.abs(endOffDay.getDateProcess().getTime() - endOffDay.getDateLastProcess().getTime());
			long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);

			if (endOffDay != null && endOffDay.getDateProcess() != null
					&& (simpleDateFormat.format(endOffDay.getDateProcess()).equals(simpleDateFormat.format(today)))
					|| diff > 1
//					(simpleDateFormat.format(lastProcessDate)
//							.equals(simpleDateFormat.format(account.getNextDate())))
			) {
				if (account.getFrequancyStmt() == Defines.None) {
					continue;
				} else if (account.getFrequancyStmt() == Defines.Daily) {
					fromDate = Utils.claculateFromDate(account.getFrequancyStmt(),
							new SimpleDateFormat("yyyy-MM-dd").format(account.getNextDate()));
					fromDateSQLFormat = Utils.claculateFromDateSQLDormat(account.getFrequancyStmt(),
							new SimpleDateFormat("yyyy-MM-dd").format(account.getNextDate()));
					toDate = new SimpleDateFormat("dd/MM/YYYY").format(account.getNextDate());
					toDateSQLFormat = new SimpleDateFormat("yyyy-MM-dd").format(account.getNextDate());

					// here if manual e-statement

					Calendar xmas = new GregorianCalendar(2021, Calendar.AUGUST, 12);
					Date d = xmas.getTime();
					fromDate = new SimpleDateFormat("dd/MM/YYYY").format(d);
					fromDateSQLFormat = new SimpleDateFormat("yyyy-MM-dd").format(d);
					xmas = new GregorianCalendar(2021, Calendar.AUGUST, 13);
					d = xmas.getTime();
					toDate = new SimpleDateFormat("dd/MM/YYYY").format(d);
					toDateSQLFormat = new SimpleDateFormat("yyyy-MM-dd").format(d);

				} else {
					fromDate = Utils.claculateFromDate(account.getFrequancyStmt(),
							new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
					fromDateSQLFormat = Utils.claculateFromDateSQLDormat(account.getFrequancyStmt(),
							new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
					toDate = new SimpleDateFormat("dd/MM/YYYY").format(new Date());
					toDateSQLFormat = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

					// here if manual e-statement
					Calendar xmas = new GregorianCalendar(2021, Calendar.JULY, 10);
					Date d = xmas.getTime();
					fromDate = new SimpleDateFormat("dd/MM/YYYY").format(d);
					fromDateSQLFormat = new SimpleDateFormat("yyyy-MM-dd").format(d);
					xmas = new GregorianCalendar(2021, Calendar.AUGUST, 10);
					d = xmas.getTime();
					toDate = new SimpleDateFormat("dd/MM/YYYY").format(d);
					toDateSQLFormat = new SimpleDateFormat("yyyy-MM-dd").format(d);
				}

				// sending Password for first time in sms//////
				if (account.getWelcomeMail() == 0)
					sendWelcomeMail(account);
				//////////////////////////////////////////////

				account.setFromDate(fromDate);
				account.setToDate(toDate);
				AccountPackage accountPackage = TransactionsDao.getAccountsPackage(account.getAccountNumber());
				double closingBalance = TransactionsDao.getClosingBalance(account.getAccountNumber(),
						account.getNextDate());

				Balance accountBalance = TransactionsDao.getOpeningBalace(account, fromDateSQLFormat, toDateSQLFormat);

				List<Transaction> transactions = TransactionsDao.getTransacationPackage(account.getAccountNumber(),
						fromDateSQLFormat, toDateSQLFormat, accountBalance.getStartBalance());
//			List<Transaction> transactions = TransactionsDao.getTransacationPackage(account.getAccountNumber(),
//					"2015-01-01", "2019-07-07", accountBalance.getStartBalance());
				long depitCount = transactions.stream().filter(t -> t.getIndicator().equalsIgnoreCase("d")).count();
				long creditCount = transactions.stream().filter(t -> t.getIndicator().equalsIgnoreCase("c")).count();

				double totalDepit = transactions.stream().filter(t -> t.getIndicator().equalsIgnoreCase("d"))
						.map(t -> t.getDebit()).collect(Collectors.summingDouble(Double::doubleValue));

				double totalCredit = transactions.stream().filter(t -> t.getIndicator().equalsIgnoreCase("c"))
						.map(t -> t.getCredit()).collect(Collectors.summingDouble(Double::doubleValue));

				double balAvailable = accountPackage.getAvailableBalance();
				System.out.println("getAvailableBalance: " + balAvailable);

				accountPackage.setAvailableBalance(closingBalance);
				accountPackage.setFromDate(fromDate);
				accountPackage.setToDate(toDate);
				accountPackage.setLastSentDate(account.getLastSentDate());
				accountPackage.setNextDate(account.getNextDate());
				accountPackage.setStartBalance(accountBalance.getStartBalance());
				accountPackage.setEndBalance(accountBalance.getEndBalance());
				accountPackage.setCreditCount(creditCount);
				accountPackage.setDebitCount(depitCount);
				accountPackage.setTotalCredit(totalCredit);
				accountPackage.setTotalDebit(totalDepit);
				List<SwiftMsg> msg_list = TransactionsDao.getSwiftPackage(account.getAccountNumber(), fromDateSQLFormat,
						toDateSQLFormat);
				// List<SwiftMsg> msg_list = TransactionsDao.getSwiftPackage(
				// "00345700000017", "2019-01-20", "2019-01-21");
				if (msg_list.size() != 0) {
//				Utils.exportToTextFile(msg_list, account.getCustomerCode());
					Utils.exportToTextFile(msg_list, account.getCustomerCode(), account.getAccountNumber());
				}
				boolean isGenerateExcel = Utils.exportToExcel(accountPackage, transactions, fromDateSQLFormat,
						toDateSQLFormat);

				boolean isGeneratePDF = false;
				EstatmentPdfTemplate pdfTest = new EstatmentPdfTemplate(accountPackage, transactions);
				try {
					if (pdfTest.openPdf()) {
						pdfTest.generatePdf(account.getFrequancyStmt());
						pdfTest.closePdf();
						isGeneratePDF = true;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

				boolean isSendMail = false;
				if (isGenerateExcel && isGeneratePDF) {
					isSendMail = Utils.sendingMail(account, fromDate);
				}
				account.setLastSentDate(new Date());
				String nextSendDate = Utils.claculateNextSendDate(account.getFrequancyStmt(),
						new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
				if (!nextSendDate.isEmpty()) {
					account.setNextDate(new SimpleDateFormat("dd/MM/yyyy").parse(nextSendDate));
					if (isSendMail) {
						if (diff > 1
//								simpleDateFormat.format(lastProcessDate).equals(simpleDateFormat.format(nextSend))
						) {
							long diffInMillies_ = Math.abs(endOffDay.getDateNextProcess().getTime() - today.getTime());
							long diffNextAndToday = TimeUnit.DAYS.convert(diffInMillies_, TimeUnit.MILLISECONDS);
							nextSend = Utils.addDays(nextSend, 1);
							if (diffNextAndToday > 1) {
//									nextSend = Utils.addDays(endOffDay.getDateNextProcess(), 1);
								account.setNextDate(endOffDay.getDateNextProcess());
								// TransactionsDao.updateAccount(account, Defines.APPROVED_STATUS, 1);
							}
						}
//						long diffInMillies = Math.abs(endOffDay.getDateNextProcess().getTime() - nextSend.getTime());
//						long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
//						nextSend = Utils.addDays(nextSend, 1);
//						if (diff > 1) {
//							nextSend = Utils.addDays(endOffDay.getDateNextProcess(), 1);
//							account.setNextDate(nextSend);
//							TransactionsDao.updateAccount(account, Defines.APPROVED_STATUS, 1);
//						}

						TransactionsDao.updateAccountDates(account);
						pdfPath = "D://BDC_APP//EStatement//Attachements//" + account.getAccountNumber() + " - "
								+ new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + ".pdf";
						excelPath = "D://BDC_APP//EStatement//Attachements//" + account.getAccountNumber() + " - "
								+ new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + ".xls";
						swiftPath = "D://BDC_APP//EStatement//Attachements//" + account.getAccountNumber() + " - "
								+ new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + ".zip";
						TransactionsDao.addToEstatementHistory(account, pdfPath, excelPath, swiftPath,
								fromDateSQLFormat, toDateSQLFormat);
					}
				}
			}
		}
		LogUtil.info("Number of Unsend Statenments: " + checkUnsendStatements());
//		}
//		else {
//			LogUtil.info("End off Day Not Finished :(");
//		}

		long endTime = System.nanoTime();
		long timeElapsed = endTime - startTime;
		System.out.println("Execution time in milliseconds : " + timeElapsed / 1000000);
		System.out.println("Execution time in Seconds : " + timeElapsed / 1000000000);
		LogUtil.info("Execution time in Seconds : " + (timeElapsed / 1000000000));
	}

	private static int checkUnsendStatements() {
		List<Account> allUnsendedAccounts = TransactionsDao.getUnsendStatements();
		String messageBody = "Dears\n\r" + "Below Accounts did not send Statements on time: \n\r";
		int count = 1;
		for (Account account : allUnsendedAccounts) {
			System.out.println(account);
			messageBody += account.getAccountNumber() + "\n\r";

			if (count++ != allUnsendedAccounts.size())
				messageBody += " - ";
		}
		if (allUnsendedAccounts.size() != 0) {
			boolean res = Utils.sendMail("Developers@bdc.com.eg;", messageBody);
			if (res) {
				for (Account account : allUnsendedAccounts) {
					TransactionsDao.updateAccountFauilerFlag(account.getAccountNumber());
				}
			}
		}
		return allUnsendedAccounts.size();
	}

}
