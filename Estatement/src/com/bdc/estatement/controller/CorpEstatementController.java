package com.bdc.estatement.controller;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.stream.Collectors;

import com.bdc.estatement.connection.ConnectionManager;
import com.bdc.estatement.dao.TransactionsDao;
import com.bdc.estatement.model.Account;
import com.bdc.estatement.model.AccountPackage;
import com.bdc.estatement.model.Balance;
import com.bdc.estatement.model.Transaction;
import com.bdc.estatement.util.CorpEstatmentPdfTemplate;
import com.bdc.estatement.util.Defines;
import com.bdc.estatement.util.LogUtil;
import com.bdc.estatement.util.Utils;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

public class CorpEstatementController {
	private static void testAix() {
		JSch jsch = new JSch();
		Session session;
		try {
			session = jsch.getSession("remote_user_name", "remote_host_or_ip", 22); // 22 for SFTP
			session.setPassword("remote_password");

			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);

			session.connect(10000);
			Channel channel = session.openChannel("sftp");
			channel.connect();

			System.out.println("Connection Opened");
			ChannelSftp channelSftp = (ChannelSftp) channel;
			// InputStream inputStream = new FileInputStream("text_file.txt");
//		channelSftp.put(inputStream, "/remote/folder/file_to_be_rewritten.txt");

			System.out.println("File should be uploaded");

			channelSftp.disconnect();
			session.disconnect();
		} catch (JSchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws ParseException {

		LogUtil.initialize();
		int stat = TransactionsDao.insertCorporateIBAcct();
		LogUtil.info("number of inserted rows are :" + stat);
//		String dateString = "to_date('01-01-2021','dd-mm-yyyy')";
		String dateString = "trunc(sysdate)";
		List<Account> accounts = TransactionsDao.getCorpAccounts(dateString);

		String fromDate = "";
		String fromDateSQLFormat = "";
		String toDate = "";
		String toDateSQLFormat = "";
		String pdfPath = "";
		Date today = new Date();
		for (Account account : accounts) {

//			Calendar c = Calendar.getInstance();
//			c.setTime(account.getSmsActivationDate());
//			System.out.println(c);
//
//			String MainpdfPath = createFolder(account.getCustomerCode(), c.get(Calendar.YEAR),
//					account.getFrequancyStmt());
			Calendar cal = Calendar.getInstance();
			String statementMonth = "";
			if (account.getFrequancyStmt() == Defines.Monthly) {
				if (account.getNextDate() == null) {
//					Date today = new Date();
					cal.setTime(today);
					int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);

					if (dayOfMonth == 1) {
						cal.add(Calendar.MONTH, -1);
						fromDate = new SimpleDateFormat("dd/MM/YYYY").format(cal.getTime());
						fromDateSQLFormat = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
						toDate = new SimpleDateFormat("dd/MM/YYYY").format(today);
						toDateSQLFormat = new SimpleDateFormat("yyyy-MM-dd").format(today);
					} else {
//						cal.add(Calendar.MONTH, -1);
//						int month = cal.get(Calendar.MONTH);
//						int year = cal.get(Calendar.YEAR);
//						cal.set(year, month, 1);
//						toDate = new SimpleDateFormat("dd/MM/YYYY").format(cal.getTime());
//						toDateSQLFormat = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
//						cal.add(Calendar.MONTH, -1);
//						month = cal.get(Calendar.MONTH);
//						year = cal.get(Calendar.YEAR);
//						cal.set(year, month, 1);
//						fromDate = new SimpleDateFormat("dd/MM/YYYY").format(cal.getTime());
//						fromDateSQLFormat = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());

						cal.add(Calendar.MONTH, -1);
						int month = cal.get(Calendar.MONTH);
						int year = cal.get(Calendar.YEAR);
						cal.set(year, month, 1);

						// here manual
//						cal.set(2021, 4, 1);

						fromDate = new SimpleDateFormat("dd/MM/YYYY").format(cal.getTime());
						fromDateSQLFormat = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());

						cal.add(Calendar.MONTH, 1);
						month = cal.get(Calendar.MONTH);
						year = cal.get(Calendar.YEAR);
						cal.set(year, month, 1);

						// here manual
//						cal.set(2021, 5, 1);

						toDate = new SimpleDateFormat("dd/MM/YYYY").format(cal.getTime());
						toDateSQLFormat = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
					}
				} else {
					fromDate = Utils.claculateFromDate(account.getFrequancyStmt(),
							new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
					fromDateSQLFormat = Utils.claculateFromDateSQLDormat(account.getFrequancyStmt(),
							new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
					toDate = new SimpleDateFormat("dd/MM/YYYY").format(new Date());
					toDateSQLFormat = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
				}

			}

			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			Date d = formatter.parse(fromDate);
			cal.setTime(d);
			// testing shady
//			String mainpdfPath = createFolder(account.getAccountNumber(), account.getCustomerCode(),
//					new SimpleDateFormat("MMM").format(d), cal.get(Calendar.YEAR));

			String x = new SimpleDateFormat("MMM").format(d);
			System.out.println("Month: " + x);
			String mainpdfPath = createFolder(account.getAccountNumber(), account.getCustomerCode(),
					new SimpleDateFormat("MMM").format(d), cal.get(Calendar.YEAR));

			System.out.println(mainpdfPath);

			account.setFromDate(fromDate);
			account.setToDate(toDate);
			AccountPackage accountPackage = TransactionsDao.getCorpAccountsPackage(account.getAccountNumber());

			Balance accountBalance = TransactionsDao.getOpeningBalace(account, fromDateSQLFormat, toDateSQLFormat);

			List<Transaction> transactions = TransactionsDao.getTransacationPackage(account.getAccountNumber(),
					fromDateSQLFormat, toDateSQLFormat, accountBalance.getStartBalance());
			long depitCount = transactions.stream().filter(t -> t.getIndicator().equalsIgnoreCase("d")).count();
			long creditCount = transactions.stream().filter(t -> t.getIndicator().equalsIgnoreCase("c")).count();

			double totalDepit = transactions.stream().filter(t -> t.getIndicator().equalsIgnoreCase("d"))
					.map(t -> t.getDebit()).collect(Collectors.summingDouble(Double::doubleValue));

			double totalCredit = transactions.stream().filter(t -> t.getIndicator().equalsIgnoreCase("c"))
					.map(t -> t.getCredit()).collect(Collectors.summingDouble(Double::doubleValue));
			if (accountPackage == null)
				accountPackage = new AccountPackage();
			accountPackage.setFromDate(fromDate);
			accountPackage.setToDate(toDate);
			accountPackage.setLastSentDate(account.getLastSentDate());
			accountPackage.setNextDate(account.getNextDate());
			accountPackage.setStartBalance(accountBalance.getStartBalance());
			accountPackage.setEndBalance(accountBalance.getEndBalance());
			accountPackage.setCreditCount(creditCount);
			accountPackage.setDebitCount(depitCount);
			accountPackage.setTotalCredit(totalCredit);
			accountPackage.setTotalDebit(totalDepit);

			boolean isGeneratePDF = false;
			CorpEstatmentPdfTemplate pdfTest = new CorpEstatmentPdfTemplate(accountPackage, transactions);
			try {
				// for production
				if (pdfTest.openPdf(mainpdfPath, account.getAccountNumber())) {
					pdfTest.generatePdf();
					pdfTest.closePdf();
					isGeneratePDF = true;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			account.setLastSentDate(new Date());
			Calendar c = Calendar.getInstance();
			SimpleDateFormat formatt = new SimpleDateFormat("dd/MM/yyyy");
			Date date = formatt.parse(toDate);
			c.setTime(date);
			Calendar call = Calendar.getInstance();
			call.setTime(today);
			int dayOfMonth = call.get(Calendar.DAY_OF_MONTH);
//			if (dayOfMonth > 1) {
//				c.add(Calendar.MONTH, 2);
//			} else {
//				c.add(Calendar.MONTH, 1);
//			}
			c.add(Calendar.MONTH, 1);
			String nextSendDate = new SimpleDateFormat("dd/MM/yyyy").format(c.getTime());
			System.out.println(nextSendDate);
			if (!nextSendDate.isEmpty()) {
				account.setNextDate(new SimpleDateFormat("dd/MM/yyyy").parse(nextSendDate));

				TransactionsDao.updateCorpAccountDates(account);
				// D://BDC_APP//EStatement//Attachements
				pdfPath = mainpdfPath + "/Statement.pdf";
				System.out.println("pdfPath:: " + pdfPath);
			}
		}
		System.out.println("Done");

	}

//	private static String createFolder(String accountNum, String codeCustomer, String month, int year) {
////		File fileStructure = new File("\\\\172.17.85.75/statements/" + accountNum.trim());
//		File fileStructure = new File("E:\\\\statements/" + codeCustomer);
//		boolean folder = false;
//		if (!fileStructure.exists()) {
//			folder = fileStructure.mkdir();
//		}
//		
//		return "E:\\\\statements/" + codeCustomer;
//
//	}

//	private static String createFolder(String accountNum, String codeCustomer, String month, int year) {
////		File fileStructure = new File("\\\\172.17.85.75/statements/" + accountNum.trim());
//		///usr/IBM/WebSphere/InternetBanking/Statements/ 
//		File fileStructure = new File("/statements/" + codeCustomer);
//		boolean folder = false;
//		if (!fileStructure.exists()) {
//			folder = fileStructure.mkdir();
//		}
//		fileStructure = new File("/statements/" + codeCustomer + "/C");
//		if (!fileStructure.exists()) {
//			folder = fileStructure.mkdir();
//		}
//		fileStructure = new File("/statements/" + codeCustomer + "/C/" + accountNum.trim());
//		if (!fileStructure.exists()) {
//			folder = fileStructure.mkdir();
//		}
//		fileStructure = new File("/statements/" + codeCustomer + "/C/" + accountNum.trim() + "/" + month + "-" + year);
//		if (!fileStructure.exists()) {
//			folder = fileStructure.mkdir();
//		}
//
//		return "/statements/" + codeCustomer + "/C/" + accountNum.trim() + "/" + month + "-" + year;
//
//	}

	private static String createFolder(String accountNum, String codeCustomer, String month, int year) {
		if (ConnectionManager.bdcUrl.contains("85.199")) {
			boolean folder = false;
			File fileStructure = new File("/usr/IBM/WebSphere/InternetBanking/Statements");

			if (!fileStructure.exists()) {
				folder = fileStructure.mkdir();
			}
			fileStructure = new File("/usr/IBM/WebSphere/InternetBanking/Statements/" + codeCustomer);
			if (!fileStructure.exists()) {
				folder = fileStructure.mkdir();
			}
			fileStructure = new File("/usr/IBM/WebSphere/InternetBanking/Statements/" + codeCustomer + "/C");
			if (!fileStructure.exists()) {
				folder = fileStructure.mkdir();
			}
			fileStructure = new File(
					"/usr/IBM/WebSphere/InternetBanking/Statements/" + codeCustomer + "/C/" + accountNum.trim());
			if (!fileStructure.exists()) {
				folder = fileStructure.mkdir();
			}
			fileStructure = new File("/usr/IBM/WebSphere/InternetBanking/Statements/" + codeCustomer + "/C/"
					+ accountNum.trim() + "/" + month + "-" + year);
			if (!fileStructure.exists()) {
				folder = fileStructure.mkdir();
			}
			return "/usr/IBM/WebSphere/InternetBanking/Statements/" + codeCustomer + "/C/" + accountNum.trim() + "/"
					+ month + "-" + year;
		} else {
			File fileStructure = new File("/statements/" + codeCustomer);
			boolean folder = false;
			if (!fileStructure.exists()) {
				folder = fileStructure.mkdir();
			}
			fileStructure = new File("/statements/" + codeCustomer + "/C");
			if (!fileStructure.exists()) {
				folder = fileStructure.mkdir();
			}
			fileStructure = new File("/statements/" + codeCustomer + "/C/" + accountNum.trim());
			if (!fileStructure.exists()) {
				folder = fileStructure.mkdir();
			}
			fileStructure = new File(
					"/statements/" + codeCustomer + "/C/" + accountNum.trim() + "/" + month + "-" + year);
			if (!fileStructure.exists()) {
				folder = fileStructure.mkdir();
			}
//			return "D:\\Statements";
			return "/statements/" + codeCustomer + "/C/" + accountNum.trim() + "/" + month + "-" + year;
		}
	}

	private static String createFolder(String CustomerID, int Year, int Freq) {
		// Create a File Object and pass directory/folder name
		// as a string to it.
		String path = "";
		File fileStructure = new File("E:\\" + CustomerID);
		// File object has a method called as exists() which
		// Tests whether the file or directory denoted by
		// this abstract pathname exists.
		if (!fileStructure.exists()) {
			// File object has a method called as mkdir() which
			// Creates the directory named by this abstract pathname.
			if (fileStructure.mkdir()) {
				System.out.println("New Folder has been created .... ");
				if (Year != 0) {
					File subFiles = new File("E:\\" + CustomerID + "\\" + Year);
					subFiles.mkdirs();
					if (Freq != 0) {
						File ssubFiles = null;
						if (Freq == 1) {
							ssubFiles = new File("E:\\" + CustomerID + "\\" + Year + "\\Daily");
							ssubFiles.mkdirs();
							path = "E:\\" + CustomerID + "\\" + Year + "\\Daily";
						} else if (Freq == 2) {
							ssubFiles = new File("E:\\" + CustomerID + "\\" + Year + "\\Weekly");
							ssubFiles.mkdirs();
							path = "E:\\" + CustomerID + "\\" + Year + "\\Weekly";
						} else if (Freq == 3) {
							ssubFiles = new File("E:\\" + CustomerID + "\\" + Year + "\\FortNightly");
							ssubFiles.mkdirs();
							path = "E:\\" + CustomerID + "\\" + Year + "\\FortNightly";
						} else if (Freq == 4) {
							ssubFiles = new File("E:\\" + CustomerID + "\\" + Year + "\\Monthly");
							ssubFiles.mkdirs();
							path = "E:\\" + CustomerID + "\\" + Year + "\\Monthly";
						} else if (Freq == 5) {
							ssubFiles = new File("E:\\" + CustomerID + "\\" + Year + "\\Bi_Monthly");
							ssubFiles.mkdirs();
							path = "E:\\" + CustomerID + "\\" + Year + "\\Bi_Monthly";
						} else if (Freq == 6) {
							ssubFiles = new File("E:\\" + CustomerID + "\\" + Year + "\\Quarterly");
							ssubFiles.mkdirs();
							path = "E:\\" + CustomerID + "\\" + Year + "\\Quarterly";
						} else if (Freq == 7) {
							ssubFiles = new File("E:\\" + CustomerID + "\\" + Year + "\\Half_Yearly");
							ssubFiles.mkdirs();
							path = "E:\\" + CustomerID + "\\" + Year + "\\Half_Yearly";
						} else if (Freq == 8) {
							ssubFiles = new File("E:\\" + CustomerID + "\\" + Year + "\\Yearly");
							ssubFiles.mkdirs();
							path = "E:\\" + CustomerID + "\\" + Year + "\\Yearly";
						}
						return path;
					}
				}
			} else {

				System.out.println("Oops!!! Something blown up file creation...");
			}
		} else {
			System.out.println("File already exists !!! ...");
			File years = new File("E:\\" + CustomerID + "\\" + Year);
			if (years.exists()) {
				System.out.println("This Year File already exists !!! ..." + Year);
				File ssubFiles = null;
				if (Freq == 1) {

					File Freqs = new File("E:\\" + CustomerID + "\\" + Year + "\\Daily");
					if (Freqs.exists()) {
						System.out.println("This Frequency File already exists !!! ..." + Freq);
						path = "E:\\" + CustomerID + "\\" + Year + "\\Daily";
					} else {
						Freqs = new File("E:\\" + CustomerID + "\\" + Year + "\\Daily");
						Freqs.mkdirs();
						path = "E:\\" + CustomerID + "\\" + Year + "\\Daily";
					}
				} else if (Freq == 2) {
					File Freqs = new File("E:\\" + CustomerID + "\\" + Year + "\\Weekly");
					if (Freqs.exists()) {
						System.out.println("This Frequency File already exists !!! ..." + Freq);
						path = "E:\\" + CustomerID + "\\" + Year + "\\Weekly";
					} else {
						Freqs = new File("E:\\" + CustomerID + "\\" + Year + "\\Weekly");
						Freqs.mkdirs();
						path = "E:\\" + CustomerID + "\\" + Year + "\\Weekly";
					}
				} else if (Freq == 3) {
					File Freqs = new File("E:\\" + CustomerID + "\\" + Year + "\\FortNightly");
					if (Freqs.exists()) {
						System.out.println("This Frequency File already exists !!! ..." + Freq);
						path = "E:\\" + CustomerID + "\\" + Year + "\\FortNightly";
					} else {
						Freqs = new File("E:\\" + CustomerID + "\\" + Year + "\\FortNightly");
						Freqs.mkdirs();
						path = "E:\\" + CustomerID + "\\" + Year + "\\FortNightly";
					}
				} else if (Freq == 4) {
					File Freqs = new File("E:\\" + CustomerID + "\\" + Year + "\\Monthly");
					if (Freqs.exists()) {
						System.out.println("This Frequency File already exists !!! ..." + Freq);
						path = "E:\\" + CustomerID + "\\" + Year + "\\Monthly";
					} else {
						Freqs = new File("E:\\" + CustomerID + "\\" + Year + "\\Monthly");
						Freqs.mkdirs();
						path = "E:\\" + CustomerID + "\\" + Year + "\\Monthly";
					}
				} else if (Freq == 5) {
					File Freqs = new File("E:\\" + CustomerID + "\\" + Year + "\\Bi_Monthly");
					if (Freqs.exists()) {
						System.out.println("This Frequency File already exists !!! ..." + Freq);
						path = "E:\\" + CustomerID + "\\" + Year + "\\Bi_Monthly";
					} else {
						Freqs = new File("E:\\" + CustomerID + "\\" + Year + "\\Bi_Monthly");
						Freqs.mkdirs();
						path = "E:\\" + CustomerID + "\\" + Year + "\\Bi_Monthly";
					}
				} else if (Freq == 6) {
					File Freqs = new File("E:\\" + CustomerID + "\\" + Year + "\\Quarterly");
					if (Freqs.exists()) {
						System.out.println("This Frequency File already exists !!! ..." + Freq);
						path = "E:\\" + CustomerID + "\\" + Year + "\\Quarterly";
					} else {
						Freqs = new File("E:\\" + CustomerID + "\\" + Year + "\\Quarterly");
						Freqs.mkdirs();
						path = "E:\\" + CustomerID + "\\" + Year + "\\Quarterly";
					}
				} else if (Freq == 7) {
					File Freqs = new File("E:\\" + CustomerID + "\\" + Year + "\\Half_Yearly");
					if (Freqs.exists()) {
						System.out.println("This Frequency File already exists !!! ..." + Freq);
						path = "E:\\" + CustomerID + "\\" + Year + "\\Half_Yearly";
					} else {
						Freqs = new File("E:\\" + CustomerID + "\\" + Year + "\\Half_Yearly");
						Freqs.mkdirs();
						path = "E:\\" + CustomerID + "\\" + Year + "\\Half_Yearly";
					}
				} else if (Freq == 8) {
					File Freqs = new File("E:\\" + CustomerID + "\\" + Year + "\\Yearly");
					if (Freqs.exists()) {
						System.out.println("This Frequency File already exists !!! ..." + Freq);
						path = "E:\\" + CustomerID + "\\" + Year + "\\Yearly";
					} else {
						Freqs = new File("E:\\" + CustomerID + "\\" + Year + "\\Yearly");
						Freqs.mkdirs();
						path = "E:\\" + CustomerID + "\\" + Year + "\\Yearly";
					}
				}
				return path;
			} else {
				File subFiles = new File("E:\\" + CustomerID + "\\" + Year);
				subFiles.mkdirs();
				if (Freq != 0) {
					File ssubFiles = null;
					if (Freq == 1) {
						ssubFiles = new File("E:\\" + CustomerID + "\\" + Year + "\\Daily");
						ssubFiles.mkdirs();
						path = "E:\\" + CustomerID + "\\" + Year + "\\Daily";
					} else if (Freq == 2) {
						ssubFiles = new File("E:\\" + CustomerID + "\\" + Year + "\\Weekly");
						ssubFiles.mkdirs();
						path = "E:\\" + CustomerID + "\\" + Year + "\\Weekly";
					} else if (Freq == 3) {
						ssubFiles = new File("E:\\" + CustomerID + "\\" + Year + "\\FortNightly");
						ssubFiles.mkdirs();
						path = "E:\\" + CustomerID + "\\" + Year + "\\FortNightly";
					} else if (Freq == 4) {
						ssubFiles = new File("E:\\" + CustomerID + "\\" + Year + "\\Monthly");
						ssubFiles.mkdirs();
						path = "E:\\" + CustomerID + "\\" + Year + "\\Monthly";
					} else if (Freq == 5) {
						ssubFiles = new File("E:\\" + CustomerID + "\\" + Year + "\\Bi_Monthly");
						ssubFiles.mkdirs();
						path = "E:\\" + CustomerID + "\\" + Year + "\\Bi_Monthly";
					} else if (Freq == 6) {
						ssubFiles = new File("E:\\" + CustomerID + "\\" + Year + "\\Quarterly");
						ssubFiles.mkdirs();
						path = "E:\\" + CustomerID + "\\" + Year + "\\Quarterly";
					} else if (Freq == 7) {
						ssubFiles = new File("E:\\" + CustomerID + "\\" + Year + "\\Half_Yearly");
						ssubFiles.mkdirs();
						path = "E:\\" + CustomerID + "\\" + Year + "\\Half_Yearly";
					} else if (Freq == 8) {
						ssubFiles = new File("E:\\" + CustomerID + "\\" + Year + "\\Yearly");
						ssubFiles.mkdirs();
						path = "E:\\" + CustomerID + "\\" + Year + "\\Yearly";
					}
					return path;
				}
			}
		}
		return path;
	}

	private static int checkUnsendStatements() {
		List<Account> allUnsendedAccounts = TransactionsDao.getUnsendStatements();
		String messageBody = "Dears\n\r" + "Below Accounts did not send Statements on time: \n\r";
		int count = 1;
		for (Account account : allUnsendedAccounts) {
			System.out.println(account);
			messageBody += account.getAccountNumber() + "\n\r";

			if (count++ != allUnsendedAccounts.size())
				messageBody += " - ";
		}
		if (allUnsendedAccounts.size() != 0) {
			boolean res = Utils.sendMail("Developers@bdc.com.eg; jermin.radwan@bdc.com.eg;shady.ashour@bdc.com.eg;",
					messageBody);
			if (res) {
				for (Account account : allUnsendedAccounts) {
					TransactionsDao.updateAccountFauilerFlag(account.getAccountNumber());
				}
			}
		}
		return allUnsendedAccounts.size();
	}
}
