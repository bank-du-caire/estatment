package com.bdc.estatement.controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.bdc.estatement.dao.SMSDao;
import com.bdc.estatement.dao.TransactionsDao;
import com.bdc.estatement.model.Account;
import com.bdc.estatement.model.SMSHistory;
import com.bdc.estatement.model.SMSModel;
import com.bdc.estatement.util.LogUtil;
import com.bdc.estatement.util.SMSUtils;
import com.bdc.estatement.util.Utils;

import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;
import okhttp3.Request;
import okhttp3.Response;

public class SMSController {

//	 long startTime = System.nanoTime();
	/* ... the code being measured starts ... */
// sleep for 5 seconds
//		TimeUnit.SECONDS.sleep(5);
//
//		/* ... the code being measured ends ... */
//
//		long endTime = System.nanoTime();
//
//		// get difference of two nanoTime values
//		long timeElapsed = endTime - startTime;
//
//		System.out.println("Execution time in nanoseconds  : " + timeElapsed);
//
//		System.out.println("Execution time in milliseconds : " +
//								timeElapsed / 1000000);

//	[1:Delivered],[3:Sent],[4: Buffered], [17:In Progress]
//	[5:Blocked],[2:UnDelivered],[16:SMSC Rejected],[1200:UNKNOWN] 5 16 1200

	public static void checkSmsStatus() {
		List<SMSHistory> smsIDs = SMSDao.getSmsIds();
		for (SMSHistory smsH : smsIDs) {
//			String accessToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6IjY3MGVkMjUwZGIxMmFmOGQwMTk0YTU4NzU3MzVmOGY3MTc3YjlkMmM1M2E2YjY3OGJkYTAwOWViMGQwNWU3MmEyNWRkMTFiY2I3MGYwNmQ1YzUzNDc3MmM2M2MzZWI1MzAwYzc2OTI5ZTVlZGMzNDk3OTQyNmNmZjc4MWViN2I2YzY4MmU0ZTQ2NmUzNjI3NDc2MmJjMzI0ZGIwNDcyMzMiLCJpYXQiOjE1NjEyODQ5NDMsImV4cCI6MzEzOTE2NDk0M30.NBVQhjizpk7fQ1Q6fz22JObgVsLONDK8oAoRQnuyfwss";
			String accessToken ="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6IjY3MGVkMjUwZGIxMmFmOGQwMTk0YTU4NzU3MzVmOGY3MTc3YjlkMmM1M2E2YjY3OGJkYTAwOWViMGQwNWU3MmEyNWRkMTFiY2I3MGYwNmQ1YzUzNDc3MmM2M2MzZWI1MzAwYzc2OTI5ZTVlZGMzNDk3OTQyNmNmZjc4MWViN2I2YzY4MmU0ZTQ2NmUzNjI3NDc2MmJjMzI0ZGIwNDcyMzMiLCJpYXQiOjE1NjEyODQ5NTEsImV4cCI6MzEzOTE2NDk1MX0.tNlyEkSB1cAkKWGqhrr8o_sWbwrE9S7GNoIuGBfc2Y0";
			OkHttpClient client = new OkHttpClient();
			String url = "http://api.cequens.com/cequens/api/v1/messaging/" + smsH.getSmsID();
			Builder builder = client.newBuilder();

			// for development on local machines un comment it and dont forget before
			// publiush to comment proxy 2 lines of code
//			Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.248.4.21", 9090));
//			builder.proxy(proxy);
			LogUtil.info("Calling Cequence Get Message Details URL : " + url);
			Request request = new Request.Builder().header("Accept", "application/json")
					.addHeader("Content-Type", "application/json")
					.addHeader("Authorization", String.format("Bearer %s", accessToken)).url(url).build();
			Response response = null;
			JSONObject jsonObject = null;
			try {
				response = builder.build().newCall(request).execute();
				System.out.println(response);
				LogUtil.info("getting response of message ID: " + smsH.getSmsID() + " : " + response.toString());
				String jsonString = response.body().string();
				jsonObject = new JSONObject(jsonString);
				String data = jsonObject.getString("data");
				System.out.println("data:: " + data);
				if (!data.isEmpty()) {
					data = data.replace("[", "");
					data = data.replace("]", "");
					JSONObject dataJason = new JSONObject(data);
					String status = dataJason.getString("Status");
					System.out.println("status:: [" + status + "]");
					System.out.println(response);
					if (!status.isEmpty() && status != "") {
						SMSDao.updateSmsHistory(smsH.getSmsID(), status, 0);
						if (status.equals("2") || status.equals("5") || status.equals("16") || status.equals("1200")) {
							StringBuilder stringBuilder = new StringBuilder();
							stringBuilder.append("Kindly note that the Customer account  “");
							stringBuilder.append(smsH.getAccountNumber());
							stringBuilder.append("” didn’t receive the ");
							stringBuilder.append("below message on the mobile number (");
							stringBuilder.append(smsH.getMobile());
							stringBuilder.append("). ");
							stringBuilder.append("<p>");
							stringBuilder.append(
									"****************************************************************************************************************************\n\r ");
							stringBuilder.append("<p>");
							stringBuilder.append("( ");
							stringBuilder.append(smsH.getMessage());
							stringBuilder.append(")<p/>");
							String messageBody = stringBuilder.toString();
							System.out.println(messageBody);
							String emails = "shady.ashour@bdc.com.eg;";
//							String emails = "shady.ashour@bdc.com.eg;";
							Utils.sendMail(emails, messageBody);
							SMSDao.updateSmsHistory(smsH.getSmsID(), status, 1);
						}
					}
				}
			} catch (IOException | JSONException e) {
				LogUtil.error(e);
			}
		}
	}

	public static void main(String[] args) throws ParseException {
		checkSmsStatus();
		long startTime = System.nanoTime();
		long startTimeDB = System.nanoTime();
		LogUtil.initialize();
		LogUtil.info("Get New Collected Cheques transcations into SMS DB");
		int result = SMSDao.insertNewCollectedChequesToLocalDB();
		LogUtil.info("Get New Returned Cheques transcations into SMS DB total records: " + result);
		result = SMSDao.insertNewReturnedChequesToLocalDB();
		LogUtil.info("Get New Customer transcations into SMS DB total records: " + result);

		long startTime2 = System.nanoTime();
		result = SMSDao.insertNewCustomerTransactionsSMS();
//		List<Account> accounts = TransactionsDao.getSMSAccountsByToday();
		LogUtil.info("Apply synchronization Between Flix cube and SMS accounts");
		List<Account> accounts = Utils.syncFlexCubeChanges(1);

		long endTimeDB = System.nanoTime();
		long timeElapsedDB = endTimeDB - startTimeDB;
		System.out.println("Execution time in seconds DB : " + timeElapsedDB / 1000000000);

		for (Account account : accounts) {

			String smsActivationDate = new SimpleDateFormat("yyyy-MM-dd").format(account.getSmsActivationDate());
			LogUtil.info("start getting data from Collected Cheques");
			List<SMSModel> smsCollectedCheques = SMSDao
					.getNewCollectedChequesTransactions(account.getAccountNumber().trim(), smsActivationDate);
			LogUtil.info("start getting data from Returned Cheques");

			List<SMSModel> smsReturnedCheques = SMSDao
					.getNewReturnedChequesTransactions(account.getAccountNumber().trim(), smsActivationDate);
			LogUtil.info("start getting data from Customer Transactions");
			List<SMSModel> smsCustomerTransactions = SMSDao.getNewCustomerTransactionsSMS(account.getAccountNumber(),
					smsActivationDate);
			LogUtil.info("Customer Transactions List size::::  " + smsCustomerTransactions.size());
			LogUtil.info("start initiating messages for Collected Cheques");
			initiateMessages(smsCollectedCheques, account, "Cheques collected");
			LogUtil.info("start initiating messages for Returned Cheques");
			initiateMessages(smsReturnedCheques, account, "Cheques returned");
			LogUtil.info("start initiating messages for Customer Transactions");
			initiateMessages(smsCustomerTransactions, account, "Deposit");
		}

		long endTime = System.nanoTime();
		long timeElapsed = endTime - startTime;
		System.out.println("Execution time in nanoseconds  : " + timeElapsed);
		System.out.println("Total Execution time in Seconds : " + (timeElapsed / 1000000000));
		System.out.println("Total Execution time in minutes : " + (timeElapsed / 1000000000) / 60);
	}

	private static void initiateMessages(List<SMSModel> sms, Account account, String msgType) {
		for (SMSModel smsModel : sms) {
			try {
				String smsMessage = SMSUtils.prepareMessage(msgType, smsModel);
				//LogUtil.info(smsMessage);
				String smsID = "";
				String mobile = account.getMobile();
				if (mobile.contains(",")) {
					String[] arr = mobile.split(",");
					for (int i = 0; i < arr.length; i++) {
						smsID = SMSUtils.sendMessage(smsMessage, arr[i].trim());
						if (smsID != null) {
							smsModel.setSmsID(smsID);
							int res = SMSDao.insertToSMSHistory(smsModel, account.getCustomerName(), smsMessage,
									arr[i].trim());
							if (res == 1)
								LogUtil.info("History Added Successfully.");
						}
					}
				} else {
					smsID = SMSUtils.sendMessage(smsMessage, mobile.trim());
					smsModel.setSmsID(smsID);
					if (smsID != null ) {
						int res = SMSDao.insertToSMSHistory(smsModel, account.getCustomerName(), smsMessage,
								mobile.trim());
						if (res == 1)
							LogUtil.info("History Added Successfully.");
					}
				}
//				String smsID = SMSUtils.sendMessage(smsMessage, account.getMobile());
//				smsID = SMSUtils.sendMessage(smsMessage, account.getMobile());
//				smsModel.setSmsID(smsID);
				if (!smsID.isEmpty() && smsID != "") {
					LogUtil.info("SMS Sent Successfully");
					switch (msgType) {
					case "Deposit":
						SMSDao.updateCustomerTransaction(smsModel);
						break;
					case "Cheques collected":
						SMSDao.updateCollectedChequesTransaction(smsModel);
						break;
					case "Cheques returned":
						SMSDao.updateReturnedChequesTransaction(smsModel);
						break;
					}
					LogUtil.info("Transaction Flagged As sent in DB.");

				} else
					LogUtil.info("SMS dose not Sent");

			} catch (Exception e) {
				LogUtil.error(e.getMessage());
			}
		}
	}
}
