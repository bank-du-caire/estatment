package com.bdc.estatement.controller;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import com.bdc.estatement.hostothost.SftpServer;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

public class NI_FTP_Connect {
	public static void main(String[] args) {
		try {
			JSch jsch = new JSch();
			HashMap properties = SftpServer.readPropertiesFileInfo("D:\\BDC_APP\\NI\\resources\\properties.txt");
//			HashMap properties = SftpServer.readPropertiesFileInfo("/home/INFADM/Test/properties.txt");
			String user = ((String) properties.get(1)).trim();
			String host = ((String) properties.get(2)).trim();
			String password = ((String) properties.get(3)).trim();
			String sftpDirectory = ((String) properties.get(4)).trim();
			int port = Integer.valueOf(((String) properties.get(5)).trim());
			String sharedFolderUser = ((String) properties.get(6)).trim();
			String sharedFolderPassword = ((String) properties.get(7)).trim();
			String privateKey = ((String) properties.get(8)).trim();
			String destiantionPath = ((String) properties.get(9)).trim();

//			if (new File(privateKey).exists()) {// && new File("D:\\BDC_APP\\SFTP_PAYMOB\\test.txt").exists()) {
			System.out.println("yes");
//				jsch.addIdentity(privateKey);
			System.out.println("identity added ");
			Session session = jsch.getSession(user, host, port);
			session.setPassword(password);
			System.out.println("session created.");
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.connect();
			System.out.println("session connected.....");
			Channel channel = session.openChannel("sftp");
			channel.setInputStream(System.in);
			channel.setOutputStream(System.out);
			channel.connect();
			System.out.println("shell channel connected....");
			ChannelSftp c = (ChannelSftp) channel;
			/*
			 * 
			 */

			List<String> allFiles = new ArrayList<String>();
			c.cd(sftpDirectory);
			//
//				File file = new File("D:\\BDC_APP\\HostToHost\\Temp\\" + workingDirectory);
//				File file = new File("D:\\BDC_APP\\SFTP_PAYMOB\\files");
//				boolean dirCreated = file.mkdir();
			Vector<ChannelSftp.LsEntry> list = c.ls("*.*");
//				Vector<ChannelSftp.LsEntry> list = channelSftp.ls("*.pdf");
			for (ChannelSftp.LsEntry entry : list) {
				System.out.println(entry.getFilename());
				if (entry.getFilename().equals("bdca_13-Jun-2021.zip")) {
//					allFiles.add("D:\\BDC_APP\\SFTP_PAYMOB\\files\\" + entry.getFilename());
					allFiles.add(entry.getFilename());
					byte[] buffer = new byte[1024];
					BufferedInputStream bis = new BufferedInputStream(c.get(entry.getFilename()));

					// here i should add the files to below shared ftp
//					File newFile = new File("D:\\BDC_APP\\SFTP_PAYMOB\\files\\" + entry.getFilename());
//					File newFile = new File(
//							"\\\\bdc.bank.local\\bdc\\Cards\\Zero Installments\\" + entry.getFilename());

					File newFile = new File(destiantionPath + entry.getFilename());

					OutputStream os = new FileOutputStream(newFile);
					BufferedOutputStream bos = new BufferedOutputStream(os);
					int readCount;
					while ((readCount = bis.read(buffer)) > 0) {
						System.out.println("Writing: " + entry.getFilename());
						bos.write(buffer, 0, readCount);
					}
					bis.close();
					bos.close();
//					c.rm(entry.getFilename());
					break;
				}
			}

			/*
			 * 
			 */

//				String fileName = "D:\\BDC_APP\\SFTP_PAYMOB\\test.txt";
//				c.put(fileName, sftpDirectory);
//				c.exit();
			System.out.println("done");
//			} else {
//				System.out.println("resource files not exists");
//			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}

}
