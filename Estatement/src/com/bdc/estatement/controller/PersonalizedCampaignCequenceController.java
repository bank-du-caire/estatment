package com.bdc.estatement.controller;

import java.io.File;
import java.io.IOException;
import org.json.JSONException;
import org.json.JSONObject;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class PersonalizedCampaignCequenceController {

	public static void main(String[] args) {

		String idToken = getIdToken();
		String awsS3Url = uploadFileOnS3(idToken, new File("D:/BDC_APP/SMS_Retail/campaign.csv"));
		idToken = getIdToken();
		sendCampaign(idToken, awsS3Url);
//		getCampaignStatus("03-323870", idToken);

	}

	public static String getIdToken() {
		String idToken = "";
		OkHttpClient client = new OkHttpClient();
		MediaType mediaType = MediaType.parse("application/json");
		RequestBody body = RequestBody.create(mediaType,
				"{\"email\":\"CorpTransSms\",\"password\":\"sMs_tRans_Corp_2020\"}");
		Request request = new Request.Builder().url("https://api.console.cequens.net/v1/usrmngmnt/login").post(body)
				.addHeader("content-type", "application/json").build();
		try {
			Response response = client.newCall(request).execute();
			System.out.println(response);
			ResponseBody responseBody = response.body();
			String respBody = response.body().string();
			System.out.println(respBody);
			JSONObject jsonObject = new JSONObject(respBody);
			String userName = jsonObject.getString("username");
			idToken = jsonObject.getString("idToken");
			String accessToken = jsonObject.getString("accessToken");
			String refreshToken = jsonObject.getString("refreshToken");
			System.out.println("userName: " + userName);
			System.out.println("idToken: " + idToken);
			System.out.println("accessToken: " + accessToken);
			System.out.println("refreshToken: " + refreshToken);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return idToken;
	}

	public static String uploadFileOnS3(String idToken, File file) {
		OkHttpClient client = new OkHttpClient().newBuilder().build();
		MediaType mediaType = MediaType.parse("text/plain");
		RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
				.addFormDataPart("file", file.getAbsolutePath(),
						RequestBody.create(MediaType.parse("application/octet-stream"), file))
				.addFormDataPart("FileName", file.getName()).build();
		Request request = new Request.Builder().url("https://upload.console.cequens.com/api/files/upload")
				.method("POST", body).addHeader("x-user-auth", "bearer " + idToken).build();
		try {
			Response response = client.newCall(request).execute();
			String respBody = response.body().string();
			System.out.println(respBody);
			JSONObject jsonObject = new JSONObject(respBody);
			String awsS3Url = jsonObject.get("url").toString();
			System.out.println(awsS3Url);
			return awsS3Url;
		} catch (IOException | JSONException e) {
			System.out.println(e);
		}
		return "";
	}

	public static void sendCampaign(String idToken, String awsS3Url) {
		OkHttpClient client = new OkHttpClient();

		MediaType mediaType = MediaType.parse("application/json");
		RequestBody body = RequestBody.create(mediaType,
				"{\"campaignName\":\"Your Campaign Name\",\"smsSenderID\":3576,\"messageText\":\"hello {{name}}\",\"campaignStatusID\":1,\"smsCampaignTypeID\":2,\"isPinned\":false,\"callToAction\":false,\"ctaRedirectUrl\":null,\"scheduleDetails\":{\"scheduledDate\":null,\"timezoneId\":null},\"recipients\":{\"typed\":[],\"files\":[{"
						+ " \"fileURL\": \"" + awsS3Url + "\"}]}}");
		Request request = new Request.Builder().url("https://api.console.cequens.net/v1/campaign/sms").post(body)
				.addHeader("content-type", "application/json").addHeader("x-user-auth", "bearer " + idToken).build();
		try {
			Response response = client.newCall(request).execute();
			System.out.println(response.body().string());
			System.out.println(response);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

//	public static void sendCampaign(String idToken, String awsS3Url) {
//		OkHttpClient client = new OkHttpClient();
//
//		MediaType mediaType = MediaType.parse("application/json");
//		RequestBody body = RequestBody.create(mediaType,
//				"{\"campaignName\":\"Your Campaign Name\",\"smsSenderID\":3576,\"messageText\":\"Test\",\"campaignStatusID\":1,\"smsCampaignTypeID\":1,\"isPinned\":false,\"callToAction\":false,\"ctaRedirectUrl\":null,\"scheduleDetails\":{\"scheduledDate\":null,\"timezoneId\":null},\"recipients\":{\"typed\":[\"201115424896\"],\"files\":[]}}");
//		Request request = new Request.Builder().url("https://api.console.cequens.net/v1/campaign/sms").post(body)
//				.addHeader("content-type", "application/json").addHeader("x-user-auth", "bearer " + idToken).build();
//		try {
//			Response response = client.newCall(request).execute();
//			System.out.println(response);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//	}

	public static void getCampaignStatus(String campaignID, String idToken) {
		OkHttpClient client = new OkHttpClient();

		Request request = new Request.Builder()
				.url("https://api.console.cequens.net/v1/campaign/" + campaignID + "/records").get()
				.addHeader("x-user-auth", "bearer " + idToken).build();
		try {
			Response response = client.newCall(request).execute();
			System.out.println(response.body().string());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
