package com.bdc.estatement.controller;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;

import org.json.JSONException;
import org.json.JSONObject;

import com.bdc.estatement.util.LogUtil;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class TestCequence {

	public static void main(String[] args) {
		LogUtil.initialize();
		String str1 = "Dear Customer";
		String accessToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6IjY3MGVkMjUwZGIxMmFmOGQwMTk0YTU4NzU3MzVmOGY3MTc3YjlkMmM1M2E2YjY3OGJkYTAwOWViMGQwNWU3MmEyNWRkMTFiY2I3MGYwNmQ1YzUzNDc3MmM2M2MzZWI1MzAwYzc2OTI5ZTVlZGMzNDk3OTQyNmNmZjc4MWViN2I2YzY4MmU0ZTQ2NmUzNjI3NDc2MmJjMzI0ZGIwNDcyMzMiLCJpYXQiOjE1NjEyODQ5NDMsImV4cCI6MzEzOTE2NDk0M30.NBVQhjizpk7fQ1Q6fz22JObgVsLONDK8oAoRQnuyfws";
		String headers = "{\"messageText\": \"" + str1
				+ "\",\"senderName\": \"BDC\",\"messageType\":\"text\",\"recipients\":\"01115424896\"}";
		String postURL = "http://api.cequens.com/cequens/api/v1/messaging";
		JSONObject jsonObject = sendingPostRequest(headers, postURL, accessToken);
	}

	public static JSONObject sendingPostRequest(String headers, String postURL, String accessToken) {

		LogUtil.info("inside sendingPostRequest()");
		OkHttpClient client = new OkHttpClient();
		MediaType mediaType = MediaType.parse("application/json");
		RequestBody body = RequestBody.create(mediaType, headers);
		System.out.println("body::: " + headers);
		Request request = null;
		if (accessToken == null || accessToken.isEmpty()) {
			LogUtil.info("preparing post request to get ceqens token");
			request = new Request.Builder().url(postURL).post(body).addHeader("Accept", "application/json")
					.addHeader("Content-Type", "application/json").build();
		} else {
			LogUtil.info("preparing post request to send ceqens SMS");
			request = new Request.Builder().url(postURL).post(body).addHeader("Accept", "application/json")
					.addHeader("Content-Type", "application/json")
					.addHeader("Authorization", String.format("Bearer %s", accessToken))
					.addHeader("Cache-Control", "no-cache").build();
		}
		String jsonString = "";
		JSONObject jsonObject = null;
		try {
			Proxy proxyTest = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.248.4.21", 9090));
			Builder builder = client.newBuilder();
			builder.proxy(proxyTest);
			LogUtil.info("Sending Cequens Post Request");
			Response response = builder.build().newCall(request).execute();
			LogUtil.info(response.toString());
			jsonString = response.body().string();
			LogUtil.info("Cequens Body Response is: " + jsonString);
			jsonObject = new JSONObject(jsonString);
		} catch (IOException e) {
			LogUtil.error(e.toString());
			e.printStackTrace();
		} catch (JSONException e) {
			LogUtil.error(e.toString());
			e.printStackTrace();
		}
		return jsonObject;
	}

}
