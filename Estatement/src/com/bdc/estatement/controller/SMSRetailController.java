
package com.bdc.estatement.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.bdc.estatement.dao.SMSDao;
import com.bdc.estatement.model.RetailTransaction;
import com.bdc.estatement.util.Utils;

public class SMSRetailController {

	public static void main(String[] args) {
		long startTime = System.nanoTime();
		List<RetailTransaction> retailTransactions = new ArrayList<RetailTransaction>();
		int result = SMSDao.getRetailSMSsTransactions();
		System.out.println("Size: " + result);
//		for(RetailTransaction retailTransaction: retailTransactions) {
//			System.out.println(retailTransaction.getAccountNumber());
//		}
		long endTime = System.nanoTime();
		long timeElapsed = endTime - startTime;
		System.out.println("Execution time in nanoseconds  : " + timeElapsed);
		System.out.println("Total Execution time in Seconds : " + (timeElapsed / 1000000000));
		System.out.println("Total Execution time in minutes : " + (timeElapsed / 1000000000) / 60);
	}

}
