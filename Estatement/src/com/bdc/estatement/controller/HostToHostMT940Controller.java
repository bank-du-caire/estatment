package com.bdc.estatement.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.bdc.estatement.dao.TransactionsDao;
import com.bdc.estatement.hostothost.SftpServer;
import com.bdc.estatement.model.Account;
import com.bdc.estatement.model.EndOffDay;
import com.bdc.estatement.model.SwiftMsg;
import com.bdc.estatement.util.Defines;
import com.bdc.estatement.util.LogUtil;
import com.bdc.estatement.util.Utils;

public class HostToHostMT940Controller {

	public static void main(String[] args) throws ParseException {
		// Utils.uploadToSftpServer("D:\\BDC_APP\\HostToHost\\Attachements\\test.zip");
		LogUtil.initialize();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date today = Calendar.getInstance().getTime();
		List<Account> accounts = TransactionsDao.getApprovedHotToHostAccounts();
		String fromDate = "";
		String fromDateSQLFormat = "";
		String toDate = "";
		String toDateSQLFormat = "";
		String pdfPath = "";
		String excelPath = "";
		String swiftPath = "";
		for (Account account : accounts) {
			Date nextSend = account.getNextDate();
			// here i need to add the values off friday.
			EndOffDay endOffDay = TransactionsDao.getEndOffDayDateProcess();
			long diffInMillies = Math
					.abs(endOffDay.getDateProcess().getTime() - endOffDay.getDateLastProcess().getTime());
			long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);

			if (true
//					endOffDay != null && endOffDay.getDateProcess() != null
//					&& (simpleDateFormat.format(endOffDay.getDateProcess()).equals(simpleDateFormat.format(today)))
//					|| diff > 1
			) {
				if (account.getFrequancyStmt() == Defines.None) {
					break;
				} else if (account.getFrequancyStmt() == Defines.Daily) {
					fromDate = Utils.claculateFromDate(account.getFrequancyStmt(),
							new SimpleDateFormat("yyyy-MM-dd").format(account.getNextDate()));
					fromDateSQLFormat = Utils.claculateFromDateSQLDormat(account.getFrequancyStmt(),
							new SimpleDateFormat("yyyy-MM-dd").format(account.getNextDate()));
					toDate = new SimpleDateFormat("dd/MM/YYYY").format(account.getNextDate());
					toDateSQLFormat = new SimpleDateFormat("yyyy-MM-dd").format(account.getNextDate());

					// here if manual e-statement

//					Calendar xmas = new GregorianCalendar(2020, Calendar.APRIL, 6);
//					Date d = xmas.getTime();
//					fromDate = new SimpleDateFormat("dd/MM/YYYY").format(d);
//					fromDateSQLFormat = new SimpleDateFormat("yyyy-MM-dd").format(d);
//					xmas = new GregorianCalendar(2020, Calendar.APRIL, 7);
//					d = xmas.getTime();
//					toDate = new SimpleDateFormat("dd/MM/YYYY").format(d);
//					toDateSQLFormat = new SimpleDateFormat("yyyy-MM-dd").format(d);

				} else {
					fromDate = Utils.claculateFromDate(account.getFrequancyStmt(),
							new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
					fromDateSQLFormat = Utils.claculateFromDateSQLDormat(account.getFrequancyStmt(),
							new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
					toDate = new SimpleDateFormat("dd/MM/YYYY").format(new Date());
					toDateSQLFormat = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

					// here if manual e-statement
//					Calendar xmas = new GregorianCalendar(2020, Calendar.FEBRUARY, 1);
//					Date d = xmas.getTime();
//					fromDate = new SimpleDateFormat("dd/MM/YYYY").format(d);
//					fromDateSQLFormat = new SimpleDateFormat("yyyy-MM-dd").format(d);
//					xmas = new GregorianCalendar(2020, Calendar.FEBRUARY, 29);
//					d = xmas.getTime();
//					toDate = new SimpleDateFormat("dd/MM/YYYY").format(d);
//					toDateSQLFormat = new SimpleDateFormat("yyyy-MM-dd").format(d);
				}
				account.setFromDate(fromDate);
				account.setToDate(toDate);
				List<SwiftMsg> msg_list = TransactionsDao.getSwiftPackage(account.getAccountNumber(), fromDateSQLFormat,
						toDateSQLFormat);
				if (msg_list.size() != 0) {
					Utils.exportToTextFileHostToHost(msg_list, account.getCustomerCode(), account.getAccountNumber());
				}
				account.setLastSentDate(new Date());
				String nextSendDate = Utils.claculateNextSendDate(account.getFrequancyStmt(),
						new SimpleDateFormat("yyyy-MM-dd").format(new Date()));

				if (!nextSendDate.isEmpty()) {
					account.setNextDate(new SimpleDateFormat("dd/MM/yyyy").parse(nextSendDate));
					if (msg_list.size() != 0) {
						if (diff > 1) {
							long diffInMillies_ = Math.abs(endOffDay.getDateNextProcess().getTime() - today.getTime());
							long diffNextAndToday = TimeUnit.DAYS.convert(diffInMillies_, TimeUnit.MILLISECONDS);
							nextSend = Utils.addDays(nextSend, 1);
							if (diffNextAndToday > 1) {
								account.setNextDate(endOffDay.getDateNextProcess());
							}
						}
//						TransactionsDao.updateAccountDatesHostToHost(account);
						swiftPath = "D://BDC_APP//HostToHost//Attachements//" + account.getAccountNumber() + " - "
								+ new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + ".zip";

						SftpServer.uploadToSftpServer(swiftPath,account.getSftpFolder());

					}
				}
				TransactionsDao.updateAccountDatesHostToHost(account);
			}
		}

	}

}
