
package com.bdc.estatement.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.bdc.estatement.util.LogUtil;

public class ConnectionManager {

//	app Development
//	public static String bdcUrl = "jdbc:oracle:thin:@172.17.63.179:1521/appdev.bdc";
//	private static String bdcDriverName = "oracle.jdbc.OracleDriver";
//	private static String bdcUsername = "ESTATEMENT";
//	private static String bdcPassword = "ESTATEMENT";

//	ATL FCT Meeza Prepaid 
	public static String fctUrl = "jdbc:oracle:thin:@172.17.66.11:1521/CUSTDB";
	private static String fctUsername = "STGR";
	private static String fctPassword = "STGR";

	// Corporate UAT Internet Banking
//	public static String bdcUrl = "jdbc:oracle:thin:@172.17.85.199:1521/NETRETAIL";
//	private static String bdcDriverName = "oracle.jdbc.OracleDriver";
//	private static String bdcUsername = "GEFUPLUS";
//	private static String bdcPassword = "GEFUPLUS";

	// Corporate Production Internet Banking
//	public static String bdcUrl = "jdbc:oracle:thin:@172.17.60.185:1521/FCRPR1";
//	private static String bdcDriverName = "oracle.jdbc.OracleDriver";
//	private static String bdcUsername = "BDCTCIBT";
//	private static String bdcPassword = "BDCTCIBT2021";

//	private static String bdcUsername = "FCCTCIB";
//	private static String bdcPassword = "FCCTCIB";

//	private static String flexUrl = "jdbc:oracle:thin:@172.17.60.252:1521/retailt24";
//	private static String flexDriverName = "oracle.jdbc.OracleDriver";
//	private static String flexUsername = "bdcefcrr";
//	private static String flexPassword = "BDC";

	// ..........................................................
	// ..........................................................

//	 app publish production
//	public static String bdcUrl = "jdbc:oracle:thin:@172.17.63.178:1521/APPPUB.BDC";
//	private static String bdcDriverName = "oracle.jdbc.OracleDriver";
//	private static String bdcUsername = "ESTATEMENT";
//	private static String bdcPassword = "ESTATEMENT";

	//	 localhost connetion
	public static String bdcUrl = "jdbc:oracle:thin:@localhost:1521/xe";
	private static String bdcDriverName = "oracle.jdbc.OracleDriver";
	private static String bdcUsername = "ESTATMENT";
	private static String bdcPassword = "ESTATMENT";

	
	// Felxcube Production.
	private static String flexUrl = "jdbc:oracle:thin:@172.17.60.185:1521/FCRPR1";
	private static String flexDriverName = "oracle.jdbc.OracleDriver";
	private static String flexUsername = "BDCEFCRR";
	private static String flexPassword = "BDCEFCRR";

	// Army production database
	private static String armyUrl = "jdbc:oracle:thin:@172.17.60.185:1521/FCRPR1";
	private static String armyDriverName = "oracle.jdbc.OracleDriver";
	private static String armyUsername = "ARMY";
	private static String armyPassword = "ARMY";

	// IBDL Server E-statement Database
//	private static String bdcUrl = "jdbc:oracle:thin:@172.17.63.144:1521/ESTATEMENT";
//	private static String bdcUrl = "jdbc:oracle:thin:@172.17.63.144:1521:ESTATEMENT";
//	private static String bdcDriverName = "oracle.jdbc.OracleDriver";
//	private static String bdcUsername = "ESTATEMENT";
//	private static String bdcPassword = "ESTATEMENT";

	// OAISIS DB Production
	public static String oaisesURL = "jdbc:oracle:thin:@172.17.5.231:1521/OASIS761";
	private static String oaisesDriverName = "oracle.jdbc.OracleDriver";
	private static String oaisesUsername = "OASISR";
	private static String oaisesPassword = "OASISR";

	public static Connection getBDCConnection() {
		Connection con = null;
		LogUtil.info("Get Connection from BDC Database.");
		LogUtil.info("Connection URL: " + bdcUrl);
		try {
			Class.forName(bdcDriverName);
			try {
				con = DriverManager.getConnection(bdcUrl, bdcUsername, bdcPassword);
				if (con != null) {
					LogUtil.info("Connected Successfully");
					con.setAutoCommit(true);
				}
			} catch (SQLException ex) {
				LogUtil.error("Failed to create the database connection. {}", ex);
			}
		} catch (ClassNotFoundException ex) {
			LogUtil.error("Driver not found.");
		}
		return con;
	}

	public static Connection getFCTConnection() {
		Connection con = null;
		LogUtil.info("Get Connection from FCT ATL Database.");
		LogUtil.info("Connection URL: " + fctUrl);
		try {
			Class.forName(bdcDriverName);
			try {
				con = DriverManager.getConnection(fctUrl, fctUsername, fctPassword);
				if (con != null) {
					LogUtil.info("Connected Successfully");
					con.setAutoCommit(true);
				}
			} catch (SQLException ex) {
				LogUtil.error("Failed to create the database connection. {}", ex);
			}
		} catch (ClassNotFoundException ex) {
			LogUtil.error("Driver not found.");
		}
		return con;
	}

	public static Connection getOAISISConnection() {
		Connection con = null;
		LogUtil.info("Get Connection from BDC Database.");
		LogUtil.info("Connection URL: " + oaisesURL);
		try {
			Class.forName(bdcDriverName);
			try {
				con = DriverManager.getConnection(oaisesURL, oaisesUsername, oaisesPassword);
				if (con != null) {
					LogUtil.info("Connected Successfully");
					con.setAutoCommit(true);
				}
			} catch (SQLException ex) {
				LogUtil.error("Failed to create the database connection. {}", ex);
			}
		} catch (ClassNotFoundException ex) {
			LogUtil.error("Driver not found.");
		}
		return con;
	}

	public static Connection getArmyBDCConnection() {
		Connection con = null;
		LogUtil.info("Get Connection from Army Database.");
		LogUtil.info("Connection URL: " + "jdbc:oracle:thin:@172.17.60.185:1521/FCRPR1");
		try {
			Class.forName(bdcDriverName);
			try {
				con = DriverManager.getConnection("jdbc:oracle:thin:@172.17.60.185:1521/FCRPR1", "ARMY", "ARMY");
				if (con != null) {
					LogUtil.info("Connected Successfully");
					con.setAutoCommit(true);
				}
			} catch (SQLException ex) {
				LogUtil.error("Failed to create the database connection. {}", ex);
			}
		} catch (ClassNotFoundException ex) {
			LogUtil.error("Driver not found.");
		}
		return con;
	}

	public static Connection getFlexConnection() {
		Connection con = null;
		LogUtil.info("Get Connection from BDC Database.");
		LogUtil.info("Connection URL: " + flexUrl);
		try {
			Class.forName(flexDriverName);
			try {
				con = DriverManager.getConnection(flexUrl, flexUsername, flexPassword);
				if (con != null) {
					LogUtil.info("Connected Successfully");
					con.setAutoCommit(true);
				}
			} catch (SQLException ex) {
				LogUtil.error("Failed to create the database connection. {}", ex);
			}
		} catch (ClassNotFoundException ex) {
			LogUtil.error("Driver not found. {}", ex);
		}
		return con;
	}

	public static Connection getFrsprodConnection() {
		Connection con = null;
		LogUtil.info("Get Connection from BDC Database.");
		LogUtil.info("Connection URL: jdbc:oracle:thin:@172.17.60.185:1521/FCRPROD");
		try {
			Class.forName(flexDriverName);
			try {
				con = DriverManager.getConnection("jdbc:oracle:thin:@172.17.60.185:1521/FCRPROD", "BDCEFCRR",
						"BDCEFCRR");
				if (con != null) {
					LogUtil.info("Connected Successfully");
					con.setAutoCommit(true);
				}
			} catch (SQLException ex) {
				LogUtil.error("Failed to create the database connection. {}", ex);
			}
		} catch (ClassNotFoundException ex) {
			LogUtil.error("Driver not found. {}", ex);
		}
		return con;
	}

}
