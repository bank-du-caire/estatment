package com.bdc.estatement.auditing;

import java.text.DateFormat;
import java.util.Date;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import org.apache.log4j.Logger;

import com.bdc.estatement.model.Account;
import com.bdc.estatement.model.SyslogAudit;
import com.bdc.estatement.util.Defines;
import com.bdc.estatement.util.LogUtil;
import com.bdc.estatement.util.Loggable;

@Loggable
@Interceptor
public class AuditingInterceptor {

	// Meta Info for Request
	private String className;
	private String methodName;

	private Long userID;
	private String sourcePort;
	private short actionStatus = 1;
	private String sourceIPAddress;
	private StringBuilder recordInfo;
	private StringBuilder actionDesc;
	private static String sourceUserName;

	// Methods Name Constants
	private static final String LOGIN = "login";
	// Session Params Constants keys
	private static final String USER_ID = "userID";
	private static final String IP = "ip";
	private static final String PORT = "port";
	private static final String USER_NAME = "username";

	private static final String INSERT_INTO_REMITTANCE_FLOW_HISTORY = "insertIntoRemittanceFlowHistory";
	private static final String UPDATE_REMITTANCE_STATUS = "updateRemittanceStatus";

	private Logger syslogLogger;
	private SyslogAudit syslogAudit;
	private Account user;

	private void getInfoFromCurrentSession() {

//		userID = (Long) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(USER_ID);
//		sourceUserName = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(USER_NAME);
//		sourceIPAddress = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(IP);
//		sourcePort = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(PORT);
	}

//	@AroundInvoke
//	public Object aroundInvoke(InvocationContext ctx) throws Exception {
//
//		Object result = null;
//		try {
//			result = ctx.proceed();
//		} catch (Exception exception) {
//			throw exception;
//		}
//		init(ctx);
//		actionStatus = 1;
//		// Object result;
//		try {
//			result = ctx.proceed();
//			if (methodName.equals(LOGIN) && result != null) {
//				user = (Account) result;
//			}
//		} catch (Exception exception) {
//			actionStatus = 0;
//			throw (exception);
//		} finally {
//			if (methodName.equals(LOGIN)) {
//				if ((user.getUserId() != null && !user.isWrongPassword()) && methodName.equals(LOGIN)) {
//					actionStatus = 1;
//					sourceUserName = user.getUserName();
//					LogUtil.syslog(writeLoggerRecordInfo(), syslogLogger);
//					// syslogAuditingService.addSyslogAudit(setSysLogObjInfo());
//				} else {
//					actionStatus = 0;
//					sourceUserName = (String) ctx.getParameters()[0];
//					LogUtil.syslog(writeLoggerRecordInfo(), syslogLogger);
//					// syslogAuditingService.addSyslogAudit(setSysLogObjInfo());
//				}
//			} else if (className.equalsIgnoreCase(INSERT_INTO_REMITTANCE_FLOW_HISTORY)) {
//				RemittanceFlowHistory historyAfter = (RemittanceFlowHistory) ctx.getParameters()[0];
//				if (historyAfter.getCurrentUserId() != null) {
//					sourceUserName = historyAfter.getCurrentUserId().getUserName();
//				} else {
//					sourceUserName = "SYSTEM";
//				}
//				methodName = prepareRemData(historyAfter);
//				LogUtil.syslog(writeLoggerRecordInfoForRemUpdate(historyAfter), syslogLogger);
//				// syslogAuditingService.addSyslogAudit(setSysLogObjInfo());
//				result = ctx.proceed();
//			} else {
//				sourceUserName = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
//						.get(USER_NAME);
//				LogUtil.syslog(writeLoggerRecordInfo(), syslogLogger);
//				// syslogAuditingService.addSyslogAudit(setSysLogObjInfo());
//			}
//		}
//		return result;
//	}

//	private String writeLoggerRecordInfoForRemUpdate(RemittanceFlowHistory history) {
//
//		JSONObject remDataJson = new JSONObject();
//		String username, remStatus = "";
//		StringBuilder sb = new StringBuilder();
//		remDataJson.put("Remittance_ID", history.getRemittanceId().getRemittanceId());
//		if (history.getCurrentUserId() != null) {
//			username = history.getCurrentUserId().getName();
//		} else {
//			username = "System";
//		}
//		if (history.getStatus() != null) {
//			remStatus = history.getStatus().getStatusEnName();
//		} else {
//			remStatus = "UNKNOWN";
//		}
//		if (history.getCompleted() != null) {
//		} else {
//		}
//
//		// Action Description Builder
//		actionDesc.append(username).append(' ').append("trying to ").append(UPDATE_REMITTANCE_STATUS).append("To")
//				.append(remStatus).append(" in ").append("RemittanceFlowHistoryService").append("Module ");
//
//		// Record Info Builder
//		recordInfo.append(sourceIPAddress).append(Defines.sysLogSeprator).append(sourceUserName)
//				.append(Defines.sysLogSeprator).append(sourcePort).append(Defines.sysLogSeprator)
//				.append(Defines.DESTINATION_IP_ADDRESS).append(Defines.sysLogSeprator).append(Defines.DESTINATION_PORT)
//				.append(Defines.sysLogSeprator).append(Defines.PROTOCOL).append(Defines.sysLogSeprator)
//				.append(actionDesc.toString())
//				.append(Defines.sysLogSeprator).append(DateFormat
//						.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT).format(new Date()).toString())
//				.append(Defines.sysLogSeprator).append(actionStatus);
//
//		return recordInfo.toString();
//	}

//	private String writeLoggerRecordInfo() {
//		// Action Description Builder
//		actionDesc.append(sourceUserName).append(' ').append("trying to ").append(methodName).append(" in ")
//				.append(className).append("Module ");
//
//		// Record Info Builder
//		recordInfo.append(sourceIPAddress).append(Defines.sysLogSeprator).append(sourceUserName)
//				.append(Defines.sysLogSeprator).append(sourcePort).append(Defines.sysLogSeprator)
//				.append(Defines.DESTINATION_IP_ADDRESS).append(Defines.sysLogSeprator).append(Defines.DESTINATION_PORT)
//				.append(Defines.sysLogSeprator).append(Defines.PROTOCOL).append(Defines.sysLogSeprator)
//				.append(actionDesc.toString())
//				.append(Defines.sysLogSeprator).append(DateFormat
//						.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT).format(new Date()).toString())
//				.append(Defines.sysLogSeprator).append(actionStatus);
//
//		return recordInfo.toString();
//	}

//	private SyslogAudit setSysLogObjInfo() {
//
//		syslogAudit.setEventTime(new Date());
//		syslogAudit.setDestinationIp(Defines.DESTINATION_IP_ADDRESS);
//		syslogAudit.setDestinationPort(String.valueOf(Defines.DESTINATION_PORT));
//		syslogAudit.setModule(className);
//		syslogAudit.setAction(methodName);
//		syslogAudit.setLogDescription(actionDesc.toString());
//		syslogAudit.setProtocol(Defines.PROTOCOL);
//		syslogAudit.setSourceIp(sourceIPAddress);
//		syslogAudit.setSourcePort(sourcePort);
//		syslogAudit.setStatus(actionStatus);
//		syslogAudit.setSourceUser(sourceUserName);
//
//		return syslogAudit;
//	}

	private void init(InvocationContext ctx) {
		getInfoFromCurrentSession();
		user = new Account();
		syslogAudit = new SyslogAudit();
		actionDesc = new StringBuilder();
		recordInfo = new StringBuilder();
		syslogLogger = LogUtil.getSysLogger();
		methodName = ctx.getMethod().getName();
		className = ctx.getTarget().getClass().getSimpleName();

	}

//	private String prepareRemData(RemittanceFlowHistory history) {
//		JSONObject remDataJson = new JSONObject();
//		String username, remStatus = "";
//		StringBuilder sb = new StringBuilder();
//		remDataJson.put("Remittance_ID", history.getRemittanceId().getRemittanceId());
//		if (history.getCurrentUserId() != null) {
//			username = history.getCurrentUserId().getName();
//		} else {
//			username = "System";
//			remDataJson.put("username", username);
//		}
//		if (history.getStatus() != null) {
//			remStatus = history.getStatus().getStatusEnName();
//		} else {
//			remStatus = "UNKNOWN";
//		}
//		if (history.getCompleted() != null) {
//		} else {
//		}
//		className = RemittanceService.class.getSimpleName();
//		methodName = UPDATE_REMITTANCE_STATUS;
//		/// sb.append("changedBy").append(username).append("To").append(remStatus).toString();
//		return methodName;
//	}

}

