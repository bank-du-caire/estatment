package com.bdc.estatement.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.bdc.estatement.model.AccountPackage;
import com.bdc.estatement.model.MeezaTransactions;
import com.bdc.estatement.model.Transaction;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;

public class MeezaPdfTemplate {
	private Document document;
	private AccountPackage account;
	private List<MeezaTransactions> meezaTransactionList;
	PdfWriter writer = null;
	private PdfTemplate t;
	private Image total;

	public MeezaPdfTemplate(List<MeezaTransactions> meezaTransactionList) {
		document = new Document();
		this.meezaTransactionList = meezaTransactionList;
	}

	public MeezaPdfTemplate() {
		document = new Document();
	}

	public boolean openPdf(String cutomerId) {
		boolean status = false;
		try {
			File pdfFile = new File("D://BDC_APP//MezzaRegCustomers//Attachements//" + cutomerId + " - "
					+ new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + ".pdf");
			if (pdfFile != null) {
				writer = PdfWriter.getInstance(document, new FileOutputStream(pdfFile));
				writer.setEncryption("concretepage".getBytes(), cutomerId.getBytes(), PdfWriter.ALLOW_COPY,
						PdfWriter.STANDARD_ENCRYPTION_40);
				writer.createXmpMetadata();
				document.open();
				t = writer.getDirectContent().createTemplate(30, 16);
				total = Image.getInstance(t);
				total.setRole(PdfName.ARTIFACT);
				status = true;
			}
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (DocumentException ex) {
			ex.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return status;
	}

	public void closePdf() {
		document.close();
	}

	public void generatePdf(int freq) throws DocumentException, IOException {

		PdfPTable header = new PdfPTable(2);
		DecimalFormat df = new DecimalFormat("###.###");
		// set defaults
		header.setWidths(new int[] { 24, 10 });
		header.setTotalWidth(527);
		header.setLockedWidth(true);
		header.getDefaultCell().setFixedHeight(40);
		header.getDefaultCell().setBorder(Rectangle.BOTTOM);
		header.getDefaultCell().setBorderColor(BaseColor.LIGHT_GRAY);

		BaseFont bf = BaseFont.createFont("D:\\BDC_APP\\EStatement\\resources\\arial.ttf", BaseFont.IDENTITY_H,
				BaseFont.EMBEDDED);

		Font font = new Font(bf, 18);

		// add text
		PdfPCell text = new PdfPCell();
		text.setPaddingBottom(15);
		text.setPaddingLeft(10);
		text.setBorder(Rectangle.BOTTOM);
		text.setBorderColor(BaseColor.LIGHT_GRAY);
		text.setRunDirection(PdfWriter.RUN_DIRECTION_LTR);
//	            text.setHorizontalAlignment(Element.ALIGN_RIGHT);
		text.addElement(new Phrase(" ", font));
		text.addElement(new Phrase("Meeza Transactions", new Font(Font.FontFamily.HELVETICA, 14)));
		header.addCell(text);
//	            header.writeSelectedRows(0, -1, 34, 803, writer.getDirectContent());
		Image logo;
		try {
//			logo = Image.getInstance("resources/bdc-logo.jpg");
			logo = Image.getInstance("D:/BDC_APP/MezzaRegCustomers/resources/bdc-logo.jpg");
			header.addCell(logo);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		Paragraph paragraph = new Paragraph();
		// Main table
		PdfPTable mainTable = new PdfPTable(2);
		mainTable.setWidthPercentage(100.0f);
		mainTable.setPaddingTop(20f);
		// First table
//		PdfPCell firstTableCell = new PdfPCell();
//		firstTableCell.setBorder(PdfPCell.NO_BORDER);

		// start 1st table

		// end 1st table

		// Second table

		DecimalFormat formatter = new DecimalFormat("#,###,##0.000");// new DecimalFormat("#,###");
		System.out.println(formatter.format(4000000.2));

		// end 2nd table

		// table
		PdfPTable table = new PdfPTable(new float[] { 3, 1.5f, 1.5f, 3, 2.75f, 2});
		table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
		table.setWidthPercentage(100.0f);
		table.addCell(prepareTableCell("Card Name Holder", Rectangle.BOX, PdfPCell.ALIGN_CENTER));
		table.addCell(prepareTableCell("Transaction Desc.", Rectangle.BOX, PdfPCell.ALIGN_CENTER));
//		table.addCell(prepareTableCell("Transaction Status", Rectangle.BOX, PdfPCell.ALIGN_CENTER));
		table.addCell(prepareTableCell("Amount", Rectangle.BOX, PdfPCell.ALIGN_CENTER));
		table.addCell(prepareTableCell("Card Number", Rectangle.BOX, PdfPCell.ALIGN_CENTER));
		table.addCell(prepareTableCell("TXN Location", Rectangle.BOX, PdfPCell.ALIGN_CENTER));
		table.addCell(prepareTableCell("Date and Timing", Rectangle.BOX, PdfPCell.ALIGN_CENTER));
		table.completeRow();
		table.setHeaderRows(1);
		PdfPCell[] cells = table.getRow(0).getCells();
		for (int j = 0; j < cells.length; j++) {
			cells[j].setBackgroundColor(BaseColor.LIGHT_GRAY);
		}

		if (meezaTransactionList != null && meezaTransactionList.size() > 0) {
			for (MeezaTransactions meezaTransactions : meezaTransactionList) {
				/*
				 * table.addCell(prepareTableCell(String.valueOf(transaction.getTransactionDate(
				 * )), Rectangle.BOX, PdfPCell.ALIGN_CENTER));
				 * table.addCell(prepareTableCell(String.valueOf(transaction.getValueDate()),
				 * Rectangle.BOX, PdfPCell.ALIGN_CENTER)); table.addCell(
				 * prepareTableCell(String.valueOf(transaction.getDesc()), Rectangle.BOX,
				 * PdfPCell.ALIGN_CENTER)); if (transaction.getDebit() == 0) {
				 * table.addCell(" "); } else {
				 * table.addCell(prepareTableCell(String.valueOf(formatter.format(transaction.
				 * getDebit())), Rectangle.BOX, PdfPCell.ALIGN_CENTER)); } if
				 * (transaction.getCredit() == 0) { table.addCell(" "); } else {
				 * table.addCell(prepareTableCell(String.valueOf(formatter.format(transaction.
				 * getCredit())), Rectangle.BOX, PdfPCell.ALIGN_CENTER)); }
				 * table.addCell(prepareTableCell(String.valueOf(formatter.format(transaction.
				 * getTotalBalance())), Rectangle.BOX, PdfPCell.ALIGN_CENTER));
				 */
				table.addCell(prepareTableCell(String.valueOf(meezaTransactions.getCUST_NAME()), Rectangle.BOX,
						PdfPCell.ALIGN_CENTER));
				table.addCell(prepareTableCell(String.valueOf(meezaTransactions.getTXN_DESC()), Rectangle.BOX,
						PdfPCell.ALIGN_CENTER));
//				table.addCell(prepareTableCell(String.valueOf(meezaTransactions.getTXN_STATUS()), Rectangle.BOX,
//						PdfPCell.ALIGN_CENTER));
				table.addCell(prepareTableCell(String.valueOf(meezaTransactions.getTXN_AMOUNT()), Rectangle.BOX,
						PdfPCell.ALIGN_CENTER));
				table.addCell(prepareTableCell(String.valueOf(meezaTransactions.getCARD_NUMBER()), Rectangle.BOX,
						PdfPCell.ALIGN_CENTER));
				table.addCell(prepareTableCell(String.valueOf(meezaTransactions.getTXN_LOCATION()), Rectangle.BOX,
						PdfPCell.ALIGN_CENTER));
				table.addCell(prepareTableCell(String.valueOf(meezaTransactions.getTXN_DATE()), Rectangle.BOX,
						PdfPCell.ALIGN_CENTER));
			}
		} else {
			table.addCell("No Transactions");
			table.addCell("No Transactions");
			table.addCell("No Transactions");
			table.addCell("No Transactions");
			table.addCell("No Transactions");
			table.addCell("No Transactions");
		}

		///

		PdfPTable footer = new PdfPTable(3);
		footer.setWidths(new int[] { 20, 4, 2 });
		footer.setTotalWidth(527);
		footer.setLockedWidth(true);
		footer.getDefaultCell().setFixedHeight(40);
		footer.getDefaultCell().setBorder(Rectangle.TOP);
		footer.getDefaultCell().setBorderColor(BaseColor.LIGHT_GRAY);
//		footer.addCell(new Phrase("\u00A9 Banque Du Caire", new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD)));
		footer.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
//		footer.addCell(new Phrase(String.format("Page %d of " + writer.getPageNumber(), writer.getCurrentPageNumber()),
//				new Font(Font.FontFamily.HELVETICA, 8)));
		// add placeholder for total page count
		PdfPCell totalPageCount = new PdfPCell(total);
		totalPageCount.setBorder(Rectangle.TOP);
		totalPageCount.setBorderColor(BaseColor.LIGHT_GRAY);
		footer.addCell(totalPageCount);
		// write page
		PdfContentByte canvas = writer.getDirectContent();
		canvas.beginMarkedContentSequence(PdfName.ARTIFACT);
		footer.writeSelectedRows(0, -1, 34, 50, canvas);
		canvas.endMarkedContentSequence();

		mainTable.setSpacingAfter(10);
		table.setSpacingAfter(10);
		paragraph.add(header);
		paragraph.add("\n");
		// paragraph.add(table2);
		paragraph.add("\n");
		// paragraph.add(table3);
		paragraph.add("\n");
		paragraph.add(mainTable);
		paragraph.add(table);
		// paragraph.add(footerTable);

		document.add(paragraph);
	}

	private static Font getArabicFont() {
		BaseFont bf2;
		Font font = null;
		try {
			bf2 = BaseFont.createFont("D:\\BDC_APP\\EStatement\\resources\\arial.ttf", BaseFont.IDENTITY_H,
					BaseFont.EMBEDDED);
			font = new Font(bf2, 10);
		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return font;
	}

	private static PdfPCell prepareTableCell(String value, int border, int alignment) {
		Paragraph paragraph = new Paragraph();
		paragraph.setFont(getArabicFont());
		paragraph.add(value);
		paragraph.setAlignment(alignment);
		PdfPCell cell = new PdfPCell();
		cell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
		cell.setPaddingTop(0f);
		cell.setPaddingBottom(5f);
		cell.setMinimumHeight(10f);
		cell.setBorder(border);
		cell.addElement(paragraph);
		return cell;
	}
}
