/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bdc.estatement.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class LogUtil {

	private static Logger logger;
	private static Logger sysLogger;

	public static void initialize() {
		try {
			// Log
			System.out.println("Initializing Core LogUtil");
			if (logger == null) {
				ClassLoader loader = Thread.currentThread().getContextClassLoader();
				URL url = loader.getResource("log4j.properties");
				if (url != null)
					PropertyConfigurator.configure(url);
				else
					PropertyConfigurator.configure("log4j.properties");

				// Get logger
				logger = Logger.getRootLogger();
				sysLogger = Logger.getLogger(Defines.SYS_LOG_FILE);
				// Log
				if (logger != null)
					System.out.println("Core LogUtil Initialized Successfully");
				else
					System.out.println("Error in initialization core log");

				if (sysLogger != null)
					System.out.println("Core sysLogger Initialized Successfully");
				else
					System.out.println("Error in initialization core sysLogger");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void initialize(Logger logger) {
		try {
			// Log
			System.out.println("Initializing Core LogUtil");
			if (LogUtil.logger == null) {
				LogUtil.logger = logger;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void trace(String message) {
		if (logger != null) {
			logger.trace(formatMessage(message));
		}
	}

	public static void debug(String message) {
		if (logger != null) {
			logger.debug(formatMessage(message));
		}
	}

	public static void info(String message) {
		if (logger != null) {
			logger.info(formatMessage(message));
		}
	}

	public static void warn(String message) {
		if (logger != null) {
			logger.warn(formatMessage(message));
		}
	}

	public static void error(String message) {
		if (logger != null) {
			logger.error(formatMessage(message));
		}
	}

	public static void fatal(String message) {
		if (logger != null) {
			logger.fatal(formatMessage(message));
		}
	}

	public static void error(Throwable e) {
		// Log error type and message
		error(e.getClass().getSimpleName() + ": " + e.getMessage());

		// Log stack trace
		StringWriter stackTrace = new StringWriter();
		e.printStackTrace(new PrintWriter(stackTrace));
		error(stackTrace.toString());
	}

	public static void error(String msg, Throwable e) {
		// Log error type and message
		logger.error(e.getClass().getSimpleName() + ": " + e.getMessage() + " appMsg(" + msg + ")");
		System.out.println(e);
		System.out.println(e.getMessage());

		// Log stack trace
		StringWriter stackTrace = new StringWriter();
		e.printStackTrace(new PrintWriter(stackTrace));
		error(stackTrace.toString());
	}

	public static String formatMessage(String message, Object... args) {
		// Get caller
		StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
		int stackDepth = 3;
		String methodName = stackTrace[stackDepth].getMethodName();
		String className = stackTrace[stackDepth].getClassName();
		if (className.indexOf('.') > 0) {
			className = className.substring(className.lastIndexOf('.') + 1);
		}
		String caller = className + "." + methodName + "()";

		// Prepare format
		String format = String.format("%s : %s", caller, message);

		// Format message
		return String.format(format, args);
	}

	public static void printTimeLog(long startime) {
		logger.debug("Out in [" + (System.currentTimeMillis() - startime) + "] msec");
	}

	public static void printStartLog() {
		logger.debug("Starting");
	}

	public static void printSuccessLog() {
		logger.debug("Success");
	}

	public static void syslog(String message, Logger logger) {
		if (logger != null) {
			logger.debug(message);
		}
	}

	public static Logger getSysLogger() {
		try {
			ClassLoader loader = Thread.currentThread().getContextClassLoader();
			URL url = loader.getResource("log4j.properties");
			if (url != null)
				PropertyConfigurator.configure(url);
			else {
				PropertyConfigurator.configure("log4j.properties");
			}

			if (sysLogger != null) {
				sysLogger = Logger.getLogger(Defines.SYS_LOG_FILE);
			}
			return sysLogger;

		} catch (Exception e) {
			e.printStackTrace();
			return sysLogger;
		}
	}
}
