package com.bdc.estatement.util;

public class Defines {

	public static final String FLEX_TABLE_NAME = "V_Bdc_Ch_nobook";
	public static final String BDC_TABLE_NAME = "TRANSACTIONS";
	public static final String SYS_LOG_FILE = "SyslogFileAppender";
	public static String sysLogSeprator = "||";

	public static final int None = 0;
	public static final int Daily = 1;
	public static final int Weekly = 2;
	public static final int FortNightly = 3;
	public static final int Monthly = 4;
	public static final int Bi_Monthly = 5;
	public static final int Quarterly = 6;
	public static final int Half_Yearly = 7;
	public static final int Yearly = 8;

	public static final String NEW_REQUEST_STATUS = "Waiting for approve - new request";
	public static final String EDIT_STATUS = "Waiting for approve - Update";
	public static final String DELETE_STATUS = "Waiting for approve - Delete";
	public static final String APPROVED_STATUS = "Approved";

	public static String findFrequencyString(int freq) {
		String freqString = "";
		switch (freq) {
		case None:
			freqString = "None";
			break;
		case Daily:
			freqString = "Daily";
			break;
		case Weekly:
			freqString = "Daily";
			break;
		case FortNightly:
			freqString = "Fort Nightly";
			break;
		case Monthly:
			freqString = "Monthly";
			break;
		case Bi_Monthly:
			freqString = "BI Monthly";
			break;
		case Quarterly:
			freqString = "Quarterly";
			break;
		case Half_Yearly:
			freqString = "Half Annually";
			break;
		case Yearly:
			freqString = "Annually";
			break;
		}
		return freqString;
	}
}
