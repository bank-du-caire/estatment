package com.bdc.estatement.util;

import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.bdc.estatement.dao.SMSDao;
import com.bdc.estatement.model.SMSModel;
import com.bdc.estatement.model.TermDepositSMS;

public class SMSUtils {
	private static final String fieldStart = "\\$\\{";
	private static final String fieldEnd = "\\}";

	private static final String regex = fieldStart + "([^}]+)" + fieldEnd;
	private static final Pattern pattern = Pattern.compile(regex);

	private static String format(String format, Map<String, Object> objects)
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		Matcher m = pattern.matcher(format);
		String result = format;
		while (m.find()) {
			String[] found = m.group(1).split("\\.");
			Object o = objects.get(found[0]);
			Field f = o.getClass().getField(found[1]);
			String newVal = f.get(o).toString();
			result = result.replaceFirst(regex, newVal);
		}
		return result;
	}

	public static String populateSmsMessage(String msg, SMSModel smsObj) throws Exception {
		List<String> msgNames = new ArrayList<String>();
		msgNames.add("${sms.chequeNumber}");
		msgNames.add("${sms.currency}");
		msgNames.add("${sms.amount}");
		msgNames.add("${sms.bankName}");
		msgNames.add("${account_number}");
		msgNames.add("${account_number}");
		msgNames.add("${date}");
		msgNames.add("${sms.refrenceNumber}");
		msgNames.add("${sms.description}");
		String res = "";
		msg.replace("Dear Customer,", "Dear Customer,\r\n");
		for (String msgName : msgNames) {
			if (msg.contains(msgName) && msgName.contains("${sms.chequeNumber}")) {

				res = msg.replace(msgName, smsObj.getChequeNumber());
			} else if (msg.contains(msgName) && msgName.contains("${sms.currency}")) {
				res = msg.replace(msgName, smsObj.getCurrency());
			} else if (msg.contains(msgName) && msgName.contains("${sms.amount}")) {

//				DecimalFormat formatter = new DecimalFormat("#,###");

				DecimalFormat formatter = new DecimalFormat("#,###,##0.000");
//				formatter.format(smsObj.getAmount());
				res = msg.replace(msgName, formatter.format(smsObj.getAmount()));

			} else if (msg.contains(msgName) && msgName.contains("${sms.bankName}")) {
				res = msg.replace(msgName, smsObj.getBankName());
			} else if (msg.contains(msgName) && msgName.contains("${account_number}")) {
				String accNO = smsObj.getAccountNumber().substring(smsObj.getAccountNumber().trim().length() - 4,
						smsObj.getAccountNumber().trim().length());
				System.out.println("accNO:[" + accNO + "]");
				res = msg.replace(msgName, accNO);
			} else if (msg.contains(msgName) && msgName.contains("${date}")) {
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
				res = msg.replace(msgName, dateFormat.format(smsObj.getValueDate()).toString());
			} else if (msg.contains(msgName) && msgName.contains("${sms.refrenceNumber}")) {
				String refNO = smsObj.getRefrenceNumber().substring(smsObj.getRefrenceNumber().length() - 4,
						smsObj.getRefrenceNumber().length());
				res = msg.replace(msgName, refNO);
			} else if (msg.contains(msgName) && msgName.contains("${sms.description}")) {
				res = msg.replace(msgName, smsObj.getDescription());
			}
			if (!res.isEmpty())
				msg = res;
		}
		return res;
	}

	private static String getLastFourDigit(String str) {
		String lastFourDigit = "";
		if (str.length() > 4) {
			lastFourDigit = str.substring(str.length() - 4);
		} else {
			lastFourDigit = str;
		}
		return lastFourDigit;
	}

	public static String prepareMessage(String messageName, SMSModel smsObj) throws Exception {
		if (smsObj.getIsReversed() == 1 && messageName.equals("Cheques collected")) {
			messageName = "Cheques collected Reversal";
		} else if (smsObj.getIsReversed() == 0 && messageName.equals("Cheques collected")) {
			messageName = "Cheques collected";
		} else if (messageName.equals("Deposit") && smsObj.getIsReversed() == 0 && smsObj.getTransfer() == null
				&& smsObj.getTransfer() != null && smsObj.getTransfer().equals("Deposit ")) {
			messageName = "Counter Cash Deposit";
		} else if (messageName.equals("Deposit") && smsObj.getIsReversed() == 1 && smsObj.getTransfer() == null
				&& smsObj.getTransfer() != null && smsObj.getTransfer().equals("Deposit ")) {
			messageName = "Counter Cash Deposit Reversal";
		} else if (messageName.equals("Deposit") && smsObj.getIsReversed() == 0 && smsObj.getTransfer() != null
				&& (smsObj.getTransfer().equals("Deposit") || smsObj.getTransfer().equals("Internal Transfer")
						|| smsObj.getTransfer().equals("Ach Transfer")
						|| smsObj.getTransfer().equals("Swift Transfer"))) {
			messageName = "Inward - Internal transfer";
		} else if (messageName.equals("Deposit") && smsObj.getIsReversed() == 1 && smsObj.getTransfer() != null
				&& (smsObj.getTransfer().equals("Deposit") || smsObj.getTransfer().equals("Internal Transfer")
						|| smsObj.getTransfer().equals("Ach Transfer")
						|| smsObj.getTransfer().equals("Swift Transfer"))) {
			messageName = "Inward - Internal transfer Reversal";
		}
		String msg = SMSDao.getSmsMessageTemplate(messageName);
		return SMSUtils.populateSmsMessage(msg, smsObj);
	}

	public static String prepareMessage(TermDepositSMS termDepositSMS) {
		String msg = " تم شراء " + termDepositSMS.getNameProduct() + " " + termDepositSMS.getRateDep() + " % بمبلغ "
				+ termDepositSMS.getBalPrincipal() + " " + termDepositSMS.getCurrancy() + " فى "
				+ termDepositSMS.getDepDate();
//		StringBuilder builder = new StringBuilder("تم شراء ");
//		builder.append(termDepositSMS.getNameProduct());
//		builder.append(" عائد شهرى ");
//		builder.append(termDepositSMS.getRateDep());
//		builder.append("% بمبلغ ");
//		builder.append(termDepositSMS.getBalPrincipal());
//		builder.append(" ");
//		builder.append(termDepositSMS.getCurrancy());
//		builder.append(" في ");
//		builder.append(termDepositSMS.getDepDate());
		return msg;
	}

	public static String sendMessage(String msg, String mobile) {
		return Cequens.sendingSMS(msg, mobile);
	}

}
