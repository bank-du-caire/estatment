package com.bdc.estatement.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.bdc.estatement.model.AccountPackage;
import com.bdc.estatement.model.Transaction;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;

public class EstatmentPdfTemplate {

	private Document document;
	private AccountPackage account;
	private List<Transaction> transactions;
	PdfWriter writer = null;
	private PdfTemplate t;
	private Image total;

	public EstatmentPdfTemplate(AccountPackage account, List<Transaction> transactions) {
		document = new Document();
		this.account = account;
		this.transactions = transactions;
	}

	public EstatmentPdfTemplate() {
		document = new Document();
	}

	public boolean openPdf() {
		boolean status = false;
		try {
//			File pdfFile = new File("D://BDC_APP//EStatement//Attachements//" + account.getCustomerId() + " - "
//					+ new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + ".pdf");
			File pdfFile = new File("D://BDC_APP//EStatement//Attachements//" + account.getAccountNumber() + " - "
					+ new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + ".pdf");
			if (pdfFile != null) {
				writer = PdfWriter.getInstance(document, new FileOutputStream(pdfFile));
				writer.setEncryption("concretepage".getBytes(), account.getCustomerId().getBytes(),
						PdfWriter.ALLOW_COPY, PdfWriter.STANDARD_ENCRYPTION_40);
				writer.createXmpMetadata();
//				HeaderFooterPageEvent event = new HeaderFooterPageEvent();
//			    writer.setPageEvent(event);
				document.open();
				t = writer.getDirectContent().createTemplate(30, 16);
				total = Image.getInstance(t);
				total.setRole(PdfName.ARTIFACT);
				status = true;
			}
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (DocumentException ex) {
			ex.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return status;
	}

	public void closePdf() {
		document.close();
	}

	public void generatePdf(int freq) throws DocumentException, IOException {

		PdfPTable header = new PdfPTable(2);
		DecimalFormat df = new DecimalFormat("###.###");
		// set defaults
		header.setWidths(new int[] { 24, 10 });
		header.setTotalWidth(527);
		header.setLockedWidth(true);
		header.getDefaultCell().setFixedHeight(40);
		header.getDefaultCell().setBorder(Rectangle.BOTTOM);
		header.getDefaultCell().setBorderColor(BaseColor.LIGHT_GRAY);

		BaseFont bf = BaseFont.createFont("D:/BDC_APP/EStatement/resources/arial.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

		Font font = new Font(bf, 18);

		// add text
		PdfPCell text = new PdfPCell();
		text.setPaddingBottom(15);
		text.setPaddingLeft(10);
		text.setBorder(Rectangle.BOTTOM);
		text.setBorderColor(BaseColor.LIGHT_GRAY);
		text.setRunDirection(PdfWriter.RUN_DIRECTION_LTR);
//	            text.setHorizontalAlignment(Element.ALIGN_RIGHT);
		text.addElement(new Phrase("     كشف حساب", font));
		text.addElement(new Phrase("Estatment Account", new Font(Font.FontFamily.HELVETICA, 14)));
		header.addCell(text);
//	            header.writeSelectedRows(0, -1, 34, 803, writer.getDirectContent());
		Image logo;
		try {
//			logo = Image.getInstance("resources/bdc-logo.jpg");
			logo = Image.getInstance("D:/BDC_APP/EStatement/resources/bdc-logo.jpg");
			header.addCell(logo);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		Paragraph paragraph = new Paragraph();
		// Main table
		PdfPTable mainTable = new PdfPTable(2);
		mainTable.setWidthPercentage(100.0f);
		mainTable.setPaddingTop(20f);
		// First table
//		PdfPCell firstTableCell = new PdfPCell();
//		firstTableCell.setBorder(PdfPCell.NO_BORDER);

		// start 1st table

		PdfPTable table2 = new PdfPTable(new float[] { 3, 3, 3, 3, 3, 3 });

		table2.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
		table2.setWidthPercentage(100.0f);

		table2.addCell(prepareTableCell("Customer ID", Rectangle.BOX, PdfPCell.ALIGN_CENTER));
		table2.addCell(prepareTableCell("Account Description", Rectangle.BOX, PdfPCell.ALIGN_CENTER));
		table2.addCell(prepareTableCell("Mailing Address", Rectangle.BOX, PdfPCell.ALIGN_CENTER));
		table2.addCell(prepareTableCell("Branch Name", Rectangle.BOX, PdfPCell.ALIGN_CENTER));
		table2.addCell(prepareTableCell("From", Rectangle.BOX, PdfPCell.ALIGN_CENTER));
		table2.addCell(prepareTableCell("TO", Rectangle.BOX, PdfPCell.ALIGN_CENTER));

		table2.completeRow();
		table2.setHeaderRows(1);
		PdfPCell[] cells2 = table2.getRow(0).getCells();
		for (int j = 0; j < cells2.length; j++) {
			cells2[j].setBackgroundColor(BaseColor.LIGHT_GRAY);
		}
		if (account != null) {

			table2.addCell(prepareTableCell((account.getCustomerId() != null ? account.getCustomerId() : " "),
					Rectangle.BOX, PdfPCell.ALIGN_CENTER));
			table2.addCell(prepareTableCell((account.getFullName() != null ? account.getFullName() : " "),
					Rectangle.BOX, PdfPCell.ALIGN_CENTER));
			table2.addCell(prepareTableCell((account.getAddress() != null ? account.getAddress() : " "), Rectangle.BOX,
					PdfPCell.ALIGN_CENTER));
			table2.addCell(prepareTableCell((account.getBranchName() != null ? account.getBranchName() : " "),
					Rectangle.BOX, PdfPCell.ALIGN_CENTER));
			table2.addCell(prepareTableCell((account.getFromDate() != null ? account.getFromDate() : " "),
					Rectangle.BOX, PdfPCell.ALIGN_CENTER));
			if (freq == Defines.Daily) {
				table2.addCell(prepareTableCell((account.getFromDate() != null ? account.getFromDate() : " "),
						Rectangle.BOX, PdfPCell.ALIGN_CENTER));
			} else {
				table2.addCell(prepareTableCell((account.getToDate() != null ? account.getToDate() : " "),
						Rectangle.BOX, PdfPCell.ALIGN_CENTER));
			}
			table2.addCell(prepareTableCell((account.getToDate() != null ? account.getToDate() : " "), Rectangle.BOX,
					PdfPCell.ALIGN_CENTER));

		} else {
			table2.addCell(" ");
			table2.addCell(" ");
			table2.addCell(" ");
			table2.addCell(" ");
			table2.addCell(" ");
			table2.addCell(" ");
		}

		// end 1st table

		// Second table

		DecimalFormat formatter = new DecimalFormat("#,###,##0.000");// new DecimalFormat("#,###");
		System.out.println(formatter.format(4000000.2));

		PdfPTable table3 = new PdfPTable(new float[] { 3, 3, 3, 3 });

		table3.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
		table3.setWidthPercentage(100.0f);

		table3.addCell(prepareTableCell("Account NO", Rectangle.BOX, PdfPCell.ALIGN_CENTER));
		table3.addCell(prepareTableCell("Account Currency", Rectangle.BOX, PdfPCell.ALIGN_CENTER));
		table3.addCell(prepareTableCell("Account Type", Rectangle.BOX, PdfPCell.ALIGN_CENTER));
		table3.addCell(prepareTableCell("Opening Balance", Rectangle.BOX, PdfPCell.ALIGN_CENTER));
		table3.completeRow();
		table3.setHeaderRows(1);
		PdfPCell[] cells3 = table3.getRow(0).getCells();
		for (int j = 0; j < cells3.length; j++) {
			cells3[j].setBackgroundColor(BaseColor.LIGHT_GRAY);
		}
		if (account != null) {
			table3.addCell(prepareTableCell((account.getAccountNumber() != null ? account.getAccountNumber() : " "),
					Rectangle.BOX, PdfPCell.ALIGN_CENTER));
			table3.addCell(prepareTableCell((account.getCurrecy() != null ? account.getCurrecy() : " "), Rectangle.BOX,
					PdfPCell.ALIGN_CENTER));
			table3.addCell(prepareTableCell((account.getProductName() != null ? account.getProductName() : " "),
					Rectangle.BOX, PdfPCell.ALIGN_CENTER));
			table3.addCell(prepareTableCell(String.valueOf(formatter.format(account.getStartBalance())), Rectangle.BOX,
					PdfPCell.ALIGN_CENTER));
		} else {
			table3.addCell(" ");
			table3.addCell(" ");
			table3.addCell(" ");
			table3.addCell(" ");
		}

		// end 2nd table

		// table
		PdfPTable table = new PdfPTable(new float[] { 3, 3, 4.5f, 1.5f, 1.5f, 1.5f });

		table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
		table.setWidthPercentage(100.0f);
		table.addCell(prepareTableCell("Transaction Date", Rectangle.BOX, PdfPCell.ALIGN_CENTER));
		table.addCell(prepareTableCell("Value Date", Rectangle.BOX, PdfPCell.ALIGN_CENTER));
		table.addCell(prepareTableCell("Description", Rectangle.BOX, PdfPCell.ALIGN_CENTER));
		table.addCell(prepareTableCell("Debit", Rectangle.BOX, PdfPCell.ALIGN_CENTER));
		table.addCell(prepareTableCell("Credit", Rectangle.BOX, PdfPCell.ALIGN_CENTER));
		table.addCell(prepareTableCell("Balance", Rectangle.BOX, PdfPCell.ALIGN_CENTER));
		table.completeRow();
		table.setHeaderRows(1);
		PdfPCell[] cells = table.getRow(0).getCells();
		for (int j = 0; j < cells.length; j++) {
			cells[j].setBackgroundColor(BaseColor.LIGHT_GRAY);
		}

		if (transactions != null && transactions.size() > 0) {
			for (Transaction transaction : transactions) {
				table.addCell(prepareTableCell(String.valueOf(transaction.getTransactionDate()), Rectangle.BOX,
						PdfPCell.ALIGN_CENTER));
				table.addCell(prepareTableCell(String.valueOf(transaction.getValueDate()), Rectangle.BOX,
						PdfPCell.ALIGN_CENTER));
				table.addCell(
						prepareTableCell(String.valueOf(transaction.getDesc()), Rectangle.BOX, PdfPCell.ALIGN_CENTER));
				if (transaction.getDebit() == 0) {
					table.addCell(" ");
				} else {
					table.addCell(prepareTableCell(String.valueOf(formatter.format(transaction.getDebit())),
							Rectangle.BOX, PdfPCell.ALIGN_CENTER));
				}
				if (transaction.getCredit() == 0) {
					table.addCell(" ");
				} else {
					table.addCell(prepareTableCell(String.valueOf(formatter.format(transaction.getCredit())),
							Rectangle.BOX, PdfPCell.ALIGN_CENTER));
				}
				table.addCell(prepareTableCell(String.valueOf(formatter.format(transaction.getTotalBalance())),
						Rectangle.BOX, PdfPCell.ALIGN_CENTER));
			}
		} else {
			table.addCell(" ");
			table.addCell(" ");
			table.addCell(" ");
			table.addCell(" ");
			table.addCell(" ");
			table.addCell(" ");
		}

		///

		PdfPTable footerTable = new PdfPTable(new float[] { 3, 3, 3, 3, 3, 3 });

		footerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
		footerTable.setWidthPercentage(100.0f);

		footerTable.addCell(prepareTableCell("Number Of Debits", Rectangle.BOX, PdfPCell.ALIGN_CENTER));
		footerTable.addCell(prepareTableCell("Total Debits", Rectangle.BOX, PdfPCell.ALIGN_CENTER));
		footerTable.addCell(prepareTableCell("Number Of Credits", Rectangle.BOX, PdfPCell.ALIGN_CENTER));
		footerTable.addCell(prepareTableCell("Total Credits", Rectangle.BOX, PdfPCell.ALIGN_CENTER));
//		footerTable.addCell(prepareTableCell("Available Balance", Rectangle.BOX, PdfPCell.ALIGN_CENTER));
		footerTable.addCell(prepareTableCell("Closing Balance", Rectangle.BOX, PdfPCell.ALIGN_CENTER));
		footerTable.addCell(prepareTableCell("Available Balance", Rectangle.BOX, PdfPCell.ALIGN_CENTER));
//		PdfPCell cell5 = prepareTableCell("Opening Balance", Rectangle.BOX, PdfPCell.ALIGN_CENTER);
//		cell5.setBackgroundColor(BaseColor.LIGHT_GRAY);
//		footerTable.addCell(cell5);
//		footerTable.completeRow();
		footerTable.setHeaderRows(1);
		PdfPCell[] footercells = footerTable.getRow(0).getCells();
		for (int j = 0; j < cells.length; j++) {
			footercells[j].setBackgroundColor(BaseColor.LIGHT_GRAY);
		}

		footerTable.addCell(
				prepareTableCell(String.valueOf(account.getDebitCount()), Rectangle.BOX, PdfPCell.ALIGN_CENTER));
		footerTable.addCell(prepareTableCell(String.valueOf(formatter.format(account.getTotalDebit())), Rectangle.BOX,
				PdfPCell.ALIGN_CENTER));
		footerTable.addCell(
				prepareTableCell(String.valueOf(account.getCreditCount()), Rectangle.BOX, PdfPCell.ALIGN_CENTER));
		footerTable.addCell(prepareTableCell(String.valueOf(formatter.format(account.getTotalCredit())), Rectangle.BOX,
				PdfPCell.ALIGN_CENTER));
		footerTable.addCell(prepareTableCell(formatter.format(account.getAvailableBalance()), Rectangle.BOX,
				PdfPCell.ALIGN_CENTER));
		footerTable.addCell(prepareTableCell(String.valueOf(formatter.format(account.getBookBalance())), Rectangle.BOX,
				PdfPCell.ALIGN_CENTER));
//		footerTable.addCell(prepareTableCell(String.valueOf(df.format(account.getStartBalance())), Rectangle.BOX,
//				PdfPCell.ALIGN_CENTER));

		PdfPTable footer = new PdfPTable(3);
		footer.setWidths(new int[] { 20, 4, 2 });
		footer.setTotalWidth(527);
		footer.setLockedWidth(true);
		footer.getDefaultCell().setFixedHeight(40);
		footer.getDefaultCell().setBorder(Rectangle.TOP);
		footer.getDefaultCell().setBorderColor(BaseColor.LIGHT_GRAY);
//		footer.addCell(new Phrase("\u00A9 Banque Du Caire", new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD)));
		footer.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
//		footer.addCell(new Phrase(String.format("Page %d of " + writer.getPageNumber(), writer.getCurrentPageNumber()),
//				new Font(Font.FontFamily.HELVETICA, 8)));
		// add placeholder for total page count
		PdfPCell totalPageCount = new PdfPCell(total);
		totalPageCount.setBorder(Rectangle.TOP);
		totalPageCount.setBorderColor(BaseColor.LIGHT_GRAY);
		footer.addCell(totalPageCount);
		// write page
		PdfContentByte canvas = writer.getDirectContent();
		canvas.beginMarkedContentSequence(PdfName.ARTIFACT);
		footer.writeSelectedRows(0, -1, 34, 50, canvas);
		canvas.endMarkedContentSequence();

		mainTable.setSpacingAfter(10);
		table.setSpacingAfter(10);
		paragraph.add(header);
		paragraph.add("\n");
		paragraph.add(table2);
		paragraph.add("\n");
		paragraph.add(table3);
		paragraph.add("\n");
		paragraph.add(mainTable);
		paragraph.add(table);
		paragraph.add(footerTable);

		document.add(paragraph);
	}

	private static Font getArabicFont() {
		BaseFont bf2;
		Font font = null;
		try {
			bf2 = BaseFont.createFont("D:/BDC_APP/EStatement/resources/arial.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			font = new Font(bf2, 10);
		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return font;
	}

	private static PdfPCell prepareTableCell(String value, int border, int alignment) {
		Paragraph paragraph = new Paragraph();
		paragraph.setFont(getArabicFont());
		paragraph.add(value);
		paragraph.setAlignment(alignment);
		PdfPCell cell = new PdfPCell();
		cell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
		cell.setPaddingTop(0f);
		cell.setPaddingBottom(5f);
		cell.setMinimumHeight(10f);
		cell.setBorder(border);
		cell.addElement(paragraph);
		return cell;
	}
}