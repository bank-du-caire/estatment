package com.bdc.estatement.util;

import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Cequens {

	public static String sendingSMS(String message, String mobile) {
		String mess = message.replace("\n", "").replace("\r", "");
		String mob = "";
		mob = mobile.trim();
		System.out.println("[" + mess + "]");
		System.out.println("[" + mob + "]");
		String accessToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6IjY3MGVkMjUwZGIxMmFmOGQwMTk0YTU4NzU3MzVmOGY3MTc3YjlkMmM1M2E2YjY3OGJkYTAwOWViMGQwNWU3MmEyNWRkMTFiY2I3MGYwNmQ1YzUzNDc3MmM2M2MzZWI1MzAwYzc2OTI5ZTVlZGMzNDk3OTQyNmNmZjc4MWViN2I2YzY4MmU0ZTQ2NmUzNjI3NDc2MmJjMzI0ZGIwNDcyMzMiLCJpYXQiOjE1NjEyODQ5NTEsImV4cCI6MzEzOTE2NDk1MX0.tNlyEkSB1cAkKWGqhrr8o_sWbwrE9S7GNoIuGBfc2Y0";
		String headers = "{\"messageText\": \"" + mess
				+ "\",\"senderName\": \"BDC\",\"messageType\":\"text\",\"recipients\":\"" + mob + "\"}";
		String postURL = "http://api.cequens.com/cequens/api/v1/messaging";
		System.out.println(headers);
		return sendingPostRequest(headers, postURL, accessToken);
	}

	public static String sendingPostRequest(String headers, String postURL, String accessToken) {
		LogUtil.info("inside sendingPostRequest()");
		String smsID = "";
		OkHttpClient client = new OkHttpClient();
		MediaType mediaType = MediaType.parse("application/json");
		RequestBody body = RequestBody.create(mediaType, headers);
		System.out.println("body::: " + headers);
		Request request = null;
		LogUtil.info("preparing post request to send ceqens SMS");
		request = new Request.Builder().url(postURL).post(body).addHeader("Accept", "application/json")
				.addHeader("Content-Type", "application/json")
				.addHeader("Authorization", String.format("Bearer %s", accessToken))
				.addHeader("Cache-Control", "no-cache").build();
		String jsonString = "";
		System.out.println(request.toString());
		JSONObject jsonObject = null;
		try {
			Builder builder = client.newBuilder();
//			Proxy proxyTest = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.248.4.21", 9090));
//			builder.proxy(proxyTest);
			LogUtil.info("Sending Cequens Post Request");
			Response response = builder.build().newCall(request).execute();
			LogUtil.info(response.toString());
			if (response.code() != 200) {
				return null;
			}
			jsonString = response.body().string();
			LogUtil.info("Cequens Body Response is: " + jsonString);
			jsonObject = new JSONObject(jsonString);
			String data = jsonObject.getString("data");
			JSONObject dataJson = new JSONObject(data);
			String sentSMSIDs = dataJson.getString("SentSMSIDs");
			sentSMSIDs = sentSMSIDs.replace("[", "");
			sentSMSIDs = sentSMSIDs.replace("]", "");
			System.out.println("sentSMSIDs: [" + sentSMSIDs + "]");
			JSONObject SMSIdJson = new JSONObject(sentSMSIDs);
			smsID = SMSIdJson.getString("SMSId");
			System.out.println("SMS ID: [" + smsID + "]");
		} catch (IOException e) {
			LogUtil.error(e.toString());
			e.printStackTrace();
		} catch (JSONException e) {
			LogUtil.error(e.toString());
			e.printStackTrace();
		}
		return smsID;
	}
}
