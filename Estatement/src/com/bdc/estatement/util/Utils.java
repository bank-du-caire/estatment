package com.bdc.estatement.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import com.jcraft.jsch.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.FromStringTerm;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;

import org.apache.poi.hssf.record.crypto.Biff8EncryptionKey;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import com.bdc.estatement.dao.TransactionsDao;
import com.bdc.estatement.model.Account;
import com.bdc.estatement.model.AccountPackage;
import com.bdc.estatement.model.MeezaRegCustomers;
import com.bdc.estatement.model.MeezaTransactions;
import com.bdc.estatement.model.SwiftMsg;
import com.bdc.estatement.model.Transaction;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;

import microsoft.exchange.webservices.data.core.ExchangeService;
import microsoft.exchange.webservices.data.core.enumeration.misc.ExchangeVersion;
import microsoft.exchange.webservices.data.core.service.item.EmailMessage;
import microsoft.exchange.webservices.data.credential.ExchangeCredentials;
import microsoft.exchange.webservices.data.credential.WebCredentials;
import microsoft.exchange.webservices.data.property.complex.MessageBody;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;

public class Utils {

	public static Date addDays(Date date, int days) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, days); // minus number would decrement the days
		return cal.getTime();
	}

	// smsFlag = 1 and anything else for estatment
	public static List<Account> syncFlexCubeChanges(int smsFlag) {
		TransactionsDao.syncNonePerformingAccounts();
		List<Account> allAccounts = new ArrayList<Account>();
		List<Account> accounts = new ArrayList<Account>();
		if (smsFlag == 1)
			accounts = TransactionsDao.getSMSAccountsByToday();
		else
			accounts = TransactionsDao.getAccountsByToday();
		List<Account> flxAccounts = TransactionsDao.getAccountTypeList();
		for (Account account : accounts) {

			if ((account.getNonePerformingAccount() != null
					&& !account.getNonePerformingAccount().equalsIgnoreCase("N"))
					|| (account.getNonePerformingCustomer() != null
							&& (account.getNonePerformingCustomer().contains("1008")
									|| account.getNonePerformingCustomer().contains("10081")
									|| account.getNonePerformingCustomer().contains("10082")
									|| account.getNonePerformingCustomer().contains("10083")
									|| account.getNonePerformingCustomer().contains("1009")
									|| account.getNonePerformingCustomer().contains("10091")
									|| account.getNonePerformingCustomer().contains("10092")
									|| account.getNonePerformingCustomer().contains("10093")
									|| account.getNonePerformingCustomer().contains("1010")
									|| account.getNonePerformingCustomer().contains("10101")
									|| account.getNonePerformingCustomer().contains("10102")
									|| account.getNonePerformingCustomer().contains("10103")))) {
				// allAccounts.remove(account);
				continue;
//				if (allAccounts.size() == 0)
//					break;
//				else
//					continue;
			} else
				allAccounts.add(account);
			boolean found = false;
			for (Account flxAccount : flxAccounts) {

				if (account.getAccountNumber().equals(flxAccount.getAccountNumber())) {
					found = true;
					if (account.getSmsFlag() == 1 && flxAccount.getSmsFlag() == 0) {
						account.setSmsFlag(flxAccount.getSmsFlag());
						TransactionsDao.updateAccountWithSMSandEstatementFlags(account);
						allAccounts.remove(account);
					}
					if (account.getEstatementFlag() == 1 && flxAccount.getEstatementFlag() == 0) {
						account.setEstatementFlag(flxAccount.getEstatementFlag());
						TransactionsDao.updateAccountWithSMSandEstatementFlags(account);
						allAccounts.remove(account);
					}
				}
			}
//			if (!found) {
//				account.setSmsFlag(0);
//				account.setEstatementFlag(0);
//				TransactionsDao.updateAccountWithSMSandEstatementFlags(account);
//				allAccounts.remove(account);
//			}
		}
		return allAccounts;
	}

	public static boolean generatePDF(AccountPackage account, List<Transaction> transactions) {
		/*
		 * String REPORT_NAME = "report/test.rpt"; ReportClientDocument reportClientDoc
		 * = new ReportClientDocument(); try {
		 *
		 * reportClientDoc.open(REPORT_NAME, 0); //
		 * reportClientDoc.refreshReportDocument();
		 * reportClientDoc.getDataDefController() .getParameterFieldController();
		 * POJOResultSetFactory factory = new POJOResultSetFactory( Transaction.class);
		 * POJOResultSet resultSet = factory.createResultSet(transactions); String
		 * tableAlias = "Transaction";
		 * reportClientDoc.getDatabaseController().setDataSource(resultSet, tableAlias,
		 * tableAlias);
		 *
		 * List<AccountPackage> accounts = new ArrayList<AccountPackage>();
		 * accounts.add(account);
		 *
		 * POJOResultSetFactory accountFactory = new POJOResultSetFactory(
		 * AccountPackage.class); POJOResultSet rs =
		 * accountFactory.createResultSet(accounts); String accountTableAlias =
		 * "AccountPackage";
		 *
		 * reportClientDoc.getDatabaseController().setDataSource(rs, accountTableAlias,
		 * accountTableAlias); ByteArrayInputStream byteArrayInputStream =
		 * (ByteArrayInputStream) reportClientDoc
		 * .getPrintOutputController().export(ReportExportFormat.PDF); String
		 * EXPORT_OUTPUT = "D://BDC_APP//EStatement//Attachements//" +
		 * account.getCustomerId() + " - " + new
		 * SimpleDateFormat("yyyy-MM-dd").format(new Date()) + "temp.pdf";
		 * writeToFileSystem(byteArrayInputStream, EXPORT_OUTPUT);
		 * setPDFPassword(account); deleteTempFile(new File(EXPORT_OUTPUT)); return
		 * true; } catch (Exception e) { e.printStackTrace(); return false; }
		 */
		return false;
	}

	public static void addColorToSheet(HSSFWorkbook wb, short index) {
		HSSFPalette palette = wb.getCustomPalette();
		// 238, (byte) 64, (byte) 46
		byte red = (byte) (238 & 0XFF), blue = (byte) (64 & 0XFF), green = (byte) (46 & 0XFF);
		palette.setColorAtIndex(index, red, blue, green);
	}

	public static Font getCellStyleForColor(HSSFWorkbook wb) {
		HSSFCellStyle style = wb.createCellStyle();
		short index = (short) (IndexedColors.values().length + 1);
		addColorToSheet(wb, index);
		Font f = style.getFont(wb);
		f.setColor(index);
//		style.setFillForeground(index);
		return f;
	}

	public static boolean exportToExcel(AccountPackage account, List<Transaction> transactions, String fromDate,
			String toDate) {
		String[] columns = { "Post Date", "Value Date", "Description", "Debit", "Credit", "Balance" };
		HSSFWorkbook workbook = new HSSFWorkbook();
		CreationHelper createHelper = workbook.getCreationHelper();
		// Create a Sheet
		Sheet sheet = workbook.createSheet(account.getAccountNumber());
		// Create a Font for styling header cells
		Font headerFont = workbook.createFont();
		headerFont = getCellStyleForColor(workbook);
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 10);
		// Create a CellStyle with the font
		CellStyle headerCellStyle = workbook.createCellStyle();
		headerFont.setBold(true);
		headerCellStyle.setFont(headerFont);

		Row customerName = sheet.createRow(0);
		Cell customerNameCell = customerName.createCell(0);
		customerNameCell.setCellStyle(headerCellStyle);
		customerNameCell.setCellValue("Customer Name");

		Row accountNo = sheet.createRow(1);
		Cell accountNoCell = accountNo.createCell(0);
		accountNoCell.setCellStyle(headerCellStyle);
		accountNoCell.setCellValue("Account Number");

		Row currency = sheet.createRow(2);
		Cell currencyCell = currency.createCell(0);
		currencyCell.setCellStyle(headerCellStyle);
		currencyCell.setCellValue("currency");

		Row from = sheet.createRow(3);
		Cell fromCell = from.createCell(0);
		fromCell.setCellStyle(headerCellStyle);
		fromCell.setCellValue("From");

		Row to = sheet.createRow(4);
		Cell toCell = to.createCell(0);
		toCell.setCellStyle(headerCellStyle);
		toCell.setCellValue("To");

		DecimalFormat df = new DecimalFormat("#,###,##0.000");// ("###.###");
		Row openingBalance = sheet.createRow(5);
		Cell openingBalanceCell = openingBalance.createCell(0);
		openingBalanceCell.setCellStyle(headerCellStyle);
		openingBalanceCell.setCellValue("Opening Balance");

		Row endingBalance = sheet.createRow(6);
		Cell endingBalanceCell = endingBalance.createCell(0);
		endingBalanceCell.setCellStyle(headerCellStyle);
		endingBalanceCell.setCellValue("Ending Balance");
		/*
		 *
		 *
		 *
		 *
		 */

		Row headerRow = sheet.createRow(7);
		for (int i = 0; i < columns.length; i++) {
			Cell cell = headerRow.createCell(i);
			cell.setCellValue(columns[i]);
			cell.setCellStyle(headerCellStyle);
		}

		/*
		 *
		 *
		 *
		 *
		 *
		 */

		HSSFCellStyle style = workbook.createCellStyle();
		style.setFillForegroundColor(IndexedColors.BLUE.index);
		style.setFillBackgroundColor(IndexedColors.BLUE.index);
		Font f = workbook.createFont();
		f.setColor(IndexedColors.BLACK.index);
		f.setBold(true);
		style.setFont(f);
//		valueCellStyle.setFont(f);

		Cell customerNameValueCell = customerName.createCell(1);
		customerNameValueCell.setCellValue(account.getFullName());
		customerNameValueCell.setCellStyle(style);

		Cell accountNoValueCell = accountNo.createCell(1);
		accountNoValueCell.setCellValue(account.getAccountNumber());
		accountNoValueCell.setCellStyle(style);

		Cell currencyValueCell = currency.createCell(1);
		currencyValueCell.setCellValue(account.getCurrecy());
		currencyValueCell.setCellStyle(style);

		CellStyle dateCellStyle = workbook.createCellStyle();
		dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));
		dateCellStyle.setFont(f);
		Cell fromCell1 = from.createCell(1);
		fromCell1.setCellStyle(dateCellStyle);
		fromCell1.setCellValue(fromDate);

		Cell toCell1 = to.createCell(1);
		toCell1.setCellStyle(dateCellStyle);
		toCell1.setCellValue(toDate);

		Cell openingBalanceValueCell = openingBalance.createCell(1);
		openingBalanceValueCell.setCellValue(df.format(account.getStartBalance()));
		openingBalanceValueCell.setCellStyle(style);

		Cell endingBalanceValueCell = endingBalance.createCell(1);
		endingBalanceValueCell.setCellValue(df.format(account.getEndBalance()));
		endingBalanceValueCell.setCellStyle(style);

		int rowNum = 8;

		HSSFDataFormat format = workbook.createDataFormat();
//		Font font = workbook.createFont();//
//		font.setFontHeightInPoints((short) 10);
//		font.setColor(IndexedColors.BLACK.index);
//		dateCellStyle.setFont(font);
		for (Transaction t : transactions) {
			Row row = sheet.createRow(rowNum++);
			Cell postDateCell = row.createCell(0);
			postDateCell.setCellValue(t.getTransactionDate());
			postDateCell.setCellStyle(dateCellStyle);
			Cell valueDateCell = row.createCell(1);
			valueDateCell.setCellValue(t.getValueDate());
			valueDateCell.setCellStyle(dateCellStyle);
			row.createCell(2).setCellValue(t.getDesc());
			if (t.getDebit() == 0) {
				row.createCell(3).setCellValue("");
			} else {
				row.createCell(3).setCellValue(df.format(t.getDebit()));
			}
			row.getCell(3).getCellStyle().setDataFormat(format.getFormat("0.0"));
			if (t.getCredit() == 0) {
				row.createCell(4).setCellValue("");
			} else {
				row.createCell(4).setCellValue(df.format(t.getCredit()));
			}
			row.getCell(4).getCellStyle().setDataFormat(format.getFormat("0.0"));

			row.createCell(5).setCellValue(df.format(t.getTotalBalance()));
			row.getCell(5).getCellStyle().setDataFormat(format.getFormat("0.0"));

			row.getCell(0).setCellStyle(style);
			row.getCell(1).setCellStyle(style);
			row.getCell(2).setCellStyle(style);
			row.getCell(3).setCellStyle(style);
			row.getCell(4).setCellStyle(style);
			row.getCell(5).setCellStyle(style);

		}
		for (int i = 0; i < columns.length; i++) {
			sheet.autoSizeColumn(i);
		}

		try {
			Biff8EncryptionKey.setCurrentUserPassword(account.getCustomerId());
			workbook.writeProtectWorkbook(Biff8EncryptionKey.getCurrentUserPassword(), "");
//			FileOutputStream fileOut = new FileOutputStream("D://BDC_APP//EStatement//Attachements//"
//					+ account.getCustomerId() + " - " + new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + ".xls");
			FileOutputStream fileOut = new FileOutputStream(
					"D://BDC_APP//EStatement//Attachements//" + account.getAccountNumber() + " - "
							+ new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + ".xls");
			workbook.write(fileOut);
			fileOut.close();
			workbook.close();
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	static <T> FileOutputStream exportToExcel(List<T> objects) {
		FileOutputStream fileOut = null;
		try {
			HSSFWorkbook workbook = new HSSFWorkbook();
			CreationHelper createHelper = workbook.getCreationHelper();
			Sheet sheet = workbook.createSheet();
			// Create a Font for styling header cells
			Font headerFont = workbook.createFont();
			headerFont.setBold(true);
			headerFont.setFontHeightInPoints((short) 14);
			headerFont.setColor(IndexedColors.RED.getIndex());
			// Create Cell Style for formatting Date
			CellStyle dateCellStyle = workbook.createCellStyle();
			dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));
			// Create a CellStyle with the font
			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			int rowNum = 1;
			for (Object object : objects) {
				Field[] fields = object.getClass().getDeclaredFields();
				Row headerRow = sheet.createRow(0);
				for (int i = 0; i < fields.length; i++) {
					Cell cell = headerRow.createCell(i);
					cell.setCellValue(fields[i].getName());
					cell.setCellStyle(headerCellStyle);
				}
				Row row = sheet.createRow(rowNum++);
				for (int i = 0; i < fields.length; i++) {
					Field field = fields[i];
					field.setAccessible(true);
					row.createCell(i).setCellValue(String.valueOf(field.get(object)));
				}

				for (int i = 0; i < fields.length; i++) {
					sheet.autoSizeColumn(i);
				}
			}
			fileOut = new FileOutputStream("reports test.xls");
			workbook.write(fileOut);
			fileOut.close();
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fileOut;
	}

	public static boolean sendingMail(Account account, String sendDate) {
		boolean status = false;
		if (account.getEmail().contains(";")) {
			String[] emails = account.getEmail().split(";");
			for (String email : emails) {
				if (!email.isEmpty()) {
					status = sendMail(email.trim(), account.getAccountNumber(), account.getCustomerCode(), sendDate);
				}
			}
		} else {
			status = sendMail(account.getEmail(), account.getAccountNumber(), account.getCustomerCode(), sendDate);
		}

		return status;
	}

	public static boolean sendMail(String email, String accountNo, String CustomerCode, String sendDate) {
		ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2010_SP1);
//		ExchangeCredentials credentials = new WebCredentials("soa", "BdcS0@2016");
		ExchangeCredentials credentials = new WebCredentials("eStatement.BDC", "Password1");
		service.setCredentials(credentials);

		try {
//			service.setUrl(new URI("https://owa.banqueducaire.com/EWS/Exchange.asmx"));
			service.setUrl(new URI("https://mail.bdc.com.eg/EWS/Exchange.asmx"));
			// https://mail.bdc.com.eg/ews/Exchange.asmx
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		EmailMessage msg = null;
		try {
//			setSigniture(service);
			msg = new EmailMessage(service);
			msg.setSubject("Estatement");

			StringBuilder message = new StringBuilder();
			message.append("Dear Customer, ");
			message.append("<p>");
			message.append("Thank you for using Banque du Caire Corporate e-statement service.");
			message.append("<p>");
			message.append("Kindly find attached your E-Statement issued on " + sendDate);
			message.append("<p>");
			message.append(
					"In order to access your E-Statement, please make sure your device is equipped/compatible with the following settings:");
			message.append("<p>");
			message.append("1- For PC and Laptop users: Adobe reader and Microsoft Excel");
			message.append("<p>");
			message.append("2- This E-Statement is encrypted with a \"Shared Secret\" for your protection");
			message.append("<p>");
			message.append(
					"Your password is designed to secure your data and to enable you to view the attached statement file with full privacy.");
			message.append("<p>");
			message.append(
					"For more information, pls. refer to your designated Relationship Manager or BdC Corporate Customer Service Team");
			message.append("<p>");
			message.append("Thank you for your continued trust and for banking with us. ");
			message.append("<p>");
			message.append("Best regards");
			message.append("<p>");
			message.append("Banque du Caire");
			message.append("<p>");
			// message.append("<img src=\"resources\\bdc-logo.jpg\">");

			String img = "<img src=\"D:\\BDC_APP\\EStatement\\resources\\bdc-logo.svg\" width=\"350\" height=\"80\">";

			img = "<svg version=\"1.0\" xmlns=\"http://www.w3.org/2000/svg\" width=\"700.000000pt\" height=\"206.000000pt\" viewBox=\"0 0 700.000000 206.000000\" preserveAspectRatio=\"xMidYMid meet\">"
					+ "<g transform=\"translate(0.000000,206.000000) scale(0.100000,-0.100000)\" fill=\"#000000\" stroke=\"none\">"
					+ "<path d=\"M2077 1572 c-16 -18 -16 -20 1 -35 18 -16 20 -16 35 1 16 18 16 20"
					+ "-1 35 -18 16 -20 16 -35 -1z\"></path>"
					+ "<path d=\"M2166 1572 c-15 -18 -15 -20 2 -35 24 -22 29 -21 42 4 9 15 8 24 -1"
					+ "35 -15 18 -22 18 -43 -4z\"></path>"
					+ "<path d=\"M4395 1570 c-18 -19 -18 -22 -2 -37 16 -16 18 -16 37 3 20 20 20 21"
					+ "2 37 -18 16 -20 16 -37 -3z\"></path>"
					+ "<path d=\"M4487 1572 c-16 -18 -16 -20 1 -35 24 -22 29 -21 41 2 8 14 6 22 -8"
					+ "35 -16 15 -19 15 -34 -2z\"></path>"
					+ "<path d=\"M6565 1560 c-18 -19 -18 -22 -2 -37 16 -16 18 -16 37 3 20 20 20 21"
					+ "2 37 -18 16 -20 16 -37 -3z\"></path>"
					+ "<path d=\"M4181 1551 c-12 -8 -12 -33 -1 -171 l13 -162 121 6 c149 7 145 7 288"
					+ "0 l116 -6 7 31 c4 18 5 67 4 111 -2 43 0 82 3 85 11 11 -28 110 -43 110 -17 0"
					+ "-17 -20 -1 -128 7 -43 12 -98 12 -123 l0 -44 -90 0 c-49 0 -90 2 -90 5 0 2 11"
					+ "21 25 41 42 62 26 138 -36 169 -42 22 -81 11 -119 -31 -39 -45 -41 -99 -5"
					+ "-142 14 -17 25 -33 25 -36 0 -3 -43 -6 -95 -6 l-95 0 0 68 c0 91 -12 232 -19"
					+ "232 -3 0 -12 -4 -20 -9z m334 -136 c14 -13 25 -35 25 -49 0 -24 -51 -86 -72"
					+ "-86 -13 0 -88 75 -88 88 0 6 12 24 26 41 33 39 74 41 109 6z\"></path>"
					+ "<path d=\"M4806 1552 c-17 -4 -18 -10 -13 -62 4 -33 10 -109 13 -170 6 -94 9"
					+ "-110 23 -108 14 3 16 22 12 152 -4 172 -8 193 -35 188z\"></path>"
					+ "<path d=\"M6278 1552 c-22 -4 -23 -47 -4 -180 9 -68 10 -95 2 -97 -59 -20 -189"
					+ "-25 -661 -25 -364 0 -542 4 -564 11 -18 7 -38 24 -47 41 -15 28 -14 46 2 101"
					+ "3 9 1 17 -3 17 -5 0 -17 -23 -25 -51 -22 -69 -8 -116 42 -141 32 -17 81 -18"
					+ "590 -18 463 0 566 3 620 16 43 10 66 11 68 4 2 -6 47 -10 116 -10 97 0 117 3"
					+ "138 19 24 20 25 20 41 0 14 -16 30 -19 105 -19 l89 0 10 78 c8 68 7 82 -10"
					+ "122 -24 55 -25 56 -38 43 -7 -7 -6 -33 5 -86 26 -126 31 -120 -73 -115 -49 1"
					+ "-92 5 -94 8 -3 3 1 35 8 72 11 62 11 67 -6 71 -24 6 -25 5 -33 -73 -4 -36 -13"
					+ "-68 -19 -72 -7 -4 -58 -8 -114 -8 l-102 0 -5 53 c-3 28 -9 95 -13 147 -7 83"
					+ "-10 95 -25 92z\"></path>"
					+ "<path d=\"M3902 1524 c-9 -15 -1 -22 51 -48 75 -38 107 -89 107 -173 l0 -53"
					+ "-42 0 c-24 0 -53 3 -65 6 l-22 6 21 28 c58 80 -11 193 -98 160 -62 -24 -87"
					+ "-112 -44 -158 11 -12 20 -24 20 -27 0 -3 -326 -5 -725 -5 l-725 0 0 24 c0 31"
					+ "-39 156 -48 156 -4 0 -13 -4 -21 -9 -11 -7 -7 -25 19 -94 37 -97 36 -118 -12"
					+ "-173 -35 -40 -82 -64 -123 -64 -45 0 -26 -18 26 -25 65 -9 91 8 134 88 l30 57"
					+ "727 0 c448 0 737 4 752 10 17 6 35 6 52 0 14 -6 56 -13 93 -16 66 -6 68 -6 74"
					+ "17 12 43 7 143 -7 171 -25 47 -88 116 -112 122 -12 3 -29 9 -37 12 -9 3 -19"
					+ "-2 -25 -12z m20 -121 c35 -32 12 -123 -32 -123 -21 0 -80 55 -80 74 0 8 9 25"
					+ "21 40 24 30 63 34 91 9z\"></path>"
					+ "<path d=\"M5597 1498 c-27 -22 -37 -62 -22 -91 12 -24 11 -27 -7 -33 -11 -3"
					+ "-16 -10 -12 -14 5 -5 37 -1 72 8 45 12 62 21 62 34 0 14 -5 15 -30 8 -23 -6"
					+ "-35 -4 -51 9 -32 26 -19 53 23 51 52 -3 55 -1 28 20 -29 23 -40 24 -63 8z\"></path>"
					+ "<path d=\"M2125 1490 c-3 -5 0 -18 7 -29 10 -17 9 -22 -9 -38 -56 -47 -85 -121"
					+ "-67 -168 16 -41 105 -57 169 -31 42 18 46 47 14 116 -11 25 -22 53 -24 62 -4"
					+ "19 -66 98 -77 98 -4 0 -9 -4 -13 -10z m77 -146 c30 -64 30 -67 13 -80 -28 -21"
					+ "-88 -17 -119 7 -24 19 -25 22 -11 48 19 37 64 91 76 91 6 0 24 -30 41 -66z\"></path>"
					+ "<path d=\"M6720 1114 c-11 -12 -10 -18 3 -32 16 -15 18 -15 34 0 13 14 14 20 3"
					+ "32 -7 9 -16 16 -20 16 -4 0 -13 -7 -20 -16z\"></path>"
					+ "<path d=\"M2123 953 c9 -2 23 -2 30 0 6 3 -1 5 -18 5 -16 0 -22 -2 -12 -5z\"></path>"
					+ "<path d=\"M5548 953 c7 -3 16 -2 19 1 4 3 -2 6 -13 5 -11 0 -14 -3 -6 -6z\"></path>"
					+ "<path d=\"M5618 953 c7 -3 16 -2 19 1 4 3 -2 6 -13 5 -11 0 -14 -3 -6 -6z\"></path>"
					+ "<path d=\"M2113 933 c9 -2 23 -2 30 0 6 3 -1 5 -18 5 -16 0 -22 -2 -12 -5z\"></path>"
					+ "<path d=\"M5578 933 c6 -2 18 -2 25 0 6 3 1 5 -13 5 -14 0 -19 -2 -12 -5z\"></path>"
					+ "<path d=\"M5434 898 l-19 -23 23 19 c12 11 22 21 22 23 0 8 -8 2 -26 -19z\"></path>"
					+ "<path d=\"M2478 843 c7 -3 16 -2 19 1 4 3 -2 6 -13 5 -11 0 -14 -3 -6 -6z\"></path>"
					+ "<path d=\"M2883 843 c9 -2 23 -2 30 0 6 3 -1 5 -18 5 -16 0 -22 -2 -12 -5z\"></path>"
					+ "<path d=\"M3233 843 c9 -2 23 -2 30 0 6 3 -1 5 -18 5 -16 0 -22 -2 -12 -5z\"></path>"
					+ "<path d=\"M4013 843 c9 -2 23 -2 30 0 6 3 -1 5 -18 5 -16 0 -22 -2 -12 -5z\"></path>"
					+ "<path d=\"M4543 843 c9 -2 23 -2 30 0 6 3 -1 5 -18 5 -16 0 -22 -2 -12 -5z\"></path>"
					+ "<path d=\"M5873 843 c9 -2 25 -2 35 0 9 3 1 5 -18 5 -19 0 -27 -2 -17 -5z\"></path>"
					+ "<path d=\"M6683 843 c9 -2 23 -2 30 0 6 3 -1 5 -18 5 -16 0 -22 -2 -12 -5z\"></path>"
					+ "<path d=\"M2425 829 c-4 -6 -5 -12 -2 -15 2 -3 7 2 10 11 7 17 1 20 -8 4z\"></path>"
					+ "<path d=\"M2940 836 c0 -2 8 -10 18 -17 15 -13 16 -12 3 4 -13 16 -21 21 -21" + "13z\"></path>"
					+ "<path d=\"M3341 824 c0 -11 3 -14 6 -6 3 7 2 16 -1 19 -3 4 -6 -2 -5 -13z\"></path>"
					+ "<path d=\"M3754 725 c0 -66 1 -92 3 -57 2 34 2 88 0 120 -2 31 -3 3 -3 -63z\"></path>"
					+ "<path d=\"M4844 750 c0 -52 1 -74 3 -47 2 26 2 68 0 95 -2 26 -3 4 -3 -48z\"></path>"
					+ "<path d=\"M6344 715 c0 -71 1 -99 3 -62 2 37 2 96 0 130 -2 34 -3 4 -3 -68z\"></path>"
					+ "<path d=\"M3248 823 c7 -3 16 -2 19 1 4 3 -2 6 -13 5 -11 0 -14 -3 -6 -6z\"></path>"
					+ "<path d=\"M4469 813 c-13 -16 -12 -17 4 -4 9 7 17 15 17 17 0 8 -8 3 -21 -13z\"></path>"
					+ "<path d=\"M4558 823 c7 -3 16 -2 19 1 4 3 -2 6 -13 5 -11 0 -14 -3 -6 -6z\"></path>"
					+ "<path d=\"M3374 635 c0 -104 2 -146 3 -92 2 54 2 139 0 190 -1 51 -3 7 -3 -98z\"></path>"
					+ "<path d=\"M3533 750 c0 -41 2 -58 4 -37 2 20 2 54 0 75 -2 20 -4 3 -4 -38z\"></path>"
					+ "<path d=\"M3723 745 c0 -44 2 -61 4 -37 2 23 2 59 0 80 -2 20 -4 1 -4 -43z\"></path>"
					+ "<path d=\"M5064 705 c0 -66 1 -92 3 -57 2 34 2 88 0 120 -2 31 -3 3 -3 -63z\"></path>"
					+ "<path d=\"M2123 783 c9 -2 23 -2 30 0 6 3 -1 5 -18 5 -16 0 -22 -2 -12 -5z\"></path>"
					+ "<path d=\"M5422 765 c0 -16 2 -22 5 -12 2 9 2 23 0 30 -3 6 -5 -1 -5 -18z\"></path>"
					+ "<path d=\"M5951 764 c0 -11 3 -14 6 -6 3 7 2 16 -1 19 -3 4 -6 -2 -5 -13z\"></path>"
					+ "<path d=\"M2103 763 c9 -2 25 -2 35 0 9 3 1 5 -18 5 -19 0 -27 -2 -17 -5z\"></path>"
					+ "<path d=\"M2981 754 c0 -11 3 -14 6 -6 3 7 2 16 -1 19 -3 4 -6 -2 -5 -13z\"></path>"
					+ "<path d=\"M2793 670 c0 -47 2 -66 4 -42 2 23 2 61 0 85 -2 23 -4 4 -4 -43z\"></path>"
					+ "<path d=\"M2528 733 c7 -3 16 -2 19 1 4 3 -2 6 -13 5 -11 0 -14 -3 -6 -6z\"></path>"
					+ "<path d=\"M2953 665 c0 -44 2 -61 4 -37 2 23 2 59 0 80 -2 20 -4 1 -4 -43z\"></path>"
					+ "<path d=\"M5908 733 c6 -2 18 -2 25 0 6 3 1 5 -13 5 -14 0 -19 -2 -12 -5z\"></path>"
					+ "<path d=\"M3978 723 c23 -2 59 -2 80 0 20 2 1 4 -43 4 -44 0 -61 -2 -37 -4z\"></path>"
					+ "<path d=\"M6648 723 c23 -2 59 -2 80 0 20 2 1 4 -43 4 -44 0 -61 -2 -37 -4z\"></path>"
					+ "<path d=\"M5799 693 c-13 -16 -12 -17 4 -4 9 7 17 15 17 17 0 8 -8 3 -21 -13z\"></path>"
					+ "<path d=\"M5782 640 c0 -14 2 -19 5 -12 2 6 2 18 0 25 -3 6 -5 1 -5 -13z\"></path>"
					+ "<path d=\"M5435 620 c10 -11 20 -20 23 -20 3 0 -3 9 -13 20 -10 11 -20 20 -23"
					+ "20 -3 0 3 -9 13 -20z\"></path>"
					+ "<path d=\"M4681 604 c0 -11 3 -14 6 -6 3 7 2 16 -1 19 -3 4 -6 -2 -5 -13z\"></path>"
					+ "<path d=\"M2415 590 c10 -11 20 -20 23 -20 3 0 -3 9 -13 20 -10 11 -20 20 -23"
					+ "20 -3 0 3 -9 13 -20z\"></path>"
					+ "<path d=\"M4460 606 c0 -2 8 -10 18 -17 15 -13 16 -12 3 4 -13 16 -21 21 -21" + "13z\"></path>"
					+ "<path d=\"M2526 588 c3 -5 10 -6 15 -3 13 9 11 12 -6 12 -8 0 -12 -4 -9 -9z\"></path>"
					+ "<path d=\"M2768 573 c6 -2 18 -2 25 0 6 3 1 5 -13 5 -14 0 -19 -2 -12 -5z\"></path>"
					+ "<path d=\"M6158 573 c6 -2 18 -2 25 0 6 3 1 5 -13 5 -14 0 -19 -2 -12 -5z\"></path>"
					+ "<path d=\"M6348 573 c6 -2 18 -2 25 0 6 3 1 5 -13 5 -14 0 -19 -2 -12 -5z\"></path>" + "</g>"
					+ "</svg>";
			img = "<html>" + "<body>" + "<h1>My first SVG</h1>" + "<img src='data:image/svg+xml;utf8,"
					+ "<svg width=\"100\" height=\"100\">"
					+ "<circle cx=\"50\" cy=\"50\" r=\"40\" stroke=\"green\" stroke-width=\"4\" fill=\"yellow\" />"
					+ "</svg> alt=\"\" />" + "</body>" + "</html>";

			img = "<img src='data:image/svg+xml;utf8,<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" id=\"Layer_1\" x=\"0px\" y=\"0px\" viewBox=\"0 0 100 100\" enable-background=\"new 0 0 100 100\" xml:space=\"preserve\" height=\"100px\" width=\"100px\">"
					+ "<g>"
					+ "    <path d=\"M28.1,36.6c4.6,1.9,12.2,1.6,20.9,1.1c8.9-0.4,19-0.9,28.9,0.9c6.3,1.2,11.9,3.1,16.8,6c-1.5-12.2-7.9-23.7-18.6-31.3   c-4.9-0.2-9.9,0.3-14.8,1.4C47.8,17.9,36.2,25.6,28.1,36.6z\"/>"
					+ "    <path d=\"M70.3,9.8C57.5,3.4,42.8,3.6,30.5,9.5c-3,6-8.4,19.6-5.3,24.9c8.6-11.7,20.9-19.8,35.2-23.1C63.7,10.5,67,10,70.3,9.8z\"/>"
					+ "    <path d=\"M16.5,51.3c0.6-1.7,1.2-3.4,2-5.1c-3.8-3.4-7.5-7-11-10.8c-2.1,6.1-2.8,12.5-2.3,18.7C9.6,51.1,13.4,50.2,16.5,51.3z\"/>"
					+ "    <path d=\"M9,31.6c3.5,3.9,7.2,7.6,11.1,11.1c0.8-1.6,1.7-3.1,2.6-4.6c0.1-0.2,0.3-0.4,0.4-0.6c-2.9-3.3-3.1-9.2-0.6-17.6   c0.8-2.7,1.8-5.3,2.7-7.4c-5.2,3.4-9.8,8-13.3,13.7C10.8,27.9,9.8,29.7,9,31.6z\"/>"
					+ "    <path d=\"M15.4,54.7c-2.6-1-6.1,0.7-9.7,3.4c1.2,6.6,3.9,13,8,18.5C13,69.3,13.5,61.8,15.4,54.7z\"/>"
					+ "    <path d=\"M39.8,57.6C54.3,66.7,70,73,86.5,76.4c0.6-0.8,1.1-1.6,1.7-2.5c4.8-7.7,7-16.3,6.8-24.8c-13.8-9.3-31.3-8.4-45.8-7.7   c-9.5,0.5-17.8,0.9-23.2-1.7c-0.1,0.1-0.2,0.3-0.3,0.4c-1,1.7-2,3.4-2.9,5.1C28.2,49.7,33.8,53.9,39.8,57.6z\"/>"
					+ "    <path d=\"M26.2,88.2c3.3,2,6.7,3.6,10.2,4.7c-3.5-6.2-6.3-12.6-8.8-18.5c-3.1-7.2-5.8-13.5-9-17.2c-1.9,8-2,16.4-0.3,24.7   C20.6,84.2,23.2,86.3,26.2,88.2z\"/>"
					+ "    <path d=\"M30.9,73c2.9,6.8,6.1,14.4,10.5,21.2c15.6,3,32-2.3,42.6-14.6C67.7,76,52.2,69.6,37.9,60.7C32,57,26.5,53,21.3,48.6   c-0.6,1.5-1.2,3-1.7,4.6C24.1,57.1,27.3,64.5,30.9,73z\"/>"
					+ "</g>" + "</svg>' alt=\"\" />";
			message.append(img);

			msg.setBody(MessageBody.getMessageBodyFromText("Sent from Estatement Service"));
//			msg.setBody(MessageBody.getMessageBodyFromText(message.toString()));
			msg.getToRecipients().add(email);

			File pdf = new File("D://BDC_APP//EStatement//Attachements//" + accountNo + " - "
					+ new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + ".pdf");
			File excel = new File("D://BDC_APP//EStatement//Attachements//" + accountNo + " - "
					+ new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + ".xls");
			File rar = new File("D://BDC_APP//EStatement//Attachements//" + accountNo + " - "
					+ new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + ".zip");

			if (pdf.exists() && !pdf.isDirectory()) {
				msg.getAttachments().addFileAttachment(pdf.getAbsolutePath());
			}
			if (excel.exists() && !excel.isDirectory()) {
				msg.getAttachments().addFileAttachment(excel.getAbsolutePath());
			}
			if (rar.exists() && !rar.isDirectory()) {
				msg.getAttachments().addFileAttachment(rar.getAbsolutePath());
			}

			// msg.getAttachments().addFileAttachment(
			// "D://BDC_APP//EStatement//Attachements//"
			// + account.getAccountNumber() + ".rar");
			msg.send();
			System.out.println("Mail Sent ...");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean sendMail(String emails, String messageBody) {

		if (emails.contains(";")) {
			String[] allMails = emails.split(";");
			int count = 0;
			for (String email : allMails) {
				ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2010_SP1);
//				ExchangeCredentials credentials = new WebCredentials("soa", "BdcS0@2016");
				ExchangeCredentials credentials = new WebCredentials("eStatement.BDC", "Password1");
				service.setCredentials(credentials);

				try {
//					service.setUrl(new URI("https://owa.banqueducaire.com/EWS/Exchange.asmx"));
					service.setUrl(new URI("https://mail.bdc.com.eg/EWS/Exchange.asmx"));
				} catch (URISyntaxException e) {
					e.printStackTrace();
				}
				EmailMessage msg = null;
				try {
					msg = new EmailMessage(service);
					// msg.getFrom().setAddress("SOA@banqueducaire.com") ;
					msg.setSubject("Estatement-SMS Failure");
					msg.setBody(MessageBody.getMessageBodyFromText(messageBody));

					msg.getToRecipients().add(email.trim());

					msg.send();
					count++;
					System.out.println("Mail Sent to : " + email);

				} catch (Exception e) {
					e.printStackTrace();
					return false;
				}
			}
			if (count == allMails.length)
				return true;
		}
		return false;

	}

	public static boolean sendMail(String fromUser, String frompassword, String emails, String messageBody,
			String subject) {
		List<String> allMails = new ArrayList<String>();
		if (emails.contains(";")) {
			String[] allSplitMails = emails.split(";");
			for (int i = 0; i < allSplitMails.length; i++)
				allMails.add(allSplitMails[i]);
		} else
			allMails.add(emails);
		int count = 0;
		for (String email : allMails) {
			ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2010_SP1);
//			ExchangeCredentials credentials = new WebCredentials("soa", "BdcS0@2016");
//			ExchangeCredentials credentials = new WebCredentials("eStatement.BDC", "Password1");
//			ExchangeCredentials credentials = new WebCredentials("HostToHost", "P@$$w0rd");
			ExchangeCredentials credentials = new WebCredentials(fromUser, frompassword);
			service.setCredentials(credentials);

			try {
//				service.setUrl(new URI("https://owa.banqueducaire.com/EWS/Exchange.asmx"));
				service.setUrl(new URI("https://mail.bdc.com.eg/EWS/Exchange.asmx"));

			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
			EmailMessage msg = null;
			try {
				msg = new EmailMessage(service);

				// msg.getFrom().setAddress("SOA@banqueducaire.com") ;
				msg.setSubject(subject);
				msg.setBody(MessageBody.getMessageBodyFromText(messageBody));

				msg.getToRecipients().add(email.trim());

				msg.send();
				count++;
				System.out.println("Mail Sent to : " + email);

			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		if (count == allMails.size())
			return true;
		return false;

	}

	public static void setPDFPassword(AccountPackage account) {
		try {
			PdfReader reader = new PdfReader("D://BDC_APP//EStatement//Attachements//" + account.getCustomerId() + " - "
					+ new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + "temp.pdf");
			PdfStamper stamper = new PdfStamper(reader,
					new FileOutputStream("D://BDC_APP//EStatement//Attachements//" + account.getCustomerId() + " - "
							+ new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + ".pdf"));
			stamper.setEncryption(account.getCustomerId().getBytes(), account.getFullName().getBytes(),
					PdfWriter.ALLOW_PRINTING, PdfWriter.ENCRYPTION_AES_128);
			stamper.close();
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void deleteTempFile(File file) {
		if (file.delete()) {
			LogUtil.info("Temp file deleted Successfully ...");
		}

	}

	public static Date addingDays(Date currentDate, int days) {
		String DATE_FORMAT = "yyyy-MM-dd";
		DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);

		try {
			Date safeDate = new Date(currentDate.getTime());
			safeDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			LocalDateTime.ofInstant(safeDate.toInstant(), ZoneId.systemDefault());
			LocalDateTime localDateTime = safeDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
			localDateTime = localDateTime.plusDays(days);
			Date currentDatePlusDays = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
			System.out.println("\nOutput : " + dateFormat.format(currentDatePlusDays));
			return currentDatePlusDays;
		} catch (UnsupportedOperationException e) {
			LogUtil.error(e);
		}
		return null;
	}

	public static String claculateNextSendDate(int frequancy, String lastSentDate) {
		String nextSendDate = "";//
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdfOut = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Date date = sdf.parse(lastSentDate);
			Calendar c = Calendar.getInstance();
			if (!lastSentDate.equals("")) {
				try {
					c.setTime(sdf.parse(lastSentDate));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				if (frequancy == Defines.None) {

				} else if (frequancy == Defines.Daily) {

//					c.add(Calendar.DATE, 1);
//					nextSendDate = sdfOut.format(c.getTime());
					Date d = addingDays(c.getTime(), 1);
//					nextSendDate = d.getDay() + "/" + d.getMonth() + "/" + d.getYear();
					nextSendDate = sdfOut.format(addingDays(c.getTime(), 1));
					String test = new SimpleDateFormat("dd/MM/yyyy").format(d);
					nextSendDate = test;
				} else if (frequancy == Defines.Weekly) {
					c.add(Calendar.DATE, 7);
					nextSendDate = sdfOut.format(c.getTime());
				} else if (frequancy == Defines.FortNightly) {
					c.add(Calendar.DATE, 14);
					nextSendDate = sdfOut.format(c.getTime());
				} else if (frequancy == Defines.Monthly) {
					c.add(Calendar.MONTH, 1);
					nextSendDate = sdfOut.format(c.getTime());
				} else if (frequancy == Defines.Bi_Monthly) {
					c.add(Calendar.MONTH, 2);
					nextSendDate = sdfOut.format(c.getTime());
				} else if (frequancy == Defines.Quarterly) {
					c.add(Calendar.MONTH, 3);
					nextSendDate = sdfOut.format(c.getTime());
				} else if (frequancy == Defines.Half_Yearly) {
					c.add(Calendar.MONTH, 6);
					nextSendDate = sdfOut.format(c.getTime());
				} else if (frequancy == Defines.Yearly) {
					c.add(Calendar.YEAR, 1);
					nextSendDate = sdfOut.format(c.getTime());
				}
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return nextSendDate;
	}

	public static String claculateFromDate(int frequancy, String lastSentDate) {
		String nextSendDate = "";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdfOut = new SimpleDateFormat("dd/MM/YYYY");
		Calendar c = Calendar.getInstance();
		if (!lastSentDate.equals("")) {
			try {
				c.setTime(sdf.parse(lastSentDate));
			} catch (ParseException e) {
				e.printStackTrace();
			}

			if (frequancy == Defines.None) {

			} else if (frequancy == Defines.Daily) {
				c.add(Calendar.DATE, -1);
				nextSendDate = sdfOut.format(c.getTime());
			} else if (frequancy == Defines.Weekly) {
				c.add(Calendar.DATE, -7);
				nextSendDate = sdfOut.format(c.getTime());
			} else if (frequancy == Defines.FortNightly) {
				c.add(Calendar.DATE, -14);
				nextSendDate = sdfOut.format(c.getTime());
			} else if (frequancy == Defines.Monthly) {
				c.add(Calendar.MONTH, -1);
				nextSendDate = sdfOut.format(c.getTime());
			} else if (frequancy == Defines.Bi_Monthly) {
				c.add(Calendar.MONTH, -2);
				nextSendDate = sdfOut.format(c.getTime());
			} else if (frequancy == Defines.Quarterly) {
				c.add(Calendar.MONTH, -3);
				nextSendDate = sdfOut.format(c.getTime());
			} else if (frequancy == Defines.Half_Yearly) {
				c.add(Calendar.MONTH, -6);
				nextSendDate = sdfOut.format(c.getTime());
			} else if (frequancy == Defines.Yearly) {
				c.add(Calendar.YEAR, -1);
				nextSendDate = sdfOut.format(c.getTime());
			}
		}

		return nextSendDate;
	}

	public static String claculateFromDateSQLDormat(int frequancy, String lastSentDate) {
		String nextSendDate = "";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar c = Calendar.getInstance();
		if (!lastSentDate.equals("")) {
			try {
				c.setTime(sdf.parse(lastSentDate));
			} catch (ParseException e) {
				e.printStackTrace();
			}

			if (frequancy == Defines.None) {

			} else if (frequancy == Defines.Daily) {
				c.add(Calendar.DATE, -1);
				nextSendDate = sdf.format(c.getTime());
			} else if (frequancy == Defines.Weekly) {
				c.add(Calendar.DATE, -7);
				nextSendDate = sdf.format(c.getTime());
			} else if (frequancy == Defines.FortNightly) {
				c.add(Calendar.DATE, -14);
				nextSendDate = sdf.format(c.getTime());
			} else if (frequancy == Defines.Monthly) {
				c.add(Calendar.MONTH, -1);
				nextSendDate = sdf.format(c.getTime());
			} else if (frequancy == Defines.Bi_Monthly) {
				c.add(Calendar.MONTH, -2);
				nextSendDate = sdf.format(c.getTime());
			} else if (frequancy == Defines.Quarterly) {
				c.add(Calendar.MONTH, -3);
				nextSendDate = sdf.format(c.getTime());
			} else if (frequancy == Defines.Half_Yearly) {
				c.add(Calendar.MONTH, -6);
				nextSendDate = sdf.format(c.getTime());
			} else if (frequancy == Defines.Yearly) {
				c.add(Calendar.YEAR, -1);
				nextSendDate = sdf.format(c.getTime());
			}
		}
		return nextSendDate;
	}

	private static void writeToFileSystem(ByteArrayInputStream byteArrayInputStream, String exportFile)
			throws Exception {

		// Use the Java I/O libraries to write the exported content to the file
		// system.
		byte byteArray[] = new byte[byteArrayInputStream.available()];

		// Create a new file that will contain the exported result.
		File file = new File(exportFile);
		FileOutputStream fileOutputStream = new FileOutputStream(file);

		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(byteArrayInputStream.available());
		int x = byteArrayInputStream.read(byteArray, 0, byteArrayInputStream.available());

		byteArrayOutputStream.write(byteArray, 0, x);
		byteArrayOutputStream.writeTo(fileOutputStream);

		// Close streams.
		byteArrayInputStream.close();
		byteArrayOutputStream.close();
		fileOutputStream.close();
	}

	public static String getStatementFrequencyString(int freq) {
		String freqString = "";
		switch (freq) {
		case Defines.Daily:
			freqString = "Daily";
			break;
		case Defines.Weekly:
			freqString = "Weekly";
			break;
		case Defines.FortNightly:
			freqString = "Fort Nightly";
			break;
		case Defines.Monthly:
			freqString = "Monthly";
			break;
		case Defines.Bi_Monthly:
			freqString = "Bi_Monthly";
			break;
		case Defines.Quarterly:
			freqString = "Quarterly";
			break;
		case Defines.Half_Yearly:
			freqString = "Half_Yearly";
			break;
		case Defines.Yearly:
			freqString = "Yearly";
			break;
		default:
			freqString = "None";
			break;
		}
		return freqString;
	}

	public static ArrayList<File> files;

	// public static boolean exportToTextFile(List<SwiftMsg> swiftmsgs_list)
	//
	// {
	// files = new ArrayList<File>();
	// for (int i = 0; i < swiftmsgs_list.size(); i++) {
	// String FILENAME = "D:/BDC_APP/EStatement/Attachements/"
	// + swiftmsgs_list.get(i).getREF_NO() + "_" + i + ".txt";
	//
	// try (BufferedWriter bw = new BufferedWriter(
	// new FileWriter(FILENAME))) {
	// // FileOutputStream fos = new
	// // FileOutputStream("D:/BDC_APP/EStatement/Attachements/sample.zip");
	// // ZipOutputStream zipOS = new ZipOutputStream(fos);
	//
	// String content = swiftmsgs_list.get(i).getMSG();
	// if (content != null) {
	// bw.write(content);
	// } else {
	// return false;
	// }
	//
	// files.add(new File(FILENAME));
	//
	// // writeToZipFile(FILENAME, zipOS);
	//
	// // no need to close it.
	// // bw.close();
	//
	// } catch (IOException e) {
	// e.printStackTrace();
	// return false;
	// }
	//
	// }
	// String outputZip = "D:/BDC_APP/EStatement/Attachements/"
	// + swiftmsgs_list.get(1).getREF_NO() + " - "
	// + new SimpleDateFormat("yyyy-MM-dd").format(new Date())
	// + ".rar";
	// try {
	// createZipFile(outputZip, files);
	// } catch (IOException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// System.out.println("Done");
	// return true;
	// }

	// static ZipFile zipFile;

//	public static void createZipFile(String outputZip,
//			ArrayList<File> listOfFiles) throws IOException {
//
//		/*
//		 * try { zipFile = new ZipFile(outputZip); } catch (ZipException e) { //
//		 * TODO Auto-generated catch block e.printStackTrace(); } // Setting
//		 * parameters ZipParameters zipParameters = new ZipParameters();
//		 * zipParameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
//		 * zipParameters
//		 * .setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_ULTRA);
//		 * zipParameters.setEncryptFiles(true);
//		 * zipParameters.setEncryptionMethod(Zip4jConstants.ENC_METHOD_AES);
//		 * zipParameters.setAesKeyStrength(Zip4jConstants.AES_STRENGTH_256); //
//		 * Setting password zipParameters.setPassword("pass@123");
//		 *
//		 * try { zipFile.addFiles(new ArrayList<File> (listOfFiles),
//		 * zipParameters); } catch (ZipException e) { // TODO Auto-generated
//		 * catch block e.printStackTrace(); }
//		 */
//		// Create zip file
//		ZipOutputStream zipOutputStream = new ZipOutputStream(
//				new FileOutputStream(outputZip));
//		for (File file : listOfFiles) {
//			String fileName = file.getName();
//			// Read files
//			FileInputStream readFile = new FileInputStream(file);
//			// Create zip entry
//			ZipEntry zipEntry = new ZipEntry(fileName);
//			// Set zip entry
//			zipOutputStream.putNextEntry(zipEntry);
//			int readChar = -1;
//			while ((readChar = readFile.read()) != -1) {
//				zipOutputStream.write(readChar);
//			}
//			readFile.close();
//			zipOutputStream.closeEntry();
//			// System.out.println("Zipping input file: "+fileName);
//			file.delete();
//		}
//		zipOutputStream.close();
//	}

	public static boolean exportToTextFile(List<SwiftMsg> swiftmsgs_list, String customerCode, String accountNumber) {
		files = new ArrayList<File>();
		for (int i = 0; i < swiftmsgs_list.size(); i++) {
			String FILENAME = "D:/BDC_APP/EStatement/Attachements/" + swiftmsgs_list.get(i).getREF_NO() + "_" + i
					+ ".txt";
			try (BufferedWriter bw = new BufferedWriter(new FileWriter(FILENAME))) {
				String content = swiftmsgs_list.get(i).getMSG();
				if (content != null) {
					bw.write(content);
				} else {
					return false;
				}
				files.add(new File(FILENAME));
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
		}

//		String outputZip = "D:/BDC_APP/EStatement/Attachements/" + customerCode + " - "
//				+ new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + ".zip";
		String outputZip = "D:/BDC_APP/EStatement/Attachements/" + accountNumber + " - "
				+ new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + ".zip";
		try {
			createZipFile(outputZip, files, customerCode, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Done");
		return true;
	}

	public static boolean exportToTextFileHostToHost(List<SwiftMsg> swiftmsgs_list, String customerCode,
			String accountNumber) {
		files = new ArrayList<File>();
		for (int i = 0; i < swiftmsgs_list.size(); i++) {
			// D:\BDC_APP\HostToHost\Attachements
			String FILENAME = "D:/BDC_APP/HostToHost/Attachements/" + swiftmsgs_list.get(i).getREF_NO() + "_" + i
					+ ".txt";
			try (BufferedWriter bw = new BufferedWriter(new FileWriter(FILENAME))) {
				String content = swiftmsgs_list.get(i).getMSG();
				if (content != null) {
					bw.write(content);
				} else {
					return false;
				}
				files.add(new File(FILENAME));
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
		}

//		String outputZip = "D:/BDC_APP/EStatement/Attachements/" + customerCode + " - "
//				+ new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + ".zip";
		String outputZip = "D:/BDC_APP/HostToHost/Attachements/" + accountNumber + " - "
				+ new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + ".zip";
		try {
			createZipFile(outputZip, files, customerCode, false);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Done");
		return true;
	}

	public static boolean exportToTextFileMT940(List<SwiftMsg> swiftmsgs_list, String customerCode,
			String accountNumber) {
		files = new ArrayList<File>();
		for (int i = 0; i < swiftmsgs_list.size(); i++) {
			String FILENAME = "D:/BDC_APP/HostToHost/Attachements/" + swiftmsgs_list.get(i).getREF_NO() + "_" + i
					+ ".txt";
			try (BufferedWriter bw = new BufferedWriter(new FileWriter(FILENAME))) {
				String content = swiftmsgs_list.get(i).getMSG();
				if (content != null) {
					bw.write(content);
				} else {
					return false;
				}
				files.add(new File(FILENAME));
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
		}
		String outputZip = "D:/BDC_APP/HostToHost/Attachements/" + accountNumber + " - "
				+ new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + ".zip";
		try {
			createZipFile(outputZip, files, customerCode, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	public static void createZipFile(String filePath, List<File> allFiles, String Password, boolean encryptionType) {
		try {
			ZipParameters zipParameters = new ZipParameters();
			zipParameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
			zipParameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_ULTRA);
//			zipParameters.setEncryptFiles(true);
			if (encryptionType) {
				zipParameters.setEncryptFiles(encryptionType);
				zipParameters.setEncryptionMethod(Zip4jConstants.ENC_METHOD_STANDARD);
				zipParameters.setAesKeyStrength(Zip4jConstants.AES_STRENGTH_256);
				zipParameters.setPassword(Password);
			}
			ZipFile zipFile = new ZipFile(filePath);
			ArrayList<File> files = new ArrayList<File>();
			files.addAll(allFiles);
			zipFile.addFiles(files, zipParameters);
			deleteFiles(allFiles);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void deleteFiles(List<File> allFiles) {
		for (File file : allFiles)
			file.delete();
	}

//	public static void pack(String filePath) throws ZipException {
//		ZipParameters zipParameters = new ZipParameters();
//		zipParameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
//		zipParameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_ULTRA);
//		zipParameters.setEncryptFiles(true);
//		zipParameters.setEncryptionMethod(Zip4jConstants.ENC_METHOD_AES);
//		zipParameters.setAesKeyStrength(Zip4jConstants.AES_STRENGTH_256);
//		zipParameters.setPassword("test");
//
//		String destinationZipFilePath = "E:\\testingPassword.zip";
//		ZipFile zipFile = new ZipFile(destinationZipFilePath);
//
//		zipFile.addFile(new File(filePath), zipParameters);
//	}

	public static boolean sendHostToHostMail(List<String> allAttachements) {
		ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2010_SP1);
		ExchangeCredentials credentials = new WebCredentials("eStatement.BDC", "Password1");
		service.setCredentials(credentials);
		try {
			service.setUrl(new URI("https://mail.bdc.com.eg/EWS/Exchange.asmx"));
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		EmailMessage msg = null;
		try {
			msg = new EmailMessage(service);
			msg.setSubject("MT940");
			msg.setBody(MessageBody.getMessageBodyFromText("Sent from Estatement Service"));
			msg.getToRecipients().add("shady.ashour@bdc.com.eg");
			for (String attach : allAttachements) {
				File f = new File(attach);
				msg.getAttachments().addFileAttachment(f.getAbsolutePath());
			}
			msg.send();
			System.out.println("Mail Sent ...");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean exportToExcelForMeeza(List<MeezaTransactions> transactions, String custID) {
//		String[] columns = { "Card Name Holder", "Transaction Desc.", "Transaction Status", "Card Number",
//				"TXN Location", "Date and Timing" };
		String[] columns = { "Card Name Holder", "Transaction Desc.", "Amount", "Card Number", "TXN Location",
				"Date and Timing" };
		HSSFWorkbook workbook = new HSSFWorkbook();
		CreationHelper createHelper = workbook.getCreationHelper();
		// Create a Sheet
		Sheet sheet = workbook.createSheet("123");
		// Create a Font for styling header cells
		Font headerFont = workbook.createFont();
		headerFont = getCellStyleForColor(workbook);
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 10);
		// Create a CellStyle with the font
		CellStyle headerCellStyle = workbook.createCellStyle();
		headerFont.setBold(true);
		headerCellStyle.setFont(headerFont);
		/*
		*
		*
		*
		*
		*/
		Row headerRow = sheet.createRow(1);
		for (int i = 0; i < columns.length; i++) {
			Cell cell = headerRow.createCell(i);
			cell.setCellValue(columns[i]);
			cell.setCellStyle(headerCellStyle);
		}
		/*
		 *
		 *
		 *
		 *
		 *
		 */
		HSSFCellStyle style = workbook.createCellStyle();
		style.setFillForegroundColor(IndexedColors.BLUE.index);
		style.setFillBackgroundColor(IndexedColors.BLUE.index);
		Font f = workbook.createFont();
		f.setColor(IndexedColors.BLACK.index);
		f.setBold(true);
		style.setFont(f);
//		valueCellStyle.setFont(f);

		int rowNum = 2;
		CellStyle dateCellStyle = workbook.createCellStyle();
		HSSFDataFormat format = workbook.createDataFormat();
		Font font = workbook.createFont();//
		font.setFontHeightInPoints((short) 10);
		font.setColor(IndexedColors.BLACK.index);
		dateCellStyle.setFont(font);
		DecimalFormat df = new DecimalFormat("#,###,##0.000");// ("###.###");
		System.out.println("excel list size = ****  =  " + transactions.size());
		for (MeezaTransactions t : transactions) {
			Row row = sheet.createRow(rowNum++);

			Cell CardNameHolderCell = row.createCell(0);
			CardNameHolderCell.setCellValue(t.getCUST_NAME());
			CardNameHolderCell.setCellStyle(dateCellStyle);

			Cell TransactionDescCell = row.createCell(1);
			TransactionDescCell.setCellValue(t.getTXN_DESC());
			TransactionDescCell.setCellStyle(dateCellStyle);

			Cell TransactionStatusCell = row.createCell(2);
			TransactionStatusCell.setCellValue(t.getTXN_AMOUNT());
			TransactionStatusCell.setCellStyle(dateCellStyle);

			Cell CardNumberCell = row.createCell(3);
			CardNumberCell.setCellValue(t.getCARD_NUMBER());
			CardNumberCell.setCellStyle(dateCellStyle);

			Cell TXNLocationCell = row.createCell(4);
			TXNLocationCell.setCellValue(t.getTXN_LOCATION());
			TXNLocationCell.setCellStyle(dateCellStyle);

			Cell DateAndTimingCell = row.createCell(5);
			DateAndTimingCell.setCellValue(t.getTXN_DATE());
			DateAndTimingCell.setCellStyle(dateCellStyle);

//			Cell CardNameHolderCell = row.createCell(0);
//			CardNameHolderCell.setCellValue(t.getCUST_NAME());
//			CardNameHolderCell.setCellStyle(dateCellStyle);
//			
//			Cell TransactionDescCell = row.createCell(1);
//			TransactionDescCell.setCellValue(t.getTXN_DESC());
//			TransactionDescCell.setCellStyle(dateCellStyle);
////			row.createCell(2).setCellValue(t.getTXN_DESC());
//			
//			Cell TransactionStatusCell = row.createCell(2);
//			TransactionStatusCell.setCellValue(t.getTXN_STATUS());
//			TransactionStatusCell.setCellStyle(dateCellStyle);
//			
//			Cell CardNumberCell = row.createCell(3);
//			CardNumberCell.setCellValue(t.getCARD_NUMBER());
//			CardNumberCell.setCellStyle(dateCellStyle);
//			
//			Cell TXNLocationCell = row.createCell(4);
//			CardNumberCell.setCellValue(t.getTXN_LOCATION());
//			CardNumberCell.setCellStyle(dateCellStyle);
//			
//			Cell DateAndTimingCell = row.createCell(5);
//			CardNumberCell.setCellValue(t.getTXN_DATE());
//			CardNumberCell.setCellStyle(dateCellStyle);

			row.getCell(0).setCellStyle(style);
			row.getCell(1).setCellStyle(style);
			row.getCell(2).setCellStyle(style);
			row.getCell(3).setCellStyle(style);
			row.getCell(4).setCellStyle(style);
			row.getCell(5).setCellStyle(style);
		}
		if (transactions.isEmpty()) {
			Row row = sheet.createRow(rowNum++);
			Cell CardNameHolderCell = row.createCell(0);
			CardNameHolderCell.setCellValue("No Transactions");
			CardNameHolderCell.setCellStyle(dateCellStyle);

			Cell TransactionDescCell = row.createCell(1);
			TransactionDescCell.setCellValue("No Transactions");
			TransactionDescCell.setCellStyle(dateCellStyle);

			Cell TransactionStatusCell = row.createCell(2);
			TransactionStatusCell.setCellValue("No Transactions");
			TransactionStatusCell.setCellStyle(dateCellStyle);

			Cell CardNumberCell = row.createCell(3);
			CardNumberCell.setCellValue("No Transactions");
			CardNumberCell.setCellStyle(dateCellStyle);

			Cell TXNLocationCell = row.createCell(4);
			TXNLocationCell.setCellValue("No Transactions");
			TXNLocationCell.setCellStyle(dateCellStyle);

			Cell DateAndTimingCell = row.createCell(5);
			DateAndTimingCell.setCellValue("No Transactions");
			DateAndTimingCell.setCellStyle(dateCellStyle);

			row.getCell(0).setCellStyle(style);
			row.getCell(1).setCellStyle(style);
			row.getCell(2).setCellStyle(style);
			row.getCell(3).setCellStyle(style);
			row.getCell(4).setCellStyle(style);
			row.getCell(5).setCellStyle(style);
		}
		for (int i = 0; i < columns.length; i++) {
			sheet.autoSizeColumn(i);
		}

		try {
			Biff8EncryptionKey.setCurrentUserPassword(custID);
			workbook.writeProtectWorkbook(Biff8EncryptionKey.getCurrentUserPassword(), "");
//			FileOutputStream fileOut = new FileOutputStream("D://BDC_APP//EStatement//Attachements//"
//					+ account.getCustomerId() + " - " + new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + ".xls");
			FileOutputStream fileOut = new FileOutputStream("D://BDC_APP//MezzaRegCustomers//Attachements//" + custID
					+ " - " + new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + ".xls");
			workbook.write(fileOut);
			fileOut.close();
			workbook.close();
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

//	public static boolean sendingMeezaMail(MeezaRegCustomers meezaRegCustomers, String sendDate) {
//		boolean status = false;
//		if (meezaRegCustomers.getEMAIL().contains(";")) {
//			String[] emails = meezaRegCustomers.getEMAIL().split(";");
//			for (String email : emails) {
//				if (!email.isEmpty()) {
//					status = sendMeezaMail(email.trim(), meezaRegCustomers.getCOD_ACCT_NO(),
//							Integer.toString(meezaRegCustomers.getCOD_CUST()), sendDate);
//				}
//			}
//		} else {
//			status = sendMeezaMail(meezaRegCustomers.getEMAIL(), meezaRegCustomers.getCOD_ACCT_NO(),
//					Integer.toString(meezaRegCustomers.getCOD_CUST()), sendDate);
//		}
//
//		return status;
//	}

	public static boolean sendingMeezaMail(String email, String cod_acc_no, String cod_cust, String sendDate) {
		boolean status = false;
		if (email.contains(";")) {
			String[] emails = email.split(";");
			for (String mal : emails) {
				if (!mal.isEmpty()) {
					status = sendMeezaMail(mal.trim(), cod_acc_no, cod_cust, sendDate);
				}
			}
		} else {
			status = sendMeezaMail(email, cod_acc_no, cod_cust, sendDate);
		}

		return status;
	}
	
	

	public static boolean sendMeezaMail(String email, String accountNo, String CustomerCode, String sendDate) {
		ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2010_SP1);
//		ExchangeCredentials credentials = new WebCredentials("soa", "BdcS0@2016");
		ExchangeCredentials credentials = new WebCredentials("eStatement.BDC", "Password1");
		service.setCredentials(credentials);

		try {
//			service.setUrl(new URI("https://owa.banqueducaire.com/EWS/Exchange.asmx"));
			service.setUrl(new URI("https://mail.bdc.com.eg/EWS/Exchange.asmx"));
			// https://mail.bdc.com.eg/ews/Exchange.asmx
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		EmailMessage msg = null;
		try {
//			setSigniture(service);
			msg = new EmailMessage(service);
			msg.setSubject("Estatement");

			StringBuilder message = new StringBuilder();
			message.append("Dear Customer, ");
			message.append("<p>");
			message.append("Thank you for using Banque du Caire Corporate Meeza service.");
			message.append("<p>");
			message.append("Kindly find attached your Meeza issued on " + sendDate);
			message.append("<p>");
			message.append(
					"In order to access your Meeza, please make sure your device is equipped/compatible with the following settings:");
			message.append("<p>");
			message.append("1- For PC and Laptop users: Adobe reader and Microsoft Excel");
			message.append("<p>");
			message.append("2- This Meeza is encrypted with a \"Shared Secret\" for your protection");
			message.append("<p>");
			message.append(
					"Your password is designed to secure your data and to enable you to view the attached statement file with full privacy.");
			message.append("<p>");
			message.append(
					"For more information, pls. refer to your designated Relationship Manager or BdC Corporate Customer Service Team");
			message.append("<p>");
			message.append("Thank you for your continued trust and for banking with us. ");
			message.append("<p>");
			message.append("Best regards");
			message.append("<p>");
			message.append("Banque du Caire");
			message.append("<p>");
			// message.append("<img src=\"resources\\bdc-logo.jpg\">");

			String img = "<img src=\"D:\\BDC_APP\\EStatement\\resources\\bdc-logo.svg\" width=\"350\" height=\"80\">";

			img = "<svg version=\"1.0\" xmlns=\"http://www.w3.org/2000/svg\" width=\"700.000000pt\" height=\"206.000000pt\" viewBox=\"0 0 700.000000 206.000000\" preserveAspectRatio=\"xMidYMid meet\">"
					+ "<g transform=\"translate(0.000000,206.000000) scale(0.100000,-0.100000)\" fill=\"#000000\" stroke=\"none\">"
					+ "<path d=\"M2077 1572 c-16 -18 -16 -20 1 -35 18 -16 20 -16 35 1 16 18 16 20"
					+ "-1 35 -18 16 -20 16 -35 -1z\"></path>"
					+ "<path d=\"M2166 1572 c-15 -18 -15 -20 2 -35 24 -22 29 -21 42 4 9 15 8 24 -1"
					+ "35 -15 18 -22 18 -43 -4z\"></path>"
					+ "<path d=\"M4395 1570 c-18 -19 -18 -22 -2 -37 16 -16 18 -16 37 3 20 20 20 21"
					+ "2 37 -18 16 -20 16 -37 -3z\"></path>"
					+ "<path d=\"M4487 1572 c-16 -18 -16 -20 1 -35 24 -22 29 -21 41 2 8 14 6 22 -8"
					+ "35 -16 15 -19 15 -34 -2z\"></path>"
					+ "<path d=\"M6565 1560 c-18 -19 -18 -22 -2 -37 16 -16 18 -16 37 3 20 20 20 21"
					+ "2 37 -18 16 -20 16 -37 -3z\"></path>"
					+ "<path d=\"M4181 1551 c-12 -8 -12 -33 -1 -171 l13 -162 121 6 c149 7 145 7 288"
					+ "0 l116 -6 7 31 c4 18 5 67 4 111 -2 43 0 82 3 85 11 11 -28 110 -43 110 -17 0"
					+ "-17 -20 -1 -128 7 -43 12 -98 12 -123 l0 -44 -90 0 c-49 0 -90 2 -90 5 0 2 11"
					+ "21 25 41 42 62 26 138 -36 169 -42 22 -81 11 -119 -31 -39 -45 -41 -99 -5"
					+ "-142 14 -17 25 -33 25 -36 0 -3 -43 -6 -95 -6 l-95 0 0 68 c0 91 -12 232 -19"
					+ "232 -3 0 -12 -4 -20 -9z m334 -136 c14 -13 25 -35 25 -49 0 -24 -51 -86 -72"
					+ "-86 -13 0 -88 75 -88 88 0 6 12 24 26 41 33 39 74 41 109 6z\"></path>"
					+ "<path d=\"M4806 1552 c-17 -4 -18 -10 -13 -62 4 -33 10 -109 13 -170 6 -94 9"
					+ "-110 23 -108 14 3 16 22 12 152 -4 172 -8 193 -35 188z\"></path>"
					+ "<path d=\"M6278 1552 c-22 -4 -23 -47 -4 -180 9 -68 10 -95 2 -97 -59 -20 -189"
					+ "-25 -661 -25 -364 0 -542 4 -564 11 -18 7 -38 24 -47 41 -15 28 -14 46 2 101"
					+ "3 9 1 17 -3 17 -5 0 -17 -23 -25 -51 -22 -69 -8 -116 42 -141 32 -17 81 -18"
					+ "590 -18 463 0 566 3 620 16 43 10 66 11 68 4 2 -6 47 -10 116 -10 97 0 117 3"
					+ "138 19 24 20 25 20 41 0 14 -16 30 -19 105 -19 l89 0 10 78 c8 68 7 82 -10"
					+ "122 -24 55 -25 56 -38 43 -7 -7 -6 -33 5 -86 26 -126 31 -120 -73 -115 -49 1"
					+ "-92 5 -94 8 -3 3 1 35 8 72 11 62 11 67 -6 71 -24 6 -25 5 -33 -73 -4 -36 -13"
					+ "-68 -19 -72 -7 -4 -58 -8 -114 -8 l-102 0 -5 53 c-3 28 -9 95 -13 147 -7 83"
					+ "-10 95 -25 92z\"></path>"
					+ "<path d=\"M3902 1524 c-9 -15 -1 -22 51 -48 75 -38 107 -89 107 -173 l0 -53"
					+ "-42 0 c-24 0 -53 3 -65 6 l-22 6 21 28 c58 80 -11 193 -98 160 -62 -24 -87"
					+ "-112 -44 -158 11 -12 20 -24 20 -27 0 -3 -326 -5 -725 -5 l-725 0 0 24 c0 31"
					+ "-39 156 -48 156 -4 0 -13 -4 -21 -9 -11 -7 -7 -25 19 -94 37 -97 36 -118 -12"
					+ "-173 -35 -40 -82 -64 -123 -64 -45 0 -26 -18 26 -25 65 -9 91 8 134 88 l30 57"
					+ "727 0 c448 0 737 4 752 10 17 6 35 6 52 0 14 -6 56 -13 93 -16 66 -6 68 -6 74"
					+ "17 12 43 7 143 -7 171 -25 47 -88 116 -112 122 -12 3 -29 9 -37 12 -9 3 -19"
					+ "-2 -25 -12z m20 -121 c35 -32 12 -123 -32 -123 -21 0 -80 55 -80 74 0 8 9 25"
					+ "21 40 24 30 63 34 91 9z\"></path>"
					+ "<path d=\"M5597 1498 c-27 -22 -37 -62 -22 -91 12 -24 11 -27 -7 -33 -11 -3"
					+ "-16 -10 -12 -14 5 -5 37 -1 72 8 45 12 62 21 62 34 0 14 -5 15 -30 8 -23 -6"
					+ "-35 -4 -51 9 -32 26 -19 53 23 51 52 -3 55 -1 28 20 -29 23 -40 24 -63 8z\"></path>"
					+ "<path d=\"M2125 1490 c-3 -5 0 -18 7 -29 10 -17 9 -22 -9 -38 -56 -47 -85 -121"
					+ "-67 -168 16 -41 105 -57 169 -31 42 18 46 47 14 116 -11 25 -22 53 -24 62 -4"
					+ "19 -66 98 -77 98 -4 0 -9 -4 -13 -10z m77 -146 c30 -64 30 -67 13 -80 -28 -21"
					+ "-88 -17 -119 7 -24 19 -25 22 -11 48 19 37 64 91 76 91 6 0 24 -30 41 -66z\"></path>"
					+ "<path d=\"M6720 1114 c-11 -12 -10 -18 3 -32 16 -15 18 -15 34 0 13 14 14 20 3"
					+ "32 -7 9 -16 16 -20 16 -4 0 -13 -7 -20 -16z\"></path>"
					+ "<path d=\"M2123 953 c9 -2 23 -2 30 0 6 3 -1 5 -18 5 -16 0 -22 -2 -12 -5z\"></path>"
					+ "<path d=\"M5548 953 c7 -3 16 -2 19 1 4 3 -2 6 -13 5 -11 0 -14 -3 -6 -6z\"></path>"
					+ "<path d=\"M5618 953 c7 -3 16 -2 19 1 4 3 -2 6 -13 5 -11 0 -14 -3 -6 -6z\"></path>"
					+ "<path d=\"M2113 933 c9 -2 23 -2 30 0 6 3 -1 5 -18 5 -16 0 -22 -2 -12 -5z\"></path>"
					+ "<path d=\"M5578 933 c6 -2 18 -2 25 0 6 3 1 5 -13 5 -14 0 -19 -2 -12 -5z\"></path>"
					+ "<path d=\"M5434 898 l-19 -23 23 19 c12 11 22 21 22 23 0 8 -8 2 -26 -19z\"></path>"
					+ "<path d=\"M2478 843 c7 -3 16 -2 19 1 4 3 -2 6 -13 5 -11 0 -14 -3 -6 -6z\"></path>"
					+ "<path d=\"M2883 843 c9 -2 23 -2 30 0 6 3 -1 5 -18 5 -16 0 -22 -2 -12 -5z\"></path>"
					+ "<path d=\"M3233 843 c9 -2 23 -2 30 0 6 3 -1 5 -18 5 -16 0 -22 -2 -12 -5z\"></path>"
					+ "<path d=\"M4013 843 c9 -2 23 -2 30 0 6 3 -1 5 -18 5 -16 0 -22 -2 -12 -5z\"></path>"
					+ "<path d=\"M4543 843 c9 -2 23 -2 30 0 6 3 -1 5 -18 5 -16 0 -22 -2 -12 -5z\"></path>"
					+ "<path d=\"M5873 843 c9 -2 25 -2 35 0 9 3 1 5 -18 5 -19 0 -27 -2 -17 -5z\"></path>"
					+ "<path d=\"M6683 843 c9 -2 23 -2 30 0 6 3 -1 5 -18 5 -16 0 -22 -2 -12 -5z\"></path>"
					+ "<path d=\"M2425 829 c-4 -6 -5 -12 -2 -15 2 -3 7 2 10 11 7 17 1 20 -8 4z\"></path>"
					+ "<path d=\"M2940 836 c0 -2 8 -10 18 -17 15 -13 16 -12 3 4 -13 16 -21 21 -21" + "13z\"></path>"
					+ "<path d=\"M3341 824 c0 -11 3 -14 6 -6 3 7 2 16 -1 19 -3 4 -6 -2 -5 -13z\"></path>"
					+ "<path d=\"M3754 725 c0 -66 1 -92 3 -57 2 34 2 88 0 120 -2 31 -3 3 -3 -63z\"></path>"
					+ "<path d=\"M4844 750 c0 -52 1 -74 3 -47 2 26 2 68 0 95 -2 26 -3 4 -3 -48z\"></path>"
					+ "<path d=\"M6344 715 c0 -71 1 -99 3 -62 2 37 2 96 0 130 -2 34 -3 4 -3 -68z\"></path>"
					+ "<path d=\"M3248 823 c7 -3 16 -2 19 1 4 3 -2 6 -13 5 -11 0 -14 -3 -6 -6z\"></path>"
					+ "<path d=\"M4469 813 c-13 -16 -12 -17 4 -4 9 7 17 15 17 17 0 8 -8 3 -21 -13z\"></path>"
					+ "<path d=\"M4558 823 c7 -3 16 -2 19 1 4 3 -2 6 -13 5 -11 0 -14 -3 -6 -6z\"></path>"
					+ "<path d=\"M3374 635 c0 -104 2 -146 3 -92 2 54 2 139 0 190 -1 51 -3 7 -3 -98z\"></path>"
					+ "<path d=\"M3533 750 c0 -41 2 -58 4 -37 2 20 2 54 0 75 -2 20 -4 3 -4 -38z\"></path>"
					+ "<path d=\"M3723 745 c0 -44 2 -61 4 -37 2 23 2 59 0 80 -2 20 -4 1 -4 -43z\"></path>"
					+ "<path d=\"M5064 705 c0 -66 1 -92 3 -57 2 34 2 88 0 120 -2 31 -3 3 -3 -63z\"></path>"
					+ "<path d=\"M2123 783 c9 -2 23 -2 30 0 6 3 -1 5 -18 5 -16 0 -22 -2 -12 -5z\"></path>"
					+ "<path d=\"M5422 765 c0 -16 2 -22 5 -12 2 9 2 23 0 30 -3 6 -5 -1 -5 -18z\"></path>"
					+ "<path d=\"M5951 764 c0 -11 3 -14 6 -6 3 7 2 16 -1 19 -3 4 -6 -2 -5 -13z\"></path>"
					+ "<path d=\"M2103 763 c9 -2 25 -2 35 0 9 3 1 5 -18 5 -19 0 -27 -2 -17 -5z\"></path>"
					+ "<path d=\"M2981 754 c0 -11 3 -14 6 -6 3 7 2 16 -1 19 -3 4 -6 -2 -5 -13z\"></path>"
					+ "<path d=\"M2793 670 c0 -47 2 -66 4 -42 2 23 2 61 0 85 -2 23 -4 4 -4 -43z\"></path>"
					+ "<path d=\"M2528 733 c7 -3 16 -2 19 1 4 3 -2 6 -13 5 -11 0 -14 -3 -6 -6z\"></path>"
					+ "<path d=\"M2953 665 c0 -44 2 -61 4 -37 2 23 2 59 0 80 -2 20 -4 1 -4 -43z\"></path>"
					+ "<path d=\"M5908 733 c6 -2 18 -2 25 0 6 3 1 5 -13 5 -14 0 -19 -2 -12 -5z\"></path>"
					+ "<path d=\"M3978 723 c23 -2 59 -2 80 0 20 2 1 4 -43 4 -44 0 -61 -2 -37 -4z\"></path>"
					+ "<path d=\"M6648 723 c23 -2 59 -2 80 0 20 2 1 4 -43 4 -44 0 -61 -2 -37 -4z\"></path>"
					+ "<path d=\"M5799 693 c-13 -16 -12 -17 4 -4 9 7 17 15 17 17 0 8 -8 3 -21 -13z\"></path>"
					+ "<path d=\"M5782 640 c0 -14 2 -19 5 -12 2 6 2 18 0 25 -3 6 -5 1 -5 -13z\"></path>"
					+ "<path d=\"M5435 620 c10 -11 20 -20 23 -20 3 0 -3 9 -13 20 -10 11 -20 20 -23"
					+ "20 -3 0 3 -9 13 -20z\"></path>"
					+ "<path d=\"M4681 604 c0 -11 3 -14 6 -6 3 7 2 16 -1 19 -3 4 -6 -2 -5 -13z\"></path>"
					+ "<path d=\"M2415 590 c10 -11 20 -20 23 -20 3 0 -3 9 -13 20 -10 11 -20 20 -23"
					+ "20 -3 0 3 -9 13 -20z\"></path>"
					+ "<path d=\"M4460 606 c0 -2 8 -10 18 -17 15 -13 16 -12 3 4 -13 16 -21 21 -21" + "13z\"></path>"
					+ "<path d=\"M2526 588 c3 -5 10 -6 15 -3 13 9 11 12 -6 12 -8 0 -12 -4 -9 -9z\"></path>"
					+ "<path d=\"M2768 573 c6 -2 18 -2 25 0 6 3 1 5 -13 5 -14 0 -19 -2 -12 -5z\"></path>"
					+ "<path d=\"M6158 573 c6 -2 18 -2 25 0 6 3 1 5 -13 5 -14 0 -19 -2 -12 -5z\"></path>"
					+ "<path d=\"M6348 573 c6 -2 18 -2 25 0 6 3 1 5 -13 5 -14 0 -19 -2 -12 -5z\"></path>" + "</g>"
					+ "</svg>";
			img = "<html>" + "<body>" + "<h1>My first SVG</h1>" + "<img src='data:image/svg+xml;utf8,"
					+ "<svg width=\"100\" height=\"100\">"
					+ "<circle cx=\"50\" cy=\"50\" r=\"40\" stroke=\"green\" stroke-width=\"4\" fill=\"yellow\" />"
					+ "</svg> alt=\"\" />" + "</body>" + "</html>";

			img = "<img src='data:image/svg+xml;utf8,<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" id=\"Layer_1\" x=\"0px\" y=\"0px\" viewBox=\"0 0 100 100\" enable-background=\"new 0 0 100 100\" xml:space=\"preserve\" height=\"100px\" width=\"100px\">"
					+ "<g>"
					+ "    <path d=\"M28.1,36.6c4.6,1.9,12.2,1.6,20.9,1.1c8.9-0.4,19-0.9,28.9,0.9c6.3,1.2,11.9,3.1,16.8,6c-1.5-12.2-7.9-23.7-18.6-31.3   c-4.9-0.2-9.9,0.3-14.8,1.4C47.8,17.9,36.2,25.6,28.1,36.6z\"/>"
					+ "    <path d=\"M70.3,9.8C57.5,3.4,42.8,3.6,30.5,9.5c-3,6-8.4,19.6-5.3,24.9c8.6-11.7,20.9-19.8,35.2-23.1C63.7,10.5,67,10,70.3,9.8z\"/>"
					+ "    <path d=\"M16.5,51.3c0.6-1.7,1.2-3.4,2-5.1c-3.8-3.4-7.5-7-11-10.8c-2.1,6.1-2.8,12.5-2.3,18.7C9.6,51.1,13.4,50.2,16.5,51.3z\"/>"
					+ "    <path d=\"M9,31.6c3.5,3.9,7.2,7.6,11.1,11.1c0.8-1.6,1.7-3.1,2.6-4.6c0.1-0.2,0.3-0.4,0.4-0.6c-2.9-3.3-3.1-9.2-0.6-17.6   c0.8-2.7,1.8-5.3,2.7-7.4c-5.2,3.4-9.8,8-13.3,13.7C10.8,27.9,9.8,29.7,9,31.6z\"/>"
					+ "    <path d=\"M15.4,54.7c-2.6-1-6.1,0.7-9.7,3.4c1.2,6.6,3.9,13,8,18.5C13,69.3,13.5,61.8,15.4,54.7z\"/>"
					+ "    <path d=\"M39.8,57.6C54.3,66.7,70,73,86.5,76.4c0.6-0.8,1.1-1.6,1.7-2.5c4.8-7.7,7-16.3,6.8-24.8c-13.8-9.3-31.3-8.4-45.8-7.7   c-9.5,0.5-17.8,0.9-23.2-1.7c-0.1,0.1-0.2,0.3-0.3,0.4c-1,1.7-2,3.4-2.9,5.1C28.2,49.7,33.8,53.9,39.8,57.6z\"/>"
					+ "    <path d=\"M26.2,88.2c3.3,2,6.7,3.6,10.2,4.7c-3.5-6.2-6.3-12.6-8.8-18.5c-3.1-7.2-5.8-13.5-9-17.2c-1.9,8-2,16.4-0.3,24.7   C20.6,84.2,23.2,86.3,26.2,88.2z\"/>"
					+ "    <path d=\"M30.9,73c2.9,6.8,6.1,14.4,10.5,21.2c15.6,3,32-2.3,42.6-14.6C67.7,76,52.2,69.6,37.9,60.7C32,57,26.5,53,21.3,48.6   c-0.6,1.5-1.2,3-1.7,4.6C24.1,57.1,27.3,64.5,30.9,73z\"/>"
					+ "</g>" + "</svg>' alt=\"\" />";
			message.append(img);

			msg.setBody(MessageBody.getMessageBodyFromText("Sent from Mezza Prepaid Service"));
//			msg.setBody(MessageBody.getMessageBodyFromText(message.toString()));
			msg.getToRecipients().add(email);

			File pdf = new File("D://BDC_APP//MezzaRegCustomers//Attachements//" + CustomerCode + " - "
					+ new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + ".pdf");
			File excel = new File("D://BDC_APP//MezzaRegCustomers//Attachements//" + CustomerCode + " - "
					+ new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + ".xls");
			if (pdf.exists() && !pdf.isDirectory()) {
				msg.getAttachments().addFileAttachment(pdf.getAbsolutePath());
			}
			if (excel.exists() && !excel.isDirectory()) {
				msg.getAttachments().addFileAttachment(excel.getAbsolutePath());
			}
			msg.send();
			System.out.println("Mail Sent ...");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	
	public static boolean sendMail(String mailBody,String subject,String to) {
		ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2010_SP1);
		ExchangeCredentials credentials = new WebCredentials("eStatement.BDC", "Password1");
		service.setCredentials(credentials);
		try {
			service.setUrl(new URI("https://mail.bdc.com.eg/EWS/Exchange.asmx"));
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		EmailMessage msg = null;
		try {
			msg = new EmailMessage(service);
			msg.setSubject(subject);
			msg.setBody(MessageBody.getMessageBodyFromText(mailBody));
			msg.getToRecipients().add(to.trim());
//			for (String attach : allAttachements) {
//				File f = new File(attach);
//				msg.getAttachments().addFileAttachment(f.getAbsolutePath());
//			}
			msg.send();
			System.out.println("Mail Sent ...");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
}
