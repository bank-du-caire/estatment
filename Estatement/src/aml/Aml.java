package aml;

import com.side.ofac.API.LoginRequest;
import com.side.ofac.API.LoginResponse;
import com.side.ofac.API.LogoutRequest;
import com.side.ofac.API.LogoutResponse;
import com.side.ofac.API.Report;
import com.side.ofac.API.ReportEntity;
import com.side.ofac.API.ReportEntityAddress;
import com.side.ofac.API.ScanRequest;
import com.side.ofac.API.ScanResponse;
import com.side.ofac.API.SideApi;

public class Aml {
	public static ScanResponse SafWatchClientApi(String scanedName) {
		String errorMsg = "";
		ScanResponse ScanResponse = new ScanResponse();
		try {
			SideApi test = new SideApi();
			LoginRequest loginRequest = new LoginRequest();
			LoginResponse loginResponse = new LoginResponse();
			LogoutRequest logoutRequest = new LogoutRequest();
			LogoutResponse logoutResponse = new LogoutResponse();
			ScanRequest ScanRequest = new ScanRequest();
			ReportEntity entity = null;
			ReportEntityAddress entityAddress = null;
			Report report = null;
			int i = 0;
			int j = 0;
			int k = 0;
			System.out.println("Test Client API starts");
			/* login */
			// production ip
//			loginRequest.setServerIp("172.17.63.246");
//			uat IP
			loginRequest.setServerIp("172.17.91.46");
			loginRequest.setServerPort(8401);
			loginRequest.setUser("REMAML");
			loginRequest.setPassword("123123");
			if (test.SoLogin(loginRequest, loginResponse) != 0) {
				System.err.println("A problem has occured while log in on en.SafeWatch Server:\n"
						+ test.getLastErrorText() + " code : " + test.getLastErrorCode());
				errorMsg = "A problem has occured while log in on en.SafeWatch Server: Error Code "
						+ test.getLastErrorCode() + " Error Text " + test.getLastErrorText();
				System.out.println(errorMsg);
				return null;
			} else
				errorMsg = "";
			System.out.println("Login successfull");
			/* scan */
			// ScanRequest.setFormat("TEXT");
			ScanRequest.setFormat("NAME");
			ScanRequest.setFullReport(true);
			ScanRequest.setData(scanedName);
			ScanRequest.setContext("<test>true</test>");
			ScanRequest.setListSetId("21");
			ScanRequest.setRank(75);
			ScanRequest.setAutoCreateAlert(false);
			if (test.SoScan(ScanRequest, ScanResponse) != 0) {
				System.err.println("A problem has occured while scanning on en.SafeWatch Server "
						+ test.getLastErrorText() + " code : " + test.getLastErrorCode());
				errorMsg = "A problem has occured while scanning on en.SafeWatch Server : Error Code "
						+ test.getLastErrorCode() + " Error Text " + test.getLastErrorText();
				System.out.println(errorMsg);
				return null;
			} else
				errorMsg = "";
			/* display scan result */
			System.out.println("** scan result **\n\n");
			System.out.println("violation : " + ScanResponse.getViolationCount());
			System.out.println("accept : " + ScanResponse.getAcceptCount());
			System.out.println("external : " + ScanResponse.getExternalCount());
			System.out.println("detec.id : " + ScanResponse.getDetectionId());
			for (i = 0; i < ScanResponse.getReport().size(); i++) {
				report = (Report) ScanResponse.getReport().elementAt(i);
//			System.out.println("\n");
				System.out.println("Status : " + report.getStatus());
				System.out.println("Data : " + report.getData());
				System.out.println("Match : " + report.getMatch());
				if (report.getInputBic() != "") {
					System.out.println(" Scanned BIC : " + report.getInputBic());
					System.out.println(" BIC Address : " + report.getInputAddress());
					System.out.println(" BICCity : " + report.getInputCity());
					System.out.println(" BIC Country : " + report.getInputCountry());
				}
				System.out.println("Rank : " + report.getRank());
				System.out.println("List : " + report.getListName());
				System.out.println("Listdate: " + report.getListDate());
				System.out.println("ent.id : " + report.getEntityId());
				System.out.println("Category: " + report.getCategory());
				System.out.println("Remarks : " + report.getRemark());
				System.out.println("Title : " + report.getTitle());
				System.out.println("Position: From " + report.getBeginPosition() + " to " + report.getEndPosition());
				System.out.println("Field : " + report.getField());
				System.out.println("Line : " + report.getLine());
				System.out.println("Programs: " + report.getProgram());
				System.out.println("Date of birth: " + report.getDOB());
				System.out.println("Place of birth: " + report.getPOB());
				System.out.println("Ext. ID : " + report.getExternalId());
				for (j = 0; j < report.getEntity().size(); j++) {
					entity = (ReportEntity) report.getEntity().elementAt(j);
					// System.out.println(" Name Type : " + entity.getNameType());
					// System.out.println(" Name : " + entity.getName());
				}
				for (j = 0; j < report.getEntityAddress().size(); j++) {
					entityAddress = (ReportEntityAddress) report.getEntityAddress().elementAt(j);
					// System.out.println(" Address : "
					// + entityAddress.getAddress());
					// System.out.println(" City : " + entityAddress.getCity());
					// System.out.println(" Country : "
					// + entityAddress.getCountry());
				}
			}

			/* disconnect from server */
			if (test.SoLogout(logoutRequest, logoutResponse) != 0) {
				System.out.println("logout refused by en.SafeWatch server : result : " + test.getLastErrorText()
						+ " code : " + test.getLastErrorCode());
				errorMsg = "logout refused by en.SafeWatch server : Error Code " + test.getLastErrorCode()
						+ " Error Text " + test.getLastErrorText();
				System.out.println(errorMsg);
			} else {
				errorMsg = "";
				System.out.println("logout successfull ");
			}
			System.out.println("Test Client API done");

		} catch (Exception ex) {
			System.out.println(ex);
		} finally {
			return ScanResponse;
		}
	}

}
