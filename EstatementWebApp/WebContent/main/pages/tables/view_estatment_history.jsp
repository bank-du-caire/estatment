<!DOCTYPE html>
<%@page import="java.util.ArrayList"%>
<%@page import="com.bdc.estatement.model.EstatementHistory"%>
<%@page import="com.bdc.estatement.dao.TransactionsDao"%>
<%@page import="com.bdc.estatement.util.Defines"%>
<%@page import="com.bdc.estatement.model.Account"%>
<%@page import="java.util.List"%>
<%@page import="com.bdc.usermanagement.ldab.UserPrivilege"%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>
<html>
<head>

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Estatement History</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet"
	href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="../../bower_components/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="../../bower_components/Ionicons/css/ionicons.min.css">
<!-- DataTables -->
<link rel="stylesheet"
	href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<style>
div.scrollmenu {
	/*   background-color: #333; */
	overflow: auto;
	/*   white-space: nowrap; */
}

div.scrollmenu a {
	display: inline-block;
	/*   color: white; */
	text-align: center;
	padding: 14px;
	text-decoration: none;
}

div.scrollmenu a:hover {
	/*   background-color: #777; */
	
}
</style>

<%
	String fromDate = "", toDate = "";
	UserPrivilege userPrivilege = UserPrivilege.findAllUserPrivileges("view_estatment_history.jsp", request);
	int userId = session.getAttribute("USER_ID") != null ? (Integer) session.getAttribute("USER_ID") : 0;
	if (session.getAttribute("USER_ID") == null) {
		response.sendRedirect(request.getContextPath() + "/main/pages/tables/Login.jsp");
		return;
	}

	//	List<EstatementHistory> histories = TransactionsDao.getEstatementHistory();
	String freqString = "";
%>

</head>

<%
	List<EstatementHistory> histories = new ArrayList<EstatementHistory>();
	if (userPrivilege.getViewFlag() == 1 || userPrivilege.getReportView() == 1) {

		if (session.getAttribute("fromDate") != null && session.getAttribute("toDate") != null) {
			fromDate = (String) session.getAttribute("fromDate");
			toDate = (String) session.getAttribute("toDate");
		}

		if (session.getAttribute("histories") != null) {
			histories = (List<EstatementHistory>) session.getAttribute("histories");
			//session.removeAttribute("histories");
		}
%>

<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<header class="main-header">
			<a href="#" class="logo"> <span class="logo-mini"><b>BDC</b></span>
				<span class="logo-lg"><b>E-Statement</b></span>
			</a>
			<nav class="navbar navbar-static-top">
				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="push-menu"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
			</nav>
		</header>


		<!-- 		Left Side Menu -->
		<jsp:include page="LeftSideMenu.jsp"></jsp:include>




		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>E-Statement History</h1>
			</section>

			<!-- Main content -->
			<section class="content">
				<div class="row">
					<div class="col-xs-12">

						<!-- /.box -->
						<div class="box">
							<br />
							<form
								action='${pageContext.request.contextPath}/EstatementHistoryFilter'
								method='post'>
								<div class="container">
									<div class="row">
										<div class="col-xs-12">
											<table>
												<tr>
													<td width="7%">From Date:</td>
													<td width="35%"><div class='col-sm-6'>
															<div class="form-group">
																<div class='input-group date' id='datetimepicker1'>
																	<input type='text' class="form-control" name='fromDate'
																		id='fromDate' value='<%=fromDate%>'
																		required="required" /> <span
																		class="input-group-addon"> <span
																		class="glyphicon glyphicon-calendar"></span>
																</div>
															</div>
														</div></td>
													<td width="10%">To Date:</td>
													<td width="35%"><div class='col-sm-6'>
															<div class="form-group">
																<div class='input-group date' id='datetimepicker2'>
																	<input type='text' class="form-control" name='toDate'
																		id='toDate' value='<%=toDate%>' required="required" />
																	<span class="input-group-addon"> <span
																		class="glyphicon glyphicon-calendar"></span>
																</div>
															</div>
														</div></td>
												</tr>

											</table>
											
											<center>
												<label style="color: red;"> <%=session.getAttribute("errorMsg")!= null ? session.getAttribute("errorMsg"):' '%></label>
											</center>
											<center>
												<input class="btn btn-info " type='submit'
													value='Edit &amp; Search ' />
											</center>
										</div>
									</div>
								</div>
							</form>

							<div class="box-header">
								<!--               <h3 class="box-title">Data Table With Full Features</h3> -->
							</div>
							<!-- /.box-header -->

							<div class="box-body scrollmenu">
								<form action="${pageContext.request.contextPath}/history"
									method="post" id="seachAccountForm" role="form"
									accept-charset="UTF-8">
									<table id="estatementHistoryTbl"
										class="table table-bordered table-striped">
										<thead>
											<tr>
												<th>Customer Name</th>
												<th>Account Number</th>
												<th>Customer Code</th>
												<th>Statement frequency</th>
												<th>Email</th>
												<th>Send Date</th>
												<th>From</th>
												<th>To</th>
												<th>PDF</th>
												<th>Excel</th>
												<th>Swift Message</th>
											</tr>
										</thead>
										<tbody>

											<%
												for (EstatementHistory history : histories) {
											%>
											<tr>

												<td>
													<%
														if (history.getCustomerName() != null) {
													%><%=history.getCustomerName()%> <%
 	}
 %>
												</td>
												<td>
													<%
														if (history.getAccountNo() != null) {
													%><%=history.getAccountNo()%> <%
 	}
 %>
												</td>
												<td>
													<%
														if (history.getCustomerCode() != null) {
													%><%=history.getCustomerCode()%> <%
 	}
 %>
												</td>


												<%
													switch (history.getFreqStmt()) {
																case Defines.Daily :
																	freqString = "Daily";
																	break;
																case Defines.Weekly :
																	freqString = "Weekly";
																	break;
																case Defines.FortNightly :
																	freqString = "Fort Nightly";
																	break;
																case Defines.Monthly :
																	freqString = "Monthly";
																	break;
																case Defines.Bi_Monthly :
																	freqString = "Bi_Monthly";
																	break;
																case Defines.Quarterly :
																	freqString = "Quarterly";
																	break;
																case Defines.Half_Yearly :
																	freqString = "Half_Yearly";
																	break;
																case Defines.Yearly :
																	freqString = "Yearly";
																	break;
																default :
																	freqString = "None";
																	break;
															}
												%>
												<td><%=freqString%></td>
												<td><%=history.getMail() != null ? history.getMail() : ""%></td>
												<td>
													<%
														if (history.getTo() != null) {
													%><%=history.getTo()%> <%
 	}
 %>
												</td>
												<td>
													<%
														if (history.getFrom() != null) {
													%><%=history.getFrom()%> <%
 	}
 %>
												</td>
												<td>
													<%
														if (history.getTo() != null) {
													%><%=history.getTo()%> <%
 	}
 %>
												</td>
												<td><a
													href='<%=request.getContextPath()%>/Download?fileName=<%=history.getAccountNo()%>&historyDate=<%=history.getTo()%>&extention=.pdf'>PDF</a></td>
												<td><a
													href='<%=request.getContextPath()%>/Download?fileName=<%=history.getAccountNo()%>&historyDate=<%=history.getTo()%>&extention=.xls'>Excel</a></td>
												<td><a
													href='<%=request.getContextPath()%>/Download?fileName=<%=history.getAccountNo()%>&historyDate=<%=history.getTo()%>&extention=.zip'>Swift</a></td>

											</tr>
											<%
												}
											%>
										</tbody>

									</table>


									<%
										String res = "";
											res = session.getAttribute("existFlag") != null ? session.getAttribute("existFlag").toString() : "";
											System.out.println("res[" + res + "]");
											session.removeAttribute("existFlag");
											if (!res.isEmpty() && res.equals("0")) {
									%>
									<script>
										alert('This File DoseNot Exist');
									</script>
									<%
										res = "";
											}
									%>
								</form>


							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2019 <a href="#">BDC</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<aside class="control-sidebar control-sidebar-dark">
			<!-- Create the tabs -->
			<ul class="nav nav-tabs nav-justified control-sidebar-tabs">
				<li><a href="#control-sidebar-home-tab" data-toggle="tab"><i
						class="fa fa-home"></i></a></li>
				<li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i
						class="fa fa-gears"></i></a></li>
			</ul>
		</aside>
	</div>
	<!-- ./wrapper -->

	<!-- jQuery 3 -->
	<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap 3.3.7 -->
	<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- DataTables -->
	<script
		src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
	<script
		src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
	<!-- SlimScroll -->
	<script
		src="../../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<!-- FastClick -->
	<script src="../../bower_components/fastclick/lib/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="../../dist/js/adminlte.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../../dist/js/demo.js"></script>
	<!-- page script -->
	<script
		src="${pageContext.request.contextPath}/jquery/bootstrap-datetimepicker.min.js"></script>
	<script>
		$(function() {
			$('#estatementHistoryTbl').DataTable({
				'paging' : true,
				'lengthChange' : true,
				'searching' : true,
				'ordering' : false,
				'info' : true,
				'autoWidth' : false
			})
		})
	</script>

	<script type="text/javascript">
		$(function() {
			$('#datetimepicker1').datetimepicker({
				minView : 2,
				pickTime : false,
				format : 'yyyy-mm-dd',

				autoclose : true
			}).val();
		});
	</script>

	<script type="text/javascript">
		$(function() {
			$('#datetimepicker2').datetimepicker({
				minView : 2,
				pickTime : false,
				format : 'yyyy-mm-dd',

				autoclose : true
			}).val();
		});
	</script>
</body>

<%
	} else {
%>
<center>
	<h2>
		<label style="color: red; font-weight: bold;">You don't have
			Permission to access this Page!</label>
	</h2>
</center>
<%
	}
%>
</html>
