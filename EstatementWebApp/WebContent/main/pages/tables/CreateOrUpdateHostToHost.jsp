<!DOCTYPE html>
<%@page import="java.time.ZoneId"%>
<%@page import="java.util.Date"%>
<%@page import="java.time.LocalDate"%>
<%@page import="com.bdc.estatement.dao.SMSDao"%>
<%@page import="com.bdc.estatement.model.SMSHistory"%>
<%@page import="com.bdc.usermanagement.ldab.UserManagmentDBTransactions"%>
<%@page import="com.bdc.usermanagement.ldab.User"%>
<%@page import="com.bdc.usermanagement.ldab.Rule"%>
<%@page import="com.bdc.usermanagement.ldab.UserRule"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.bdc.estatement.model.EstatementHistory"%>
<%@page import="com.bdc.estatement.dao.TransactionsDao"%>
<%@page import="com.bdc.estatement.util.Defines"%>
<%@page import="com.bdc.estatement.model.Account"%>
<%@page import="java.util.List"%>
<%@page import="com.bdc.usermanagement.ldab.UserPrivilege"%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>
<html>
<head>

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Edit Account</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet"
	href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="../../bower_components/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="../../bower_components/Ionicons/css/ionicons.min.css">
<!-- DataTables -->
<link rel="stylesheet"
	href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<style>
div.scrollmenu {
	/*   background-color: #333; */
	overflow: auto;
	/*   white-space: nowrap; */
}

div.scrollmenu a {
	display: inline-block;
	/*   color: white; */
	text-align: center;
	padding: 14px;
	text-decoration: none;
}

div.scrollmenu a:hover {
	/*   background-color: #777; */
	
}
</style>

<%
	// 	UserPrivilege userPrivilege = UserPrivilege.findAllUserPrivileges("CreateOrUpdateHostToHost.jsp", request);
	// 	if (userPrivilege.getViewFlag() != 1) {
	// 		response.sendRedirect("view_accounts.jsp");
	// 		return;
	// 	}
	String accountNumber = "", sftpFolder = "", sftpPaymentFolder = "";
	List<String> accountNumbers = new ArrayList<String>();

	if (session.getAttribute("editHostToHost") != null && session.getAttribute("accountNumber") != null
			&& session.getAttribute("sftpFolder") != null) {
		accountNumber = session.getAttribute("accountNumber").toString();
		sftpFolder = session.getAttribute("sftpFolder").toString();
		sftpPaymentFolder = session.getAttribute("sftpPaymentFolder").toString();
		System.out.println(accountNumber);
		System.out.println(sftpFolder);
		accountNumbers = TransactionsDao.getHostToHostAccountNumbers(accountNumber);
	} else {
		accountNumbers = TransactionsDao.getHostToHostAccountNumbers("");
	}
%>

</head>


<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<header class="main-header">
			<a href="#" class="logo"> <span class="logo-mini"><b>BDC</b></span>
				<span class="logo-lg"><b>E-Statement</b></span>
			</a>
			<nav class="navbar navbar-static-top">
				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="push-menu"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
			</nav>
		</header>


		<!-- 		Left Side Menu -->
		<jsp:include page="LeftSideMenu.jsp"></jsp:include>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header"></section>



			<!-- Main content -->
			<section class="content">
				<!-- 			 box -->
				<div class="row">
					<div class="col-xs-12">

						<!-- /.box -->

						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Edit Host to Host Account</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body ">
								<!--put my code in here  -->
								<form
									action='${pageContext.request.contextPath}/HostToHostServlet'
									method='post' onsubmit="return ValidateForm();"
									novalidate="novalidate">
									<div class="container">
										<!-- 										<h2>Update Host to Host Account</h2> -->
										<div class="row">
											<div class="col-xs-6">
												<table>
													<tr>
														<td width="23%">Account Number:</td>
														<td width="72%"><div class='col-sm-6'>
																<div class="form-group">
																	<select class="chosen" name='accountNum'
																		value='<%=accountNumber%>' id='accountNum'
																		style="width: 325px; height: 90px;">
																		<option value="" label="" />
																		<%
																			for (String accountNum : accountNumbers) {
																				if (accountNumber.contains(accountNum)) {
																		%>
																		<option selected="selected" value="<%=accountNum%>"
																			label="<%=accountNum%>"><%=accountNum%></option>
																		<%
																			} else {
																		%>
																		<option value="<%=accountNum%>"
																			label="<%=accountNum%>"><%=accountNum%></option>
																		<%
																			}
																			}
																		%>
																	</select>
																</div>
															</div></td>
													</tr>
													<tr>
														<td width="23%">SFTP Statement Folder Name:</td>
														<td width="72%"><div class='col-sm-4'
																style="width: 350px;">
																<div class="form-group">
																	<input class="form-control" type='text'
																		value="<%=sftpFolder%>" name='SftpFolder'
																		id="SftpFolder" required />
																</div>
															</div></td>

													</tr>
													<tr>
														<td width="23%">SFTP Payment Folder Name:</td>
														<td width="72%"><div class='col-sm-4'
																style="width: 350px;">
																<div class="form-group">
																	<input class="form-control" type='text'
																		value="<%=sftpPaymentFolder%>"
																		name='sftpPaymentFolder' id="sftpPaymentFolder"
																		required />
																</div>
															</div></td>

													</tr>

													<tr>
														<td width="23%"></td>
														<td width="72%"><center>
																<%
																	if (session.getAttribute("result") != null) {
																%>
																<label style="color: red;"><%=session.getAttribute("result")%></label>
																<%
																	session.setAttribute("result", null);
																	}
																%>
															</center></td>
													</tr>
													<tr>
														<td width="23%"></td>
														<td width="72%"><center>
																<input class="btn btn-info " type='submit'
																	value='Edit &amp; Save ' />
															</center></td>
													</tr>
												</table>
											</div>


											<!-- 											<center> -->
											<!-- 												<input class="btn btn-info " type='submit' -->
											<!-- 													value='Edit &amp; Save ' /> -->
											<!-- 											</center> -->
										</div>
								</form>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>
			</section>
			<!-- /.content -->
		</div>




		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2019 <a href="#">BDC</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<aside class="control-sidebar control-sidebar-dark">
			<!-- Create the tabs -->
			<ul class="nav nav-tabs nav-justified control-sidebar-tabs">
				<li><a href="#control-sidebar-home-tab" data-toggle="tab"><i
						class="fa fa-home"></i></a></li>
				<li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i
						class="fa fa-gears"></i></a></li>
			</ul>
		</aside>
	</div>
	<!-- ./wrapper -->

	<!-- jQuery 3 -->
	<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap 3.3.7 -->
	<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- DataTables -->
	<script
		src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
	<script
		src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
	<!-- SlimScroll -->
	<script
		src="../../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<!-- FastClick -->
	<script src="../../bower_components/fastclick/lib/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="../../dist/js/adminlte.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../../dist/js/demo.js"></script>
	<!-- page script -->

	<link rel="stylesheet"
		href="${pageContext.request.contextPath}/css/bootstrap-datetimepicker.min.css">

	<script src="${pageContext.request.contextPath}/jquery/jquery.min.js"></script>

	<script
		src="${pageContext.request.contextPath}/jquery/bootstrap-datetimepicker.min.js"></script>
	<script type="text/javascript">
		$(function() {
			$('#datetimepicker1').datetimepicker({
				minView : 2,
				pickTime : false,
				format : 'yyyy-mm-dd',

				autoclose : true
			}).val();
		});
	</script>

	<link rel="stylesheet"
		href="${pageContext.request.contextPath}/css/chosen.min.css">
	<script src="${pageContext.request.contextPath}/js/chosen.jquery.js"></script>

	<script>
		jQuery(document).ready(
				function() {
					jQuery(".chosen").data("placeholder",
							"Select Frameworks...").chosen();
				});
	</script>

	<script>
		function ValidateForm() {
			debugger;
			if (document.getElementById("accountNum").value == ''
					|| document.getElementById("accountNum").value == null) {
				alert("Account Number is Required.");
				return false;
			}
			if (document.getElementById("SftpFolder").value == ''
					|| document.getElementById("SftpFolder").value == null) {
				alert("SFTP Statement Folder Name is Required.");
				return false;
			}
			if (document.getElementById("sftpPaymentFolder").value == ''
					|| document.getElementById("sftpPaymentFolder").value == null) {
				alert("SFTP Payment Folder Name is Required.");
				return false;
			}
			return true;
		}
	</script>
</body>
</html>
