<!DOCTYPE html>
<%@page import="com.bdc.usermanagement.ldab.Screen"%>
<%@page import="com.bdc.usermanagement.ldab.UserManagmentDBTransactions"%>
<%@page import="com.bdc.usermanagement.ldab.User"%>
<%@page import="com.bdc.usermanagement.ldab.Rule"%>
<%@page import="com.bdc.usermanagement.ldab.UserRule"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.bdc.estatement.model.EstatementHistory"%>
<%@page import="com.bdc.estatement.dao.TransactionsDao"%>
<%@page import="com.bdc.estatement.util.Defines"%>
<%@page import="com.bdc.estatement.model.Account"%>
<%@page import="java.util.List"%>
<%@page import="com.bdc.usermanagement.ldab.UserPrivilege"%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>
<html>
<head>

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Roles And Screens</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet"
	href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="../../bower_components/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="../../bower_components/Ionicons/css/ionicons.min.css">
<!-- DataTables -->
<link rel="stylesheet"
	href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<style>
div.scrollmenu {
	/*   background-color: #333; */
	overflow: auto;
	/*   white-space: nowrap; */
}

div.scrollmenu a {
	display: inline-block;
	/*   color: white; */
	text-align: center;
	padding: 14px;
	text-decoration: none;
}

div.scrollmenu a:hover {
	/*   background-color: #777; */
	
}
</style>

<%
	if (session.getAttribute("USER_ID") == null)
		response.sendRedirect(request.getContextPath() + "/main/pages/tables/Login.jsp");
	int sessionSuperUser = session.getAttribute("FLAG_SUPER_USER") != null
			? Integer.valueOf(session.getAttribute("FLAG_SUPER_USER").toString())
			: 0;
	UserPrivilege userPrivilege = UserPrivilege.findAllUserPrivileges("RulesAndScreens.jsp", request);
	if (userPrivilege.getUserCreation() == 1) {
		Rule rule = new Rule();
		List<Rule> allRules = new ArrayList<Rule>();
		UserManagmentDBTransactions dbTransactions = new UserManagmentDBTransactions();
		allRules = dbTransactions.findAllRules();

		Screen screen = new Screen();
		List<Screen> allScreens = new ArrayList<Screen>();
		allScreens = dbTransactions.findAllScreens();

		int userIDHidden = 0, screenIDHid = 0;
%>

</head>



<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<header class="main-header">
			<a href="#" class="logo"> <span class="logo-mini"><b>BDC</b></span>
				<span class="logo-lg"><b>E-Statement</b></span>
			</a>
			<nav class="navbar navbar-static-top">
				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="push-menu"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
			</nav>
		</header>


		<!-- 		Left Side Menu -->
		<jsp:include page="LeftSideMenu.jsp"></jsp:include>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>Roles And Screens</h1>
			</section>



			<!-- Main content -->
			<section class="content">
				<!-- 			shady box -->
				<div class="row">
					<div class="col-xs-12">

						<!-- /.box -->

						<div class="box">
							<h2>Create Roles</h2>
							<div class="box-header">
								<!--               <h3 class="box-title">Data Table With Full Features</h3> -->
							</div>
							<!-- /.box-header shady -->

							<div class="box-body scrollmenu">

								<form id="rulesFormID"
									action="${pageContext.request.contextPath}/RulesServlet"
									method="post">

									<table style="width: 50%; height: 150px;">
										<tr>
											<td>Role Name</td>
											<td><input type="text" class="form-control"
												id="ruleName" name="ruleName"
												value="<%=rule.getRuleName() != null ? rule.getRuleName() : ""%>">
												<input type="hidden" id="ruleIDHidden" name="ruleIDHidden"
												value="<%=userIDHidden%>"></td>
										</tr>
										<tr>
											<td>Role Description</td>
											<td><input type="text" class="form-control"
												id="ruleDescription" name="ruleDescription"
												value="<%=rule.getRuleDescription() != null ? rule.getRuleDescription() : ""%>"></td>
										</tr>
										<tr>
											<td colspan="2">Allow Insert<input type="checkbox"
												id="insertFlag" name="insertFlag"> Allow Update<input
												type="checkbox" id="updateFlag" name="updateFlag">
												Allow Delete<input type="checkbox" id="deleteFlag"
												name="deleteFlag"> Allow View<input type="checkbox"
												id="viewFlag" name="viewFlag"> Allow User Creation<input
												type="checkbox" id="userCreationFlag"
												name="userCreationFlag"> Allow Reports Role<input
												type="checkbox" id="viewReportRole" name="viewReportRole">

											</td>
										</tr>
										<tr>
											<td colspan="2" align="center"><input type="submit"
												class="btn btn-info" name="submit" id="submit"
												value="Create Role"></td>
										</tr>
										<tr>
											<td colspan="2" align="center">
												<%
													if (session.getAttribute("ruleResult") != null) {
															String ruleRes = "";
															ruleRes = session.getAttribute("ruleResult").toString();
															System.out.println(ruleRes);
															session.removeAttribute("ruleResult");
												%> <label id="ruleResult" name="ruleResult"
												style="color: red; font-weight: bold;"><%=ruleRes%></label>
												<%
													}
												%>
											</td>
										</tr>
									</table>




									<table id="roleTbl" class="table table-bordered table-striped">
										<thead>
											<tr>
												<th>Role Name</th>
												<th>Role Description</th>
												<th>Allow Insert</th>
												<th>Allow Update</th>
												<th>Allow Delete</th>
												<th>Allow View</th>
												<th>Allow User Creation</th>
												<th>Allow Report Role</th>
												<th>Created Date</th>
												<th>Created By User</th>
												<th>Edit</th>
											</tr>
										</thead>
										<tbody>

											<%
												for (int i = 0; i < allRules.size(); i++) {
											%>
											<tr>
												<td><input type="hidden" id="ruleID" name="ruleID"
													value="<%=allRules.get(i).getRuleID()%>"> <%=allRules.get(i).getRuleName() != null ? allRules.get(i).getRuleName() : ""%>
												</td>
												<td><%=allRules.get(i).getRuleDescription() != null ? allRules.get(i).getRuleDescription() : ""%></td>
												<td>
													<%
														if (allRules.get(i).getInsertFlag() == 1) {
													%> Yes <%
														} else {
													%> No <%
														}
													%>
												</td>
												<td>
													<%
														if (allRules.get(i).getUpdateFlag() == 1) {
													%> Yes <%
														} else {
													%> No <%
														}
													%>
												</td>
												<td>
													<%
														if (allRules.get(i).getDeleteFlag() == 1) {
													%> Yes <%
														} else {
													%> No <%
														}
													%>
												</td>

												<td>
													<%
														if (allRules.get(i).getViewFlag() == 1) {
													%> Yes <%
														} else {
													%> No <%
														}
													%>
												</td>

												<td>
													<%
														if (allRules.get(i).getUserCreationFlag() == 1) {
													%> Yes<%
														} else {
													%> NO<%
														}
													%>
												</td>
												
												<td>
													<%
														if (allRules.get(i).getViewReportRoleFlag() == 1) {
													%> Yes<%
														} else {
													%> NO<%
														}
													%>
												</td>

												<td><%=allRules.get(i).getCreatedDate()%></td>


												<td><%=allRules.get(i).getCreatedByUserName()%></td>


												<td><a id="edit" style="color: blue;" href='#'
													onclick="prepareRuleObjForEdit(<%=allRules.get(i).getRuleID()%>,'<%=allRules.get(i).getRuleName()%>'
					,'<%=allRules.get(i).getRuleDescription()%>',<%=allRules.get(i).getInsertFlag()%>,<%=allRules.get(i).getUpdateFlag()%>,
					<%=allRules.get(i).getDeleteFlag()%>,<%=allRules.get(i).getViewFlag()%>,<%=allRules.get(i).getUserCreationFlag()%>,<%=allRules.get(i).getViewReportRoleFlag()%>)">edit</a></td>

												<!-- 									<td><input type="hidden" id="deleteFlag" name="deleteFlag" -->
												<!-- 										value="0"> <a -->
												<%-- 										onclick="prepareRuleObjForDelete(<%=allRules.get(i).getRuleID()%>)" --%>
												<!-- 										href='#'>delete</a></td> -->
											</tr>
											<%
												}
											%>
										</tbody>

									</table>
								</form>

							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>


				<div class="row">
					<div class="col-xs-12">

						<!-- /.box -->

						<div class="box">
							<h2>Create Screens</h2>
							<div class="box-header">
								<!--               <h3 class="box-title">Data Table With Full Features</h3> -->
							</div>
							<!-- /.box-header shady -->

							<div class="box-body scrollmenu">
								<%
									if (session.getAttribute("USER_ID") != null && session.getAttribute("USER_ID").equals(223)) {
								%>
								<form id="screenFrm"
									action="${pageContext.request.contextPath}/ScreenServlet"
									method="post">

									<table style="width: 50%; height: 200px;">
										<tr>
											<td>Screen Name</td>
											<td><input type="text" class="form-control"
												id="screenName" name="screenName"
												value="<%=screen.getScreenName() != null ? screen.getScreenName() : ""%>">
												<input type="hidden" id="screenIDHidden"
												name="screenIDHidden" value="<%=screenIDHid%>"></td>
										</tr>
										<tr>
											<td>Screen Description</td>
											<td><input type="text" id="screenDescription"
												name="screenDescription" class="form-control"
												value="<%=screen.getScreenDescription() != null ? screen.getScreenDescription() : ""%>"></td>
										</tr>
										<tr>
											<td>Application Name</td>
											<td><input type="text" id="applicationName"
												name="applicationName" class="form-control"
												value="<%=screen.getApplicationName() != null ? screen.getApplicationName() : ""%>"></td>
										</tr>
										<tr>
											<td>Role</td>
											<td><select id="ruleList" style="width: 540px;"
												name="ruleList" class=" form-control">
													<%
														for (Rule rules : allRules) {
													%>
													<option value="<%=rules.getRuleID()%>"><%=rules.getRuleName() != null ? rules.getRuleName() : ""%></option>
													<%
														}
													%>
											</select></td>
										</tr>
										<tr>
											<td colspan="2" align="center"><input type="submit"
												class="btn btn-info" name="submitScreen" id="submitScreen"
												value="Create Screen"></td>
										</tr>
										<tr>
											<td colspan="2" align="center">
												<%
													if (session.getAttribute("screenResult") != null) {
																String screenRes = "";
																screenRes = session.getAttribute("screenResult").toString();
																System.out.println(screenRes);
																session.removeAttribute("screenResult");
												%> <label id="screenResult" name="screenResult"
												style="color: red; font-weight: bold;"><%=screenRes%></label>
												<%
													}
												%>
											</td>
										</tr>
									</table>

									<table id="screenTbl"
										class="table table-bordered table-striped">
										<thead>
											<tr>
												<th>Screen Name</th>
												<th>Screen Description</th>
												<th>Application Name</th>
												<th>Role ID</th>
												<th>Created By</th>
												<th>Edit</th>
												<th>Delete</th>
											</tr>
										</thead>
										<tbody>

											<%
												for (int i = 0; i < allScreens.size(); i++) {
											%>
											<tr>
												<td><input type="hidden" id="ruleID" name="ruleID"
													value="<%=allScreens.get(i).getScreeID()%>"> <%=allScreens.get(i).getScreenName()%>
												</td>
												<td><%=allScreens.get(i).getScreenDescription()%></td>


												<td><%=allScreens.get(i).getApplicationName()%></td>
												<td><%=allScreens.get(i).getRuleID()%></td>
												<td><%=allScreens.get(i).getCreatedByUserID()%></td>


												<td><a id="edit" style="color: blue;" href='#'
													onclick="prepareScreenObjForEdit(<%=allScreens.get(i).getScreeID()%>,'<%=allScreens.get(i).getScreenName()%>'
					,'<%=allScreens.get(i).getScreenDescription()%>','<%=allScreens.get(i).getApplicationName()%>',
					<%=allScreens.get(i).getRuleID()%>)">edit</a></td>

												<td><input type="hidden" id="deleteFlag"
													name="deleteFlag" value="0"> <a
													onclick="prepareScreenObjForDelete(<%=allScreens.get(i).getScreeID()%>)"
													href='#'>delete</a></td>
											</tr>
											<%
												}
											%>
										</tbody>

									</table>

								</form>
								<%
									}
								%>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
			</section>
			<!-- /.content -->
		</div>




		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2019 <a href="#">BDC</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<aside class="control-sidebar control-sidebar-dark">
			<!-- Create the tabs -->
			<ul class="nav nav-tabs nav-justified control-sidebar-tabs">
				<li><a href="#control-sidebar-home-tab" data-toggle="tab"><i
						class="fa fa-home"></i></a></li>
				<li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i
						class="fa fa-gears"></i></a></li>
			</ul>
		</aside>
	</div>
	<!-- ./wrapper -->

	<!-- jQuery 3 -->
	<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap 3.3.7 -->
	<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- DataTables -->
	<script
		src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
	<script
		src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
	<!-- SlimScroll -->
	<script
		src="../../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<!-- FastClick -->
	<script src="../../bower_components/fastclick/lib/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="../../dist/js/adminlte.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../../dist/js/demo.js"></script>
	<!-- page script -->
	<script>
		$(function() {
			$('#roleTbl').DataTable(
					{
					'paging' : true,
					'lengthChange' : true,
					'searching' : true,
					'ordering' : false,
					'info' : true,
					'autoWidth' : false
				
					})
			$('#screenTbl').DataTable({
				'paging' : true,
				'lengthChange' : true,
				'searching' : true,
				'ordering' : false,
				'info' : true,
				'autoWidth' : false
			})
		})
	</script>


	<link rel="stylesheet"
		href="${pageContext.request.contextPath}/css/chosen.min.css">
	<script src="${pageContext.request.contextPath}/js/chosen.jquery.js"></script>

	<script>
jQuery(document).ready(function(){
	jQuery(".chosen").data("placeholder","Select Frameworks...").chosen();
});
</script>



	<script type="text/javascript">

	function prepareRuleObjForEdit(ruleID,ruleName,ruleDescription,insertFlag,updateFlag,deleteFlag,viewFlag,userCreationFlag,viewReportFlag)
	{	
		debugger;
		document.getElementById("ruleIDHidden").value = ruleID;
		document.getElementById("ruleName").value = ruleName;
		document.getElementById("ruleDescription").value = ruleDescription;
		if(insertFlag == 1)
			document.getElementById("insertFlag").checked = true;
		else
			document.getElementById("insertFlag").checked = false;
		if(updateFlag == 1)
			document.getElementById("updateFlag").checked = true;
		else
			document.getElementById("updateFlag").checked = false;
		if(deleteFlag == 1)
			document.getElementById("deleteFlag").checked = true;
		else
			document.getElementById("deleteFlag").checked = false;
		if(viewFlag == 1)
			document.getElementById("viewFlag").checked = true;
		else
			document.getElementById("viewFlag").checked = false;
		if(userCreationFlag == 1)
			document.getElementById("userCreationFlag").checked = true;
		else
			document.getElementById("userCreationFlag").checked = false;
		if(viewReportFlag == 1)
			document.getElementById("viewReportRole").checked = true;
		else
			document.getElementById("viewReportRole").checked = false;
		
		
	}

	function prepareRuleObjForDelete(ruleID){
		alert(ruleID);
	}

	function prepareScreenObjForEdit(screenID,ScreenName,ScreenDescription,applicationName,ruleID)
	{	
		document.getElementById("screenIDHidden").value = screenID;
// 		alert(document.getElementById("screenIDHidden").value);
		document.getElementById("screenName").value = ScreenName;
		document.getElementById("screenDescription").value = ScreenDescription;
		document.getElementById("applicationName").value = applicationName;
		document.getElementById("ruleList").value = ruleID;
	}

	function prepareScreenObjForDelete(screenID){
		alert(screenID);
		
	}
</script>



</body>

<%
	} else {
%>
<center>
	<h2>
		<label style="color: red; font-weight: bold;">You don't have
			Permission to access this Page!</label>
	</h2>
</center>
<%
	}
%>
</html>
