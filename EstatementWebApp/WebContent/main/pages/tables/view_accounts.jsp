<!DOCTYPE html>
<%@page import="com.bdc.estatement.dao.TransactionsDao"%>
<%@page import="com.bdc.estatement.util.Defines"%>
<%@page import="com.bdc.estatement.model.Account"%>
<%@page import="java.util.List"%>
<%@page import="com.bdc.usermanagement.ldab.UserPrivilege"%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>
<html>
<head>

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Accounts</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet"
	href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="../../bower_components/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="../../bower_components/Ionicons/css/ionicons.min.css">
<!-- DataTables -->
<link rel="stylesheet"
	href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<style>
div.scrollmenu {
	/*   background-color: #333; */
	overflow: auto;
	/*   white-space: nowrap; */
}

div.scrollmenu a {
	display: inline-block;
	/*   color: white; */
	text-align: center;
	padding: 14px;
	text-decoration: none;
}

div.scrollmenu a:hover {
	/*   background-color: #777; */
	
}
</style>
<%
	if (session.getAttribute("USER_ID") == null) {
		response.sendRedirect(request.getContextPath() + "/main/pages/tables/Login.jsp");
		return;
	}

	UserPrivilege userPrivilege = UserPrivilege.findAllUserPrivileges("view_accounts.jsp", request);
	int userId = session.getAttribute("USER_ID") != null ? (Integer) session.getAttribute("USER_ID") : 0;

	if (userPrivilege.getViewFlag() == 1) {
		List<Account> accounts = TransactionsDao.getAccounts();//(List<Account>) request.getAttribute("accountsList");
		String freqString = "";
%>



</head>
<body class="hold-transition skin-blue sidebar-mini">

	<div class="wrapper">
		<header class="main-header">
			<a href="#" class="logo"> <span class="logo-mini"><b>BDC</b></span>
				<span class="logo-lg"><b>E-Statement</b></span>
			</a>
			<nav class="navbar navbar-static-top">
				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="push-menu"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
			</nav>
		</header>


		<!-- 		Left Side Menu -->
		<jsp:include page="LeftSideMenu.jsp"></jsp:include>




		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>All Accounts</h1>
			</section>

			<!-- Main content -->
			<section class="content">
				<div class="row">
					<div class="col-xs-12">

						<!-- /.box -->

						<div class="box">
							<div class="box-header">
								<!--               <h3 class="box-title">Data Table With Full Features</h3> -->
							</div>
							<!-- /.box-header shady -->

							<div class="box-body scrollmenu">

								<table id="accountTbl"
									class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>Customer Code</th>
											<th>Account Number</th>
											<th>Customer Name</th>
											<th>Email</th>
											<th>Branch</th>
											<th>Mobile</th>
											<th>SMS</th>
											<th>E-Statement</th>
											<th>None-Performing Account</th>
											<th>None-Performing Client</th>
											<th>Created Date</th>
											<th>Statement frequency</th>
											<th>Next Send Date</th>
											<th>Status</th>
											<%
												if (userPrivilege.getUpdateFlag() == 1) {
											%>
											<th>Approve</th>
											<th>Edit</th>
											<%
												}
											%>
											<%
												if (userPrivilege.getDeleteFlag() == 1) {
											%>
											<th>Delete</th>
											<%
												}
											%>
										</tr>
									</thead>
									<tbody>

										<%
											for (Account account : accounts) {
										%>
										<tr>
											<td>
												<%
													if (account.getCustomerCode() != null) {
												%><%=account.getCustomerCode()%> <%
 	}
 %>
											</td>
											<td>
												<%
													if (account.getAccountNumber() != null) {
												%><%=account.getAccountNumber()%> <%
 	}
 %>
											</td>
											<td>
												<%
													if (account.getCustomerName() != null) {
												%><%=account.getCustomerName()%> <%
 	}
 %>
											</td>
											<td
												style="max-width: 180px; width: 180px; word-wrap: break-word;">
												<%
													if (account.getEmail() != null) {
												%><%=account.getEmail()%> <%
 	}
 %>
											</td>
											<td>
												<%
													if (account.getBranchName() != null) {
												%><%=account.getBranchName()%> <%
 	}
 %>
											</td>
											<td>
												<%
													if (account.getMobile() != null) {
												%><%=account.getMobile()%> <%
 	}
 %>
											</td>
											<td style="width: 20px; max-width: 20px;">
												<%
													if (account.getSmsFlag() == 1) {
												%> Yes <%
													} else {
												%> NO <%
													}
												%>
											</td>

											<td style="width: 20px; max-width: 20px;">
												<%
													if (account.getEstatementFlag() == 1) {
												%> Yes <%
													} else {
												%> NO <%
													}
												%>
											</td>

											<td style="width: 20px; max-width: 20px;">
												<%
													if (account.getNonePerformingAccount() != null
																	&& !account.getNonePerformingAccount().equalsIgnoreCase("N")) {
												%> Yes <%
													} else {
												%> NO <%
													}
												%>
											</td>

											<td style="width: 20px; max-width: 20px;">
												<%
													if (account.getNonePerformingCustomer() != null
																	&& (account.getNonePerformingCustomer().contains("1008")
																			|| account.getNonePerformingCustomer().contains("10081")
																			|| account.getNonePerformingCustomer().contains("10082")
																			|| account.getNonePerformingCustomer().contains("10083")
																			|| account.getNonePerformingCustomer().contains("1009")
																			|| account.getNonePerformingCustomer().contains("10091")
																			|| account.getNonePerformingCustomer().contains("10092")
																			|| account.getNonePerformingCustomer().contains("10093")
																			|| account.getNonePerformingCustomer().contains("1010")
																			|| account.getNonePerformingCustomer().contains("10101")
																			|| account.getNonePerformingCustomer().contains("10102")
																			|| account.getNonePerformingCustomer().contains("10103"))) {
												%> Yes <%
													} else {
												%> NO <%
													}
												%>
											</td>

											<td style="width: 20px; max-width: 20px;"><%=account.getCreationDate()%>
											</td>
											<%
												switch (account.getFrequancyStmt()) {
															case Defines.Daily :
																freqString = "Daily";
																break;
															case Defines.Weekly :
																freqString = "Weekly";
																break;
															// 													case Defines.FortNightly :
															// 														freqString = "Fort Nightly";
															// 														break;
															case Defines.Monthly :
																freqString = "Monthly";
																break;
															// 													case Defines.Bi_Monthly :
															// 														freqString = "Bi_Monthly";
															// 														break;
															case Defines.Quarterly :
																freqString = "Quarterly";
																break;
															case Defines.Half_Yearly :
																freqString = "Semi-Annually";
																break;
															case Defines.Yearly :
																freqString = "Annually";
																break;
															default :
																freqString = "None";
																break;
														}
											%>
											<td><%=freqString%></td>
											<td>
												<%
													if (account.getNextDate() != null) {
												%><%=account.getNextDate()%> <%
 	}
 %>
											</td>

											<td>
												<%
													if (account.getStatus() != null) {
												%> <%=account.getStatus()%></td>
											<%
												}
											%>
											
											<%
												if (userPrivilege.getUpdateFlag() == 1) {
											%>
											
											<td>
												<%
													if (account.getIsApproved() == 0 && userId != 0 && userId != account.getCreatedBy()) {
												%> <a
												href='<%=request.getContextPath()%>/approve?accountNO=<%=account.getAccountNumber()%>'>Approve</a>
												<%
													}
												%>
											</td>
											
											<td style="width: 30px; max-width: 30px;">
												<%
													if (account.getIsApproved() != 0) {
												%> <a
												href='<%=request.getContextPath()%>/EditServlet?accountNo=<%=account.getAccountNumber()%>&freq=<%=account.getFrequancyStmt()%>+&edit=account'>Edit</a>
												<%
													}
												%>
											</td>
											<%
												}
														if (userPrivilege.getDeleteFlag() == 1) {
											%>
											<td style="width: 30px; max-width: 30px;"><a
												href='<%=request.getContextPath()%>/DeleteServlet?accountNo=<%=account.getAccountNumber()%>&freq=<%=account.getFrequancyStmt()%>&customerCode=<%=account.getCustomerCode()%>'>Delete
											</a></td>
											<%
												}
											%>
										</tr>
										<%
											}
										%>
									</tbody>
								</table>
								<a class="btn btn-info"
									href="${pageContext.request.contextPath}/account?export=excel">Export
									Excel</a>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2019 <a href="#">BDC</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<aside class="control-sidebar control-sidebar-dark">
			<!-- Create the tabs -->
			<ul class="nav nav-tabs nav-justified control-sidebar-tabs">
				<li><a href="#control-sidebar-home-tab" data-toggle="tab"><i
						class="fa fa-home"></i></a></li>
				<li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i
						class="fa fa-gears"></i></a></li>
			</ul>
		</aside>
	</div>
	<!-- ./wrapper -->

	<!-- jQuery 3 -->
	<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap 3.3.7 -->
	<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- DataTables -->
	<script
		src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
	<script
		src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
	<!-- SlimScroll -->
	<script
		src="../../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<!-- FastClick -->
	<script src="../../bower_components/fastclick/lib/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="../../dist/js/adminlte.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../../dist/js/demo.js"></script>
	<!-- page script -->
	<script>
		$(function() {
			$('#accountTbl').DataTable({
				'paging' : true,
				'lengthChange' : true,
				'searching' : true,
				'ordering' : false,
				'info' : true,
				'autoWidth' : false
			})
		})
	</script>
</body>

<%
	} else {
%>
<br>
<br>
<center>
	<h2>
		<label style="color: red; font-weight: bold;">You don't have
			Permission to access this Page!</label>
	</h2>
</center>
<%
	}
%>
</html>
