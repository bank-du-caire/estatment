<!DOCTYPE html>
<%@page import="com.bdc.estatement.model.Customer"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.bdc.estatement.dao.TransactionsDao"%>
<%@page import="com.bdc.estatement.util.Defines"%>
<%@page import="com.bdc.estatement.model.MeezaRegCustomers"%>
<%@page import="java.util.List"%>
<%@page import="com.bdc.usermanagement.ldab.UserPrivilege"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>
<html>
<head>

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>MeezaRegCustomer</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet"
	href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="../../bower_components/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="../../bower_components/Ionicons/css/ionicons.min.css">
<!-- DataTables -->
<link rel="stylesheet"
	href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<style>
div.scrollmenu {
	/*   background-color: #333; */
	overflow: auto;
	/*   white-space: nowrap; */
}

div.scrollmenu a {
	display: inline-block;
	/*   color: white; */
	text-align: center;
	padding: 14px;
	text-decoration: none;
}

div.scrollmenu a:hover {
	/*   background-color: #777; */
	
}
</style>
<%



	List<MeezaRegCustomers> meezaRegCustomerList = TransactionsDao.getAllMeezaRegCustomer();
	String formType = "";

	Customer customer = (session.getAttribute("customer") != null)
			? (Customer) session.getAttribute("customer")
			: null;

	if (customer != null) {
		formType = "save";
		System.out.println(customer.getName());
		session.setAttribute("customer", null);

	}

	else {
		formType = "search";
	}
%>
<script type="text/javascript">
 
 
 </script>
</head>
<body class="hold-transition skin-blue sidebar-mini">

	<div class="wrapper">
		<header class="main-header">
			<a href="#" class="logo"> <span class="logo-mini"><b>BDC</b></span>
				<span class="logo-lg"><b>E-Statement</b></span>
			</a>
			<nav class="navbar navbar-static-top">
				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="push-menu"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
			</nav>
		</header>

		<jsp:include page="LeftSideMenu.jsp"></jsp:include>
		
		
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>Meeza Customer</h1>
			</section>
			<section class="content">
				<div class="row">
					<div class="col-xs-12">

						<!-- /.box -->


						<div class="box">
							

							<div class="box-header">
								<!--               <h3 class="box-title">Data Table With Full Features</h3> -->
								<div class="container" id="searchDiv">
									<form method="get" onsubmit="return ValidateSearchForm();"
										action='${pageContext.request.contextPath}/MezzaRegCustomers'>
										<label>Customer ID</label><input type="text"
											id="custIdSearchParam" name="custId" width="10px"
											value="<%=(customer != null) ? customer.getId() : ""%>">
										<button type="submit" name="save" class="btn btn-primary">Search</button>
										<br>
									</form>
								</div>
								<div class="container" id="saveDiv" style="display: none;">
									<form method="post"
										action='${pageContext.request.contextPath}/MezzaRegCustomers'>

										<table>
											<tr>
												<td><label>Customer ID</label></td>
												<td><input type="text" name="custId" width="10px"
													value="<%=(customer != null) ? customer.getId() : ""%>"></td>
											</tr>
											<tr>
												<td><label>Customer Name</label></td>
												<td><input type="text" name="name"
													value="<%=(customer != null) ? customer.getName() : ""%>"></td>
											</tr>
											<tr>
												<td><label>Email</label></td>
												<td><input type="text" name="email" size="100"></td>
											</tr>
											<tr>
												<td><label>Address</label></td>
												<td><input type="text" name="address" size="100"
												value="<%=(customer != null) ? customer.getAddress() : ""%>"></td>
											</tr>
											<tr>
												<td>
													<button type="submit" name="save" class="btn btn-primary">Save</button>
												</td>
												<td>
													<button type="button" onclick="reloadSearchPage()"
														name="cancelBtn" class="btn btn-primary">Cancel</button>

												</td>
											</tr>
										</table>


									</form>
								</div>


							</div>
							<!-- /.box-header shady -->

							<div class="box-body scrollmenu">

								<table id="accountTbl"
									class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>Customer Code</th>
											<th>Account Number</th>
											<th>Customer Name</th>
											<th>Email</th>
											<th>Created Date</th>
											<th>Next Send Date</th>
											<th>Status</th>
											<th>CREATED_BY</th>
											<th>Approve</th>
											<th>Reject</th>
										</tr>
									</thead>
									<tbody>
										<%
											for (MeezaRegCustomers customers : meezaRegCustomerList) {
										%>
										<tr>
											<td>
												<%
													if (customers.getCOD_CUST() != 0) {
												%><%=customers.getCOD_CUST()%> <%
 	}
 %>
											</td>
											<td>
												<%
													if (customers.getCOD_ACCT_NO() != null) {
												%><%=customers.getCOD_ACCT_NO()%> <%
 	}
 %>
											</td>
											<td>
												<%
													if (customers.getCUSTOMER_NAME() != null) {
												%><%=customers.getCUSTOMER_NAME()%> <%
 	}
 %>
											</td>
											<td>
												<%
													if (customers.getEMAIL() != null) {
												%><%=customers.getEMAIL()%> <%
 	}
 %>
											</td>
											<td>
												<%
													if (customers.getCREATION_DATE() != null) {
												%><%=customers.getCREATION_DATE()%> <%
 	}
 %>
											</td>
											<td>
												<%
													if (customers.getSTMT_NEXT_DATE() != null) {
												%><%=customers.getSTMT_NEXT_DATE()%> <%
 	}
 %>
											</td>
											<td>
												<%
													
												%><%=customers.getIS_APPROVED()%>

											</td>
											<td>
												<%
													if (customers.getCREATED_BY_USER_ID() != 0) {
												%><%=customers.getCREATED_BY_USER_ID()%> <%
 	}
 %>
											</td>
											<td><button type="button" value="<%=customers.getCOD_CUST() %>"
												name="approve" onClick='approve(this)' >approve</button></td>
										
										
											<td><button type="button" value="<%=customers.getCOD_CUST() %>"
												name="reject" onClick='reject(this)' >reject</button></td>
										
										
										<%
											}
										%>

									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>


		<script type="text/javascript">


var formType="<%=formType%>";
 			if (formType == "search") {
				document.getElementById("searchDiv").style.display = "block";
				document.getElementById("saveDiv").style.display = "none";

			} else {
				document.getElementById("searchDiv").style.display = "none";
				document.getElementById("saveDiv").style.display = "block";

			}
			
			<%formType = "";
			customer = null;%>
			formType="";
			
 			
//======================================================================================================================================================================
	
			
			
			
			function reloadSearchPage(){
				
				 location.href ="<%=request.getContextPath()%>"	+ "/MezzaRegCustomers";

			}
//======================================================================================================================================================================
	function changeStatus(codeCustId ,newStatus ){
		 
	var param=JSON.stringify({codeCustId: codeCustId, status: newStatus});
		var xhr = new XMLHttpRequest();
	    xhr.onreadystatechange = function() {
	        if (xhr.readyState == 4) {
	            var data = xhr.responseText;
	            alert(data);
	        }
	    }
	    xhr.open('PUT', '${pageContext.request.contextPath}/MezzaRegCustomers', true);
	    xhr.send(param);
	
	
	
	
	
}

	
	
//======================================================================================================================================================================
			function approve( clickedButton){			
	
				changeStatus(clickedButton.value,1);
				reloadSearchPage();
 	
}
	
//======================================================================================================================================================================

				function reject( clickedButton){
					changeStatus(clickedButton.value,0);
					reloadSearchPage();
 	
	
}
	
//======================================================================================================================================================================

	function ValidateSearchForm() {
				var custId = document.getElementById("custIdSearchParam").value;

				if (custId == "" || custId == null) {
					alert("Sorry, Please insert Customer ID");
					return false;
				}
				return true;
			}
		</script>
</body>
</html>