<%@page import="com.bdc.usermanagement.ldab.UserManagmentDBTransactions"%>
<%@page import="com.bdc.usermanagement.ldab.UserPrivilege"%>
<%
	UserPrivilege priv = UserPrivilege.findAllUserPrivileges("view_accounts.jsp", request);
	boolean hostToHost = UserPrivilege.checkIfHostToHostPrivilege(request);
	UserManagmentDBTransactions dbTransactions = new UserManagmentDBTransactions();
	boolean meezaPrepaid = dbTransactions.checkIfMeezaPrepaid(session.getAttribute("USER_ID").toString());
%>

<aside class="main-sidebar">
	<section class="sidebar">
		<div class="user-panel">
			<div
				style="height: 60px; max-height: 60px; width: 80px; max-width: 80px; margin-left: -8px;">
				<img src="../../dist/img/BDC_logo.png" class="img" alt="User Image">
			</div>
		</div>
		<ul class="sidebar-menu" data-widget="tree">
			<!-- 			<li class="treeview"><a href="#"> <i class="fa fa-edit"></i> -->
			<!-- 					<span>Inputs</span> <span class="pull-right-container"> <i -->
			<!-- 						class="fa fa-angle-left pull-right"></i> -->
			<!-- 				</span> -->
			<!-- 			</a> -->
			<!-- 				<ul class="treeview-menu"> -->



			<!-- 					<li><a href="../forms/general.html"><i -->
			<!-- 							class="fa fa-circle-o"></i> General Elements</a></li> -->
			<!-- 					<li><a href="../forms/advanced.html"><i -->
			<!-- 							class="fa fa-circle-o"></i> Advanced Elements</a></li> -->
			<!-- 					<li><a href="../forms/editors.html"><i -->
			<!-- 							class="fa fa-circle-o"></i> Editors</a></li> -->
			<!-- 				</ul></li> -->

			<%
				if (!hostToHost && ((priv.getUserCreation() == 0 && priv.getDeleteFlag() == 1 && priv.getInsertFlag() == 1
						&& priv.getUpdateFlag() == 1 && priv.getViewFlag() == 1)
						|| (priv.getUserCreation() == 1 && priv.getDeleteFlag() == 1 && priv.getInsertFlag() == 1
								&& priv.getUpdateFlag() == 1 && priv.getViewFlag() == 1)
						|| priv.getViewFlag() == 1)) {
			%>
			<li class="treeview active"><a href="#"> <i
					class="fa fa-table"></i> <span>Estatment - SMS</span> <span
					class="pull-right-container"> <i
						class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
				<ul class="treeview-menu">
					<li><a href="view_accounts.jsp"><i class="fa fa-circle-o"></i>
							Accounts</a></li>
					<li><a href="new_requstes.jsp"><i class="fa fa-circle-o"></i>
							New Requests</a></li>
				</ul></li>



			<%
				}
				if (hostToHost) {
			%>
			<li class="treeview "><a href="#"> <i class="fa fa-table"></i>
					<span>Host To Host</span> <span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
				<ul class="treeview-menu">
					<li><a href="HosttoHost.jsp"><i class="fa fa-circle-o"></i>
							Host to Host Service</a></li>
				</ul></li>
			<%
				}
				if (!hostToHost && ((priv.getViewFlag() == 0 && priv.getInsertFlag() == 0 && priv.getUpdateFlag() == 0
						&& priv.getDeleteFlag() == 0 && priv.getUserCreation() == 1)
						|| (priv.getUserCreation() == 1 && priv.getDeleteFlag() == 1 && priv.getInsertFlag() == 1
								&& priv.getUpdateFlag() == 1 && priv.getViewFlag() == 1))) {
			%>

			<li class="treeview"><a href="#"> <i class="fa fa-edit"></i>
					<span>User Management</span> <span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
				<ul class="treeview-menu">
					<li><a
						href="<%=request.getContextPath()%>/main/pages/tables/UserManagement.jsp"><i
							class="fa fa-circle-o"></i> User Management</a></li>
					<!-- 					<li><a -->
					<%-- 						href="<%=request.getContextPath()%>/main/pages/tables/RulesAndScreens.jsp"><i --%>
					<!-- 							class="fa fa-circle-o"></i> Roles</a></li> -->
				</ul></li>
			<%
				}
				// 				System.out.println("priv.getReportView()[" + priv.getReportView() + "]");
				if (!hostToHost && (priv.getReportView() == 1 || priv.getViewFlag() == 1)) {
			%>


			<li class="treeview"><a href="#"> <i class="fa fa-folder"></i>
					<span>Audits</span> <span class="pull-right-container"> <i
						class="fa fa-angle-left pull-right"></i>
				</span>
			</a>

				<ul class="treeview-menu">

					<li><a
						href="<%=request.getContextPath()%>/main/pages/tables/AccountsAudit.jsp"><i
							class="fa fa-circle-o"></i> Account Audits</a></li>
					<li><a
						href="<%=request.getContextPath()%>/main/pages/tables/UsersAudit.jsp"><i
							class="fa fa-circle-o"></i> User Audits</a></li>


				</ul></li>

			<li class="treeview"><a href="#"> <i class="fa fa-folder"></i>
					<span>History</span> <span class="pull-right-container"> <i
						class="fa fa-angle-left pull-right"></i>
				</span>
			</a>

				<ul class="treeview-menu">
					<li><a
						href="<%=request.getContextPath()%>/main/pages/tables/view_estatment_history.jsp"><i
							class="fa fa-circle-o"></i> E-statement History</a></li>
					<li><a
						href="<%=request.getContextPath()%>/main/pages/tables/sms_history.jsp"><i
							class="fa fa-circle-o"></i> SMS History</a></li>
				</ul></li>

			<%
				}
				if (meezaPrepaid) {
			%>				
				
				<li><a href="<%=request.getContextPath()%>/main/pages/tables/Mezza_Reg_Customers.jsp"><i
					class="fa fa-circle-o"></i> Meeza Registered Customers</a></li>
				
			<%
				}
			%>
			
			
			<li><a href="${pageContext.request.contextPath}/Logout"><i
					class="fa fa-circle-o"></i> Log out</a></li>
		</ul>
	</section>
	<!-- /.sidebar -->
</aside>