<!DOCTYPE html>
<%@page import="com.bdc.usermanagement.ldab.UserManagmentDBTransactions"%>
<%@page import="com.bdc.usermanagement.ldab.User"%>
<%@page import="com.bdc.usermanagement.ldab.Rule"%>
<%@page import="com.bdc.usermanagement.ldab.UserRule"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.bdc.estatement.model.EstatementHistory"%>
<%@page import="com.bdc.estatement.dao.TransactionsDao"%>
<%@page import="com.bdc.estatement.util.Defines"%>
<%@page import="com.bdc.estatement.model.Account"%>
<%@page import="java.util.List"%>
<%@page import="com.bdc.usermanagement.ldab.UserPrivilege"%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>
<html>
<head>

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>User Management</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet"
	href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="../../bower_components/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="../../bower_components/Ionicons/css/ionicons.min.css">
<!-- DataTables -->
<link rel="stylesheet"
	href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<style>
div.scrollmenu {
	/*   background-color: #333; */
	overflow: auto;
	/*   white-space: nowrap; */
}

div.scrollmenu a {
	display: inline-block;
	/*   color: white; */
	text-align: center;
	padding: 14px;
	text-decoration: none;
}

div.scrollmenu a:hover {
	/*   background-color: #777; */
	
}
</style>

<%
	String approveUserIDHid = "", approveRoleIDHid = "";
	if (session.getAttribute("USER_ID") == null) {
		response.sendRedirect(request.getContextPath() + "/main/pages/tables/Login.jsp");
		return;
	}

	UserPrivilege userPrivilege = UserPrivilege.findAllUserPrivileges("UserManagement.jsp", request);

	if (userPrivilege.getUserCreation() == 1) {
		int flagImmediateDelete = 0;
		String actionDescription = "";
		User user = new User();
		String res = "";
		if (session.getAttribute("result") != null) {
			res = session.getAttribute("result").toString();
			session.setAttribute("result", null);
		}
		List<User> allUser = new ArrayList<User>();
		UserManagmentDBTransactions dbQueries = new UserManagmentDBTransactions();
		String searchUserName = request.getParameter("userNameSearch");
		int loggedInUserId = session.getAttribute("USER_ID")!=null? Integer.valueOf(session.getAttribute("USER_ID").toString()) : 0;
		allUser = dbQueries.findAllUsersWithoutLoggedInUser(loggedInUserId);
		int userID = 0;
		int hid_del_userID = 0, ur_userIDHid = 0, ur_ruleIDHid = 0, userRuleEditFlag = 0,
				userRuleDeleteFlag = 0;

		List<Rule> allRules = new ArrayList<Rule>();
		allRules = dbQueries.findAllRules();

		List<UserRule> allUserRules = new ArrayList<UserRule>();
		allUserRules = dbQueries.findAllUserRules();
%>

</head>



<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<header class="main-header">
			<a href="#" class="logo"> <span class="logo-mini"><b>BDC</b></span>
				<span class="logo-lg"><b>E-Statement</b></span>
			</a>
			<nav class="navbar navbar-static-top">
				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="push-menu"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
			</nav>
		</header>


		<!-- 		Left Side Menu -->
		<jsp:include page="LeftSideMenu.jsp"></jsp:include>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>User Management</h1>
			</section>



			<!-- Main content -->
			<section class="content">
				<!-- 			shady box -->
				<div class="row">
					<div class="col-xs-12">

						<!-- /.box -->

						<div class="box">
							<h2>Create Users</h2>
							<div class="box-header">
								<!--               <h3 class="box-title">Data Table With Full Features</h3> -->
							</div>
							<!-- /.box-header shady -->

							<div class="box-body scrollmenu">

								<form action="${pageContext.request.contextPath}/UserMangement"
									method="post">

									<input type="hidden" id="hid_del_userID" name="hid_del_userID"
										value="hid_del_userID">
									<table width="50%">
										<tr>
											<td>User Name</td>
											<td><input type="hidden" id="userIDHidden"
												name="userIDHidden" value="<%=userID%>"> <input
												type="text" class="form-control" placeholder="User Name"
												value="<%=user.getUserName()%>" id="userName"
												name="userName"></td>
											<td></td>
										</tr>
										<tr>
											<td colspan="3"><br> <input type="checkbox"
												id="locked" name="locked"> Locked 
<!-- 												<input type="checkbox" id="disabled" name="disabled">Disabled -->
											</td>
										</tr>

										<tr>
											<td colspan="3" align="center"><br> <input
												type="submit" class="btn btn-info " id="submit"
												name="subimt" value="Create User"></td>

										</tr>
										<%
											if (!res.isEmpty()) {
										%>
										<tr>

											<td colspan="3" align="center"><label id="result"
												name="result" style="color: red; font-weight: bold;"><%=res%></label>
												<%
													res = "";
												%></td>
										</tr>
										<%
											}
										%>
									</table>

									<input type="hidden" id="flagImmediateDelete"
										name="flagImmediateDelete" value="<%=flagImmediateDelete%>">
									
									<input type="hidden" id="actionDescription"
										name="actionDescription" value="<%=actionDescription%>">

									<input type="hidden" id="approveUserIDHidden"
										name="approveUserIDHidden" value="<%=approveUserIDHid%>">


									<table id="userManagementTbl"
										class="table table-bordered table-striped">
										<thead>
											<tr>
												<th>User Name</th>
												<!-- 										<th>E-Mail</th> -->
												<!-- 										<th>Branch</th> -->
												<th>Locked</th>
												
												<th>Created By User</th>
												<th>Created Date</th>
												<th>Last Login Date</th>
												<th>Status</th>
												<th>Action Description</th>
												<th>Approve</th>
												<th>Reject</th>
												<th>Edit</th>
												<th>Delete</th>
											</tr>
										</thead>
										<tbody>

											<%
												for (int i = 0; i < allUser.size(); i++) {
											%>
											<tr>
												<td><input type="hidden" id="UserID" name="UserID"
													value="<%=allUser.get(i).getUserID()%>"> <%=allUser.get(i).getUserName()%>
												</td>
												<%-- 										<td><%=allUser.get(i).getMail()%></td> --%>
												<%-- 										<td><%=allUser.get(i).getBranch()%></td> --%>
												<td>
													<%
														if (allUser.get(i).getFlagLocked() == 1) {
													%> Yes <%
														} else {
													%> No <%
														}
													%>
												</td>
												
												<td><input type="hidden" name="CreatedByUserID" id=""
													value="<%=allUser.get(i).getCreatedByUserID()%>"> <%=allUser.get(i).getCreatedByUserName()%>
												</td>
												<td><%=allUser.get(i).getCreatedDate()%></td>
												<td><%=allUser.get(i).getLastLoginDate()!= null?allUser.get(i).getLastLoginDate():"" %></td>
												<td>
													<%
														if (allUser.get(i).getApproveFlag() == 0 || allUser.get(i).getFlagDeleted() == 1) {
													%> Pending <%
														} else {
													%> Approved <%
														}
													%>
												</td>
												<td>
												<%if(allUser.get(i).getDescription() != null){%>
													<%=allUser.get(i).getDescription()%>
												<% }else{  %>
												
												<%} %>
												</td>
												
												<td>
													<%
													System.out.println("allUser.get(i).getFlagDeleted():::::"+allUser.get(2).getFlagDeleted());
														if ((allUser.get(i).getApproveFlag() == 0 || allUser.get(i).getFlagDeleted() == 1)
																		&& !allUser.get(i).getCreatedByUserName().equals(session.getAttribute("USER_Name"))) {
													%> <a style="color: blue;" href='#'
													onclick="prepareUserObjForApprove('<%=allUser.get(i).getUserID()%>','<%=allUser.get(i).getFlagDeleted()%>')">Approve</a>
													<%
														}
													%>
												</td>
												
												<td>
													<%
													System.out.println("allUser.get(i).getFlagDeleted():::::"+allUser.get(2).getFlagDeleted());
														if ((allUser.get(i).getApproveFlag() == 0 || allUser.get(i).getFlagDeleted() == 1)
																		&& !allUser.get(i).getCreatedByUserName().equals(session.getAttribute("USER_Name"))) {
													%> <a style="color: blue;" href='#'
													onclick="prepareUserObjForReject('<%=allUser.get(i).getUserID()%>','<%=allUser.get(i).getDescription()%>')">Reject</a>
													<%
														}
													%>
												</td>

												<td>
													<%
													System.out.println("allUser.get(i).getApproveFlag()::::"+allUser.get(2).getApproveFlag());
														if (allUser.get(i).getApproveFlag() == 1 && allUser.get(i).getFlagDeleted() == 0) {
													%> <a id="edit" style="color: blue;" href='#'
													onclick="prepareUserObjForEdit(<%=allUser.get(i).getUserID()%>,'<%=allUser.get(i).getUserName()%>'
					,<%=allUser.get(i).getFlagLocked()%>,<%=allUser.get(i).getFlagDisabled()%>,
					'<%=allUser.get(i).getCreatedDate()%>',<%=allUser.get(i).getCreatedByUserID()%>)">edit</a>
													<%
														}
													%>
												</td>
													<td><%
														if (allUser.get(i).getApproveFlag() == 1 && allUser.get(i).getFlagDeleted() == 0) {
													%>
												<input type="hidden" id="deleteFlag"
													name="deleteFlag" value="0"> <a
													onclick="prepareUserObjForDelete(<%=allUser.get(i).getUserID()%>)"
													href='#'>delete</a>
													<%} %></td>
											</tr>
											<%
												}
											%>
										</tbody>

									</table>


									<!-- 									export here -->
									<a class="btn btn-info"
										href="${pageContext.request.contextPath}/UserMangement?export=excel">Export
										Excel</a>
								</form>

							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>


				<div class="row">
					<div class="col-xs-12">

						<!-- /.box -->

						<div class="box">
							<h2>Assign Roles to User</h2>
							<div class="box-header">
								<!--               <h3 class="box-title">Data Table With Full Features</h3> -->
							</div>
							<!-- /.box-header shady -->

							<div class="box-body scrollmenu">
								<form
									action="${pageContext.request.contextPath}/UserRulesServlet"
									method="post">

									<input type="hidden" id="userRuleDeleteFlag"
										name="userRuleDeleteFlag" value="<%=userRuleDeleteFlag%>">




									<table width="50%" style="height: 150px;">
										<tr>
											<td>User Name</td>
											<td><input type="hidden" value="<%=ur_userIDHid%>"
												id="ur_userIDHid" name="ur_userIDHid"> <input
												type="hidden" value="<%=userRuleEditFlag%>"
												id="userRuleEditFlag" name="userRuleEditFlag"> <select
												id="userList" name="userList" class="chosen"
												style="width: 100%; max-width: 100%;">
													<%
														for (User users : allUser) {
													%>
													<option value="<%=users.getUserID()%>"><%=users.getUserName() != null ? users.getUserName() : ""%></option>
													<%
														}
													%>
											</select></td>
										</tr>

										<tr>
											<td>Roles</td>
											<td><input type="hidden" value="<%=ur_ruleIDHid%>"
												id="ur_ruleIDHid" name="ur_ruleIDHid"> <select
												multiple="true" id="ruleList" name="ruleList" class="chosen"
												style="width: 100%; max-width: 100%;">
													<%
														for (Rule rules : allRules) {
													%>
													<%
														if (ur_ruleIDHid != 0 && ur_ruleIDHid == rules.getRuleID()) {
													%>
													<option selected value="<%=rules.getRuleID()%>"><%=rules.getRuleName() != null ? rules.getRuleName() : ""%></option>
													<%
														} else {
													%>
													<option value="<%=rules.getRuleID()%>"><%=rules.getRuleName() != null ? rules.getRuleName() : ""%></option>
													<%
														}
															}
													%>
											</select></td>
										</tr>
										<tr>

											<td colspan="2" align="center"><br> <input
												type="submit" class="btn btn-info" id="submit_"
												name="submit_" value="Create"></td>
										</tr>
										<tr>
											<td colspan="2" align="center">
												<%
													if (session.getAttribute("userRuleResult") != null) {
															String u_res = session.getAttribute("userRuleResult").toString();
															session.removeAttribute("userRuleResult");
												%> <br> <label style="color: red; font-weight: bold;"
												id="userRuleResult" name="userRuleResult"><%=u_res%></label>
												<%
													}
												%>
											</td>
										</tr>
									</table>

									<input type="hidden" id="approveUserIDHid"
										name="approveUserIDHid" value="<%=approveUserIDHid%>">

									<input type="hidden" id="approveRoleIDHid"
										name="approveRoleIDHid" value="<%=approveRoleIDHid%>">

									<table id="userRoleTbl"
										class="table table-bordered table-striped">
										<thead>
											<tr>
												<th>User Name</th>
												<th>Role Name</th>
												<th>Created By User</th>
												<th>Created Date</th>
												<th>Last Login Date</th>
												<th>Status</th>
												<th>Action Description</th>
												<th>Approve</th>
												<!-- 												<th>Edit</th> -->
												<th>Delete</th>
											</tr>
										</thead>
										<tbody>

											<%
												for (int i = 0; i < allUserRules.size(); i++) {
											%>
											<tr>
												<td><input type="hidden" id="UserID_UR"
													name="UserID_UR"
													value="<%=allUserRules.get(i).getUserID()%>"> <%=allUserRules.get(i).getUserName()%>
												</td>
												<td><input type="hidden" id="RuleID_UR"
													name="RuleID_UR"
													value="<%=allUserRules.get(i).getRuleID()%>"> <%=allUserRules.get(i).getRuleName()%>
												</td>
												<td><%=allUserRules.get(i).getCreatedByUserName()%></td>
												<td><%=allUserRules.get(i).getCreatedDate()%></td>
												<td><%=allUserRules.get(i).getLastLoginDate()!=null?allUserRules.get(i).getLastLoginDate():"" %></td>
												<td>
													<%
														if (allUserRules.get(i).getApproveFlag() == 1) {
													%>Approved <%
														} else {
													%> Pending<%
														}
													%>
												</td>
												<td>
													<%=allUserRules.get(i).getDescription() %>
												</td>
												<td>
													<%
														if (allUserRules.get(i).getApproveFlag() == 0
																		&& !allUserRules.get(i).getCreatedByUserName().equals(session.getAttribute("USER_Name"))) {
													%> <a style="color: blue;" href='#'
													onclick="prepareUserRoleForApprove('<%=allUserRules.get(i).getUserID()%>','<%=allUserRules.get(i).getRuleID()%>')">Approve</a>
													<%
														}
													%>
												</td>
												<!-- 												<td><a id="edit" style="color: blue;" href='#' -->
												<%-- 													onclick="prepareUserRuleObjForEdit(<%=allUserRules.get(i).getUserID()%>,<%=allUserRules.get(i).getRuleID()%>)">edit</a></td> --%>

												<td><a href='#'
													onclick="prepareUserRuleObjForDelete(<%=allUserRules.get(i).getUserID()%>,<%=allUserRules.get(i).getRuleID()%>)">
														delete</a></td>
											</tr>
											<%
												}
											%>
										</tbody>
									</table>
									<a class="btn btn-info"
										href="${pageContext.request.contextPath}/UserRulesServlet?export=excel">Export
										Excel</a>
								</form>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
			</section>
			<!-- /.content -->
		</div>




		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2019 <a href="#">BDC</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<aside class="control-sidebar control-sidebar-dark">
			<!-- Create the tabs -->
			<ul class="nav nav-tabs nav-justified control-sidebar-tabs">
				<li><a href="#control-sidebar-home-tab" data-toggle="tab"><i
						class="fa fa-home"></i></a></li>
				<li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i
						class="fa fa-gears"></i></a></li>
			</ul>
		</aside>
	</div>
	<!-- ./wrapper -->

	<!-- jQuery 3 -->
	<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap 3.3.7 -->
	<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- DataTables -->
	<script
		src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
	<script
		src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
	<!-- SlimScroll -->
	<script
		src="../../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<!-- FastClick -->
	<script src="../../bower_components/fastclick/lib/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="../../dist/js/adminlte.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../../dist/js/demo.js"></script>
	<!-- page script -->
	<script>
		$(function() {
			$('#userManagementTbl').DataTable(
					{
						'paging' : true,
						'lengthChange' : true,
						'searching' : true,
						'ordering' : false,
						'info' : true,
						'autoWidth' : false
					})
			$('#userRoleTbl').DataTable({
				'paging' : true,
				'lengthChange' : true,
				'searching' : true,
				'ordering' : false,
				'info' : true,
				'autoWidth' : false
			})
		})
	</script>


	<link rel="stylesheet"
		href="${pageContext.request.contextPath}/css/chosen.min.css">
	<script src="${pageContext.request.contextPath}/js/chosen.jquery.js"></script>

	<script>
jQuery(document).ready(function(){
	jQuery(".chosen").data("placeholder","Select Frameworks...").chosen();
});
</script>



	<script type="text/javascript">

	function prepareUserObjForApprove(userID,flagImmediateDelete){
		document.getElementById("approveUserIDHidden").value = userID;
		document.getElementById("flagImmediateDelete").value=flagImmediateDelete;
		
// 		alert(document.getElementById("approveUserIDHidden").value );
		document.getElementById("submit").click() ;
	}
	
	function prepareUserObjForReject(userID,actionDescription){
		document.getElementById("approveUserIDHidden").value = userID;
		document.getElementById("actionDescription").value=actionDescription;
		
// 		alert(document.getElementById("approveUserIDHidden").value );
		document.getElementById("submit").click() ;
	}
	
	function prepareUserRoleForApprove(userID,RoleID){
		document.getElementById("approveUserIDHid").value = userID;
		document.getElementById("approveRoleIDHid").value = RoleID;
// 		alert(document.getElementById("approveUserRoleIDHidden").value );
		document.getElementById("submit_").click() ;
	}
	
	function prepareUserObjForEdit(UserID,UserName,Locked,Disabled,CreatedDate,CreatedByUserID) {
		debugger;
		document.getElementById("userIDHidden").value = UserID;
		document.getElementById("userName").value = UserName;	
// 		document.getElementById("branch").value = Branch;
// 		document.getElementById("mail").value = mail;

		
		if(Locked == 1)
			document.getElementById("locked").checked = true;
		else
			document.getElementById("locked").checked = false;
		if(Disabled == 1)
			document.getElementById("disabled").checked = true;
		else
			document.getElementById("disabled").checked = false;
	}

	function prepareUserObjForDelete(UserID) {
		debugger;
		if(confirm("You are Going to delete This User!!"))
		{
			document.getElementById("hid_del_userID").value = UserID;
			document.getElementById("deleteFlag").value = 1;
			document.getElementById("submit").click() ;
			return;
		}
		else
		{
			document.getElementById("deleteFlag").value = 0;
			 return false;
		}
	}



	function prepareUserRuleObjForEdit(userID,ruleID) {
		debugger;
		
		document.getElementById("userList").value = userID;
		document.getElementById("ur_userIDHid").value = userID;
		
		var selectedUserRole = document.getElementById('userList').value;
	    var str_array = selectedUserRole.split(',');
	    for (var i = 0; i < str_array.length; i++) {
	        str_array[i] = str_array[i].replace(/^\s*/, "").replace(/\s*$/, "");
	    }
	    $("#userList").val(str_array).trigger("chosen:updated");
	    
	    
 		document.getElementById("ruleList").value = ruleID;
//  		$("#ruleList").val(ruleID).trigger("change");
		document.getElementById("ur_ruleIDHid").value = ruleID;
		document.getElementById("userRuleEditFlag").value = 1;

		
		
		
		var selectedUserRole = document.getElementById('ruleList').value;
	    var str_array = selectedUserRole.split(',');
	    for (var i = 0; i < str_array.length; i++) {
	        str_array[i] = str_array[i].replace(/^\s*/, "").replace(/\s*$/, "");
	    }
	    $("#ruleList").val(str_array).trigger("chosen:updated");
		
		
		
// 		document.location.reload(true);
// 		document.getElementById("userList").focus();
	}
	
	
	
	function prepareUserRuleObjForDelete(userID,ruleID) {
		debugger;
		if(confirm("You are going to delete this Role!!"))
		{
			document.getElementById("ur_userIDHid").value = userID;
			document.getElementById("ur_ruleIDHid").value = ruleID;
			document.getElementById("userRuleDeleteFlag").value = 1;
			document.getElementById("submit_").click();
			return true;
		}
		else
		{
			document.getElementById("userRuleDeleteFlag").value = 0;
			 return false;
		}
	}
</script>

</body>

<%
	} else {
%>
<center>
	<h2>
		<label style="color: red; font-weight: bold;">You don't have
			Permission to access this Page!</label>
	</h2>
</center>
<%
	}
%>
</html>
