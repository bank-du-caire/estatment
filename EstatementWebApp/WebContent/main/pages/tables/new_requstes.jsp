<!DOCTYPE html>
<%@page import="com.bdc.estatement.dao.TransactionsDao"%>
<%@page import="com.bdc.estatement.util.Defines"%>
<%@page import="com.bdc.estatement.model.Account"%>
<%@page import="java.util.List"%>
<%@page import="com.bdc.usermanagement.ldab.UserPrivilege"%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>
<html>
<head>

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>New Requests</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet"
	href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="../../bower_components/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="../../bower_components/Ionicons/css/ionicons.min.css">
<!-- DataTables -->
<link rel="stylesheet"
	href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<style>
div.scrollmenu {
	/*   background-color: #333; */
	overflow: auto;
	/*   white-space: nowrap; */
}

div.scrollmenu a {
	display: inline-block;
	/*   color: white; */
	text-align: center;
	padding: 14px;
	text-decoration: none;
}

div.scrollmenu a:hover {
	/*   background-color: #777; */
	
}
</style>

<%
	String ACC_NO = "";
	String Cust_Name = "";
	String Cust_ID = "";

	if (session.getAttribute("USER_ID") == null) {
		response.sendRedirect(request.getContextPath() + "/main/pages/tables/Login.jsp");
		return;
	}
	UserPrivilege userPrivilege = UserPrivilege.findAllUserPrivileges("new_requstes.jsp", request);
	String freqString = "";
	List<Account> accounts = TransactionsDao.getAccountTypeList();//(List<Account>) request.getAttribute("accountsList");
%>

</head>

<%
	if (userPrivilege.getViewFlag() == 1) {
%>

<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<header class="main-header">
			<a href="#" class="logo"> <span class="logo-mini"><b>BDC</b></span>
				<span class="logo-lg"><b>E-Statement</b></span>
			</a>
			<nav class="navbar navbar-static-top">
				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="push-menu"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
			</nav>
		</header>
		
		<!-- 		Left Side Menu -->
		<jsp:include page="LeftSideMenu.jsp"></jsp:include>




		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>New Requests</h1>
			</section>

			<!-- Main content -->
			<section class="content">
				<div class="row">
					<div class="col-xs-12">

						<!-- /.box -->

						<div class="box">
							<div class="box-header">
								<!--               <h3 class="box-title">Data Table With Full Features</h3> -->
							</div>
							<!-- /.box-header shady -->

							<div class="box-body scrollmenu">

								<table id="accountsTbl" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>Customer Code</th>
											<th>Account Number</th>
											<th>Customer Name</th>
											<th>Statement frequency</th>
											<th>Branch Name</th>
											<th>SMS</th>
											<th>Estatement</th>
											<%
												if (userPrivilege.getInsertFlag() == 1) {
											%>
											<th>Edit</th>
											<%
												}
											%>
										</tr>
									</thead>
									<tbody>

										<%
											for (Account account : accounts) {
										%>
										<tr>
											<td><%=account.getCustomerCode()%></td>
											<td><%=account.getAccountNumber()%></td>
											<td>
												<%
													if (account.getCustomerName() != null) {
												%><%=account.getCustomerName()%> <%
 	}
 %>
											</td>
											<%
										switch (account.getFrequancyStmt()) {
														case Defines.Daily :
															freqString = "Daily";
															break;
														case Defines.Weekly :
															freqString = "Weekly";
															break;
														// 													case Defines.FortNightly :
														// 														freqString = "Fort Nightly";
														// 														break;
														case Defines.Monthly :
															freqString = "Monthly";
															break;
														// 													case Defines.Bi_Monthly :
														// 														freqString = "Bi_Monthly";
														// 														break;
														case Defines.Quarterly :
															freqString = "Quarterly";
															break;
														case Defines.Half_Yearly :
															freqString = "Semi-Annually";
															break;
														case Defines.Yearly :
															freqString = "Annually";
															break;
														default :
															freqString = "None";
															break;
													}
									%>
									<td><%=freqString%></td>
											<td><%=account.getBranchName()%></td>
											<td>
												<%
													if (account.getSmsFlag() == 1) {
												%> Y<%
													} else {
												%> N<%
													}
												%>
											</td>
											<td>
												<%
													if (account.getEstatementFlag() == 1) {
												%> Y<%
													} else {
													}
												%>
											</td>
											<%
												if (userPrivilege.getInsertFlag() == 1) {
											%>
											<td><a
												href='${pageContext.request.contextPath}/EditServlet?accountNo=<%=account.getAccountNumber()%>
					+&freq=<%=account.getFrequancyStmt()%>+&smsFlag=<%=account.getSmsFlag()%>+&estatementFlag=<%=account.getEstatementFlag()%>+&edit=newRequest'>edit</a></td>
											<%
												}
											%>
										</tr>
										<%
											}
										%>
									</tbody>

								</table>
								<a class="btn btn-info"
									href="${pageContext.request.contextPath}/NewRequests?export=excel">Export
									Excel</a>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2019 <a href="#">BDC</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<aside class="control-sidebar control-sidebar-dark">
			<!-- Create the tabs -->
			<ul class="nav nav-tabs nav-justified control-sidebar-tabs">
				<li><a href="#control-sidebar-home-tab" data-toggle="tab"><i
						class="fa fa-home"></i></a></li>
				<li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i
						class="fa fa-gears"></i></a></li>
			</ul>
		</aside>
	</div>
	<!-- ./wrapper -->

	<!-- jQuery 3 -->
	<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap 3.3.7 -->
	<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- DataTables -->
	<script
		src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
	<script
		src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
	<!-- SlimScroll -->
	<script
		src="../../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<!-- FastClick -->
	<script src="../../bower_components/fastclick/lib/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="../../dist/js/adminlte.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../../dist/js/demo.js"></script>
	<!-- page script -->
	<script>
		$(function() {
			$('#accountsTbl').DataTable({
				'paging' : true,
				'lengthChange' : true,
				'searching' : true,
				'ordering' : false,
				'info' : true,
				'autoWidth' : false
			})
		})
	</script>
</body>

<%
	} else {
%>
<center>
	<h2>
		<label style="color: red; font-weight: bold;">You don't have
			Permission to access this Page!</label>
	</h2>
</center>
<%
	}
%>
</html>
