<!DOCTYPE html>
<%@page import="com.bdc.usermanagement.ldab.UserRoleAudit"%>
<%@page import="com.bdc.usermanagement.ldab.RoleAudit"%>
<%@page import="com.bdc.usermanagement.ldab.ScreenAudit"%>
<%@page import="com.bdc.usermanagement.ldab.UserAudit"%>
<%@page import="com.bdc.usermanagement.ldab.UserManagmentDBTransactions"%>
<%@page import="com.bdc.usermanagement.ldab.User"%>
<%@page import="com.bdc.usermanagement.ldab.Rule"%>
<%@page import="com.bdc.usermanagement.ldab.UserRule"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.bdc.estatement.model.EstatementHistory"%>
<%@page import="com.bdc.estatement.dao.TransactionsDao"%>
<%@page import="com.bdc.estatement.util.Defines"%>
<%@page import="com.bdc.estatement.model.Account"%>
<%@page import="java.util.List"%>
<%@page import="com.bdc.usermanagement.ldab.UserPrivilege"%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>
<html>
<head>

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>User Audits</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet"
	href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="../../bower_components/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="../../bower_components/Ionicons/css/ionicons.min.css">
<!-- DataTables -->
<link rel="stylesheet"
	href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<style>
div.scrollmenu {
	/*   background-color: #333; */
	overflow: auto;
	/*   white-space: nowrap; */
}

div.scrollmenu a {
	display: inline-block;
	/*   color: white; */
	text-align: center;
	padding: 14px;
	text-decoration: none;
}

div.scrollmenu a:hover {
	/*   background-color: #777; */
	
}
</style>

<%
	if (session.getAttribute("USER_ID") == null) {
		response.sendRedirect(request.getContextPath() + "/main/pages/tables/Login.jsp");
		return;
	}
	List<UserAudit> allUserAudits = new ArrayList<UserAudit>();
	List<ScreenAudit> allScreenAudits = new ArrayList<ScreenAudit>();
	List<RoleAudit> allRoleAudit = new ArrayList<RoleAudit>();
	List<UserRoleAudit> allUserRoleAudits = new ArrayList<UserRoleAudit>();

	UserManagmentDBTransactions dbTransactions = new UserManagmentDBTransactions();

	allUserAudits = dbTransactions.findAllUserAutids();
	allScreenAudits = dbTransactions.findAllScreenAudits();
	allRoleAudit = dbTransactions.findAllroleAudits();
	allUserRoleAudits = dbTransactions.findAllUserRoleAudits();

	String freqString = "";
%>

</head>



<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<header class="main-header">
			<a href="#" class="logo"> <span class="logo-mini"><b>BDC</b></span>
				<span class="logo-lg"><b>E-Statement</b></span>
			</a>
			<nav class="navbar navbar-static-top">
				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="push-menu"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
			</nav>
		</header>


		<!-- 		Left Side Menu -->
		<jsp:include page="LeftSideMenu.jsp"></jsp:include>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>Audits</h1>
			</section>



			<!-- Main content -->
			<section class="content">


				<!-- User Audit Table -->
				<div class="row">
					<div class="col-xs-12">
						<!-- /.box -->
						<div class="box">
							<h2 style="color: orange !important;">User Audits</h2>
							<div class="box-header">
								<!--               <h3 class="box-title">Data Table With Full Features</h3> -->
							</div>
							<!-- /.box-header shady -->

							<div class="box-body scrollmenu">
								<table id="UserTbl" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>User Name</th>
											<th>Flag Locked</th>
											<th>Flag Disable</th>
											<th>Action</th>
											<th>Created Date</th>
											<th>Created By User Name</th>
										</tr>
									</thead>
									<tbody>

										<%
											for (UserAudit userAudit : allUserAudits) {
										%>
										<tr>
											<td><%=userAudit.getUserName()%></td>
											<td>
												<%
													if (userAudit.getFlagLocked() == 1) {
												%> YES <%
													} else {
												%> NO <%
													}
												%>
											</td>
											<td>
												<%
													if (userAudit.getFlagDisable() == 1) {
												%> YES <%
													} else {
												%> NO <%
													}
												%>
											</td>
											<td><%=userAudit.getAction()%></td>
											<td><%=userAudit.getCreatedDate()%></td>
											<td><%=userAudit.getCreatedByUserName()%></td>
										</tr>
										<%
											}
										%>
									</tbody>

								</table>
								<a class="btn btn-info"
									href="${pageContext.request.contextPath}/UserAuditServlet?exportUserAudit=excel">Export
									Excel</a>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>
				<!-- end of user audits -->


				<!-- start of User Role Audits -->
				<div class="row">
					<div class="col-xs-12">

						<!-- /.box -->

						<div class="box">
							<h2 style="color: orange !important;">User Role Audits</h2>
							<div class="box-header">
								<!--               <h3 class="box-title">Data Table With Full Features</h3> -->
							</div>
							<!-- /.box-header shady -->

							<div class="box-body scrollmenu">
								<table id="UserRoleTbl" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>Role Name</th>
											<th>User Name</th>
											<th>Created Date</th>
											<th>Created By User Name</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>

										<%
											for (UserRoleAudit userRoleAudit : allUserRoleAudits) {
										%>
										<tr>
											<td><%=userRoleAudit.getRoleName()%></td>
											<td><%=userRoleAudit.getUserName()%></td>
											<td><%=userRoleAudit.getCreationDate()%></td>
											<td><%=userRoleAudit.getCreatedByUserName()%></td>
											<td><%=userRoleAudit.getAction()%></td>
										</tr>
										<%
											}
										%>
									</tbody>

								</table>
								<a class="btn btn-info"
									href="${pageContext.request.contextPath}/UserAuditServlet?exportUserRoleAudit=exportUserRoleAudit">Export
									Excel</a>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
				<!-- end of User Role Audits -->



			</section>
			<!-- /.content -->
		</div>




		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2019 <a href="#">BDC</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<aside class="control-sidebar control-sidebar-dark">
			<!-- Create the tabs -->
			<ul class="nav nav-tabs nav-justified control-sidebar-tabs">
				<li><a href="#control-sidebar-home-tab" data-toggle="tab"><i
						class="fa fa-home"></i></a></li>
				<li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i
						class="fa fa-gears"></i></a></li>
			</ul>
		</aside>
	</div>
	<!-- ./wrapper -->

	<!-- jQuery 3 -->
	<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap 3.3.7 -->
	<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- DataTables -->
	<script
		src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
	<script
		src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
	<!-- SlimScroll -->
	<script
		src="../../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<!-- FastClick -->
	<script src="../../bower_components/fastclick/lib/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="../../dist/js/adminlte.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../../dist/js/demo.js"></script>
	<!-- page script -->
	<script>
		$(function() {
			$('#UserTbl').DataTable({
				'paging' : true,
				'lengthChange' : true,
				'searching' : true,
				'ordering' : false,
				'info' : true,
				'autoWidth' : false
			})
			$('#screenTbl').DataTable({
				'paging' : true,
				'lengthChange' : true,
				'searching' : true,
				'ordering' : false,
				'info' : true,
				'autoWidth' : false
			})
			$('#RoleTbl').DataTable({
				'paging' : true,
				'lengthChange' : true,
				'searching' : true,
				'ordering' : false,
				'info' : true,
				'autoWidth' : false
			})
			$('#UserRoleTbl').DataTable({
				'paging' : true,
				'lengthChange' : true,
				'searching' : true,
				'ordering' : false,
				'info' : true,
				'autoWidth' : false
			})
		})
		//UserRoleTbl
	</script>
</body>
</html>
