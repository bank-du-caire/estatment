<!DOCTYPE html>
<%@page import="java.time.ZoneId"%>
<%@page import="java.util.Date"%>
<%@page import="java.time.LocalDate"%>
<%@page import="com.bdc.estatement.dao.SMSDao"%>
<%@page import="com.bdc.estatement.model.SMSHistory"%>
<%@page import="com.bdc.usermanagement.ldab.UserManagmentDBTransactions"%>
<%@page import="com.bdc.usermanagement.ldab.User"%>
<%@page import="com.bdc.usermanagement.ldab.Rule"%>
<%@page import="com.bdc.usermanagement.ldab.UserRule"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.bdc.estatement.model.EstatementHistory"%>
<%@page import="com.bdc.estatement.dao.TransactionsDao"%>
<%@page import="com.bdc.estatement.util.Defines"%>
<%@page import="com.bdc.estatement.model.Account"%>
<%@page import="java.util.List"%>
<%@page import="com.bdc.usermanagement.ldab.UserPrivilege"%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>
<html>
<head>

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Edit Account</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet"
	href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="../../bower_components/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="../../bower_components/Ionicons/css/ionicons.min.css">
<!-- DataTables -->
<link rel="stylesheet"
	href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<style>
div.scrollmenu {
	/*   background-color: #333; */
	overflow: auto;
	/*   white-space: nowrap; */
}

div.scrollmenu a {
	display: inline-block;
	/*   color: white; */
	text-align: center;
	padding: 14px;
	text-decoration: none;
}

div.scrollmenu a:hover {
	/*   background-color: #777; */
	
}
</style>

<%
	Account ac = null;

	if (session.getAttribute("account") != null)
		ac = (Account) session.getAttribute("account");
	UserPrivilege userPrivilege = UserPrivilege.findAllUserPrivileges("view_accounts.jsp", request);
	if (userPrivilege.getViewFlag() != 1) {
		response.sendRedirect("view_accounts.jsp");
		return;
	}
	String nextMonth = "", nextQuarter = "", nextSemi = "";
%>

</head>


<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<header class="main-header">
			<a href="#" class="logo"> <span class="logo-mini"><b>BDC</b></span>
				<span class="logo-lg"><b>E-Statement</b></span>
			</a>
			<nav class="navbar navbar-static-top">
				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="push-menu"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
			</nav>
		</header>


		<!-- 		Left Side Menu -->
		<jsp:include page="LeftSideMenu.jsp"></jsp:include>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header"></section>



			<!-- Main content -->
			<section class="content">
				<!-- 			 box -->
				<div class="row">
					<div class="col-xs-12">

						<!-- /.box -->

						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Edit Account</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body ">
								<!--put my code in here  -->
								<form action='${pageContext.request.contextPath}/EditServlet2'
									method='post' onsubmit="return ValidateForm();"
									novalidate="novalidate">
									<div class="container">
										<h2>Update Account</h2>
										<div class="row">
											<div class="col-xs-6">
												<table>
													<tr>
														<td width="23%">Account Number:</td>
														<td width="72%"><div class='col-sm-6'>
																<div class="form-group">
																	<input class="form-control" type='text'
																		style="width: 350px;"
																		value='<%=ac.getAccountNumber()%>' disabled />
																</div>
															</div></td>
													</tr>
													<tr>
														<td width="23%">Customer Code:</td>
														<td width="72%"><div class='col-sm-6'>
																<div class="form-group">
																	<input class="form-control" type='text'
																		name='customerCode' style="width: 350px;"
																		value='<%=ac.getCustomerCode()%>' disabled />
																</div>
															</div></td>
													</tr>
													<tr>
														<td width="23%">Customer Name:</td>
														<td width="72%"><div class='col-sm-4'>
																<div class="form-group">
																	<input class="form-control" type='text'
																		name='customerName' style="width: 350px;"
																		value='<%if (ac.getCustomerName() != null) {%><%=ac.getCustomerName()%><%}%>'
																		disabled />
																</div>
															</div></td>
													</tr>
													<tr>
														<td width="23%">Branch Name:</td>
														<td width="72%"><div class='col-sm-4'>
																<div class="form-group">
																	<input class="form-control" type='text'
																		name='branchName' style="width: 350px;"
																		value='<%if (ac.getBranchName() != null) {%><%=ac.getBranchName()%><%}%>'
																		disabled />
																</div>
															</div></td>
													</tr>
												</table>
											</div>

											<div class="row">
												<div class="col-xs-6">
													<table>
														<tr>
															<%
																String currentFreqString;
																switch (ac.getFrequancyStmt()) {
																	case Defines.Daily :
																		currentFreqString = "Daily";
																		break;
																	case Defines.Weekly :
																		currentFreqString = "Weekly";
																		break;
																	// 									case Defines.FortNightly :
																	// 										currentFreqString = "Fort Nightly";
																	// 										break;
																	case Defines.Monthly :
																		currentFreqString = "Monthly";
																		break;
																	// 									case Defines.Bi_Monthly :
																	// 										currentFreqString = "Bi_Monthly";
																	// 										break;
																	case Defines.Quarterly :
																		currentFreqString = "Quarterly";
																		break;
																	case Defines.Half_Yearly :
																		currentFreqString = "Semi-Annually";
																		break;
																	case Defines.Yearly :
																		currentFreqString = "Annually";
																		break;
																	default :
																		currentFreqString = "None";
																		break;
																}
															%>
															<td width="23%">Statement Frequency:</td>
															<td width="72%">
																<div class='col-sm-4' style="width: 350px;">
																	<div class="form-group">
																		<select class="form-control" name='freq' id='freq'
																			onchange="populateNextSend()">

																			<%
																				if (ac.getFrequancyStmt() > 0) {
																			%>
																			<option value=""><%=currentFreqString%></option>
																			<%
																				}
																			%>
																			<%
																				if (ac.getFrequancyStmt() == Defines.None || ac == null) {
																			%>
																			<option value="<%=Defines.None%>" selected="selected">None</option>
																			<%
																				} else {
																			%>
																			<option value="<%=Defines.None%>">None</option>
																			<%
																				}
																				if (ac.getFrequancyStmt() == Defines.Daily) {
																			%>
																			<%
																				} else {
																			%>
																			<option value="<%=Defines.Daily%>">Daily</option>
																			<%
																				}
																				if (ac.getFrequancyStmt() == Defines.Weekly) {
																			%>
																			<%
																				} else {
																			%>
																			<option value="<%=Defines.Weekly%>">Weekly</option>
																			<%
																				}
																				// 												if (ac.getFrequancyStmt() == Defines.FortNightly) {
																			%>
																			<%
																				// 												} else {
																			%>
																			<%-- 											<option value="<%=Defines.FortNightly%>">FortNightly</option> --%>
																			<%
																				// 												}
																				if (ac.getFrequancyStmt() == Defines.Monthly) {
																			%>
																			<%
																				} else {
																			%>
																			<option value="<%=Defines.Monthly%>">Monthly</option>
																			<%
																				}
																				// 												if (ac.getFrequancyStmt() == Defines.Bi_Monthly) {
																			%>
																			<%
																				// 												} else {
																			%>
																			<%-- 											<option value="<%=Defines.Bi_Monthly%>">Bi_Monthly</option> --%>
																			<%
																				// 												}
																				if (ac.getFrequancyStmt() == Defines.Quarterly) {
																			%>
																			<%
																				} else {
																			%>
																			<option value="<%=Defines.Quarterly%>">Quarterly</option>
																			<%
																				}
																				if (ac.getFrequancyStmt() == Defines.Half_Yearly) {
																			%>
																			<%
																				} else {
																			%>
																			<option value="<%=Defines.Half_Yearly%>">Semi-Annually</option>
																			<%
																				}
																				if (ac.getFrequancyStmt() == Defines.Yearly) {
																			%>
																			<%
																				} else {
																			%>
																			<option value="<%=Defines.Yearly%>">Annually</option>
																			<%
																				}
																			%>
																		</select>
																	</div>
																</div>
															</td>
														</tr>

														<tr>
															<td width="23%" id="nextDateLabel"><div
																	id="nextDateDiv"
																	style="display: <%=(ac.getFrequancyStmt() != 4 && ac.getFrequancyStmt() != 6 && ac.getFrequancyStmt() != 7
					&& ac.getFrequancyStmt() != 8) ? "block" : "none"%>">Next
																	Date:</div></td>
															<td width="72%" id="nextDateValue"><div
																	class="container" id="nextDateTR"
																	style="display: <%=(ac.getFrequancyStmt() != 4 && ac.getFrequancyStmt() != 6 && ac.getFrequancyStmt() != 7
					&& ac.getFrequancyStmt() != 8) ? "block" : "none"%>">
																	<div class="row">
																		<div class='col-sm-4'>
																			<div class="form-group">
																				<div class='input-group date' id='datetimepicker1'>
																					<%
																						System.out.println("session.getAttribute[estatementFlag]: " + session.getAttribute("estatementFlag"));
																						System.out.println("session.getAttribute[smsFlag]: " + session.getAttribute("smsFlag"));
																						System.out.println(session.getAttribute("estatementFlag") != null
																								&& session.getAttribute("estatementFlag").toString().contains("1"));
																						// 																						if (ac.getEstatementFlag() == 1) {
																						if ((session.getAttribute("estatementFlag") != null
																								&& session.getAttribute("estatementFlag").toString().contains("1"))
																								|| (ac.getEstatementFlag() == 1)) {
																					%>
																					<input type='text' class="form-control"
																						name='nextDate' id='datepicker'
																						value='<%if (ac.getNextDate() != null) {%><%=ac.getNextDate()%><%}%>'
																						required="required" /> <span
																						class="input-group-addon"> <span
																						class="glyphicon glyphicon-calendar"></span> <%
 	} else {
 %> <input type='text' class="form-control" name='nextDate'
																						id='datepicker' style="width: 355px;"
																						value='<%if (ac.getNextDate() != null) {%><%=ac.getNextDate()%><%}%>'
																						disabled /> <span class="input-group-addon"
																						style="display: none;"> <span
																							class="glyphicon glyphicon-calendar"
																							style="display: none;"></span> <%
 	}
 %>
																				</div>

																			</div>
																		</div>
																	</div>
																</div></td>
														</tr>
														<%
															// 															if (ac.getEstatementFlag() == 1) {
														%>

														<tr>
															<%
																System.out.println("ac.getFrequancyStmt() :" + ac.getFrequancyStmt());
															%>
															<td width="23%" id="nextMonthLabel"><div
																	id="nextMonthDiv"
																	style="display: <%=(ac.getFrequancyStmt() == 4) ? "block" : "none"%>;">Next
																	Month:</div></td>
															<td width="72%" id="nextMonthValue">
																<div class='col-sm-4' id="nextMonthTR"
																	style="width: 350px; display: <%=(ac.getFrequancyStmt() == 4) ? "block" : "none"%>;">
																	<div class="form-group">
																		<%
																			Date date = new Date();
																			LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
																			int month = localDate.getMonthValue();
																		%>
																		<select class="form-control" type="text"
																			name='nextMonth' id='nextMonth'
																			value='<%=nextMonth%>'>
																			<%
																				if (month == 12 || month <= 1) {
																			%>
																			<option value="1">January</option>
																			<%
																				}
																				if (month == 12 || month <= 2) {
																			%>
																			<option value="2">February</option>
																			<%
																				}
																				if (month == 12 || month <= 3) {
																			%>
																			<option value="3">March</option>
																			<%
																				}
																				if (month == 12 || month <= 4) {
																			%>
																			<option value="4">April</option>
																			<%
																				}
																				if (month == 12 || month <= 5) {
																			%>
																			<option value="5">May</option>
																			<%
																				}
																				if (month == 12 || month <= 6) {
																			%>
																			<option value="6">June</option>
																			<%
																				}
																				if (month == 12 || month <= 7) {
																			%>
																			<option value="7">July</option>
																			<%
																				}
																				if (month == 12 || month <= 8) {
																			%>
																			<option value="8">August</option>
																			<%
																				}
																				if (month == 12 || month <= 9) {
																			%>
																			<option value="9">September</option>
																			<%
																				}
																				if (month == 12 || month <= 10) {
																			%>
																			<option value="10">October</option>
																			<%
																				}
																				if (month == 12 || month <= 11) {
																			%>
																			<option value="11">November</option>
																			<%
																				}
																				if (month == 12) {
																			%>
																			<option value="12">December</option>
																			<%
																				}
																			%>
																		</select>
																	</div>
																</div>
															</td>
														</tr>
														<%
															// 															}
														%>

														<%
															//if (ac.getEstatementFlag() == 6) {
														%>

														<%
															//if (ac.getFrequancyStmt() == Defines.Quarterly)
															{

																int nMonth = ac.getNextDate() != null ? ac.getNextDate().getMonth() + 1 : 0;
														%>
														<tr>
															<td width="23%" id="nextQuarterLabel"><div
																	id="nextQuarterDiv"
																	style="display: <%=ac.getFrequancyStmt() == 6 ? "block" : "none"%>;">Next
																	Quarter:</div></td>
															<td width="72%" id="nextQuarterValue">
																<div class='col-sm-4' id="nextQuarterTR"
																	style="width: 350px; display: <%=ac.getFrequancyStmt() == 6 ? "block" : "none"%>;">
																	<div class="form-group">
																		<select class="form-control" type="text"
																			name='nextQuarter' id='nextQuarter'
																			value='<%=nextQuarter%>'>
																			<%
																				if (nMonth == 1 || nMonth == 2 || nMonth == 3) {
																			%>
																			<option selected="selected" value="1">First
																				Quarter</option>
																			<%
																				} else {
																			%>
																			<option value="1">First Quarter</option>
																			<%
																				}
																					if (nMonth == 4 || nMonth == 5 || nMonth == 6) {
																			%>
																			<option selected="selected" value="2">Second
																				Quarter</option>
																			<%
																				} else {
																			%>
																			<option value="2">Second Quarter</option>
																			<%
																				}
																					if (nMonth == 7 || nMonth == 8 || nMonth == 9) {
																			%>
																			<option selected="selected" value="3">Third
																				Quarter</option>
																			<%
																				} else {
																			%>
																			<option value="3">Third Quarter</option>
																			<%
																				}
																					if (nMonth == 10 || nMonth == 11 || nMonth == 12) {
																			%>
																			<option selected="selected" value="4">Fourth
																				Quarter</option>
																			<%
																				} else {
																			%>
																			<option value="4">Fourth Quarter</option>
																			<%
																				}
																			%>
																		</select>
																	</div>
																</div>
															</td>
														</tr>
														<%
															}
														%>

														<!-- 														Adding semi Annually here -->

														<%
															// 															if (ac.getFrequancyStmt() == Defines.Half_Yearly){

															int nnMonth = ac.getNextDate() != null ? ac.getNextDate().getMonth() + 1 : 0;
															System.out.println("nnMonth[" + nnMonth + "]");
															System.out.println("getNextDate[" + ac.getNextDate() + "]");
														%>
														<tr>
															<td width="23%" id="nextSemiLabel"><div
																	id="nextSemiDiv"
																	style="display: <%=ac.getFrequancyStmt() == 7 ? "block" : "none"%>;">Next
																	Annual Half:</div></td>
															<td width="72%" id="nextSemiValue">
																<div class='col-sm-4' id="nextSemiTR"
																	style="width: 350px; display: <%=ac.getFrequancyStmt() == 7 ? "block" : "none"%>;">
																	<div class="form-group">
																		<select class="form-control" type="text"
																			name='nextSemi' id='nextSemi' value='<%=nextSemi%>'>
																			<%
																				if (nnMonth == 2 || nnMonth == 3 || nnMonth == 4 || nnMonth == 5 || nnMonth == 6 || nnMonth == 7) {
																			%>
																			<option selected="selected" value="1">First
																				Half</option>
																			<%
																				} else {
																			%>
																			<option value="1">First Half</option>
																			<%
																				}
																				if (nnMonth == 8 || nnMonth == 9 || nnMonth == 10 || nnMonth == 11 || nnMonth == 12 || nnMonth == 1) {
																			%>
																			<option selected="selected" value="2">Second
																				Half</option>
																			<%
																				} else {
																			%>
																			<option value="2">Second Half</option>
																			<%
																				}
																			%>
																		</select>
																	</div>
																</div>
															</td>
														</tr>
														<%
															// 															}
														%>

														<!-- 														Ending semi Annually here -->

														<tr>
															<td width="23%">Mobile:</td>
															<td width="72%"><div class='col-sm-4'
																	style="width: 350px;">
																	<div class="form-group">
																		<%
																			// 		if (ac.getSmsFlag() == 1) {
																			System.out.println("ac.getSmsFlag():" + ac.getSmsFlag());

																			if ((session.getAttribute("smsFlag") != null && session.getAttribute("smsFlag").equals("1"))
																					|| (ac.getSmsFlag() == 1)) {
																		%>
																		<input class="form-control" type='text' name='mobile'
																			id="mobile"
																			value='<%if (ac.getMobile() != null) {%><%=ac.getMobile()%><%}%>'
																			required />
																		<%
																			} else {
																		%>
																		<input class="form-control" type='text' name='mobile'
																			value='<%if (ac.getMobile() != null) {%><%=ac.getMobile()%><%}%>'
																			disabled />
																		<%
																			}
																		%>
																	</div>
																</div></td>
														</tr>

														<tr>
															<td width="23%">Email:</td>
															<td width="72%"><div class='col-sm-4'
																	style="width: 350px;">
																	<div class="form-group">
																		<%
																			// 		if (ac.getEstatementFlag() == 1) {
																			if ((session.getAttribute("estatementFlag") != null
																					&& session.getAttribute("estatementFlag").toString().contains("1"))
																					|| (ac.getEstatementFlag() == 1)) {
																		%>
																		<input class="form-control" type="text" name='email'
																			id='email'
																			value='<%if (ac.getEmail() != null) {%><%=ac.getEmail()%><%}%>'
																			required />
																		<%
																			} else {
																		%>
																		<input class="form-control" type="text" name='email'
																			id='email'
																			value='<%if (ac.getEmail() != null) {%><%=ac.getEmail()%><%}%>'
																			disabled />
																		<%
																			}
																		%>
																	</div>
																	<div>
																		<%
																			if (session.getAttribute("editResult") != null) {
																		%>
																		<p style="color: red"><%=session.getAttribute("editResult")%></p>
																		<%
																			}
																		%>
																		<p id="demo" style="color: red"></p>
																	</div>
																</div></td>
														</tr>
													</table>
												</div>
											</div>
											<center>
												<input class="btn btn-info " type='submit'
													value='Edit &amp; Save ' />
											</center>
										</div>

										<input type='hidden' name='accountNumber'
											value='<%=ac.getAccountNumber()%>' /> <input type='hidden'
											name='custCode' value='<%=ac.getCustomerCode()%>' /> <input
											type='hidden' name='accountFreq'
											value='<%=ac.getFrequancyStmt()%>' /> <input type='hidden'
											name='customerName_' value='<%=ac.getCustomerName()%>' /> <input
											type='hidden' name='branchName_'
											value='<%=ac.getBranchName()%>'> <input type='hidden'
											name='sms' value='<%=ac.getSmsFlag()%>'> <input
											type='hidden' name='estatement'
											value='<%=ac.getEstatementFlag()%>'>
								</form>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>
			</section>
			<!-- /.content -->
		</div>




		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2019 <a href="#">BDC</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<aside class="control-sidebar control-sidebar-dark">
			<!-- Create the tabs -->
			<ul class="nav nav-tabs nav-justified control-sidebar-tabs">
				<li><a href="#control-sidebar-home-tab" data-toggle="tab"><i
						class="fa fa-home"></i></a></li>
				<li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i
						class="fa fa-gears"></i></a></li>
			</ul>
		</aside>
	</div>
	<!-- ./wrapper -->

	<!-- jQuery 3 -->
	<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap 3.3.7 -->
	<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- DataTables -->
	<script
		src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
	<script
		src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
	<!-- SlimScroll -->
	<script
		src="../../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<!-- FastClick -->
	<script src="../../bower_components/fastclick/lib/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="../../dist/js/adminlte.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../../dist/js/demo.js"></script>
	<!-- page script -->

	<link rel="stylesheet"
		href="${pageContext.request.contextPath}/css/bootstrap-datetimepicker.min.css">

	<script src="${pageContext.request.contextPath}/jquery/jquery.min.js"></script>

	<script
		src="${pageContext.request.contextPath}/jquery/bootstrap-datetimepicker.min.js"></script>
	<script type="text/javascript">
		$(function() {
			$('#datetimepicker1').datetimepicker({
				minView : 2,
				pickTime : false,
				format : 'yyyy-mm-dd',

				autoclose : true
			}).val();
		});
	</script>




	<script>
		function populateNextSend() {// nextDateTR nextMonthTR
			debugger;
			var frequency = document.getElementById("freq").value;
			// 			alert(frequency);
			if (frequency == '4') {// 4 means monthly
				document.getElementById("nextMonthTR").style.display = "block";
				document.getElementById("nextMonthDiv").style.display = "block";

				document.getElementById("nextDateTR").style.display = "none";
				document.getElementById("nextDateDiv").style.display = "none";

				document.getElementById("nextQuarterTR").style.display = "none";
				document.getElementById("nextQuarterDiv").style.display = "none";

				document.getElementById("nextSemiTR").style.display = "none";
				document.getElementById("nextSemiDiv").style.display = "none";

			} else if (frequency == '6') { //6 for quartly
				document.getElementById("nextMonthTR").style.display = "none";
				document.getElementById("nextMonthDiv").style.display = "none";

				document.getElementById("nextDateTR").style.display = "none";
				document.getElementById("nextDateDiv").style.display = "none";

				document.getElementById("nextQuarterTR").style.display = "block";
				document.getElementById("nextQuarterDiv").style.display = "block";

				document.getElementById("nextSemiTR").style.display = "none";
				document.getElementById("nextSemiDiv").style.display = "none";

			} else if (frequency == '7') { //7 for semi Annually

				document.getElementById("nextMonthTR").style.display = "none";
				document.getElementById("nextMonthDiv").style.display = "none";

				document.getElementById("nextDateTR").style.display = "none";
				document.getElementById("nextDateDiv").style.display = "none";

				document.getElementById("nextQuarterTR").style.display = "none";
				document.getElementById("nextQuarterDiv").style.display = "none";

				document.getElementById("nextSemiTR").style.display = "block";
				document.getElementById("nextSemiDiv").style.display = "block";

			} else if (frequency == '8') { //8 for Annually
				document.getElementById("nextMonthTR").style.display = "none";
				document.getElementById("nextMonthDiv").style.display = "none";

				document.getElementById("nextDateTR").style.display = "none";
				document.getElementById("nextDateDiv").style.display = "none";

				document.getElementById("nextQuarterTR").style.display = "none";
				document.getElementById("nextQuarterDiv").style.display = "none";

				document.getElementById("nextSemiTR").style.display = "none";
				document.getElementById("nextSemiDiv").style.display = "none";

			} else {
				document.getElementById("nextDateTR").style.display = "block";
				document.getElementById("nextDateDiv").style.display = "block";

				document.getElementById("nextMonthTR").style.display = "none";
				document.getElementById("nextMonthDiv").style.display = "none";

				document.getElementById("nextQuarterTR").style.display = "none";
				document.getElementById("nextQuarterDiv").style.display = "none";

				document.getElementById("nextSemiTR").style.display = "none";
				document.getElementById("nextSemiDiv").style.display = "none";
			}
		}

		function ValidateForm() {
			debugger;
			var today = new Date();
			var todayDate = Date.parse(today.getFullYear() + '-'
					+ (today.getMonth() + 1) + '-' + today.getDate());
			var nextDate = Date
					.parse(document.getElementById("datepicker").value);
			// 			if (document.getElementById("mobile").value == ''
			// 					|| document.getElementById("mobile").value == null
			// 					|| document.getElementById("mobile").value.isEmpty()) {
			// 				alert("Sorry, Mobile Is Required.");
			// 				return false;
			// 			}
			// 			if (document.getElementById("datepicker").value == ''
			// 				|| document.getElementById("datepicker").value == null
			// 				|| document.getElementById("datepicker").value.isEmpty()) {
			// 			alert("Sorry, Next Date Is Required.");
			// 			return false;
			// 		}

			if (document.getElementById("datepicker").value == null) {
				alert("Sorry, Next send date should be as today's date or future date.");
				return false;
			}
			if (nextDate < todayDate) {
				alert("Sorry, Next send date should be as today's date or future date.");
				return false;
			}

			var emailList = $("#email").val().split(';');
			for (i = 0; i < emailList.length; i++) {
				for (i = 0; i < emailList.length; i++) {
					var regex = /^([\w-\.]+@(?!gmail.com)(?!yahoo.com)(?!hotmail.com)(?!yahoo.co.in)(?!aol.com)(?!abc.com)(?!xyz.com)(?!pqr.com)(?!rediffmail.com)(?!live.com)(?!outlook.com)(?!me.com)(?!msn.com)(?!ymail.com)([\w-]+\.)+[\w-]{2,4})?$/;
					// /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
					var result = regex.test(emailList[i].replace(/\s/g, ""));
					if (!result) {
						document.getElementById("demo").innerHTML = "Please make sure to use corporate E-mails [sepertated by ;]";
						return false;
					}
				}
			}
			return true;
		}
	</script>
</body>
</html>
