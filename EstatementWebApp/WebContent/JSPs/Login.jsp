<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>E-Statement Login</title>



<style type="text/css">
@import "bourbon";

body {
	background: #eee !important;
}

.wrapper {
	margin-top: 80px;
	margin-bottom: 80px;
}

.form-signin {
	max-width: 380px;
	padding: 15px 35px 45px;
	margin: 0 auto;
	background-color: #fff;
	border: 1px solid rgba(0, 0, 0, 0.1);
	.
	form-signin-heading
	,
	.checkbox
	{
	margin-bottom
	:
	30px;
}

.checkbox {
	font-weight: normal;
}

.form-control {
	position: relative;
	font-size: 16px;
	height: auto;
	padding: 10px;
	@
	include
	box-sizing(border-box);
	&:
	focus
	{
	z-index
	:
	2;
}

}
input[type="text"] {
	margin-bottom: -1px;
	border-bottom-left-radius: 0;
	border-bottom-right-radius: 0;
}

input[type="password"] {
	margin-bottom: 20px;
	border-top-left-radius: 0;
	border-top-right-radius: 0;
}
}
</style>

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/bootstrap.min.css">

<script src="${pageContext.request.contextPath}/jquery/jquery.min.js"></script>
<script
	src="${pageContext.request.contextPath}/jquery/jquery-3.3.1.slim.min.js"></script>
<script src="${pageContext.request.contextPath}/jquery/bootstrap.min.js"></script>


<%
	if (session.getAttribute("USER_ID") != null) {
		response.sendRedirect("account");
	}
%>

</head>
<body>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<div class="container">
		<div class="wrapper">
			<form class="form-signin" style="width: 450px; max-width: 450px;"
				action="${pageContext.request.contextPath}/Login" method="post">
				<h2 class="form-signin-heading">Login</h2>
				<div>
					<input type="text" class="form-control" name="userName"
						id="userName" placeholder="User Name" required="" autofocus="" />
					<br> <input type="password" class="form-control"
						name="password" id="password" placeholder="Password" required="" />
				</div>
				<br> <input class="btn btn-lg btn-primary btn-block"
					type="submit" value="Login"> <br>
				<%
					System.out.println("the result is "
							+ session.getAttribute("result"));
					if (session.getAttribute("result") != null) {
						String res = session.getAttribute("result").toString();
						session.removeAttribute("result");
				%>
				<center>
					<label style="color: red; font-weight: bold;" id="result"
						name="result"> <%=res%></label>
				</center>
				<%
					}
				%>
			</form>
		</div>
	</div>
</body>
</html>