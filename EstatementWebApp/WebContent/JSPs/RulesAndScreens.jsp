<%@page import="com.bdc.usermanagement.ldab.UserPrivilege"%>
<%@page import="com.bdc.usermanagement.ldab.UserManagmentDBTransactions"%>
<%@page import="com.bdc.usermanagement.ldab.Screen"%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.bdc.usermanagement.ldab.Rule"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Roles And Screens</title>
</head>


<%
	if (session.getAttribute("USER_ID") == null)
		response.sendRedirect(request.getContextPath() + "/JSPs/Login.jsp");
	int sessionSuperUser = session.getAttribute("FLAG_SUPER_USER") != null
			? Integer.valueOf(session.getAttribute("FLAG_SUPER_USER").toString())
			: 0;
	UserPrivilege userPrivilege = UserPrivilege.findAllUserPrivileges("RulesAndScreens.jsp", request);
	if (userPrivilege.getUserCreation() == 1) {
		Rule rule = new Rule();
		List<Rule> allRules = new ArrayList<Rule>();
		UserManagmentDBTransactions dbTransactions = new UserManagmentDBTransactions();
		allRules = dbTransactions.findAllRules();

		Screen screen = new Screen();
		List<Screen> allScreens = new ArrayList<Screen>();
		allScreens = dbTransactions.findAllScreens();

		int userIDHidden = 0, screenIDHid = 0;
%>

<body>
	<jsp:include page="TemplatePage.jsp" />
	<div class="container">
		<form id="rulesFormID"
			action="${pageContext.request.contextPath}/RulesServlet"
			method="post">
			<table>
				<tr>
					<td>Role Name</td>
					<td><input type="text" id="ruleName" name="ruleName"
						value="<%=rule.getRuleName() != null ? rule.getRuleName() : ""%>">
						<input type="hidden" id="ruleIDHidden" name="ruleIDHidden"
						value="<%=userIDHidden%>"></td>
				</tr>
				<tr>
					<td>Role Description</td>
					<td><input type="text" id="ruleDescription"
						name="ruleDescription"
						value="<%=rule.getRuleDescription() != null ? rule.getRuleDescription() : ""%>"></td>
				</tr>
				<tr>
					<td colspan="2">Allow Insert<input type="checkbox"
						id="insertFlag" name="insertFlag"> Allow Update<input
						type="checkbox" id="updateFlag" name="updateFlag"> Allow
						Delete<input type="checkbox" id="deleteFlag" name="deleteFlag">
						Allow View<input type="checkbox" id="viewFlag" name="viewFlag">
						Allow User Creation<input type="checkbox" id="userCreationFlag"
						name="userCreationFlag">
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center"><input type="submit"
						name="submit" id="submit" value="Save"></td>
				</tr>
				<tr>
					<td colspan="2" align="center">
						<%
							if (session.getAttribute("ruleResult") != null) {
									String ruleRes = "";
									ruleRes = session.getAttribute("ruleResult").toString();
									System.out.println(ruleRes);
									session.removeAttribute("ruleResult");
						%> <label id="ruleResult" name="ruleResult"
						style="color: red; font-weight: bold;"><%=ruleRes%></label> <%
 	}
 %>
					</td>
				</tr>
			</table>

			<hr>



			<div class="container">
				<div class="row">
					<div>
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Role Name</th>
									<th>Role Description</th>
									<th>Allow Insert</th>
									<th>Allow Update</th>
									<th>Allow Delete</th>
									<th>Allow View</th>
									<th>Allow User Creation</th>
									<th>Created Date</th>
									<th>Created By User</th>
									<th>Edit</th>
									<!-- 									<th>Delete</th> -->
								</tr>
							</thead>
							<tbody id="myTable">
								<%
									for (int i = 0; i < allRules.size(); i++) {
								%>
								<tr>
									<td><input type="hidden" id="ruleID" name="ruleID"
										value="<%=allRules.get(i).getRuleID()%>"> <%=allRules.get(i).getRuleName() != null ? allRules.get(i).getRuleName() : ""%>
									</td>
									<td><%=allRules.get(i).getRuleDescription() != null ? allRules.get(i).getRuleDescription() : ""%></td>
									<td>
										<%
											if (allRules.get(i).getInsertFlag() == 1) {
										%> Yes <%
											} else {
										%> No <%
											}
										%>
									</td>
									<td>
										<%
											if (allRules.get(i).getUpdateFlag() == 1) {
										%> Yes <%
											} else {
										%> No <%
											}
										%>
									</td>
									<td>
										<%
											if (allRules.get(i).getDeleteFlag() == 1) {
										%> Yes <%
											} else {
										%> No <%
											}
										%>
									</td>

									<td>
										<%
											if (allRules.get(i).getViewFlag() == 1) {
										%> Yes <%
											} else {
										%> No <%
											}
										%>
									</td>

									<td>
										<%
											if (allRules.get(i).getUserCreationFlag() == 1) {
										%> Yes<%
											} else {
										%> NO<%
											}
										%>
									</td>

									<td><%=allRules.get(i).getCreatedDate()%></td>


									<td><%=allRules.get(i).getCreatedByUserName()%></td>


									<td><a id="edit" style="color: blue;" href='#'
										onclick="prepareRuleObjForEdit(<%=allRules.get(i).getRuleID()%>,'<%=allRules.get(i).getRuleName()%>'
					,'<%=allRules.get(i).getRuleDescription()%>',<%=allRules.get(i).getInsertFlag()%>,<%=allRules.get(i).getUpdateFlag()%>,
					<%=allRules.get(i).getDeleteFlag()%>,<%=allRules.get(i).getViewFlag()%>,<%=allRules.get(i).getUserCreationFlag()%>)">edit</a></td>

									<!-- 									<td><input type="hidden" id="deleteFlag" name="deleteFlag" -->
									<!-- 										value="0"> <a -->
									<%-- 										onclick="prepareRuleObjForDelete(<%=allRules.get(i).getRuleID()%>)" --%>
									<!-- 										href='#'>delete</a></td> -->
								</tr>
								<%
									}
								%>
							</tbody>
						</table>
					</div>
					<div class="col-md-12 text-center">
						<ul class="pagination pagination-lg pager" id="myPager"></ul>
					</div>
				</div>
			</div>
		</form>

		<hr>
		<%
			if (session.getAttribute("USER_ID") != null && session.getAttribute("USER_ID").equals(223)) {
		%>
		<form id="screenFrm"
			action="${pageContext.request.contextPath}/ScreenServlet"
			method="post">

			<table>
				<tr>
					<td>Screen Name</td>
					<td><input type="text" id="screenName" name="screenName"
						value="<%=screen.getScreenName() != null ? screen.getScreenName() : ""%>">
						<input type="hidden" id="screenIDHidden" name="screenIDHidden"
						value="<%=screenIDHid%>"></td>
				</tr>
				<tr>
					<td>Screen Description</td>
					<td><input type="text" id="screenDescription"
						name="screenDescription"
						value="<%=screen.getScreenDescription() != null ? screen.getScreenDescription() : ""%>"></td>
				</tr>
				<tr>
					<td>Application Name</td>
					<td><input type="text" id="applicationName"
						name="applicationName"
						value="<%=screen.getApplicationName() != null ? screen.getApplicationName() : ""%>"></td>
				</tr>
				<tr>
					<td>Role</td>
					<td><select id="ruleList" name="ruleList">
							<%
								for (Rule rules : allRules) {
							%>
							<option value="<%=rules.getRuleID()%>"><%=rules.getRuleName() != null ? rules.getRuleName() : ""%></option>
							<%
								}
							%>
					</select></td>
				</tr>
				<tr>
					<td colspan="2" align="center"><input type="submit"
						name="submitScreen" id="submitScreen" value="Save"></td>
				</tr>
				<tr>
					<td colspan="2" align="center">
						<%
							if (session.getAttribute("screenResult") != null) {
										String screenRes = "";
										screenRes = session.getAttribute("screenResult").toString();
										System.out.println(screenRes);
										session.removeAttribute("screenResult");
						%> <label id="screenResult" name="screenResult"
						style="color: red; font-weight: bold;"><%=screenRes%></label> <%
 	}
 %>
					</td>
				</tr>
			</table>

			<hr>

			<div class="container">
				<div class="row">
					<div>
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Screen Name</th>
									<th>Screen Description</th>
									<th>Application Name</th>
									<th>Role ID</th>
									<th>Created By</th>
									<th>Edit</th>
									<th>Delete</th>
								</tr>
							</thead>
							<tbody id="myTable_">
								<%
									for (int i = 0; i < allScreens.size(); i++) {
								%>
								<tr>
									<td><input type="hidden" id="ruleID" name="ruleID"
										value="<%=allScreens.get(i).getScreeID()%>"> <%=allScreens.get(i).getScreenName()%>
									</td>
									<td><%=allScreens.get(i).getScreenDescription()%></td>


									<td><%=allScreens.get(i).getApplicationName()%></td>
									<td><%=allScreens.get(i).getRuleID()%></td>
									<td><%=allScreens.get(i).getCreatedByUserID()%></td>


									<td><a id="edit" style="color: blue;" href='#'
										onclick="prepareScreenObjForEdit(<%=allScreens.get(i).getScreeID()%>,'<%=allScreens.get(i).getScreenName()%>'
					,'<%=allScreens.get(i).getScreenDescription()%>','<%=allScreens.get(i).getApplicationName()%>',
					<%=allScreens.get(i).getRuleID()%>)">edit</a></td>

									<td><input type="hidden" id="deleteFlag" name="deleteFlag"
										value="0"> <a
										onclick="prepareScreenObjForDelete(<%=allScreens.get(i).getScreeID()%>)"
										href='#'>delete</a></td>
								</tr>
								<%
									}
								%>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="col-md-12 text-center">
						<ul class="pagination pagination-lg pager" id="myPager_"></ul>
					</div>
				</div>
			</div>
		</form>
		<%
			}
		%>
	</div>

</body>
<%
	} else {
%>
<br>
<br>
<center>
	<h2>
		<label style="color: red; font-weight: bold;">You don't have
			Permission to access this Page!</label>
	</h2>
</center>
<%
	}
%>

<script type="text/javascript">

	function prepareRuleObjForEdit(ruleID,ruleName,ruleDescription,insertFlag,updateFlag,deleteFlag,viewFlag,userCreationFlag)
	{	
		debugger;
		document.getElementById("ruleIDHidden").value = ruleID;
		document.getElementById("ruleName").value = ruleName;
		document.getElementById("ruleDescription").value = ruleDescription;
		if(insertFlag == 1)
			document.getElementById("insertFlag").checked = true;
		else
			document.getElementById("insertFlag").checked = false;
		if(updateFlag == 1)
			document.getElementById("updateFlag").checked = true;
		else
			document.getElementById("updateFlag").checked = false;
		if(deleteFlag == 1)
			document.getElementById("deleteFlag").checked = true;
		else
			document.getElementById("deleteFlag").checked = false;
		if(viewFlag == 1)
			document.getElementById("viewFlag").checked = true;
		else
			document.getElementById("viewFlag").checked = false;
		if(userCreationFlag == 1)
			document.getElementById("userCreationFlag").checked = true;
		else
			document.getElementById("userCreationFlag").checked = false;
		
	}

	function prepareRuleObjForDelete(ruleID){
		alert(ruleID);
	}

	function prepareScreenObjForEdit(screenID,ScreenName,ScreenDescription,applicationName,ruleID)
	{	
		document.getElementById("screenIDHidden").value = screenID;
// 		alert(document.getElementById("screenIDHidden").value);
		document.getElementById("screenName").value = ScreenName;
		document.getElementById("screenDescription").value = ScreenDescription;
		document.getElementById("applicationName").value = applicationName;
		document.getElementById("ruleList").value = ruleID;
	}

	function prepareScreenObjForDelete(screenID){
		alert(screenID);
		
	}
</script>

</html>