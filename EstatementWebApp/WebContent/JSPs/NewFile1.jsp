<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>My webpage</title>
        <link href="path/to/my-styles.css" rel="stylesheet" />
        <link href="path/to/mmenu.css" rel="stylesheet" />
    </head>
    <body>
        <!-- The page -->
        <div id="my-page">
            <div id="my-header">
                <a href="#my-menu">Open the menu</a>
            </div>
            <div id="my-content">
                <p><strong>This is a demo.</strong><br />
                    <a id="add_li" href="#">Add a new menu item</a><br />
                    <a id="add_ul" href="#">Add a new submenu</a></p>
            </div>
        </div>

        <!-- The menu -->
        <nav id="my-menu">
            <ul id="my-list">
                <li><a href="/">Home</a></li>
                <li id="my-item"><a href="/about/">About us</a></li>
                <li><a href="/contact/">Contact</a></li>
            </ul>
        </nav>

        <script src="path/to/mmenu.js"></script>
        <script>
            Mmenu.configs.offCanvas.page.selector = "#my-page";

            document.addEventListener(
                "DOMContentLoaded", () => {
                    const menu = new Mmenu( "#my-menu" );
                    const api = menu.API;

                    document.querySelector( "#add_li" )
                        .addEventListener(
                            "click", ( evnt ) => {
                                evnt.preventDefault();

                                //    Find the panel and listitem.
                                const panel = document.querySelector( "#my-list" );
                                const listitem = document.querySelector( "#my-item" );

                                //    Create the new listview.
                                const listview = document.createElement( "ul" );
                                listview.innerHTML = `
                                    <li><a href="/about/history">History</a></li>
                                    <li><a href="/about/team">The team</a></li>
                                    <li><a href="/about/address">Our address</a></li>`;

                                //    Add the listview to the listitem.
                                listitem.append( listview );

                                //    Update the listview.
                                api.initPanels( [panel] );
                            }
                        );

                    document.querySelector( "#add_ul" )
                        .addEventListener(
                            "click", ( evnt ) => {
                                evnt.preventDefault();

                                //    Find the panel and listitem.
                                const panel = document.querySelector( "#my-list" );
                                const listitem = document.querySelector( "#my-item" );

                                //    Create the new listview.
                                const listview = document.createElement( "ul" );
                                listview.innerHTML = `
                                    <li><a href="/about/history">History</a></li>
                                    <li><a href="/about/team">The team</a></li>
                                    <li><a href="/about/address">Our address</a></li>`;

                                //    Add the listview to the listitem.
                                listitem.append( listview );

                                //    Update the listview.
                                api.initPanels( [panel] );
                            }
                        );
                }
            );
        </script>
    </body>
</html>