<%@page import="com.bdc.usermanagement.ldab.UserPrivilege"%>
<%@page import="com.bdc.usermanagement.ldab.UserRule"%>
<%@page import="com.bdc.usermanagement.ldab.User"%>
<%@page import="com.bdc.usermanagement.ldab.UserManagmentDBTransactions"%>
<%@page import="com.bdc.usermanagement.ldab.Rule"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java"
	contentType="text/html; text/css; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/css; charset=ISO-8859-1">
<title>User Management</title>
</head>
<%
	if (session.getAttribute("USER_ID") == null) {
		response.sendRedirect(request.getContextPath() + "/JSPs/Login.jsp");
		return;
	}

	UserPrivilege userPrivilege = UserPrivilege.findAllUserPrivileges("UserManagement.jsp", request);

	if (userPrivilege.getUserCreation() == 1) {

		User user = new User();
		// 		String userNameError = "";
		// 		String mailError = "";
		String res = "";
		if (session.getAttribute("result") != null) {
			res = session.getAttribute("result").toString();
			session.setAttribute("result", null);
		}
		List<User> allUser = new ArrayList<User>();
		UserManagmentDBTransactions dbQueries = new UserManagmentDBTransactions();
		String searchUserName = request.getParameter("userNameSearch");
		if (session.getAttribute("allUser") != null)
			allUser = (List<User>) session.getAttribute("allUser");
		else
			allUser = dbQueries.findAllUsers();
		int userID = 0;
		int hid_del_userID = 0, ur_userIDHid = 0, ur_ruleIDHid = 0, userRuleEditFlag = 0,
				userRuleDeleteFlag = 0;

		List<Rule> allRules = new ArrayList<Rule>();
		allRules = dbQueries.findAllRules();

		List<UserRule> allUserRules = new ArrayList<UserRule>();
		if (session.getAttribute("allUserRules") != null)
			allUserRules = (List<UserRule>) session.getAttribute("allUserRules");
		else
			allUserRules = dbQueries.findAllUserRules();
%>

<body>
	<div class="container-fluid">
		<jsp:include page="TemplatePage.jsp" />


		<div class="Container">

			<h2>User Management</h2>
			<hr>
			<form action="${pageContext.request.contextPath}/UserMangement"
				method="post">

				<div class="row">
					<div class="form-group col-xs-5">
						<select id="userNameSearch" name="userNameSearch"
							class="chosen  form-control" style="width: 100%; max-width: 100%;">
							<option value="">Select User</option>
							<%
								for (User users : allUser) {
							%>
							<option value="<%=users.getUserName()%>"><%=users.getUserName() != null ? users.getUserName() : ""%></option>
							<%
								}
							%>
						</select>
					</div>

					<div class="form-group col-xs-5">
						<button type="submit" class="btn btn-info">
							<span class="glyphicon glyphicon-search"></span> Search
						</button>
						<a href="${pageContext.request.contextPath}/UserMangement?reset=1"
							class="btn btn-info"> <span class="glyphicon"></span> Reset
						</a>
					</div>
				</div>
				<hr>

				<table width="50%">
					<tr>
						<td>User Name</td>
						<td><input type="hidden" id="userIDHidden"
							name="userIDHidden" value="<%=userID%>"> <input
							type="text" class="form-control" placeholder="User Name"
							value="<%=user.getUserName()%>" id="userName" name="userName"></td>
						<td></td>
					</tr>
					<tr>
						<td colspan="3"><br> <input type="checkbox" id="locked"
							name="locked"> Locked <input type="checkbox"
							id="disabled" name="disabled"> Disabled</td>
					</tr>

					<tr>
						<td colspan="3" align="center"><br> <input type="submit"
							class="btn btn-info " id="submit" name="subimt" value="Create User"></td>

					</tr>
					<%
						if (!res.isEmpty()) {
					%>
					<tr>

						<td colspan="3" align="center"><label id="result"
							name="result" style="color: red; font-weight: bold;"><%=res%></label>
							<%
								res = "";
							%></td>
					</tr>
					<%
						}
					%>
				</table>


				<hr>



				<hr>
				<input type="hidden" id="hid_del_userID" name="hid_del_userID"
					value="hid_del_userID">


				<div class="container">
					<div class="row">
						<div>
							<table class="table table-hover">
								<thead>
									<tr>
										<th>User Name</th>
										<!-- 										<th>E-Mail</th> -->
										<!-- 										<th>Branch</th> -->
										<th>Locked</th>
										<th>Disabled</th>
										<th>Created Date</th>
										<th>Created By User</th>
										<th>Edit</th>
										<th>Delete</th>
									</tr>
								</thead>
								<tbody id="myTable">
									<%
										for (int i = 0; i < allUser.size(); i++) {
									%>
									<tr>
										<td><input type="hidden" id="UserID" name="UserID"
											value="<%=allUser.get(i).getUserID()%>"> <%=allUser.get(i).getUserName()%>
										</td>
										<%-- 										<td><%=allUser.get(i).getMail()%></td> --%>
										<%-- 										<td><%=allUser.get(i).getBranch()%></td> --%>
										<td>
											<%
												if (allUser.get(i).getFlagLocked() == 1) {
											%> Yes <%
												} else {
											%> No <%
												}
											%>
										</td>
										<td>
											<%
												if (allUser.get(i).getFlagDisabled() == 1) {
											%> Yes <%
												} else {
											%> No <%
												}
											%>
										</td>
										<td><%=allUser.get(i).getCreatedDate()%></td>


										<td><input type="hidden" name="CreatedByUserID" id=""
											value="<%=allUser.get(i).getCreatedByUserID()%>"> <%=allUser.get(i).getCreatedByUserName()%>
										</td>


										<td><a id="edit" style="color: blue;" href='#'
											onclick="prepareUserObjForEdit(<%=allUser.get(i).getUserID()%>,'<%=allUser.get(i).getUserName()%>'
					,<%=allUser.get(i).getFlagLocked()%>,<%=allUser.get(i).getFlagDisabled()%>,
					'<%=allUser.get(i).getCreatedDate()%>',<%=allUser.get(i).getCreatedByUserID()%>)">edit</a></td>

										<td><input type="hidden" id="deleteFlag"
											name="deleteFlag" value="0"> <a
											onclick="prepareUserObjForDelete(<%=allUser.get(i).getUserID()%>)"
											href='#'>delete</a></td>
									</tr>
									<%
										}
									%>
								</tbody>
							</table>
						</div>
						<div class="col-md-12 text-center">
							<ul class="pagination pagination-lg pager" id="myPager"></ul>
						</div>
					</div>
				</div>
			</form>

			<hr>
			<form action="${pageContext.request.contextPath}/UserRulesServlet"
				method="post">
				<input type="hidden" id="userRuleDeleteFlag"
					name="userRuleDeleteFlag" value="<%=userRuleDeleteFlag%>">
				<h2>Assign Roles to User</h2>
				<hr>

				<table width="50%" style="height: 150px;">
					<tr>
						<td>User Name</td>
						<td><input type="hidden" value="<%=ur_userIDHid%>"
							id="ur_userIDHid" name="ur_userIDHid"> <input
							type="hidden" value="<%=userRuleEditFlag%>" id="userRuleEditFlag"
							name="userRuleEditFlag"> <select id="userList"
							name="userList" class="chosen"
							style="width: 100%; max-width: 100%;">
								<%
									for (User users : allUser) {
								%>
								<option value="<%=users.getUserID()%>"><%=users.getUserName() != null ? users.getUserName() : ""%></option>
								<%
									}
								%>
						</select></td>
					</tr>

					<tr>
						<td>Roles</td>
						<td><input type="hidden" value="<%=ur_ruleIDHid%>"
							id="ur_ruleIDHid" name="ur_ruleIDHid"> <select
							multiple="true" id="ruleList" name="ruleList" class="chosen"
							style="width: 100%; max-width: 100%;">
								<%
									for (Rule rules : allRules) {
								%>
								<%
									if (ur_ruleIDHid != 0 && ur_ruleIDHid == rules.getRuleID()) {
								%>
								<option selected value="<%=rules.getRuleID()%>"><%=rules.getRuleName() != null ? rules.getRuleName() : ""%></option>
								<%
									} else {
								%>
								<option value="<%=rules.getRuleID()%>"><%=rules.getRuleName() != null ? rules.getRuleName() : ""%></option>
								<%
									}
										}
								%>
						</select></td>
					</tr>
					<tr>

						<td colspan="2" align="center"><br> <input type="submit"
							class="btn btn-info" id="submit_" name="submit_" value="Save"></td>
					</tr>
					<tr>
						<td colspan="2" align="center">
							<%
								if (session.getAttribute("userRuleResult") != null) {
										String u_res = session.getAttribute("userRuleResult").toString();
										session.removeAttribute("userRuleResult");
							%> <br> <label style="color: red; font-weight: bold;"
							id="userRuleResult" name="userRuleResult"><%=u_res%></label> <%
 	}
 %>
						</td>
					</tr>
				</table>
				<br>






				<div class="container">
					<div class="row">
						<div>
							<table class="table table-hover">
								<thead>
									<tr>
										<th>User Name</th>
										<th>Role Name</th>
										<th>Created Date</th>
										<th>Created By User</th>
										<th>Edit</th>
										<th>Delete</th>
									</tr>
								</thead>
								<tbody id="myTable_">
									<%
										for (int i = 0; i < allUserRules.size(); i++) {
									%>
									<tr>
										<td><input type="hidden" id="UserID_UR" name="UserID_UR"
											value="<%=allUserRules.get(i).getUserID()%>"> <%=allUserRules.get(i).getUserName()%>
										</td>
										<td><input type="hidden" id="RuleID_UR" name="RuleID_UR"
											value="<%=allUserRules.get(i).getRuleID()%>"> <%=allUserRules.get(i).getRuleName()%>
										</td>
										<td><%=allUserRules.get(i).getCreatedDate()%></td>
										<td><%=allUserRules.get(i).getCreatedByUserName()%></td>



										<td><a id="edit" style="color: blue;" href='#'
											onclick="prepareUserRuleObjForEdit(<%=allUserRules.get(i).getUserID()%>,<%=allUserRules.get(i).getRuleID()%>)">edit</a></td>

										<td><a href='#'
											onclick="prepareUserRuleObjForDelete(<%=allUserRules.get(i).getUserID()%>,<%=allUserRules.get(i).getRuleID()%>)">
												delete</a></td>
									</tr>
									<%
										}
									%>
								</tbody>
							</table>
						</div>
						<div class="col-md-12 text-center">
							<ul class="pagination pagination-lg pager" id="myPager_"></ul>
						</div>
					</div>
				</div>

			</form>
		</div>
	</div>

</body>
<%
	} else {
%>
<br>
<br>
<center>
	<h2>
		<label style="color: red; font-weight: bold;">You don't have
			Permission to access this Page!</label>
	</h2>
</center>
<%
	}
%>

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/chosen.min.css">
<script src="${pageContext.request.contextPath}/js/chosen.jquery.js"></script>

<script>
jQuery(document).ready(function(){
	jQuery(".chosen").data("placeholder","Select Frameworks...").chosen();
});
</script>



<script type="text/javascript">

	function prepareUserObjForEdit(UserID,UserName,Locked,Disabled,CreatedDate,CreatedByUserID) {
		debugger;
		document.getElementById("userIDHidden").value = UserID;
		document.getElementById("userName").value = UserName;	
// 		document.getElementById("branch").value = Branch;
// 		document.getElementById("mail").value = mail;

		
		if(Locked == 1)
			document.getElementById("locked").checked = true;
		else
			document.getElementById("locked").checked = false;
		if(Disabled == 1)
			document.getElementById("disabled").checked = true;
		else
			document.getElementById("disabled").checked = false;
	}

	function prepareUserObjForDelete(UserID) {
		debugger;
		if(confirm("You are Going to delete This User!!"))
		{
			document.getElementById("hid_del_userID").value = UserID;
			document.getElementById("deleteFlag").value = 1;
			document.getElementById("submit").click() ;
			return true;
		}
		else
		{
			document.getElementById("deleteFlag").value = 0;
			 return false;
		}
	}



	function prepareUserRuleObjForEdit(userID,ruleID) {
		debugger;
		
		document.getElementById("userList").value = userID;
		document.getElementById("ur_userIDHid").value = userID;
		
		var selectedUserRole = document.getElementById('userList').value;
	    var str_array = selectedUserRole.split(',');
	    for (var i = 0; i < str_array.length; i++) {
	        str_array[i] = str_array[i].replace(/^\s*/, "").replace(/\s*$/, "");
	    }
	    $("#userList").val(str_array).trigger("chosen:updated");
	    
	    
 		document.getElementById("ruleList").value = ruleID;
//  		$("#ruleList").val(ruleID).trigger("change");
		document.getElementById("ur_ruleIDHid").value = ruleID;
		document.getElementById("userRuleEditFlag").value = 1;

		
		
		
		var selectedUserRole = document.getElementById('ruleList').value;
	    var str_array = selectedUserRole.split(',');
	    for (var i = 0; i < str_array.length; i++) {
	        str_array[i] = str_array[i].replace(/^\s*/, "").replace(/\s*$/, "");
	    }
	    $("#ruleList").val(str_array).trigger("chosen:updated");
		
		
		
// 		document.location.reload(true);
// 		document.getElementById("userList").focus();
	}
	
	
	
	function prepareUserRuleObjForDelete(userID,ruleID) {
		debugger;
		if(confirm("You are going to delete this Role!!"))
		{
			document.getElementById("ur_userIDHid").value = userID;
			document.getElementById("ur_ruleIDHid").value = ruleID;
			document.getElementById("userRuleDeleteFlag").value = 1;
			document.getElementById("submit_").click();
			return true;
		}
		else
		{
			document.getElementById("userRuleDeleteFlag").value = 0;
			 return false;
		}
	}
</script>


</html>