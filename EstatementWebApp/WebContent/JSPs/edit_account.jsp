<%@page import="java.util.Calendar"%>
<%@page import="java.time.ZoneId"%>
<%@page import="java.time.LocalDate"%>
<%@page import="java.util.Date"%>
<%@page import="com.bdc.usermanagement.ldab.UserPrivilege"%>
<%@page import="com.bdc.estatement.util.Utils"%>
<%@page import="com.bdc.estatement.model.Account"%>
<%@page import="com.bdc.estatement.util.Defines"%>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<title>Update Account</title>

<%
	Account ac = (Account) request.getAttribute("account");
	UserPrivilege userPrivilege = UserPrivilege.findAllUserPrivileges("view_accounts.jsp", request);
%>
<%
	if (userPrivilege.getViewFlag() != 1) {
		response.sendRedirect("/EstatementWebApp/account");
		return;
	}
	String nextMonth = "";
%>



<jsp:include page="TemplatePage.jsp" />
<!-- <div class="container"> -->


<form action='${pageContext.request.contextPath}/EditServlet2'
	method='post' onsubmit="return ValidateForm();">
	<div class="container">
		<h2>Update Account</h2>
		<div class="row">
			<div class="col-xs-6">
				<table>
					<tr>
						<td width="23%">Account Number:</td>
						<td width="72%"><div class='col-sm-6'>
								<div class="form-group">
									<input class="form-control" type='text' style="width: 350px;"
										value='<%=ac.getAccountNumber()%>' disabled />
								</div>
							</div></td>
					</tr>
					<tr>
						<td width="23%">Customer Code:</td>
						<td width="72%"><div class='col-sm-6'>
								<div class="form-group">
									<input class="form-control" type='text' name='customerCode'
										style="width: 350px;" value='<%=ac.getCustomerCode()%>'
										disabled />
								</div>
							</div></td>
					</tr>
					<tr>
						<td width="23%">Customer Name:</td>
						<td width="72%"><div class='col-sm-4'>
								<div class="form-group">
									<input class="form-control" type='text' name='customerName'
										style="width: 350px;"
										value='<%if (ac.getCustomerName() != null) {%><%=ac.getCustomerName()%><%}%>'
										disabled />
								</div>
							</div></td>
					</tr>
					<tr>
						<td width="23%">Branch Name:</td>
						<td width="72%"><div class='col-sm-4'>
								<div class="form-group">
									<input class="form-control" type='text' name='branchName'
										style="width: 350px;"
										value='<%if (ac.getBranchName() != null) {%><%=ac.getBranchName()%><%}%>'
										disabled />
								</div>
							</div></td>
					</tr>
				</table>
			</div>

			<div class="row">
				<div class="col-xs-6">
					<table>
						<tr>
							<%
								String currentFreqString = "";
								switch (ac.getFrequancyStmt()) {
									case Defines.Daily :
										currentFreqString = "Daily";
										break;
									case Defines.Weekly :
										currentFreqString = "Weekly";
										break;
									// 									case Defines.FortNightly :
									// 										currentFreqString = "Fort Nightly";
									// 										break;
									case Defines.Monthly :
										currentFreqString = "Monthly";
										break;
									// 									case Defines.Bi_Monthly :
									// 										currentFreqString = "Bi_Monthly";
									// 										break;
									case Defines.Quarterly :
										currentFreqString = "Quarterly";
										break;
									case Defines.Half_Yearly :
										currentFreqString = "Semi-Annually";
										break;
									case Defines.Yearly :
										currentFreqString = "Annually";
										break;
									default :
										currentFreqString = "None";
										break;
								}
							%>
							<td width="23%">Statement Frequency:</td>
							<td width="72%">
								<div class='col-sm-4' style="width: 350px; ">
									<div class="form-group">
										<select class="form-control" name='freq' id='freq'
											onchange="populateNextSend()">

											<%
												if (ac.getFrequancyStmt() > 0) {
											%>
											<option value=""><%=currentFreqString%></option>
											<%
												}
											%>
											<%
												if (ac.getFrequancyStmt() == Defines.None) {
											%>
											<%
												} else {
											%>
											<option value="<%=Defines.None%>">None</option>
											<%
												}
												if (ac.getFrequancyStmt() == Defines.Daily) {
											%>
											<%
												} else {
											%>
											<option value="<%=Defines.Daily%>">Daily</option>
											<%
												}
												if (ac.getFrequancyStmt() == Defines.Weekly) {
											%>
											<%
												} else {
											%>
											<option value="<%=Defines.Weekly%>">Weekly</option>
											<%
												}
												// 												if (ac.getFrequancyStmt() == Defines.FortNightly) {
											%>
											<%
												// 												} else {
											%>
											<%-- 											<option value="<%=Defines.FortNightly%>">FortNightly</option> --%>
											<%
												// 												}
												if (ac.getFrequancyStmt() == Defines.Monthly) {
											%>
											<%
												} else {
											%>
											<option value="<%=Defines.Monthly%>">Monthly</option>
											<%
												}
												// 												if (ac.getFrequancyStmt() == Defines.Bi_Monthly) {
											%>
											<%
												// 												} else {
											%>
											<%-- 											<option value="<%=Defines.Bi_Monthly%>">Bi_Monthly</option> --%>
											<%
												// 												}
												if (ac.getFrequancyStmt() == Defines.Quarterly) {
											%>
											<%
												} else {
											%>
											<option value="<%=Defines.Quarterly%>">Quarterly</option>
											<%
												}
												if (ac.getFrequancyStmt() == Defines.Half_Yearly) {
											%>
											<%
												} else {
											%>
											<option value="<%=Defines.Half_Yearly%>">Semi-Annually</option>
											<%
												}
												if (ac.getFrequancyStmt() == Defines.Yearly) {
											%>
											<%
												} else {
											%>
											<option value="<%=Defines.Yearly%>">Annually</option>
											<%
												}
											%>
										</select>
									</div>
								</div>
							</td>
						</tr>

						<tr>
							<td width="23%" id="nextDateLabel"><div id="nextDateDiv"
									style="display: <%=ac.getFrequancyStmt() != 4 ? "block" : "none"%>">Next
									Date:</div></td>
							<td width="72%" id="nextDateValue"><div class="container"
									id="nextDateTR"
									style="display: <%=ac.getFrequancyStmt() != 4 ? "block" : "none"%>">
									<div class="row">
										<div class='col-sm-4'>
											<div class="form-group">
												<div class='input-group date' id='datetimepicker1'>
													<%
														if (ac.getEstatementFlag() == 1) {
													%>
													<input type='text' class="form-control" name='nextDate'
														id='datepicker'
														value='<%if (ac.getNextDate() != null) {%><%=ac.getNextDate()%><%}%>'
														required /> <span class="input-group-addon"> <span
														class="glyphicon glyphicon-calendar"></span> <%
 	} else {
 %> <input type='text' class="form-control" name='nextDate'
														id='datepicker' style="width: 355px;"
														value='<%if (ac.getNextDate() != null) {%><%=ac.getNextDate()%><%}%>'
														disabled /> <span class="input-group-addon"
														style="display: none;"> <span
															class="glyphicon glyphicon-calendar"
															style="display: none;"></span> <%
 	}
 %>
												</div>

											</div>
										</div>
									</div>
								</div></td>
						</tr>
						<%
							if (ac.getEstatementFlag() == 1) {
						%>

						<tr>
							<td width="23%" id="nextMonthLabel"><div id="nextMonthDiv"
									style="display: <%=ac.getFrequancyStmt() == 4 ? "block" : "none"%>;">Next
									Month:</div></td>
							<td width="72%" id="nextMonthValue"> 
								<div class='col-sm-4' id="nextMonthTR"
									style="width: 350px; display: <%=ac.getFrequancyStmt() == 4 ? "block" : "none"%>;">
									<div class="form-group">
										<%
											Date date = new Date();
												LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
												int month = localDate.getMonthValue();
										%>
										<select class="form-control" type="text" name='nextMonth'
											id='nextMonth' value='<%=nextMonth%>'>
											<%
												if (month == 12 || month <= 1) {
											%>
											<option value="1">January</option>
											<%
												}
													if (month == 12 || month <= 2) {
											%>
											<option value="2">February</option>
											<%
												}
													if (month == 12 || month <= 3) {
											%>
											<option value="3">March</option>
											<%
												}
													if (month == 12 || month <= 4) {
											%>
											<option value="4">April</option>
											<%
												}
													if (month == 12 || month <= 5) {
											%>
											<option value="5">May</option>
											<%
												}
													if (month == 12 || month <= 6) {
											%>
											<option value="6">June</option>
											<%
												}
													if (month == 12 || month <= 7) {
											%>
											<option value="7">July</option>
											<%
												}
													if (month == 12 || month <= 8) {
											%>
											<option value="8">August</option>
											<%
												}
													if (month == 12 || month <= 9) {
											%>
											<option value="9">September</option>
											<%
												}
													if (month == 12 || month <= 10) {
											%>
											<option value="10">October</option>
											<%
												}
													if (month == 12 || month <= 11) {
											%>
											<option value="11">November</option>
											<%
												}
													if (month == 12) {
											%>
											<option value="12">December</option>
											<%
												}
											%>
										</select>
									</div>
								</div>
							</td>
						</tr>
						<%
							}
						%>
						<tr>
							<td width="23%">Mobile:</td>
							<td width="72%"><div class='col-sm-4' style="width: 350px; ">
									<div class="form-group">
										<%
											if (ac.getSmsFlag() == 1) {
										%>
										<input class="form-control" type='text' name='mobile'
											value='<%if (ac.getMobile() != null) {%><%=ac.getMobile()%><%}%>'
											required />
										<%
											} else {
										%>
										<input class="form-control" type='text' name='mobile'
											value='<%if (ac.getMobile() != null) {%><%=ac.getMobile()%><%}%>'
											disabled />
										<%
											}
										%>
									</div>
								</div></td>
						</tr>

						<tr>
							<td width="23%">Email:</td>
							<td width="72%"><div class='col-sm-4' style="width: 350px; ">
									<div class="form-group">
										<%
											if (ac.getEstatementFlag() == 1) {
										%>
										<input class="form-control" type="text" name='email'
											id='email'
											value='<%if (ac.getEmail() != null) {%><%=ac.getEmail()%><%}%>'
											required />
										<%
											} else {
										%>
										<input class="form-control" type="text" name='email'
											id='email'
											value='<%if (ac.getEmail() != null) {%><%=ac.getEmail()%><%}%>'
											disabled />
										<%
											}
										%>
									</div>
									<div>
										<p id="demo" style="color: red"></p>
									</div>
								</div></td>
						</tr>
					</table>
				</div>
			</div>
			<center>
				<input class="btn btn-info " type='submit' value='Edit &amp; Save ' />
			</center>
		</div>
		<input type='hidden' name='accountNumber'
			value='<%=ac.getAccountNumber()%>' /> 
			<input type='hidden' name='custCode' value='<%=ac.getCustomerCode() %>' />
			<input	type='hidden' name='accountFreq' value='<%=ac.getFrequancyStmt() %>' />
		    <input type='hidden' name='customerName_'	value='<%=ac.getCustomerName()%>' /> 
			<input type='hidden' name='branchName_' value='<%=ac.getBranchName()%>'>
			<input	type='hidden' name='sms' value='<%=ac.getSmsFlag()%>'> 
			<input	type='hidden' name='estatement' value='<%=ac.getEstatementFlag()%>'>
</form>

<script>
	function populateNextSend() {// nextDateTR nextMonthTR
	// 		debugger;
		var frequency = document.getElementById("freq").value;
		// 		alert(frequency);
		if (frequency == '4') {// 4 means monthly
			document.getElementById("nextMonthTR").style.display = "block";
			document.getElementById("nextMonthDiv").style.display = "block";
			document.getElementById("nextDateTR").style.display = "none";
			document.getElementById("nextDateDiv").style.display = "none";

		} else {
			document.getElementById("nextDateTR").style.display = "block";
			document.getElementById("nextDateDiv").style.display = "block";
			document.getElementById("nextMonthTR").style.display = "none";
			document.getElementById("nextMonthDiv").style.display = "none";
		}
	}

	function ValidateForm() {
		debugger;
		var today = new Date();
		var todayDate = Date.parse(today.getFullYear() + '-'
				+ (today.getMonth() + 1) + '-' + today.getDate());
		var nextDate = Date.parse(document.getElementById("datepicker").value);
		if (nextDate < todayDate) {
			alert("Sorry, Next send date should be as today's date or future date.");
			return false;
		}

		var emailList = $("#email").val().split(';');
		for (i = 0; i < emailList.length; i++) {
			for (i = 0; i < emailList.length; i++) {
				var regex = /^([\w-\.]+@(?!gmail.com)(?!yahoo.com)(?!hotmail.com)(?!yahoo.co.in)(?!aol.com)(?!abc.com)(?!xyz.com)(?!pqr.com)(?!rediffmail.com)(?!live.com)(?!outlook.com)(?!me.com)(?!msn.com)(?!ymail.com)([\w-]+\.)+[\w-]{2,4})?$/;
				// /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
				var result = regex.test(emailList[i].replace(/\s/g, ""));
				if (!result) {
					document.getElementById("demo").innerHTML = "Sorry, Enter a valid email [separate by ; ]";
					return false;
				}
			}
		}
		return true;
	}
</script>
