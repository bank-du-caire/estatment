<%@page import="com.bdc.usermanagement.ldab.UserPrivilege"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%
	UserPrivilege privilege = UserPrivilege.findAllUserPrivileges("UserManagement.jsp", request);
%>
<style type="text/css">
<%if (privilege.getUserCreation() == 1) {%> 
.left , .right { top:0%;
	float: left;
	transform: translateY(300%);
}

<%} else {%> 
.left , .right { top:0%;
	float: left;
	transform: translateY(200%);
}

<%}%>
.left {
	background: #59b9d1;
	display: inline-block;
	white-space: nowrap;
	width: 33px;
	transition: width .5s;
}

.right {
	background: #fff;
	width: 200px;
	transition: width 1s;
	border-style: solid;
	border-color: #ccc;
	border-width: 1px;
}

.left:hover {
	width: 200px;
}

.item:hover {
	background-color: #222;
}

.left .fas {
	margin: 15px;
	width: 15px;
	color: #fff;
}

i.fas {
	font-size: 17px;
	vertical-align: middle !important;
	height: 90%;
}

.item {
	height: 30px;
	overflow: hidden;
	color: #fff;
}
</style>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/bootstrap.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/bootstrap-datetimepicker.min.css">

<script src="${pageContext.request.contextPath}/jquery/jquery.min.js"></script>

<script
	src="${pageContext.request.contextPath}/jquery/bootstrap-datetimepicker.min.js"></script>

<script type="text/javascript">
	function logout_() {
		debugger;
		document.getElementById("btn").click();
		return true;
	}
</script>




<div class="left " style="margin-top: -300px;">
	<%
		if (privilege.getUserCreation() == 0) {
	%>
	<div class="item">
		<i class="fas fa-fw fa-bars"></i><a
			href="${pageContext.request.contextPath}/account"
			style="color: white;">Account</a>
	</div>
	<div class="item">
		<i class="fas fa-fw fa-bars"></i><a
			href="${pageContext.request.contextPath}/NewRequests"
			style="color: white;">New Requests</a>
	</div>
	<div class="item">
		<i class="fas fa-fw fa-bars"></i><a
			href="${pageContext.request.contextPath}/history"
			style="color: white;">Estatement History</a>
	</div>

	<div class="item">
		<i class="fas fa-fw fa-bars"></i><a
			href="${pageContext.request.contextPath}/JSPs/sms_history.jsp"
			style="color: white;">SMS History</a>
	</div>

	<div class="item active">
		<i class="fas fa-fw fa-map-marked-alt"></i> <a
			href="${pageContext.request.contextPath}/AccountAuditServlet"
			style="color: white;">Account Audits</a>
	</div>
	<%
		} else {
	%>
	<div class="item active">
		<i class="fas fa-fw fa-map-marked-alt"></i> <a
			href="${pageContext.request.contextPath}/JSPs/UserManagement.jsp"
			style="color: white;">User Mangement</a>
	</div>
	<div class="item active">
		<i class="fas fa-fw fa-map-marked-alt"></i> <a
			href="${pageContext.request.contextPath}/UserAuditServlet"
			style="color: white;">User Audit</a>
	</div>
	<div class="item">
		<i class="fas fa-fw fa-columns"></i> <a
			href="${pageContext.request.contextPath}/JSPs/RulesAndScreens.jsp"
			style="color: white;">Roles And Screens</a>
	</div>
	<%
		}
	%>
	<div class="item">
		<i class="fas fa-fw fa-columns"></i> <a
			href="${pageContext.request.contextPath}/Logout"
			style="color: white;">Logout</a>
	</div>


</div>




<script type="text/javascript">
	$(function() {
		$('#datetimepicker1').datetimepicker({
			minView : 2,
			pickTime : false,
			format : 'yyyy-mm-dd',

			autoclose : true
		}).val();
	});
</script>



<script>
	$.fn.pageMe = function(opts) {
		var $this = this, defaults = {
			perPage : 7,
			showPrevNext : false,
			hidePageNumbers : false
		}, settings = $.extend(defaults, opts);

		var listElement = $this;
		var perPage = settings.perPage;
		var children = listElement.children();
		var pager = $('.pager');

		if (typeof settings.childSelector != "undefined") {
			children = listElement.find(settings.childSelector);
		}

		if (typeof settings.pagerSelector != "undefined") {
			pager = $(settings.pagerSelector);
		}

		var numItems = children.size();
		var numPages = Math.ceil(numItems / perPage);

		pager.data("curr", 0);

		if (settings.showPrevNext) {
			$('<li><a href="#" class="prev_link">�</a></li>').appendTo(pager);
		}

		var curr = 0;
		while (numPages > curr && (settings.hidePageNumbers == false)) {
			$('<li><a href="#" class="page_link">' + (curr + 1) + '</a></li>')
					.appendTo(pager);
			curr++;
		}

		if (settings.showPrevNext) {
			$('<li><a href="#" class="next_link">�</a></li>').appendTo(pager);
		}

		pager.find('.page_link:first').addClass('active');
		pager.find('.prev_link').hide();
		if (numPages <= 1) {
			pager.find('.next_link').hide();
		}
		pager.children().eq(1).addClass("active");

		children.hide();
		children.slice(0, perPage).show();

		pager.find('li .page_link').click(function() {
			var clickedPage = $(this).html().valueOf() - 1;
			goTo(clickedPage, perPage);
			return false;
		});
		pager.find('li .prev_link').click(function() {
			previous();
			return false;
		});
		pager.find('li .next_link').click(function() {
			next();
			return false;
		});

		function previous() {
			var goToPage = parseInt(pager.data("curr")) - 1;
			goTo(goToPage);
		}

		function next() {
			goToPage = parseInt(pager.data("curr")) + 1;
			goTo(goToPage);
		}

		function goTo(page) {
			var startAt = page * perPage, endOn = startAt + perPage;

			children.css('display', 'none').slice(startAt, endOn).show();

			if (page >= 1) {
				pager.find('.prev_link').show();
			} else {
				pager.find('.prev_link').hide();
			}

			if (page < (numPages - 1)) {
				pager.find('.next_link').show();
			} else {
				pager.find('.next_link').hide();
			}

			pager.data("curr", page);
			pager.children().removeClass("active");
			pager.children().eq(page + 1).addClass("active");

		}
	};

	$(document).ready(function() {

		$('#myTable').pageMe({
			pagerSelector : '#myPager',
			showPrevNext : true,
			hidePageNumbers : false,
			perPage : 5
		});

	});

	$(document).ready(function() {

		$('#myTable_').pageMe({
			pagerSelector : '#myPager_',
			showPrevNext : true,
			hidePageNumbers : false,
			perPage : 5
		});

	});

	$(document).ready(function() {

		$('#myTable_1').pageMe({
			pagerSelector : '#myPager_1',
			showPrevNext : true,
			hidePageNumbers : false,
			perPage : 5
		});

	});

	$(document).ready(function() {

		$('#myTable_2').pageMe({
			pagerSelector : '#myPager_2',
			showPrevNext : true,
			hidePageNumbers : false,
			perPage : 5
		});

	});

	$(document).ready(function() {

		$('#myTable_15').pageMe({
			pagerSelector : '#myPager_15',
			showPrevNext : true,
			hidePageNumbers : false,
			perPage : 15
		});

	});
</script>

