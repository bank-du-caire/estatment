<%@page import="com.bdc.usermanagement.ldab.User"%>
<%@page import="com.bdc.usermanagement.ldab.UserRoleAudit"%>
<%@page import="com.bdc.usermanagement.ldab.RoleAudit"%>
<%@page import="com.bdc.usermanagement.ldab.ScreenAudit"%>
<%@page import="com.bdc.usermanagement.ldab.UserAudit"%>
<%@page import="com.bdc.usermanagement.ldab.UserManagmentDBTransactions"%>
<%@page import="com.bdc.estatement.util.Defines"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.bdc.usermanagement.ldab.AccountAudit"%>
<%@page import="java.util.List"%>
<%@page import="com.bdc.usermanagement.ldab.UserPrivilege"%>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Users Audits</title>
<%
	if (session.getAttribute("USER_ID") == null) {
		response.sendRedirect(request.getContextPath() + "/JSPs/Login.jsp");
		return;
	}
	// 	UserPrivilege userPrivilege = UserPrivilege.findAllUserPrivileges("UsersAudit.jsp", request);
%>

</head>
<body>

	<%
		List<UserAudit> allUserAudits = new ArrayList<UserAudit>();
		List<ScreenAudit> allScreenAudits = new ArrayList<ScreenAudit>();
		List<RoleAudit> allRoleAudit = new ArrayList<RoleAudit>();
		List<UserRoleAudit> allUserRoleAudits = new ArrayList<UserRoleAudit>();

		UserManagmentDBTransactions dbTransactions = new UserManagmentDBTransactions();
		allUserAudits = (List<UserAudit>) session.getAttribute("allUserAudits");
		allScreenAudits = (List<ScreenAudit>) session.getAttribute("allScreenAudits");
		allRoleAudit = (List<RoleAudit>) session.getAttribute("allRoleAudit");
		allUserRoleAudits = (List<UserRoleAudit>) session.getAttribute("allUserRoleAudits");

		String freqString = "";
		List<String> allUser = new ArrayList<String>();

		for (UserAudit userAudit : allUserAudits) {
			if (userAudit.getUserName() != null && !allUser.contains(userAudit.getUserName().trim().toUpperCase()))
				allUser.add(userAudit.getUserName().trim().toUpperCase());
			if (userAudit.getCreatedByUserName() != null
					&& !allUser.contains(userAudit.getCreatedByUserName().trim().toUpperCase()))
				allUser.add(userAudit.getCreatedByUserName().trim().toUpperCase());
		}
	%>
	<jsp:include page="TemplatePage.jsp" />

	<div class="container ">

		<h2>User Audits</h2>
		<!--Search Form -->
		<form id="seachAccountForm" role="form"
			action="${pageContext.request.contextPath}/UserAuditServlet"
			method="post">
			<input type="hidden" id="searchAction" name="searchAction"
				value="searchByName" />
			<div class="row">
				<div class="form-group col-xs-5">
					<select id="userNameSearch" name="userNameSearch"
						class="chosen form-control" style="width: 100%; max-width: 100%;">
						<%
							for (String users : allUser) {
						%>
						<option value="<%=users%>"><%=users != null ? users : ""%></option>
						<%
							}
						%>
					</select>
				</div>
				<div class="form-group col-xs-5">
					<button type="submit" class="btn btn-info">
						<span class="glyphicon glyphicon-search"></span> Search
					</button>
					<a href="${pageContext.request.contextPath}/UserAuditServlet"
						class="btn btn-info"> <span class="glyphicon"></span> Reset
					</a>
				</div>

			</div>
			<!-- 		</form> -->
			<%
				if (allUserAudits != null && allUserAudits.size() > 0) {
			%>

			<div class="container">
				<h2 style="color: orange !important;">User Audits</h2>
				<!-- 				<hr> -->
				<div class="row">
					<div>

						<table class="table table-hover">
							<thead>
								<tr>
									<th>User Name</th>
									<th>Flag Locked</th>
									<th>Flag Disable</th>
									<th>Action</th>
									<th>Created Date</th>
									<th>Created By User Name</th>
								</tr>
							</thead>
							<tbody id="myTable">
								<%
									for (UserAudit userAudit : allUserAudits) {
								%>
								<tr>
									<td><%=userAudit.getUserName()%></td>
									<td>
										<%
											if (userAudit.getFlagLocked() == 1) {
										%> YES <%
											} else {
										%> NO <%
											}
										%>
									</td>
									<td>
										<%
											if (userAudit.getFlagDisable() == 1) {
										%> YES <%
											} else {
										%> NO <%
											}
										%>
									</td>
									<td><%=userAudit.getAction()%></td>
									<td><%=userAudit.getCreatedDate()%></td>
									<td><%=userAudit.getCreatedByUserName()%></td>
								</tr>
								<%
									}
								%>
							</tbody>
						</table>

						<%
							} else {
						%>
						<br />
						<div class="alert alert-info">No Audits found matching your
							search criteria</div>
						<%
							}
						%>

					</div>
					<a class="btn btn-info"
						href="${pageContext.request.contextPath}/UserAuditServlet?exportUserAudit=excel">Export
						Excel</a>

				</div>

				<div class="col-md-12 text-center">
					<ul class="pagination pagination-lg pager" id="myPager"></ul>
				</div>

				<!-- 				start screen table -->


				<div class="row">
					<h2 style="color: orange !important;">Screen Audits</h2>
					<div>
						<table class="table table-hover">
							<thead>
								<tr>

									<th>Screen Name</th>
									<th>Screen Description</th>
									<th>Application Name</th>
									<th>Role Name</th>
									<th>Created By User Name</th>
									<th>Created Date</th>
									<th>Action</th>
								</tr>
							</thead>
							<%
								if (allScreenAudits != null && allScreenAudits.size() > 0) {
							%>
							<tbody id="myTable_">
								<%
									for (ScreenAudit screenAudit : allScreenAudits) {
								%>
								<tr>
									<td><%=screenAudit.getScreenName()%></td>
									<td><%=screenAudit.getScreenDescription()%></td>
									<td><%=screenAudit.getApplicationName()%></td>
									<td><%=screenAudit.getRoleName()%></td>
									<td><%=screenAudit.getCreatedByUserName()%></td>
									<td><%=screenAudit.getCreatedDate()%></td>
									<td><%=screenAudit.getAction()%></td>
								</tr>
								<%
									}
								%>
							</tbody>
						</table>

						<%
							} else {
						%>
						<br />
						<div class="alert alert-info">No Audits found matching your
							search criteria</div>
						<%
							}
						%>

					</div>
					<a class="btn btn-info"
						href="${pageContext.request.contextPath}/UserAuditServlet?exportScreenAudit=exportScreenAudit">Export
						Excel</a>

				</div>
				<div class="col-md-12 text-center">
					<ul class="pagination pagination-lg pager" id="myPager_"></ul>
				</div>
				<!-- 				end screen table -->


				<!-- 				start role table -->
				<div class="row">
					<h2 style="color: orange !important;">Role Audits</h2>
					<div>
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Role Name</th>
									<th>Role Description</th>
									<th>Insert Flag</th>
									<th>Update Flag</th>
									<th>Delete Flag</th>
									<th>View Flag</th>
									<th>User Creation Flag</th>
									<th>Created By User Name</th>
									<th>Created Date</th>
									<th>Action</th>
								</tr>
							</thead>
							<%
								if (allRoleAudit != null && allRoleAudit.size() > 0) {
							%>
							<tbody id="myTable_1">
								<%
									for (RoleAudit roleAudit : allRoleAudit) {
								%>
								<tr>
									<td><%=roleAudit.getRoleName()%></td>
									<td><%=roleAudit.getRoleDescription()%></td>
									<td><%=roleAudit.getInsertFlag() == 1 ? "YES" : "NO"%></td>
									<td><%=roleAudit.getUpdateFlag() == 1 ? "YES" : "NO"%></td>
									<td><%=roleAudit.getDeleteFlag() == 1 ? "YES" : "NO"%></td>
									<td><%=roleAudit.getViewFlag() == 1 ? "YES" : "NO"%></td>
									<td><%=roleAudit.getUserCreationFlag() == 1 ? "YES" : "NO"%></td>
									<td><%=roleAudit.getUserName()%></td>
									<td><%=roleAudit.getCreatedDate()%></td>
									<td><%=roleAudit.getAction()%></td>
								</tr>
								<%
									}
								%>
							</tbody>
						</table>

						<%
							} else {
						%>
						<br />
						<div class="alert alert-info">No Audits found matching your
							search criteria</div>
						<%
							}
						%>

					</div>
					<a class="btn btn-info"
						href="${pageContext.request.contextPath}/UserAuditServlet?exportRoleAudit=exportRoleAudit">Export
						Excel</a>

				</div>
				<div class="col-md-12 text-center">
					<ul class="pagination pagination-lg pager" id="myPager_1"></ul>
				</div>
				<!-- 				end role table -->


				<!-- 				start user role table -->
				<div class="row">
					<h2 style="color: orange !important;">User Roles Audits</h2>
					<div>
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Role Name</th>
									<th>User Name</th>
									<th>Created Date</th>
									<th>Created By User Name</th>
									<th>Action</th>
								</tr>
							</thead>
							<%
								if (allUserRoleAudits != null && allUserRoleAudits.size() > 0) {
							%>
							<tbody id="myTable_2">
								<%
									for (UserRoleAudit userRoleAudit : allUserRoleAudits) {
								%>
								<tr>
									<td><%=userRoleAudit.getRoleName()%></td>
									<td><%=userRoleAudit.getUserName()%></td>
									<td><%=userRoleAudit.getCreationDate()%></td>
									<td><%=userRoleAudit.getCreatedByUserName()%></td>
									<td><%=userRoleAudit.getAction()%></td>
								</tr>
								<%
									}
								%>
							</tbody>
						</table>

						<%
							} else {
						%>
						<br />
						<div class="alert alert-info">No Audits found matching your
							search criteria</div>
						<%
							}
						%>

					</div>
					<a class="btn btn-info"
						href="${pageContext.request.contextPath}/UserAuditServlet?exportUserRoleAudit=exportUserRoleAudit">Export
						Excel</a>

				</div>
				<div class="col-md-12 text-center">
					<ul class="pagination pagination-lg pager" id="myPager_2"></ul>
				</div>
				<!-- 				end user role table -->

			</div>
	</div>


	</form>

	<link rel="stylesheet"
		href="${pageContext.request.contextPath}/css/chosen.min.css">
	<script src="${pageContext.request.contextPath}/js/chosen.jquery.js"></script>

	<script>
		jQuery(document).ready(
				function() {
					jQuery(".chosen").data("placeholder",
							"Select Frameworks...").chosen();
				});
	</script>
</body>
</html>

