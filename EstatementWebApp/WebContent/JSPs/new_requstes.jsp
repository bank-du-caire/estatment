<%@page import="com.bdc.usermanagement.ldab.UserPrivilege"%>
<%@page import="com.bdc.estatement.model.Account"%>
<%@page import="com.bdc.estatement.util.Defines"%>
<%@page import="java.util.List"%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />


<%
	String ACC_NO = "";
	String Cust_Name = "";
	String Cust_ID = "";

	if (session.getAttribute("USER_ID") == null) {
		response.sendRedirect(request.getContextPath() + "/JSPs/Login.jsp");
		return;
	}
	UserPrivilege userPrivilege = UserPrivilege.findAllUserPrivileges("new_requstes.jsp", request);
	List<Account> accounts = (List<Account>) request.getAttribute("accountsList");

	String dropAccount = "<select name=\"accountNo\" id=\"accountNo\">";
	String dropCustName = "<select name=\"customerName\" id=\"customerName\">";
	String dropCustID = "<select name=\"customerId\" id=\"customerId\">";

	dropAccount = dropAccount + "<option value=''>--Select Account Number--</option>";
	dropCustName = dropCustName + "<option value=''>--Select Customer Name--</option>";
	dropCustID = dropCustID + "<option value=''>--Select Customer ID-</option>";

	if (request.getParameter("accountNo") != null || request.getParameter("customerName") != null
			|| request.getParameter("customerId") != null) {

		ACC_NO = request.getParameter("accountNo");
		Cust_Name = request.getParameter("customerName");
		Cust_ID = request.getParameter("customerId");

	}

	for (int i = 0; i < accounts.size(); i++) {
		if (accounts.get(i).getAccountNumber().equals(ACC_NO)
				|| accounts.get(i).getCustomerName().equals(Cust_Name)
				|| accounts.get(i).getCustomerCode().equals(Cust_ID)) {
			dropAccount = dropAccount + "<option value=" + accounts.get(i).getAccountNumber() + " selected >"
					+ accounts.get(i).getAccountNumber() + "</option> ";
			dropCustName = dropCustName + "<option value=" + accounts.get(i).getCustomerName() + " selected >"
					+ accounts.get(i).getCustomerName() + "</option> ";
			dropCustID = dropCustID + "<option value=" + accounts.get(i).getCustomerCode() + " selected >"
					+ accounts.get(i).getCustomerCode() + "</option> ";
		} else {
			dropAccount = dropAccount + "<option value=" + accounts.get(i).getAccountNumber() + ">"
					+ accounts.get(i).getAccountNumber() + "</option> ";
			dropCustName = dropCustName + "<option value=" + accounts.get(i).getCustomerName() + ">"
					+ accounts.get(i).getCustomerName() + "</option> ";
			dropCustID = dropCustID + "<option value=" + accounts.get(i).getCustomerCode() + ">"
					+ accounts.get(i).getCustomerCode() + "</option> ";
		}

	}
	dropAccount = dropAccount + "</select >";
	dropCustName = dropCustName + "</select >";
	dropCustID = dropCustID + "</select >";
%>
<title>New Requests</title>
</head>

<%
	if (userPrivilege.getViewFlag() == 1) {
%>
<body>
	<jsp:include page="TemplatePage.jsp" />
	<div class="container"></div>

	<!--Search Form -->
	<div class="container">
		<h2>New Requests</h2>
		<form action="${pageContext.request.contextPath}/NewRequests"
			method="get" id="seachAccountForm" role="form" accept-charset="UTF-8">
			<input type="hidden" id="searchAction" name="searchAction"
				value="searchByName" />
			<div class="row">
				<div class="form-group col-xs-5">
					<%=dropCustName%>
					<!-- <input type="text" name="customerName" id="customerName"
					class="form-control" placeholder="Type the Customer Name" /> -->
				</div>
				<div class="form-group col-xs-5">
					<%=dropCustID%>
					<!-- <input type="text" name="customerId" id="customerId"
					class="form-control" placeholder="Type the Customer Id" /> -->
				</div>
				<div class="form-group col-xs-5">
					<%=dropAccount%>
					<!-- <input type="text" name="accountNo" id="accountNo"
					class="form-control" placeholder="Type the Account Number" /> -->

				</div>
				<div class="form-group col-xs-5">
					<button type="submit" class="btn btn-info">
						<span class="glyphicon glyphicon-search"></span> Search
					</button>
					<a href="${pageContext.request.contextPath}/NewRequests"
						class="btn btn-info"> <span class="glyphicon"></span> Reset
					</a>
				</div>
			</div>

			<%
				String freqString = "";
					if (accounts != null && accounts.size() > 0) {
			%>
			<div class="container">
				<div class="row">
					<div>
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Customer Code</th>
									<th>Account Number</th>
									<th>Customer Name</th>
									<th>Statement frequency</th>
									<th>Branch Name</th>
									<th>SMS</th>
									<th>Estatement</th>
									<%
										if (userPrivilege.getInsertFlag() == 1) {
									%>
									<th>Edit</th>
									<%
										}
									%>
								</tr>
							</thead>
							<tbody id="myTable_15">
								<%
									for (Account account : accounts) {
								%>
								<tr>
									<td><%=account.getCustomerCode()%></td>
									<td><%=account.getAccountNumber()%></td>
									<td>
										<%
											if (account.getCustomerName() != null) {
										%><%=account.getCustomerName()%> <%
 	}
 %>
									</td>
									<%
										switch (account.getFrequancyStmt()) {
														case Defines.Daily :
															freqString = "Daily";
															break;
														case Defines.Weekly :
															freqString = "Weekly";
															break;
														// 													case Defines.FortNightly :
														// 														freqString = "Fort Nightly";
														// 														break;
														case Defines.Monthly :
															freqString = "Monthly";
															break;
														// 													case Defines.Bi_Monthly :
														// 														freqString = "Bi_Monthly";
														// 														break;
														case Defines.Quarterly :
															freqString = "Quarterly";
															break;
														case Defines.Half_Yearly :
															freqString = "Semi-Annually";
															break;
														case Defines.Yearly :
															freqString = "Annually";
															break;
														default :
															freqString = "None";
															break;
													}
									%>
									<td><%=freqString%></td>
									<td><%=account.getBranchName()%></td>
									<td>
										<%
											if (account.getSmsFlag() == 1) {
										%> Y<%
											} else {
										%> N<%
											}
										%>
									</td>
									<td>
										<%
											if (account.getEstatementFlag() == 1) {
										%> Y<%
											} else {
										%> N<%
											}
										%>
									</td>
									<%
										if (userPrivilege.getInsertFlag() == 1) {
									%>
									<td><a
										href='EditServlet?accountNo=<%=account.getAccountNumber()%>
					+&freq=<%=account.getFrequancyStmt()%>+&edit=newRequest'>edit</a></td>
									<%
										}
									%>
								</tr>
								<%
									}
								%>
							</tbody>
						</table>
						<a class="btn btn-info"
							href="${pageContext.request.contextPath}/NewRequests?export=excel">Export
							Excel</a>
						<%
							} else {
						%>
						<br />
						<div class="alert alert-info">No New Requests Found</div>
						<%
							}
						%>
					</div>
					<div class="col-md-12 text-center">
						<ul class="pagination pagination-lg pager" id="myPager_15"></ul>
					</div>
				</div>
			</div>



		</form>
	</div>
</body>
<%
	} else {
%>
<center>
	<h2>
		<label style="color: red; font-weight: bold;">You don't have
			Permission to access this Page!</label>
	</h2>
</center>
<%
	}
%>

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/chosen.min.css">
<script src="${pageContext.request.contextPath}/js/chosen.jquery.js"></script>



<script type="text/javascript">
	$(document).ready(function() {
		$('select').chosen({
			search_contains : true,
			width : '400px',
			height : '500px !important'

		});
	});
</script>


</html>

