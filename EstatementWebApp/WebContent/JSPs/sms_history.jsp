<%@page import="com.bdc.estatement.dao.SMSDao"%>
<%@page import="com.bdc.estatement.model.SMSHistory"%>
<%@page import="com.bdc.usermanagement.ldab.UserPrivilege"%>
<%@page import="com.bdc.estatement.model.EstatementHistory"%>
<%@page import="com.bdc.estatement.util.Defines"%>
<%@page import="java.util.List"%>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<html>
<head>
<title>SMS History</title>
</head>
<%
	if (session.getAttribute("USER_ID") == null) {
		response.sendRedirect(request.getContextPath() + "/JSPs/Login.jsp");
		return;
	}
	UserPrivilege userPrivilege = UserPrivilege.findAllUserPrivileges("sms_history.jsp", request);
	String freqString = "";
%>


<%
	if (userPrivilege.getViewFlag() == 1) {

		List<SMSHistory> histories = SMSDao.getSMSHistory();
%>
<jsp:include page="TemplatePage.jsp" />
<div class="container">
	<h2>SMS History</h2>
	<!--Search Form -->
	<form action="${pageContext.request.contextPath}/SMSHistoryServlet"
		method="post" id="seachAccountForm" role="form">
		<input type="hidden" id="searchAction" name="searchAction"
			value="searchByName" />
		<div class="row">
			<div class="form-group col-xs-5">
				<input type="text" name="customerName" id="customerName"
					class="form-control" required="true"
					placeholder="Type the Customer Name" />
			</div>
			<div class="form-group col-xs-5">
				<button type="submit" class="btn btn-info">
					<span class="glyphicon glyphicon-search"></span> Search
				</button>
				<a href="${pageContext.request.contextPath}/account"
					class="btn btn-info"> <span class="glyphicon"></span> Reset
				</a>
			</div>
			<div class="form-group col-xs-5">
				<a href="${pageContext.request.contextPath}/NewRequests"
					class="btn btn-info"> <span class="glyphicon"></span> New
					Requests
				</a>
			</div>
		</div>
		<%
			if (histories != null && histories.size() > 0) {
		%>




		<div class="container">
			<div class="row">
				<div>
					<table class="table table-hover">
						<thead>
							<tr>
								<th>Customer Name</th>
								<th>Account Number</th>
								<th>Message</th>
								<th>Mobile</th>
								<th>Send Date</th>
							</tr>
						</thead>
						<tbody id="myTable">
							<%
								for (SMSHistory history : histories) {
							%>
							<tr>

								<td>
								
									<%
									
										if (history.getCustomerName() != null) {
									%><%=history.getCustomerName()%> <%
 	}
 %>
								</td>
								<td>
									<%
										if (history.getAccountNumber() != null) {
									%><%=history.getAccountNumber()%> <%
 	}
 %>
								</td>
								<td>
									<%
										if (history.getMobile() != null) {
									%><%=history.getMobile()%> <%
 	}
 %>
								</td>
								<td>
									<%
										if (history.getMessage() != null) {
									%><%=history.getMessage()%> <%
 	}
 %>
								</td>
								<td>
									<%
										if (history.getCreationDate() != null) {
									%><%=history.getCreationDate()%> <%
 	}
 %>
								</td>
							</tr>
							<%
								}
							%>
						</tbody>
					</table>
					
					<%
						} else {
					%>
					<br />
					<div class="alert alert-info">No History found matching your
						search criteria</div>
					<%
						}
					%>
					<a class="btn btn-info"
						href="${pageContext.request.contextPath}/SMSHistoryServlet?export=excel">Export
						Excel</a>
				</div>
				<div class="col-md-12 text-center">
					<ul class="pagination pagination-lg pager" id="myPager"></ul>
				</div>
			</div>
		</div>
	</form>
</div>
<%
	} else {
%>
<center>
	<h2>
		<label style="color: red; font-weight: bold;">You don't have
			Permission to access this Page!</label>
	</h2>
</center>
<%
	}
%>
</html>