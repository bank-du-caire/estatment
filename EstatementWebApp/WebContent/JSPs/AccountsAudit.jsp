<%@page import="com.bdc.usermanagement.ldab.UserManagmentDBTransactions"%>
<%@page import="com.bdc.estatement.util.Defines"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.bdc.usermanagement.ldab.AccountAudit"%>
<%@page import="java.util.List"%>
<%@page import="com.bdc.usermanagement.ldab.UserPrivilege"%>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Account Audits</title>
<%
	if (session.getAttribute("USER_ID") == null) {
		response.sendRedirect(request.getContextPath() + "/JSPs/Login.jsp");
		return;
	}
	UserPrivilege userPrivilege = UserPrivilege.findAllUserPrivileges("AccountsAudit.jsp", request);
%>

</head>
<body>

	<%
	
	String ACC_NO = "";
	String Cust_Name = "";
	String Cust_ID = "";
	if (request.getCharacterEncoding() == null) {
	    request.setCharacterEncoding("ISO-8859-6");
	}
		if (userPrivilege.getViewFlag() == 1) {
		/* 	List<AccountAudit> accounts = new ArrayList<AccountAudit>();
			UserManagmentDBTransactions dbTransactions = new UserManagmentDBTransactions();
			accounts = dbTransactions.findAllAccountAudits(); */
			
			List<AccountAudit> accounts = (List<AccountAudit>) request.getAttribute("auditAccountsList");
			String freqString = "";
		
			

			String dropAccount = "<select name=\"accountNo\" id=\"accountNo\">";
			String dropCustName = "<select name=\"customerName\" id=\"customerName\">";
			String dropCustID = "<select name=\"customerId\" id=\"customerId\">";

			dropAccount = dropAccount + "<option value=''>--Select Account Number--</option>";
			dropCustName = dropCustName + "<option value=''>--Select Customer Name--</option>";
			dropCustID = dropCustID + "<option value=''>--Select Customer ID-</option>";

			if (request.getParameter("accountNo") != null || request.getParameter("customerName") != null
					|| request.getParameter("customerId") != null) {

				ACC_NO = request.getParameter("accountNo");
				Cust_Name = request.getParameter("customerName");
				Cust_ID = request.getParameter("customerId");

			}

			for (int i = 0; i < accounts.size(); i++) {
				
				if (accounts.get(i).getAccountNumber().equals(ACC_NO)
						|| accounts.get(i).getCustomerName().equals(Cust_Name)
						|| accounts.get(i).getCustomerCode().equals(Cust_ID)) {
					if (!dropAccount.contains(accounts.get(i).getAccountNumber()))
					dropAccount = dropAccount + "<option value=" + accounts.get(i).getAccountNumber() + " selected >"
							+ accounts.get(i).getAccountNumber() + "</option> ";
					if (!dropCustName.contains(accounts.get(i).getCustomerName()))
						dropCustName = dropCustName + "<option value=" + accounts.get(i).getCustomerName()
								+ " selected >" + accounts.get(i).getCustomerName() + "</option> ";
					if (!dropCustID.contains(accounts.get(i).getCustomerCode()))
					dropCustID = dropCustID + "<option value=" + accounts.get(i).getCustomerCode() + " selected >"
							+ accounts.get(i).getCustomerCode() + "</option> ";
				} else {
					if (!dropAccount.contains(accounts.get(i).getAccountNumber()))
					dropAccount = dropAccount + "<option value=" + accounts.get(i).getAccountNumber() + ">"
							+ accounts.get(i).getAccountNumber() + "</option> ";
					if (!dropCustName.contains(accounts.get(i).getCustomerName()))
						dropCustName = dropCustName + "<option value=" + accounts.get(i).getCustomerName() + ">"
								+ accounts.get(i).getCustomerName() + "</option> ";
					if (!dropCustID.contains(accounts.get(i).getCustomerCode()))
					dropCustID = dropCustID + "<option value=" + accounts.get(i).getCustomerCode() + ">"
							+ accounts.get(i).getCustomerCode() + "</option> ";
				}
			}
			dropAccount = dropAccount + "</select >";
			dropCustName = dropCustName + "</select >";
			dropCustID = dropCustID + "</select >";
	%>
	<jsp:include page="TemplatePage.jsp" />

	<div class="container ">

		<h2>Account Audits</h2>
		<!--Search Form -->
		<form id="seachAccountForm" role="form"
			action="${pageContext.request.contextPath}/AccountAuditServlet"
			method="get">
			<input type="hidden" id="searchAction" name="searchAction"
				value="searchByName" />
			<div class="row">
				<div class="form-group col-xs-5">
				<%=dropCustName%>
				<!-- <input type="text" name="customerName" id="customerName"
					class="form-control" placeholder="Type the Customer Name" /> -->
			</div>
			<div class="form-group col-xs-5">
				<%=dropCustID%>
				<!-- <input type="text" name="customerId" id="customerId"
					class="form-control" placeholder="Type the Customer Id" /> -->
			</div>
			<div class="form-group col-xs-5">
				<%=dropAccount%>
				<!-- <input type="text" name="accountNo" id="accountNo"
					class="form-control" placeholder="Type the Account Number" /> -->

			</div>
			<div class="form-group col-xs-5">
				<select class="form-control" name='statementFrequency'>
					<option value="">--Select Statement frequency</option>
					<option value="1">Daily</option>
					<option value="2">Weekly</option>
					<option value="3">FortNightly</option>
					<option value="4">Monthly</option>
					<option value="5">Bi_Monthly</option>
					<option value="6">Quarterly</option>
					<option value="7">Half_Yearly</option>
					<option value="8">Yearly</option>
					

				</select>
			</div>
				<div class="form-group col-xs-5">
					<button type="submit" class="btn btn-info">
						<span class="glyphicon glyphicon-search"></span> Search
					</button>
					<a href="${pageContext.request.contextPath}/AccountAuditServlet"
						class="btn btn-info"> <span class="glyphicon"></span> Reset
					</a>
				</div>

			</div>
			<!-- 		</form> -->
			<%
				if (accounts != null && accounts.size() > 0) {
			%>

			<div class="container">
				<div class="row">
					<div>
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Customer Code</th>
									<th>Account Number</th>
									<th>Customer Name</th>
									<th>Email</th>
									<th>Branch</th>
									<th>Next Send Date</th>
									<th>Mobile</th>
									<th>Statement Frequency</th>
									<th>Created By</th>
									<th>Created Date</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody id="myTable">
								<%
									for (AccountAudit account : accounts) {
								%>
								<tr>
									<td>
										<%
											if (account.getCustomerCode() != null) {
										%><%=account.getCustomerCode()%> <%
 	}
 %>
									</td>
									<td>
										<%
											if (account.getAccountNumber() != null) {
										%><%=account.getAccountNumber()%> <%
 	}
 %>
									</td>
									<td>
										<%
											if (account.getCustomerName() != null) {
										%><%=account.getCustomerName()%> <%
 	}
 %>
									</td>
									<td>
										<%
											if (account.getEmail() != null) {
										%><%=account.getEmail()%> <%
 	}
 %>
									</td>
									<td>
										<%
											if (account.getBranchName() != null) {
										%><%=account.getBranchName()%> <%
 	}
 %>
									</td>
									
									<td>
										<%
											if (account.getNextDate() != null) {
										%><%=account.getNextDate()%> <%
 	}
 %>
									</td>
									<td>
										<%
											if (account.getMobile() != null) {
										%><%=account.getMobile()%> <%
 	}
 %>
									</td>
									<%
										switch (account.getFrequancyStmt()) {
													case Defines.Daily:
														freqString = "Daily";
														break;
													case Defines.Weekly:
														freqString = "Weekly";
														break;
													// 												case Defines.FortNightly:
													// 													freqString = "Fort Nightly";
													// 													break;
													case Defines.Monthly:
														freqString = "Monthly";
														break;
													// 												case Defines.Bi_Monthly:
													// 													freqString = "Bi_Monthly";
													// 													break;
													case Defines.Quarterly:
														freqString = "Quarterly";
														break;
													case Defines.Half_Yearly:
														freqString = "Semi-Annually";
														break;
													case Defines.Yearly:
														freqString = "Annually";
														break;
													default:
														freqString = "None";
														break;
													}
									%>
									<td><%=freqString%></td>
									
									<td><%=account.getCreatedBy()%></td>
									<td><%=account.getCreatedDate()%></td>
									<td><%=account.getAction()%></td>
								</tr>
								<%
									}
								%>

							</tbody>
						</table>

						<%
							} else {
						%>
						<br />
						<div class="alert alert-info">No Audits found matching your
							search criteria</div>
						<%
							}
						%>

					</div>


				</div>
				<div class="col-md-12 text-center">
					<ul class="pagination pagination-lg pager" id="myPager"></ul>
				</div>
			</div>
			<a class="btn btn-info"
				href="${pageContext.request.contextPath}/AccountAuditServlet?export=excel">Export
				Excel</a>
	</div>
	<%
		} else {
	%>
	<br>
	<br>
	<center>
		<h2>
			<label style="color: red; font-weight: bold;">You don't have
				Permission to access this Page!</label>
		</h2>
	</center>
	<%
		}
	%>


	</form>

</body>

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/chosen.min.css">
<script src="${pageContext.request.contextPath}/js/chosen.jquery.js"></script>



<script type="text/javascript">
	$(document).ready(function() {
		$('select').chosen({
			search_contains : true,
			width : '400px',
			height : '500px !important'

		});
	});
</script>

</html>

