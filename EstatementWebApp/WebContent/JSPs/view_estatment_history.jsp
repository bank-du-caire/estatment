<%@page import="com.bdc.usermanagement.ldab.UserPrivilege"%>
<%@page import="com.bdc.estatement.model.EstatementHistory"%>
<%@page import="com.bdc.estatement.util.Defines"%>
<%@page import="java.util.List"%>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type"  content="text/html; charset=UTF-8">
<%
	UserPrivilege userPrivilege = UserPrivilege.findAllUserPrivileges("view_estatment_history.jsp", request);
	int userId = session.getAttribute("USER_ID") != null ? (Integer) session.getAttribute("USER_ID") : 0;
%>
<title>Estatement History</title>
</head>
<%


String ACC_NO = "";
String Cust_Name = "";
String Cust_ID = "";


	if (session.getAttribute("USER_ID") == null) {
		response.sendRedirect(request.getContextPath() + "/JSPs/Login.jsp");
		return;
	}
	//UserPrivilege userPrivilege = UserPrivilege.findAllUserPrivileges("view_estatment_history.jsp", request);
	List<EstatementHistory> histories = (List<EstatementHistory>) request.getAttribute("historyList");
	String freqString = "";
	
	String dropAccount = "<select name=\"accountNo\" id=\"accountNo\">";
	String dropCustName = "<select name=\"customerName\" id=\"customerName\">";
	String dropCustID = "<select name=\"customerId\" id=\"customerId\">";

	dropAccount = dropAccount + "<option value=''>--Select Account Number--</option>";
	dropCustName = dropCustName + "<option value=''>--Select Customer Name--</option>";
	dropCustID = dropCustID + "<option value=''>--Select Customer ID-</option>";

	if (request.getParameter("accountNo") != null || request.getParameter("customerName") != null
			|| request.getParameter("customerId") != null) {

		ACC_NO = request.getParameter("accountNo");
		Cust_Name = request.getParameter("customerName");
		Cust_ID = request.getParameter("customerId");

	}

	for (int i = 0; i < histories.size(); i++) {
		
		if (histories.get(i).getAccountNo().equals(ACC_NO)
				|| histories.get(i).getCustomerName().equals(Cust_Name)
				|| histories.get(i).getCustomerCode().equals(Cust_ID)) {
			if (!dropAccount.contains(histories.get(i).getAccountNo()))
			dropAccount = dropAccount + "<option value=" + histories.get(i).getAccountNo() + " selected >"
					+ histories.get(i).getAccountNo() + "</option> ";
			if (!dropCustName.contains(histories.get(i).getCustomerName()))
				dropCustName = dropCustName + "<option value=" + histories.get(i).getCustomerName()
						+ " selected >" + histories.get(i).getCustomerName() + "</option> ";
			if (!dropCustID.contains(histories.get(i).getCustomerCode()))
			dropCustID = dropCustID + "<option value=" + histories.get(i).getCustomerCode() + " selected >"
					+ histories.get(i).getCustomerCode() + "</option> ";
		} else {
			if (!dropAccount.contains(histories.get(i).getAccountNo()))
			dropAccount = dropAccount + "<option value=" + histories.get(i).getAccountNo() + ">"
					+ histories.get(i).getAccountNo() + "</option> ";
			if (!dropCustName.contains(histories.get(i).getCustomerName()))
				dropCustName = dropCustName + "<option value=" + histories.get(i).getCustomerName() + ">"
						+ histories.get(i).getCustomerName() + "</option> ";
			if (!dropCustID.contains(histories.get(i).getCustomerCode()))
			dropCustID = dropCustID + "<option value=" + histories.get(i).getCustomerCode() + ">"
					+ histories.get(i).getCustomerCode() + "</option> ";
		}
	}
	dropAccount = dropAccount + "</select >";
	dropCustName = dropCustName + "</select >";
	dropCustID = dropCustID + "</select >";
%>


<%
	if (userPrivilege.getViewFlag() == 1) {
%>
<jsp:include page="TemplatePage.jsp" />
<div class="container">
	<h2>Estatment History</h2>
	<!--Search Form -->
	<form action="${pageContext.request.contextPath}/history" method="get"
		id="seachAccountForm" role="form" accept-charset="UTF-8">
		<input type="hidden" id="searchAction" name="searchAction"
			value="searchByName" />
		<div class="row">
			<div class="form-group col-xs-5">
				<%=dropCustName%>
				<!-- <input type="text" name="customerName" id="customerName"
					class="form-control" placeholder="Type the Customer Name" /> -->
			</div>
			<div class="form-group col-xs-5">
				<%=dropCustID%>
				<!-- <input type="text" name="customerId" id="customerId"
					class="form-control" placeholder="Type the Customer Id" /> -->
			</div>
			<div class="form-group col-xs-5">
				<%=dropAccount%>
				<!-- <input type="text" name="accountNo" id="accountNo"
					class="form-control" placeholder="Type the Account Number" /> -->

			</div>
			<div class="form-group col-xs-5">
				<select class="form-control" name='statementFrequency'>
					<option value="">--Select Statement frequency</option>
					<option value="1">Daily</option>
					<option value="2">Weekly</option>
					<option value="3">FortNightly</option>
					<option value="4">Monthly</option>
					<option value="5">Bi_Monthly</option>
					<option value="6">Quarterly</option>
					<option value="7">Half_Yearly</option>
					<option value="8">Yearly</option>
					

				</select>
			</div>
			<div class="form-group col-xs-5">
				<button type="submit" class="btn btn-info">
					<span class="glyphicon glyphicon-search"></span> Search
				</button>
				<a href="${pageContext.request.contextPath}/history"
					class="btn btn-info"> <span class="glyphicon"></span> Reset
				</a>
			</div>
			<div class="form-group col-xs-5">
				<a href="${pageContext.request.contextPath}/NewRequests"
					class="btn btn-info"> <span class="glyphicon"></span> New
					Requests
				</a>
			</div>
		</div>
		<%
			if (histories != null && histories.size() > 0) {
		%>




		<div class="container">
			<div class="row">
				<div>
					<table class="table table-hover" style="width: 100%">
						<thead>
							<tr>
								<th>Customer Name</th>
								<th>Account Number</th>
								<th>Customer Code</th>
								<th>Statement frequency</th>
								<th>Send Date</th>
								<th>From</th>
								<th>To</th>
								<th>PDF</th>
								<th>Excel</th>
								<th>Swift Message</th>
							</tr>
						</thead>
						<tbody id="myTable">
							<%
								for (EstatementHistory history : histories) {
							%>
							<tr>

								<td>
									<%
										if (history.getCustomerName() != null) {
									%><%=history.getCustomerName()%> <%
 	}
 %>
								</td>
								<td>
									<%
										if (history.getAccountNo() != null) {
									%><%=history.getAccountNo()%> <%
 	}
 %>
								</td>
								<td>
									<%
										if (history.getCustomerCode() != null) {
									%><%=history.getCustomerCode()%> <%
 	}
 %>
								</td>


								<%
									switch (history.getFreqStmt()) {
													case Defines.Daily :
														freqString = "Daily";
														break;
													case Defines.Weekly :
														freqString = "Weekly";
														break;
													case Defines.FortNightly :
														freqString = "Fort Nightly";
														break;
													case Defines.Monthly :
														freqString = "Monthly";
														break;
													case Defines.Bi_Monthly :
														freqString = "Bi_Monthly";
														break;
													case Defines.Quarterly :
														freqString = "Quarterly";
														break;
													case Defines.Half_Yearly :
														freqString = "Half_Yearly";
														break;
													case Defines.Yearly :
														freqString = "Yearly";
														break;
													default :
														freqString = "None";
														break;
												}
								%>
								<td><%=freqString%></td>
								<td>
									<%
										if (history.getTo() != null) {
									%><%=history.getTo()%> <%
 	}
 %>
								</td>
								<td>
									<%
										if (history.getFrom() != null) {
									%><%=history.getFrom()%> <%
 	}
 %>
								</td>
								<td>
									<%
										if (history.getTo() != null) {
									%><%=history.getTo()%> <%
 	}
 %>
								</td>
								<td><a
									href='Download?fileName=<%=history.getCustomerCode()%>&historyDate=<%=history.getTo()%>&extention=.pdf'>PDF</a></td>
								<td><a
									href='Download?fileName=<%=history.getCustomerCode()%>&historyDate=<%=history.getTo()%>&extention=.xls'>Excel</a></td>
								<td><a
									href='Download?fileName=<%=history.getCustomerCode()%>&historyDate=<%=history.getTo()%>&extention=.rar'>Swift</a></td>

							</tr>
							<%
								}
							%>
						</tbody>
					</table>
					<%
						} else {
					%>
					<br />
					<div class="alert alert-info">No History found matching your
						search criteria</div>
					<%
						}
					%>
				</div>
				<div class="col-md-12 text-center">
					<ul class="pagination pagination-lg pager" id="myPager"></ul>
				</div>
			</div>
			<center>
				<%
					String res = session.getAttribute("existFlag") != null
								? session.getAttribute("existFlag").toString()
								: "";
						if (res.equals("0")) {
				%>
				<script>
					alert('This File DoseNot Exist');
				</script>
				<%
					res = "";
						}
				%>
			</center>
		</div>
	</form>
</div>
<%
	} else {
%>
<center>
	<h2>
		<label style="color: red; font-weight: bold;">You don't have
			Permission to access this Page!</label>
	</h2>
</center>
<%
	}
%>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/chosen.min.css">
<script src="${pageContext.request.contextPath}/js/chosen.jquery.js"></script>



<script type="text/javascript">
	$(document).ready(function() {
		$('select').chosen({
			search_contains : true,
			width : '400px',
			height : '500px !important'

		});
	});
</script>
</html>