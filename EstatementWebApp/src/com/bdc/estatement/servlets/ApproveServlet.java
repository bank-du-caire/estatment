package com.bdc.estatement.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bdc.estatement.dao.TransactionsDao;

@WebServlet("/approve")
public class ApproveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		String accountNO = request.getParameter("accountNO");
		int status = 0;
		HttpSession session = request.getSession();
		status = TransactionsDao.approveAccount(accountNO, session.getAttribute("USER_ID").toString());
		if (status > 0) {
//			response.sendRedirect("account");
			response.sendRedirect(request.getContextPath() + "/main/pages/tables/view_accounts.jsp");
		} else {
			out.println("Sorry! unable to Approve record");
		}

	}
}
