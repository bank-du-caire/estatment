package com.bdc.estatement.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bdc.estatement.dao.TransactionsDao;
import com.bdc.estatement.util.LogUtil;

@WebServlet("/DeleteHostToHost")
public class DeleteHostToHost extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			String accountNum = request.getParameter("accountNo");
			int res = TransactionsDao.deleteHostToHostAccounts(accountNum);
			HttpSession session = request.getSession();
			if (res >= 1) {
				session.setAttribute("res", "service deleted successfully");
				response.sendRedirect(request.getContextPath() + "/main/pages/tables/HosttoHost.jsp");
			} else {
				session.setAttribute("res", "service did not deleted");
				response.sendRedirect(request.getContextPath() + "/main/pages/tables/HosttoHost.jsp");
			}
		} catch (Exception e) {
			response.sendRedirect(request.getContextPath() + "/main/pages/tables/HosttoHost.jsp");
			LogUtil.error(e);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
