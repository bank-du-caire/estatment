package com.bdc.estatement.servlets;

import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import com.bdc.estatement.dao.TransactionsDao;
import com.bdc.estatement.model.Account;
import com.bdc.estatement.service.Impl.TransactionServiceImpl;
import com.bdc.estatement.util.Utils;

@WebServlet(name = "ViewAccount", urlPatterns = { "/account" })
public class ViewServlet extends HttpServlet {
	TransactionServiceImpl transcationService = new TransactionServiceImpl();

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		request.setCharacterEncoding("UTF-8");
		try {
			HttpSession session = request.getSession();
			if (session.getAttribute("USER_ID") == null) {
				response.sendRedirect(request.getContextPath() + "/main/pages/tables/Login.jsp");
				return;
			}
			List<Account> list2 = TransactionsDao.getAccounts();
			String excel = null;
			excel = request.getParameter("export");
			if (excel != null && !excel.isEmpty()) {
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=accounts.xls");
				Utils.exportToExcel_mainAccount(list2, response);
				return;
			}

			String customerName = request.getParameter("customerName");
			if (customerName != null)
				customerName = new String(customerName.getBytes("8859_1"), "UTF-8");
			System.out.println(customerName);
			String customerId = request.getParameter("customerId");
			String accountNo = request.getParameter("accountNo");
			String status = request.getParameter("status");
			String statementFrequency = request.getParameter("statementFrequency");

			if (customerId != null && !customerId.isEmpty()) {
				searchAccountById(request, response);
			} else if (accountNo != null && !accountNo.isEmpty()) {
				searchAccountsByAccountNo(request, response);
			} else if (customerName != null && !customerName.isEmpty()) {
				searchAccountByName(request, response);
			} else if (status != null && !status.isEmpty()) {
				searchAccountByStatus(request, response);
			} else if (statementFrequency != null && !statementFrequency.isEmpty()) {
				searchAccountByFrequency(request, response);
			} else {
				List<Account> list = TransactionsDao.getAccounts();
				forwardListAccounts(request, response, list);
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}

//	@Override
//	protected void doPost(HttpServletRequest request, HttpServletResponse response)
//			throws ServletException, IOException {
//		request.setCharacterEncoding("UTF-8");
//		response.setCharacterEncoding("UTF-8");
//	}

	private void searchAccountByName(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String customerName = req.getParameter("customerName");
		customerName = new String(customerName.getBytes("8859_1"), "UTF-8");
		System.out.println(customerName);
		List<Account> result = transcationService.searchAccountsByName(customerName.trim());
		req.setAttribute("customerName", customerName);
		forwardListAccounts(req, resp, result);
	}

	private void searchAccountById(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String customerId = req.getParameter("customerId");
		List<Account> result = transcationService.searchAccountsById(customerId);
		forwardListAccounts(req, resp, result);
	}

	private void searchAccountsByAccountNo(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String accountNo = req.getParameter("accountNo");
		List<Account> result = transcationService.searchAccountsByAccountNo(accountNo);
		forwardListAccounts(req, resp, result);
	}

	private void searchAccountByStatus(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String status = req.getParameter("status");
		List<Account> result = transcationService.searchAccountByStatus(status);
		forwardListAccounts(req, resp, result);
	}

	private void searchAccountByFrequency(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String statementFrequency = req.getParameter("statementFrequency");
		List<Account> result = transcationService.searchAccountByFrequency(statementFrequency);
		forwardListAccounts(req, resp, result);
	}

	private void forwardListAccounts(HttpServletRequest req, HttpServletResponse resp, List<Account> accountList)
			throws ServletException, IOException {
		String nextJSP = "/JSPs/view_accounts.jsp";
//		String nextJSP = "/main/pages/tables/view_accounts.jsp";
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
		req.setAttribute("accountsList", accountList);
		dispatcher.forward(req, resp);
	}

	private <T> void exportToExcel(List<T> objects, HttpServletResponse response) {
		FileOutputStream fileOut = null;
		try {
			HSSFWorkbook workbook = new HSSFWorkbook();
			CreationHelper createHelper = workbook.getCreationHelper();
			Sheet sheet = workbook.createSheet();
			// Create a Font for styling header cells
			Font headerFont = workbook.createFont();
			headerFont.setFontHeightInPoints((short) 14);
			headerFont.setColor(IndexedColors.RED.getIndex());
			// Create Cell Style for formatting Date
			CellStyle dateCellStyle = workbook.createCellStyle();
			dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));
			// Create a CellStyle with the font
			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			int rowNum = 1;
			for (Object object : objects) {
				Field[] fields = object.getClass().getDeclaredFields();
				Row headerRow = sheet.createRow(0);
				for (int i = 0; i < fields.length; i++) {
					Cell cell = headerRow.createCell(i);
					cell.setCellValue(fields[i].getName());
					cell.setCellStyle(headerCellStyle);
				}
				Row row = sheet.createRow(rowNum++);
				for (int i = 0; i < fields.length; i++) {
					Field field = fields[i];
					field.setAccessible(true);
					row.createCell(i).setCellValue(String.valueOf(field.get(object)));
				}

				for (int i = 0; i < fields.length; i++) {
					sheet.autoSizeColumn(i);
				}
			}
			fileOut = new FileOutputStream("report.xls");
			workbook.write(response.getOutputStream());
			fileOut.close();
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		// return fileOut;
	}

}
