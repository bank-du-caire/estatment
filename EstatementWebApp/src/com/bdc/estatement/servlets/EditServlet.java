package com.bdc.estatement.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bdc.estatement.dao.TransactionsDao;
import com.bdc.estatement.model.Account;
import com.bdc.estatement.util.WebUtils;

@WebServlet("/EditServlet")
public class EditServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html; charset=utf-8");

		String accountNo = request.getParameter("accountNo");
		String freq = request.getParameter("freq").trim();
		String editFlag = request.getParameter("edit");

		Account account = null;
		HttpSession session = request.getSession();
		switch (editFlag) {
		case "newRequest":
//			account = TransactionsDao.getAccountByAccountNo(accountNo);
			int smsFlag = 0;
			int estatementFlag = 0;
			if (request.getParameter("smsFlag") != null)
				smsFlag = Integer.parseInt(request.getParameter("smsFlag").trim());
			if (request.getParameter("estatementFlag") != null)
				estatementFlag = Integer.parseInt(request.getParameter("estatementFlag").trim());

			account = TransactionsDao.getAccountByAccountNo(accountNo);
			if (account == null) {
				List<Account> accounts = TransactionsDao.getAccountTypeList();
				account = WebUtils.getAccountFromList(accounts, accountNo);
			} else {
				if (account.getEstatementFlag() == 1 && smsFlag == 1) {
					estatementFlag = 0;
					account.setEstatementFlag(0);
					account.setSmsFlag(1);
				}
				if (account.getSmsFlag() == 1 && estatementFlag == 1) {
					smsFlag = 0;
					account.setSmsFlag(0);
					account.setEstatementFlag(1);
				}
			}

			session.setAttribute("smsFlag", smsFlag);
			session.setAttribute("estatementFlag", estatementFlag);

			break;
		case "account": {
//			HttpSession session = request.getSession();
			session.removeAttribute("smsFlag");
			session.removeAttribute("estatementFlag");
			account = TransactionsDao.getAccountByAccountNoAndFrequency(accountNo, Integer.valueOf(freq));
			break;
		}
		default:
			System.out.println("############ Null");
		}
		forwardAccount(request, response, account);

	}

	private void forwardAccount(HttpServletRequest req, HttpServletResponse resp, Account account)
			throws ServletException, IOException {
//		String nextJSP = "/main/pages/tables/edit_account.jsp";
		String nextJSP = "/main/pages/tables/edit_account.jsp";
//		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
		HttpSession session = req.getSession();
		session.setAttribute("account", account);
		resp.sendRedirect(req.getContextPath() + nextJSP);
//		dispatcher.forward(req, resp);
	}

}
