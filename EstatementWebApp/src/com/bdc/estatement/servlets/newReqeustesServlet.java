package com.bdc.estatement.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bdc.estatement.dao.TransactionsDao;
import com.bdc.estatement.model.Account;
import com.bdc.estatement.service.Impl.TransactionServiceImpl;
import com.bdc.estatement.util.Utils;

@WebServlet(name = "NewRequests", urlPatterns = { "/NewRequests" })
public class newReqeustesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	TransactionServiceImpl transcationService = new TransactionServiceImpl();

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		request.setCharacterEncoding("UTF-8");

		List<Account> accounts = TransactionsDao.getAccountTypeList();
		String excel = request.getParameter("export");
		if (excel != null && !excel.isEmpty()) {
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename=accounts.xls");
			Utils.exportToExcel(accounts, response);
			return;
		}

//		String customerName = request.getParameter("customerName");
//		if (customerName != null && !customerName.isEmpty())
//			customerName = new String(customerName.getBytes("8859_1"), "UTF-8");
//		String customerId = request.getParameter("customerId");
//		String accountNo = request.getParameter("accountNo");

//		if (customerId != null && !customerId.isEmpty()) {
//			searchAccountById(request, response);
//		} else if (accountNo != null && !accountNo.isEmpty()) {
//			searchAccountsByAccountNo(request, response);
//		} else if (customerName != null && !customerName.isEmpty()) {
//			searchAccountByName(request, response);
//		} else {
		// List<Account> list = TransactionsDao.getAccounts();

		forwardListAccounts(request, response, accounts);
//		}
//		request.setAttribute("accountsList", accounts);
//	    this.getServletContext().getRequestDispatcher("/JSPs/new_requstes.jsp").
		// include(request, response);
	}

//	private void searchAccountByName(HttpServletRequest req, HttpServletResponse resp)
//			throws ServletException, IOException {
//		String customerName = req.getParameter("customerName");
//		if (customerName != null && !customerName.isEmpty())
//			customerName = new String(customerName.getBytes("8859_1"), "UTF-8");
//		System.out.println("customerName : [" + customerName + "]");
//		List<Account> result = transcationService.searchNewRequestsByName(customerName);
//		forwardListAccounts(req, resp, result);
//	}

//	private void searchAccountById(HttpServletRequest req, HttpServletResponse resp)
//			throws ServletException, IOException {
//		String customerId = req.getParameter("customerId");
//		List<Account> result = transcationService.searchNewRequestsById(customerId);
//		forwardListAccounts(req, resp, result);
//	}

//	private void searchAccountsByAccountNo(HttpServletRequest req, HttpServletResponse resp)
//			throws ServletException, IOException {
//		String accountNo = req.getParameter("accountNo").trim();
//		List<Account> result = transcationService.searchNewRequestsByAccountNo(accountNo);
//		forwardListAccounts(req, resp, result);
//	}

	private void forwardListAccounts(HttpServletRequest req, HttpServletResponse resp, List<Account> accountList)
			throws ServletException, IOException {
//		String nextJSP = "/JSPs/new_requstes.jsp";
		String nextJSP = "/main/pages/tables/new_requstes.jsp";
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
		req.setAttribute("accountsList", accountList);
		dispatcher.forward(req, resp);
	}
}
