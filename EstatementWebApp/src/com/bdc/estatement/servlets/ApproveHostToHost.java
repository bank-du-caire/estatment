package com.bdc.estatement.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bdc.estatement.dao.TransactionsDao;

@WebServlet("/ApproveHostToHost")
public class ApproveHostToHost extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("USER_ID") == null) {
			response.sendRedirect(request.getContextPath() + "/main/pages/tables/Login.jsp");
		}
		String accountNumber = request.getParameter("accountNO");
		String approvedByUserID = session.getAttribute("USER_ID").toString();
		int res = TransactionsDao.approveHostToHostAccounts(accountNumber, 1, approvedByUserID);
		if (res == 1) {
			session.setAttribute("result", "Request Approved Successfully");
		} else {
			session.setAttribute("result", "Request not Approved");
		}
		response.sendRedirect(request.getContextPath() + "/main/pages/tables/HosttoHost.jsp");
	}

}
