package com.bdc.estatement.servlets;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bdc.estatement.dao.TransactionsDao;
import com.bdc.estatement.model.EstatementHistory;
import com.bdc.estatement.util.LogUtil;

/**
 * Servlet implementation class EstatementHistoryFilter
 */
@WebServlet("/EstatementHistoryFilter")
public class EstatementHistoryFilter extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();

		String fromDate = (String) request.getParameter("fromDate");
		String toDate = (String) request.getParameter("toDate");
		System.out.println(fromDate);
		System.out.println(toDate);
		long diff = 0L;
		try {
			Date fromDate2 = new SimpleDateFormat("yyyy-MM-dd").parse(fromDate);
			Date toDate2 = new SimpleDateFormat("yyyy-MM-dd").parse(toDate);
			long diffInMillies = Math.abs(toDate2.getTime() - fromDate2.getTime());
			diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
		} catch (ParseException e) {
			System.out.println(e);
			LogUtil.error(e);
		}
		session.setAttribute("fromDate", fromDate);
		session.setAttribute("toDate", toDate);
		if (diff > 15) {
			if (session.getAttribute("histories") != null)
				session.removeAttribute("histories");
			session.setAttribute("errorMsg", "Please filter with maximum 15 days!!");
			response.sendRedirect(request.getContextPath() + "/main/pages/tables/view_estatment_history.jsp");
		} else {
			if (session.getAttribute("errorMsg") != null)
				session.removeAttribute("errorMsg");
			List<EstatementHistory> histories = new ArrayList<EstatementHistory>();
			histories = TransactionsDao.getEstatementHistory(fromDate, toDate);
			session.setAttribute("histories", histories);
			response.sendRedirect(request.getContextPath() + "/main/pages/tables/view_estatment_history.jsp");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
