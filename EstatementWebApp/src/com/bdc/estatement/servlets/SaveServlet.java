package com.bdc.estatement.servlets;


import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bdc.estatement.dao.TransactionsDao;
import com.bdc.estatement.model.Account;
@WebServlet("/SaveServlet")
public class SaveServlet extends HttpServlet {
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html; charset=utf-8");
		PrintWriter out=response.getWriter();
		
		String customerCode=request.getParameter("customerCode");
		String accountNo=request.getParameter("accountNo");
		String email=request.getParameter("email");
		String freq=request.getParameter("freq");
		
		Account account = new Account();
		account.setCustomerCode(customerCode);
		account.setAccountNumber(accountNo);
		account.setEmail(email);
		account.setFrequancyStmt(Integer.valueOf(freq));
		
		int status = TransactionsDao.saveAccount(account);
		if(status>0){
			out.print("<p>Record saved successfully!</p>");
			request.getRequestDispatcher("index.html").include(request, response);
		}else{
			out.println("Sorry! unable to save record");
		}
		
		out.close();
	}

}
