package com.bdc.estatement.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bdc.estatement.dao.TransactionsDao;
import com.bdc.estatement.model.Transaction;
import com.bdc.estatement.util.LogUtil;
import com.sun.net.httpserver.HttpsServer;

@WebServlet("/HostToHostServlet")
public class HostToHostServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public HostToHostServlet() {

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LogUtil.initialize();
		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		request.setCharacterEncoding("UTF-8");
		try {

		} catch (Exception e) {
			System.out.println(e);
			LogUtil.error(e);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		LogUtil.initialize();
		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		request.setCharacterEncoding("UTF-8");
		try {
			HttpSession session = request.getSession();
			if (session.getAttribute("USER_ID") == null) {
				response.sendRedirect(request.getContextPath() + "/main/pages/tables/Login.jsp");
			}
			String accountNum = request.getParameter("accountNum");
			String sftpStatementFolder = request.getParameter("SftpFolder");
			String sftpPaymentFolder = request.getParameter("sftpPaymentFolder");
			System.out.println(accountNum);
			System.out.println(sftpStatementFolder);
			if (session.getAttribute("editHostToHost") != null && session.getAttribute("editHostToHost") == "1") {
				// here edit
				int res = TransactionsDao.updateHostToHostAccount(accountNum, sftpStatementFolder, sftpPaymentFolder);
				if (res == 1) {
					session.removeAttribute("editHostToHost");
					session.removeAttribute("sftpFolder");
					session.removeAttribute("accountNumber");
					session.removeAttribute("sftpPaymentFolder");
					session.setAttribute("result", "Request Updated Successfully");
					response.sendRedirect(request.getContextPath() + "/main/pages/tables/HosttoHost.jsp");
				} else {
					session.setAttribute("result", "Request not Updated please communicate system admin");
					response.sendRedirect(request.getContextPath() + "/main/pages/tables/CreateOrUpdateHostToHost.jsp");
				}
			} else {

				String loggedInUserName = session.getAttribute("USER_Name").toString();

				List<String> info = TransactionsDao.getUserNameAndCode(accountNum);
				if (info.size() == 0) {
					session.setAttribute("result", "An Error Happened please communicate your system admin.");
					response.sendRedirect(request.getContextPath() + "/main/pages/tables/CreateOrUpdateHostToHost.jsp");
				}
				String userID = session.getAttribute("USER_ID").toString();
				int res = TransactionsDao.insertHostToHostAccounts(info.get(0), info.get(1), accountNum,
						sftpStatementFolder, loggedInUserName, userID, sftpPaymentFolder);
				if (res == 1) {
					session.setAttribute("result", "Request Added Successfully");
					response.sendRedirect(request.getContextPath() + "/main/pages/tables/CreateOrUpdateHostToHost.jsp");
				} else {
					session.setAttribute("result", "Request not Added");
					response.sendRedirect(request.getContextPath() + "/main/pages/tables/CreateOrUpdateHostToHost.jsp");
				}
			}
		} catch (Exception e) {
			System.out.println(e);
			LogUtil.error(e);
		}
	}

}
