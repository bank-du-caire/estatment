package com.bdc.estatement.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bdc.estatement.dao.SMSDao;
import com.bdc.estatement.util.Utils;

/**
 * Servlet implementation class EstatmentHistoryServlet
 */
@WebServlet("/SMSHistoryServlet")
public class SMSHistoryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
//		List<SMSHistory> smsHistories = SMSDao.getSMSHistory();
//		String nextJSP = "/main/pages/tables/sms_history.jsp";
////		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
//		request.setAttribute("historyList", smsHistories);
//		response.sendRedirect(request.getContextPath() + nextJSP);
		System.out.println("testing Get() SMS history");
		if (request.getParameter("export") != null) {
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename=SMS History.xls");
			Utils.exportToExcelSMSHistory(SMSDao.getSMSHistory(), response);
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("testing Post() SMS history");
		if (request.getAttribute("export") != null) {
			Utils.exportToExcelSMSHistory(SMSDao.getSMSHistory(), response);
			response.sendRedirect(request.getContextPath() + "/main/pages/tables/sms_history.jsp");
			return;
		}
	}

}
