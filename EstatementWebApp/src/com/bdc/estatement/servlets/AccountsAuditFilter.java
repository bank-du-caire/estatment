package com.bdc.estatement.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.mail.Session;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bdc.estatement.dao.TransactionsDao;
import com.bdc.estatement.util.LogUtil;
import com.bdc.usermanagement.ldab.AccountAudit;
import com.bdc.usermanagement.ldab.UserManagmentDBTransactions;

@WebServlet("/AccountsAuditFilter")
public class AccountsAuditFilter extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();

		String fromDate = (String) request.getParameter("fromDate");
		String toDate = (String) request.getParameter("toDate");
		System.out.println(fromDate);
		System.out.println(toDate);
		long diff = 0L;

		if (fromDate != null && toDate != null) {
			try {
				Date fromDate2 = new SimpleDateFormat("yyyy-MM-dd").parse(fromDate);
				Date toDate2 = new SimpleDateFormat("yyyy-MM-dd").parse(toDate);
				long diffInMillies = Math.abs(toDate2.getTime() - fromDate2.getTime());
				diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
			} catch (ParseException e) {
				System.out.println(e);
				LogUtil.error(e);
			}
		}
		session.setAttribute("fromDate2", fromDate);
		session.setAttribute("toDate2", toDate);
		System.out.println("diff:  " + diff);
		if (diff > 2) {
			if (session.getAttribute("accountAudits") != null)
				session.removeAttribute("accountAudits");
			session.setAttribute("errorMsg2", "Please filter with maximum 3 days!!");
			response.sendRedirect(request.getContextPath() + "/main/pages/tables/AccountsAudit.jsp");
		} else {
			if (session.getAttribute("errorMsg2") != null)
				session.removeAttribute("errorMsg2");

			List<AccountAudit> accounts = new ArrayList<AccountAudit>();
			UserManagmentDBTransactions dbTransactions = new UserManagmentDBTransactions();
			try {
				Date fromDate2 = new SimpleDateFormat("yyyy-MM-dd").parse(fromDate);
				Date toDate2 = new SimpleDateFormat("yyyy-MM-dd").parse(toDate);
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
				String fromDateStr = formatter.format(fromDate2);
				String toDateStr = formatter.format(toDate2);
				accounts = dbTransactions.findAllAccountAudits(fromDateStr, toDateStr);
			} catch (Exception e) {
				LogUtil.error("SQL Error: " + e);
			}
			session.setAttribute("accountAudits", accounts);
			response.sendRedirect(request.getContextPath() + "/main/pages/tables/AccountsAudit.jsp");
		}
	}
}
