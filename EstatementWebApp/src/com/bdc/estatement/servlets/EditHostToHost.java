package com.bdc.estatement.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/EditHostToHost")
public class EditHostToHost extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("USER_ID") == null) {
			response.sendRedirect(request.getContextPath() + "/main/pages/tables/Login.jsp");
		}
		String accountNumber = request.getParameter("accountNO");
		String sftpFolder = request.getParameter("sftpFolder");
		String sftpPaymentFolder = request.getParameter("sftpPaymentFolder");
		session.setAttribute("editHostToHost", "1");
		session.setAttribute("accountNumber", accountNumber);
		session.setAttribute("sftpFolder", sftpFolder);
		session.setAttribute("sftpPaymentFolder", sftpPaymentFolder);

		response.sendRedirect(request.getContextPath() + "/main/pages/tables/CreateOrUpdateHostToHost.jsp");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
