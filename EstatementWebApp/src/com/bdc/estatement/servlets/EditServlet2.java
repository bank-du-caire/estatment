package com.bdc.estatement.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoField;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bdc.estatement.dao.TransactionsDao;
import com.bdc.estatement.model.Account;
import com.bdc.estatement.util.WebUtils;

@WebServlet("/EditServlet2")
public class EditServlet2 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession();
		int userId = (Integer) session.getAttribute("USER_ID");

		String sFreq = request.getParameter("freq");
		if (sFreq.equals("") || sFreq.isEmpty()) {
			sFreq = request.getParameter("accountFreq");
		}
		int freq = Integer.parseInt(sFreq);
		String accountNo = request.getParameter("accountNumber");
		String mobile = request.getParameter("mobile");
		String email = request.getParameter("email");
		String nextDate = "";
		Account a = session.getAttribute("account") != null ? (Account) session.getAttribute("account") : null;
		// means it's SMS with Estatement
		if (a != null && a.getSmsFlag() == 1 && a.getEstatementFlag() == 0 && a.getNextDate() != null
				&& a.getEmail() != null) {
			email = a.getEmail();
			nextDate = a.getNextDate().toString();
		} else if (a != null && a.getSmsFlag() == 0 && a.getEstatementFlag() == 1 && a.getMobile() != null) {
			mobile = a.getMobile();
		}
		if (sFreq != null && !sFreq.isEmpty() && sFreq.equals("0") && a.getEstatementFlag() == 1) {
			session.setAttribute("editResult", "Statement Frequency Is Required");
			response.sendRedirect(request.getContextPath() + "/main/pages/tables/edit_account.jsp");
			return;
		}
		if (sFreq.equals("4") && nextDate.isEmpty()) {// means monthly
			int year = Calendar.getInstance().get(Calendar.YEAR);
			String nextMonth = request.getParameter("nextMonth");

			String date = (Integer.parseInt(nextMonth) + 1) + "/1/" + year;
			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			Date convertedDate;
			try {
				convertedDate = dateFormat.parse(date);
				Calendar c = Calendar.getInstance();
				c.setTime(convertedDate);
				c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
				nextDate = String.valueOf(c.get(Calendar.YEAR)) + "-" + String.valueOf((c.get(Calendar.MONTH) + 1))
						+ "-1";
				System.out.println(nextDate);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		} else if (sFreq.equals("6") && nextDate.isEmpty()) {// means Quarterly.
			int year = Calendar.getInstance().get(Calendar.YEAR);
			int nextQuarter = 0;
			if (request.getParameter("nextQuarter") != null)
				nextQuarter = Integer.parseInt(request.getParameter("nextQuarter"));
			Date date = new Date();
			LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			int month = localDate.getMonthValue();
			System.out.println("month:[" + month + "]");

			switch (nextQuarter) {
			case 1: {
				if (month > 3)
					year++;
				nextDate = year + "-4-1";
				break;
			}
			case 2: {
				if (month > 6)
					year++;
				nextDate = year + "-7-1";
				break;
			}
			case 3: {
				if (month > 9)
					year++;
				nextDate = year + "-10-1";
				break;
			}
			case 4: {
				if (month >= 9 && month <= 12)
					year++;
				nextDate = year + "-1-1";
				break;
			}
			}
			System.out.println(nextDate);

		} else if (sFreq.equals("7") && nextDate.isEmpty()) {// means semi annually.
			Date date = new Date();
			LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			int month = localDate.getMonthValue();
			int year = Calendar.getInstance().get(Calendar.YEAR);
			int nextSemi = 0;
			if (request.getParameter("nextQuarter") != null)
				nextSemi = Integer.parseInt(request.getParameter("nextSemi"));
			if (nextSemi == 1) {
				if (month >= 6)
					year++;
				nextDate = year + "-7-1";
			} else if (nextSemi == 2) {
				year++;
				nextDate = year + "-1-1";
			}
			System.out.println(nextDate);
		} else if (sFreq.equals("8") && nextDate.isEmpty()) {// 8 means yearly
			Date date = new Date();
			LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			int year = Calendar.getInstance().get(Calendar.YEAR) + 1;
			nextDate = year + "-1-1";
		} else if (sFreq.equals("2") && nextDate.isEmpty()) {
			nextDate = request.getParameter("nextDate");
			try {
				Date da = new SimpleDateFormat("yyyy-MM-dd").parse(nextDate);
				LocalDate lD = da.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
				DayOfWeek day = DayOfWeek.of(lD.get(ChronoField.DAY_OF_WEEK));
				if (day != DayOfWeek.SUNDAY) {
					session.setAttribute("editResult", "Sorry, Weekly Frequency date must on a sunday");
					response.sendRedirect(request.getContextPath() + "/main/pages/tables/edit_account.jsp");
					return;
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
		} else if (nextDate.isEmpty())
			nextDate = request.getParameter("nextDate");

		String custCode = request.getParameter("custCode");
		String customerName = request.getParameter("customerName_");
		String branchName = request.getParameter("branchName_");
		int sms = Integer.valueOf(request.getParameter("sms"));
		int estatement = Integer.valueOf(request.getParameter("estatement"));

		if (a != null && a.getSmsFlag() == 1 && a.getEstatementFlag() == 0 && a.getNextDate() != null
				&& a.getEmail() != null) {
			estatement = 1;
		} else if (a != null && a.getSmsFlag() == 0 && a.getEstatementFlag() == 1 && a.getMobile() != null) {
			sms = 1;
		}

		Date nextD = null;
		int status = 0;
		try {
			if (nextDate != null && !nextDate.isEmpty())
				nextD = new SimpleDateFormat("yyyy-MM-dd").parse(nextDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Account ac = new Account();
		ac.setFailureMail(0);
		ac.setAccountNumber(accountNo);
		ac.setFrequancyStmt(freq);
		ac.setMobile(mobile);
		if (email != null && !email.isEmpty())
			ac.setEmail(email);
		if (nextD != null)
			ac.setNextDate(nextD);
		ac.setCustomerCode(custCode);
		ac.setCreatedBy(userId);
		ac.setCustomerName(customerName);
		ac.setBranchName(branchName);
		ac.setSmsFlag(sms);
		ac.setEstatementFlag(estatement);

		if ((nextDate == null || nextDate.isEmpty()) && ac.getEstatementFlag() == 1) {
			session.setAttribute("editResult", "Next Date Is Required");
			response.sendRedirect(request.getContextPath() + "/main/pages/tables/edit_account.jsp");
			return;
		}
		if ((ac.getEmail() == null || ac.getEmail().isEmpty()) && ac.getEstatementFlag() == 1) {
			session.setAttribute("editResult", "Email Is Required");
			response.sendRedirect(request.getContextPath() + "/main/pages/tables/edit_account.jsp");
			return;
		}

		if ((ac.getMobile() == null || ac.getMobile().isEmpty()) && ac.getSmsFlag() == 1) {
			session.setAttribute("editResult", "Mobile Number Is Required");
			response.sendRedirect(request.getContextPath() + "/main/pages/tables/edit_account.jsp");
			return;
		}

		if (ac.getMobile() != null && !ac.getMobile().isEmpty() && ac.getMobile().contains(",")) {
			String arr[] = ac.getMobile().split(",");
			for (int i = 0; i < arr.length; i++) {
				if (arr[i].trim().length() != 11) {
					session.setAttribute("editResult", "Mobile Number must be 11 Digits Seperated by Comma ',' ");
					response.sendRedirect(request.getContextPath() + "/main/pages/tables/edit_account.jsp");
					return;
				}
			}
		} else if (ac.getMobile() != null && !ac.getMobile().isEmpty() && ac.getSmsFlag() == 1
				&& ac.getMobile().length() != 11) {
			session.setAttribute("editResult", "Mobile Number must be 11 Digits");
			response.sendRedirect(request.getContextPath() + "/main/pages/tables/edit_account.jsp");
			return;
		}

		List<Account> accounts = TransactionsDao.getAccounts();
		// Shady code
		// Account account = WebUtils.getAccountFromList(accounts, accountNo);
		Account account = WebUtils.getAccountFromListWithFrequancy(accounts, accountNo, ac.getFrequancyStmt());
		if (account != null) {
			status = TransactionsDao.updateAccountByFrequency(ac);
		} else {
			status = TransactionsDao.saveAccount(ac);
		}
		if (status > 0) {
//			response.sendRedirect("account");
			session.removeAttribute("editResult");
			session.removeAttribute("account");
			response.sendRedirect(request.getContextPath() + "/main/pages/tables/view_accounts.jsp");
		} else {
			out.println("Sorry! unable to update record");
		}
		out.close();
	}

}
