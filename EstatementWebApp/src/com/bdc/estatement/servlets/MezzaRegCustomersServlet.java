package com.bdc.estatement.servlets;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
//import org.apache.tomcat.util.json.JSONParser;
import org.json.JSONObject;

import com.bdc.estatement.dao.TransactionsDao;
import com.bdc.estatement.model.Customer;
import com.bdc.estatement.service.Impl.TransactionServiceImpl;

@WebServlet("/MezzaRegCustomers")
public class MezzaRegCustomersServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private final String CustomerSessionObject = "customer";
	// TransactionServiceImpl transcationService;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MezzaRegCustomersServlet() {
		super();// transcationService = new TransactionServiceImpl();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		request.setCharacterEncoding("UTF-8");
		// search conditions
		String custId = request.getParameter("custId");
		if (custId != null) {
			Customer c = TransactionsDao.getCustomerById(Integer.parseInt(custId));

			HttpSession session = request.getSession();

			if (c != null && c.getName() != null) {
				session.setAttribute(CustomerSessionObject, c);
				request.setAttribute(CustomerSessionObject, c);

			}
		}
		// RequestDispatcher rd=request.dis("/Mezza_Reg_Customers.jsp");

		response.sendRedirect(request.getContextPath() + "/main/pages/tables/Mezza_Reg_Customers.jsp");
		// response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		resp.setContentType("text/html; charset=UTF-8");
		resp.setCharacterEncoding("UTF-8");
		resp.setCharacterEncoding("UTF-8");
		String jsonString = inputStreamToString(req.getInputStream());// req.in().read();

//	JSONParser parser = new JSONParser(req.getInputStream());

		JSONObject params = new JSONObject(jsonString);
		String status = params.getString("status");
		String codeCustId = params.getString("codeCustId");
		boolean updated = TransactionsDao.updateMeezaRegCust(Integer.parseInt(status), Integer.parseInt(codeCustId));

		String msg = "";
		if (updated) {
			msg = "stats updatted ";

			resp.setStatus(HttpServletResponse.SC_OK);
		} else {
			msg = "Sorrey ,  updatte failed";
			resp.setStatus(HttpServletResponse.SC_EXPECTATION_FAILED);
		}

		// TODO Auto-generated method stub
		resp.getWriter().append(msg);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		request.setCharacterEncoding("UTF-8");
		System.out.println("=======customer Id =====>" + request.getParameter("custId"));
		Integer custId = Integer.parseInt(request.getParameter("custId"));
		String email = request.getParameter("email");
		String name = request.getParameter("name");
		String address = request.getParameter("address");
		Customer customer = new Customer();
		customer.setId(custId);
		customer.setName(name);
		customer.setEmail(email);
		customer.setAddress(address);
		TransactionsDao.insertIntoMeezaRegCustomer(customer);
		request.getSession().setAttribute("customer", null);
		request.setAttribute("customer", null);

		response.sendRedirect(request.getContextPath() + "/main/pages/tables/Mezza_Reg_Customers.jsp");
		// doGet(request, response);
	}

	private static String inputStreamToString(InputStream inputStream) {
		Scanner scanner = new Scanner(inputStream, "UTF-8");
		return scanner.hasNext() ? scanner.useDelimiter("\\A").next() : "";
	}
}
