package com.bdc.estatement.servlets;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Download
 */
@WebServlet("/Download")
public class Download extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final int ARBITARY_SIZE = Integer.MAX_VALUE - 1;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Download() {
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String fileName = request.getParameter("fileName");
		String historyDate = request.getParameter("historyDate");
		String extention = request.getParameter("extention");

		response.setContentType("text/plain");
//		response.setHeader("Content-disposition", "attachment; filename=" + fileName + " - " + historyDate + extention + " ");

//		File file = new File("D://BDC_APP//EStatement//Attachements//"
//				+ fileName+" - "+new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
		File file = new File("D://BDC_APP//EStatement//Attachements//" + fileName + " - " + historyDate + extention);
		HttpSession session = request.getSession();
		session.removeAttribute("existFlag");
		if (file.exists()) {
			System.out.println("file exist ...");
			response.setHeader("Content-disposition",
					"attachment; filename=" + fileName + " - " + historyDate + extention + " ");

			session.setAttribute("existFlag", "1");
			try (DataInputStream in = new DataInputStream(new FileInputStream(file));
					OutputStream out = response.getOutputStream()) {
//				byte[] buffer = new byte[ARBITARY_SIZE];
				byte[] buffer = new byte[10000];
				List<Byte> b = new ArrayList<Byte>();

				int numBytesRead;
				while ((numBytesRead = in.read(buffer)) > 0) {

					out.write(buffer, 0, numBytesRead);
				}
			}

		} else {
			System.out.println("file not found ...");

			session.setAttribute("existFlag", "0");
			response.sendRedirect(request.getContextPath() + "/main/pages/tables/view_estatment_history.jsp");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

}
