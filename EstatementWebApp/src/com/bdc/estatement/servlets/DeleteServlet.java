package com.bdc.estatement.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bdc.estatement.dao.TransactionsDao;

@WebServlet("/DeleteServlet")
public class DeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		String accountNo = request.getParameter("accountNo");
//		accountNo = accountNo.trim();
		HttpSession session = request.getSession();
		int userId = 0;
		if (session.getAttribute("USER_ID") != null) {
			userId = (Integer) session.getAttribute("USER_ID");
		} else {
			response.sendRedirect(request.getContextPath() + "/main/pages/tables/Login.jsp");
			return;
		}
		String customerCode = request.getParameter("customerCode");
		String freq = request.getParameter("freq");
		int freq1 = Integer.parseInt(freq);
//		int status = TransactionsDao.updateDeletedAccount(accountNo,userId);
		// Shady Code
		//int status = TransactionsDao.deleteAccount(accountNo);
		// *****///
		int status = TransactionsDao.deleteAccountWithFreq(accountNo,freq1);
		accountNo = accountNo.trim();
		if (status > 0) {
			response.sendRedirect(request.getContextPath() + "/main/pages/tables/view_accounts.jsp");
		} else {
			out.println("Sorry! unable to delete record");
		}

	}
}
