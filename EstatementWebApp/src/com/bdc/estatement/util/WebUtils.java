package com.bdc.estatement.util;

import java.util.List;

import com.bdc.estatement.model.Account;

public class WebUtils {
//	public static Account getAccountFromList(List<Account> accounts,String accountNo,int freq) {
//		Account account =  null;
//		for(Account a : accounts){
//			String x = a.getAccountNumber().trim();
//			String y = accountNo.trim();
//			if(x.equals(y) && a.getFrequancyStmt() == freq){
//				account = a;
//			}
//		}
//		return account;
//	}
	
	public static Account getAccountFromList(List<Account> accounts,String accountNo) {
		Account account =  null;
		for(Account a : accounts){
			String x = a.getAccountNumber().trim();
			String y = accountNo.trim();
			if(x.equals(y)){
				account = a;
			}
		}
		return account;
	}
	public static Account getAccountFromListWithFrequancy(List<Account> accounts, String accountNo, int frequancyStmt) {
		Account account = null;
		for (Account a : accounts) {
			String x = a.getAccountNumber().trim();
			String y = accountNo.trim();
			int freq = a.getFrequancyStmt();
			if (x.equals(y) && freq == frequancyStmt) {
				account = a;
			}
		}
		return account;
	}
}
