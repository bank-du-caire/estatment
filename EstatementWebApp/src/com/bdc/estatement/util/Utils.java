package com.bdc.estatement.util;

import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import com.bdc.estatement.model.Account;
import com.bdc.estatement.model.SMSHistory;
import com.bdc.usermanagement.ldab.AccountAudit;
import com.bdc.usermanagement.ldab.RoleAudit;
import com.bdc.usermanagement.ldab.ScreenAudit;
import com.bdc.usermanagement.ldab.User;
import com.bdc.usermanagement.ldab.UserAudit;
import com.bdc.usermanagement.ldab.UserRoleAudit;
import com.bdc.usermanagement.ldab.UserRule;

public class Utils {
	public static Account getAccountFromList(List<Account> accounts, String accountNo) {
		Account account = null;
		for (Account a : accounts) {
			String x = a.getAccountNumber().trim();
			String y = accountNo.trim();
			if (x.equals(y)) {
				account = a;
			}
		}
//				accounts.stream()
//				.filter(ac -> ac.getAccountNumber().equals(accountNo))
//				.findFirst().orElse(null);
		return account;
	}

	public static <T> FileOutputStream exportToExcelSMSHistory(List<SMSHistory> allHistory,
			HttpServletResponse response) {
		FileOutputStream fileOut = null;
		try {
			HSSFWorkbook workbook = new HSSFWorkbook();
			CreationHelper createHelper = workbook.getCreationHelper();
			Sheet sheet = workbook.createSheet();
			// Create a Font for styling header cells
			Font headerFont = workbook.createFont();
			headerFont.setFontHeightInPoints((short) 14);
			headerFont.setColor(IndexedColors.RED.getIndex());
			// Create Cell Style for formatting Date
			CellStyle dateCellStyle = workbook.createCellStyle();
			dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("yyyy-MM-dd"));
			// Create a CellStyle with the font
			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			int rowNum = 1;

			// creating headers
			Row headerRow = sheet.createRow(0);
			Cell cell = headerRow.createCell(0);
			cell.setCellValue("Customer Name");
			cell.setCellStyle(headerCellStyle);

			Cell cell1 = headerRow.createCell(1);
			cell1.setCellValue("Account Number");
			cell1.setCellStyle(headerCellStyle);

			Cell cell3 = headerRow.createCell(2);
			cell3.setCellValue("Message");
			cell3.setCellStyle(headerCellStyle);

			Cell cell4 = headerRow.createCell(3);
			cell4.setCellValue("Mobile");
			cell4.setCellStyle(headerCellStyle);

			Cell cell5 = headerRow.createCell(4);

			cell5.setCellValue("Send Date");
			cell5.setCellStyle(headerCellStyle);
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			for (SMSHistory smsHistory : allHistory) {
				Row row = sheet.createRow(rowNum++);

				row.createCell(0).setCellValue(smsHistory.getCustomerName());
				row.createCell(1).setCellValue(smsHistory.getAccountNumber());
				row.createCell(2).setCellValue(smsHistory.getMessage());
				row.createCell(3).setCellValue(smsHistory.getMobile());
				row.createCell(4).setCellValue(dateFormat.format(smsHistory.getCreationDate()));
			}

			for (int i = 0; i < 5; i++) {
				sheet.autoSizeColumn(i);
			}
			fileOut = new FileOutputStream("SMS History.xls");
			workbook.write(response.getOutputStream());
			fileOut.close();
			workbook.close();
		} catch (

		Exception e) {
			e.printStackTrace();
		}
		return fileOut;
	}

	public static <T> FileOutputStream exportToExcelUsers(List<User> allUsers, HttpServletResponse response) {
		FileOutputStream fileOut = null;
		try {
			HSSFWorkbook workbook = new HSSFWorkbook();
			CreationHelper createHelper = workbook.getCreationHelper();
			Sheet sheet = workbook.createSheet();
			// Create a Font for styling header cells
			Font headerFont = workbook.createFont();
			headerFont.setFontHeightInPoints((short) 14);
			headerFont.setColor(IndexedColors.RED.getIndex());
			// Create Cell Style for formatting Date
			CellStyle dateCellStyle = workbook.createCellStyle();
			dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("yyyy-MM-dd"));
			// Create a CellStyle with the font
			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			int rowNum = 1;

			// creating headers
			Row headerRow = sheet.createRow(0);
			Cell cell = headerRow.createCell(0);
			cell.setCellValue("User Name");
			cell.setCellStyle(headerCellStyle);

			Cell cell1 = headerRow.createCell(1);
			cell1.setCellValue("Locked");
			cell1.setCellStyle(headerCellStyle);

			Cell cell3 = headerRow.createCell(2);
			cell3.setCellValue("Disabled");
			cell3.setCellStyle(headerCellStyle);

			Cell cell4 = headerRow.createCell(3);
			cell4.setCellValue("Created By User");
			cell4.setCellStyle(headerCellStyle);

			Cell cell5 = headerRow.createCell(4);
			cell5.setCellValue("Created Date");
			cell5.setCellStyle(headerCellStyle);

			Cell cell6 = headerRow.createCell(5);
			cell6.setCellValue("Status");
			cell6.setCellStyle(headerCellStyle);

			Cell cell7 = headerRow.createCell(6);
			cell7.setCellValue("Last Login Date");
			cell7.setCellStyle(headerCellStyle);

			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			for (User user : allUsers) {
				Row row = sheet.createRow(rowNum++);

				row.createCell(0).setCellValue(user.getUserName());
				row.createCell(1).setCellValue(user.getFlagLocked() == 1 ? "YES" : "NO");
				row.createCell(2).setCellValue(user.getFlagDisabled() == 1 ? "YES" : "NO");
				row.createCell(3).setCellValue(user.getCreatedByUserName());
				row.createCell(4).setCellValue(user.getCreatedDate());
				row.createCell(5).setCellValue(user.getApproveFlag() == 1 ? "Approved" : "Pending");
				row.createCell(6).setCellValue(user.getLastLoginDate());
			}

			for (int i = 0; i < 7; i++) {
				sheet.autoSizeColumn(i);
			}
			fileOut = new FileOutputStream("Users.xls");
			workbook.write(response.getOutputStream());
			fileOut.close();
			workbook.close();
		} catch (

		Exception e) {
			e.printStackTrace();
		}
		return fileOut;
	}

	public static <T> FileOutputStream exportToExcelUserRoles(List<UserRule> allUserRoles,
			HttpServletResponse response) {
		FileOutputStream fileOut = null;
		try {
			HSSFWorkbook workbook = new HSSFWorkbook();
			CreationHelper createHelper = workbook.getCreationHelper();
			Sheet sheet = workbook.createSheet();
			// Create a Font for styling header cells
			Font headerFont = workbook.createFont();
			headerFont.setFontHeightInPoints((short) 14);
			headerFont.setColor(IndexedColors.RED.getIndex());
			// Create Cell Style for formatting Date
			CellStyle dateCellStyle = workbook.createCellStyle();
			dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("yyyy-MM-dd"));
			// Create a CellStyle with the font
			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			int rowNum = 1;

			// creating headers
			Row headerRow = sheet.createRow(0);
			Cell cell = headerRow.createCell(0);
			cell.setCellValue("User Name");
			cell.setCellStyle(headerCellStyle);

			Cell cell1 = headerRow.createCell(1);
			cell1.setCellValue("Role Name");
			cell1.setCellStyle(headerCellStyle);

			Cell cell3 = headerRow.createCell(2);
			cell3.setCellValue("Created Date");
			cell3.setCellStyle(headerCellStyle);

			Cell cell4 = headerRow.createCell(3);
			cell4.setCellValue("Created By User");
			cell4.setCellStyle(headerCellStyle);

			Cell cell5 = headerRow.createCell(4);
			cell5.setCellValue("Status");
			cell5.setCellStyle(headerCellStyle);

			Cell cell6 = headerRow.createCell(5);
			cell6.setCellValue("Last Login Date");
			cell6.setCellStyle(headerCellStyle);

			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			for (UserRule userRule : allUserRoles) {
				Row row = sheet.createRow(rowNum++);

				row.createCell(0).setCellValue(userRule.getUserName());
				row.createCell(1).setCellValue(userRule.getRuleName());
				row.createCell(2).setCellValue(userRule.getCreatedDate());
				row.createCell(3).setCellValue(userRule.getCreatedByUserName());
				row.createCell(4).setCellValue(userRule.getApproveFlag() == 1 ? "Approved" : "Pending");
				row.createCell(5).setCellValue(userRule.getLastLoginDate());
			}

			for (int i = 0; i < 6; i++) {
				sheet.autoSizeColumn(i);
			}
			fileOut = new FileOutputStream("UserRoles.xls");
			workbook.write(response.getOutputStream());
			fileOut.close();
			workbook.close();
		} catch (

		Exception e) {
			e.printStackTrace();
		}
		return fileOut;
	}

	public static <T> FileOutputStream exportToExcelUserRoleAudits(List<UserRoleAudit> allUserRoleAudits,
			HttpServletResponse response) {
		FileOutputStream fileOut = null;
		try {
			HSSFWorkbook workbook = new HSSFWorkbook();
			CreationHelper createHelper = workbook.getCreationHelper();
			Sheet sheet = workbook.createSheet();
			// Create a Font for styling header cells
			Font headerFont = workbook.createFont();
			headerFont.setFontHeightInPoints((short) 14);
			headerFont.setColor(IndexedColors.RED.getIndex());
			// Create Cell Style for formatting Date
			CellStyle dateCellStyle = workbook.createCellStyle();
			dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("yyyy-MM-dd"));
			// Create a CellStyle with the font
			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			int rowNum = 1;

			// creating headers
			Row headerRow = sheet.createRow(0);
			Cell cell = headerRow.createCell(0);
			cell.setCellValue("Role Name");
			cell.setCellStyle(headerCellStyle);

			Cell cell1 = headerRow.createCell(1);
			cell1.setCellValue("User Name");
			cell1.setCellStyle(headerCellStyle);

			Cell cell10 = headerRow.createCell(2);
			cell10.setCellValue("Created Date");
			cell10.setCellStyle(headerCellStyle);

			Cell cell3 = headerRow.createCell(3);
			cell3.setCellValue("Created By User Name");
			cell3.setCellStyle(headerCellStyle);

			Cell cell4 = headerRow.createCell(4);
			cell4.setCellValue("Action");
			cell4.setCellStyle(headerCellStyle);

			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			for (UserRoleAudit userRoleAudit : allUserRoleAudits) {
				Row row = sheet.createRow(rowNum++);

				row.createCell(0).setCellValue(userRoleAudit.getRoleName());
				row.createCell(1).setCellValue(userRoleAudit.getUserName());
				row.createCell(2).setCellValue(dateFormat.format(userRoleAudit.getCreationDate()));
				row.createCell(3).setCellValue(userRoleAudit.getCreatedByUserName());
				row.createCell(4).setCellValue(userRoleAudit.getAction());
			}

			for (int i = 0; i < 5; i++) {
				sheet.autoSizeColumn(i);
			}
			fileOut = new FileOutputStream("SMS History.xls");
			workbook.write(response.getOutputStream());
			fileOut.close();
			workbook.close();
		} catch (

		Exception e) {
			e.printStackTrace();
		}
		return fileOut;
	}

	public static <T> FileOutputStream exportToExcelRoleAudits(List<RoleAudit> allRoleAudits,
			HttpServletResponse response) {
		FileOutputStream fileOut = null;
		try {
			HSSFWorkbook workbook = new HSSFWorkbook();
			CreationHelper createHelper = workbook.getCreationHelper();
			Sheet sheet = workbook.createSheet();
			// Create a Font for styling header cells
			Font headerFont = workbook.createFont();
			headerFont.setFontHeightInPoints((short) 14);
			headerFont.setColor(IndexedColors.RED.getIndex());
			// Create Cell Style for formatting Date
			CellStyle dateCellStyle = workbook.createCellStyle();
			dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("yyyy-MM-dd"));
			// Create a CellStyle with the font
			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			int rowNum = 1;

			// creating headers
			Row headerRow = sheet.createRow(0);
			Cell cell = headerRow.createCell(0);
			cell.setCellValue("Role Name");
			cell.setCellStyle(headerCellStyle);

			Cell cell1 = headerRow.createCell(1);
			cell1.setCellValue("Role Description");
			cell1.setCellStyle(headerCellStyle);

			Cell cell10 = headerRow.createCell(2);
			cell10.setCellValue("Insert Flag");
			cell10.setCellStyle(headerCellStyle);

			Cell cell3 = headerRow.createCell(3);
			cell3.setCellValue("Update Flag");
			cell3.setCellStyle(headerCellStyle);

			Cell cell4 = headerRow.createCell(4);
			cell4.setCellValue("Delete Flag");
			cell4.setCellStyle(headerCellStyle);

			Cell cell5 = headerRow.createCell(5);
			cell5.setCellValue("View Flag");
			cell5.setCellStyle(headerCellStyle);

			Cell cell6 = headerRow.createCell(6);
			cell6.setCellValue("User Creation Flag");
			cell6.setCellStyle(headerCellStyle);

			Cell cell7 = headerRow.createCell(7);
			cell7.setCellValue("Created By User Name");
			cell7.setCellStyle(headerCellStyle);

			Cell cell8 = headerRow.createCell(8);
			cell8.setCellValue("Created Date");
			cell8.setCellStyle(headerCellStyle);

			Cell cell9 = headerRow.createCell(9);
			cell9.setCellValue("Action");
			cell9.setCellStyle(headerCellStyle);

			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			for (RoleAudit roleAudit : allRoleAudits) {
				Row row = sheet.createRow(rowNum++);

				row.createCell(0).setCellValue(roleAudit.getRoleName());
				row.createCell(1).setCellValue(roleAudit.getRoleDescription());
				row.createCell(2).setCellValue(roleAudit.getInsertFlag() == 1 ? "YES" : "NO");
				row.createCell(3).setCellValue(roleAudit.getUpdateFlag() == 1 ? "YES" : "NO");
				row.createCell(4).setCellValue(roleAudit.getDeleteFlag() == 1 ? "YES" : "NO");
				row.createCell(5).setCellValue(roleAudit.getViewFlag() == 1 ? "YES" : "NO");
				row.createCell(6).setCellValue(roleAudit.getUserCreationFlag() == 1 ? "YES" : "NO");
				row.createCell(7).setCellValue(roleAudit.getUserName());
				row.createCell(8).setCellValue(dateFormat.format(roleAudit.getCreatedDate()));
				row.createCell(9).setCellValue(roleAudit.getAction());
			}

			for (int i = 0; i < 10; i++) {
				sheet.autoSizeColumn(i);
			}
			fileOut = new FileOutputStream("SMS History.xls");
			workbook.write(response.getOutputStream());
			fileOut.close();
			workbook.close();
		} catch (

		Exception e) {
			e.printStackTrace();
		}
		return fileOut;
	}

	public static <T> FileOutputStream exportToExcelScreenAudits(List<ScreenAudit> allScreenAudits,
			HttpServletResponse response) {
		FileOutputStream fileOut = null;
		try {
			HSSFWorkbook workbook = new HSSFWorkbook();
			CreationHelper createHelper = workbook.getCreationHelper();
			Sheet sheet = workbook.createSheet();
			// Create a Font for styling header cells
			Font headerFont = workbook.createFont();
			headerFont.setFontHeightInPoints((short) 14);
			headerFont.setColor(IndexedColors.RED.getIndex());
			// Create Cell Style for formatting Date
			CellStyle dateCellStyle = workbook.createCellStyle();
			dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("yyyy-MM-dd"));
			// Create a CellStyle with the font
			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			int rowNum = 1;

			// creating headers
			Row headerRow = sheet.createRow(0);
			Cell cell = headerRow.createCell(0);
			cell.setCellValue("Screen Name");
			cell.setCellStyle(headerCellStyle);

			Cell cell1 = headerRow.createCell(1);
			cell1.setCellValue("Screen Description");
			cell1.setCellStyle(headerCellStyle);

			Cell cell3 = headerRow.createCell(2);
			cell3.setCellValue("Application Name");
			cell3.setCellStyle(headerCellStyle);

			Cell cell4 = headerRow.createCell(3);
			cell4.setCellValue("Role Name");
			cell4.setCellStyle(headerCellStyle);

			Cell cell5 = headerRow.createCell(4);
			cell5.setCellValue("Created By User Name");
			cell5.setCellStyle(headerCellStyle);

			Cell cell6 = headerRow.createCell(5);
			cell6.setCellValue("Created Date");
			cell6.setCellStyle(headerCellStyle);

			Cell cell7 = headerRow.createCell(6);
			cell7.setCellValue("Action");
			cell7.setCellStyle(headerCellStyle);

			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			for (ScreenAudit screenAudit : allScreenAudits) {
				Row row = sheet.createRow(rowNum++);

				row.createCell(0).setCellValue(screenAudit.getScreenName());
				row.createCell(1).setCellValue(screenAudit.getScreenDescription());
				row.createCell(2).setCellValue(screenAudit.getApplicationName());
				row.createCell(3).setCellValue(screenAudit.getRoleName());
				row.createCell(4).setCellValue(screenAudit.getCreatedByUserName());
				row.createCell(5).setCellValue(dateFormat.format(screenAudit.getCreatedDate()));
				row.createCell(6).setCellValue(screenAudit.getAction());
			}

			for (int i = 0; i < 7; i++) {
				sheet.autoSizeColumn(i);
			}
			fileOut = new FileOutputStream("SMS History.xls");
			workbook.write(response.getOutputStream());
			fileOut.close();
			workbook.close();
		} catch (

		Exception e) {
			e.printStackTrace();
		}
		return fileOut;
	}

	public static <T> FileOutputStream exportToExcelUserAudits(List<UserAudit> allUserAudits,
			HttpServletResponse response) {
		FileOutputStream fileOut = null;
		try {
			HSSFWorkbook workbook = new HSSFWorkbook();
			CreationHelper createHelper = workbook.getCreationHelper();
			Sheet sheet = workbook.createSheet();
			// Create a Font for styling header cells
			Font headerFont = workbook.createFont();
			headerFont.setFontHeightInPoints((short) 14);
			headerFont.setColor(IndexedColors.RED.getIndex());
			// Create Cell Style for formatting Date
			CellStyle dateCellStyle = workbook.createCellStyle();
			dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("yyyy-MM-dd"));
			// Create a CellStyle with the font
			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			int rowNum = 1;

			// creating headers
			Row headerRow = sheet.createRow(0);
			Cell cell = headerRow.createCell(0);
			cell.setCellValue("User Name");
			cell.setCellStyle(headerCellStyle);

			Cell cell1 = headerRow.createCell(1);
			cell1.setCellValue("Flag Locked");
			cell1.setCellStyle(headerCellStyle);

			Cell cell3 = headerRow.createCell(2);
			cell3.setCellValue("Flag Disable");
			cell3.setCellStyle(headerCellStyle);

			Cell cell4 = headerRow.createCell(3);
			cell4.setCellValue("Action");
			cell4.setCellStyle(headerCellStyle);

			Cell cell5 = headerRow.createCell(4);
			cell5.setCellValue("Creation Date");
			cell5.setCellStyle(headerCellStyle);

			Cell cell6 = headerRow.createCell(5);
			cell6.setCellValue("Created By User Name");
			cell6.setCellStyle(headerCellStyle);

			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			for (UserAudit userAudit : allUserAudits) {
				Row row = sheet.createRow(rowNum++);

				row.createCell(0).setCellValue(userAudit.getUserName());
				row.createCell(1).setCellValue(userAudit.getFlagLocked() == 1 ? "YES" : "NO");
				row.createCell(2).setCellValue(userAudit.getFlagDisable() == 1 ? "YES" : "NO");
				row.createCell(3).setCellValue(userAudit.getAction());
				row.createCell(4).setCellValue(dateFormat.format(userAudit.getCreatedDate()));
				row.createCell(5).setCellValue(userAudit.getCreatedByUserName());
			}

			for (int i = 0; i < 6; i++) {
				sheet.autoSizeColumn(i);
			}
			fileOut = new FileOutputStream("SMS History.xls");
			workbook.write(response.getOutputStream());
			fileOut.close();
			workbook.close();
		} catch (

		Exception e) {
			e.printStackTrace();
		}
		return fileOut;
	}

	public static <T> FileOutputStream exportToExcel(List<T> objects, HttpServletResponse response) {
		FileOutputStream fileOut = null;
		try {
			HSSFWorkbook workbook = new HSSFWorkbook();
			CreationHelper createHelper = workbook.getCreationHelper();
			Sheet sheet = workbook.createSheet();
			// Create a Font for styling header cells
			Font headerFont = workbook.createFont();
			headerFont.setFontHeightInPoints((short) 14);
			headerFont.setColor(IndexedColors.RED.getIndex());
			// Create Cell Style for formatting Date
			CellStyle dateCellStyle = workbook.createCellStyle();
			dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));
			// Create a CellStyle with the font
			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			int rowNum = 1;
			for (Object object : objects) {
				Field[] fields = object.getClass().getDeclaredFields();
				Row headerRow = sheet.createRow(0);
				for (int i = 0; i < fields.length; i++) {
					if (i == 11 || i == 12 || i == 13 || i == 14 || i == 16 || i == 17 || i == 18 || i == 19 || i == 20
							|| i == 24)
						continue;
					Cell cell = null;
					if (i <= 10) {
						cell = headerRow.createCell(i);
					} else if (i == 15) {
						cell = headerRow.createCell(11);
					} else if (i == 21) {
						cell = headerRow.createCell(12);
					} else if (i == 22) {
						cell = headerRow.createCell(13);
					} else if (i == 23) {
						cell = headerRow.createCell(14);
					} else if (i == 25) {
						cell = headerRow.createCell(15);
					}

					switch (fields[i].getName()) {
					case "customerCode":
						cell.setCellValue("Customer Code");
						cell.setCellStyle(headerCellStyle);
						break;
					case "accountNumber":
						cell.setCellValue("Account Number");
						cell.setCellStyle(headerCellStyle);
						break;
					case "frequancyStmt":
						cell.setCellValue("Statement Frequency");
						cell.setCellStyle(headerCellStyle);
						break;
					case "email":
						cell.setCellValue("Email");
						cell.setCellStyle(headerCellStyle);
						break;
					case "nextDate":
						cell.setCellValue("Next statement Date");
						cell.setCellStyle(headerCellStyle);
						break;
					case "description":
						cell.setCellValue("Description");
						cell.setCellStyle(headerCellStyle);
						break;
					case "lastSentDate":
						cell.setCellValue("Last Sent Date");
						cell.setCellStyle(headerCellStyle);
						break;
					case "noOfFaildAttemps":
						cell.setCellValue("Number of Failed Attemps");
						cell.setCellStyle(headerCellStyle);
						break;
					case "password":
						cell.setCellValue("Password");
						cell.setCellStyle(headerCellStyle);
						break;
					case "status":
						cell.setCellValue("Status");
						cell.setCellStyle(headerCellStyle);
						break;
					case "mobile":
						cell.setCellValue("Mobile");
						cell.setCellStyle(headerCellStyle);
						break;
					case "customerName":
						cell.setCellValue("Customer Name");
						cell.setCellStyle(headerCellStyle);
						break;
					case "isApproved":
						cell.setCellValue("Approved");
						cell.setCellStyle(headerCellStyle);
						break;
					case "smsFlag":
						cell.setCellValue("SMS");
						cell.setCellStyle(headerCellStyle);
						break;
					case "estatementFlag":
						cell.setCellValue("Mail");
						cell.setCellStyle(headerCellStyle);
						break;

					}
//					cell.setCellValue(fields[i].getName());
//					cell.setCellStyle(headerCellStyle);
				}

				/*
				 * from 11 to 14
				 *
				 * from 16 to 20 and 24
				 */
				Row row = sheet.createRow(rowNum++);

				for (int i = 0; i < fields.length; i++) {
					if (i == 11 || i == 12 || i == 13 || i == 14 || i == 16 || i == 17 || i == 18 || i == 19 || i == 20
							|| i == 24)
						continue;
					Field field = fields[i];
					field.setAccessible(true);
					if (i <= 10) {
						row.createCell(i).setCellValue(String.valueOf(field.get(object)));
					} else if (i == 15) {
						row.createCell(11).setCellValue(String.valueOf(field.get(object)));
					} else if (i == 21) {
						row.createCell(12).setCellValue(String.valueOf(field.get(object)));
					} else if (i == 22) {
						row.createCell(13).setCellValue(String.valueOf(field.get(object)));
					} else if (i == 23) {
						row.createCell(14).setCellValue(String.valueOf(field.get(object)));
					} else if (i == 25) {
						row.createCell(15).setCellValue(String.valueOf(field.get(object)));
					}

				}

				for (int i = 0; i < fields.length; i++) {
					if (i == 11 || i == 12 || i == 13 || i == 14 || i == 16 || i == 17 || i == 18 || i == 19 || i == 20
							|| i == 24)
						continue;
					if (i <= 10) {
						sheet.autoSizeColumn(i);
					} else if (i == 15) {
						sheet.autoSizeColumn(11);
					} else if (i == 21) {
						sheet.autoSizeColumn(12);
					} else if (i == 22) {
						sheet.autoSizeColumn(13);
					} else if (i == 23) {
						sheet.autoSizeColumn(14);
					} else if (i == 25) {
						sheet.autoSizeColumn(15);
					}

				}
			}
			fileOut = new FileOutputStream("report.xls");
			workbook.write(response.getOutputStream());
			fileOut.close();
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fileOut;
	}

	public static <T> FileOutputStream exportToExcel_mainAccount(List<Account> accounts, HttpServletResponse response) {
		FileOutputStream fileOut = null;
		try {
			HSSFWorkbook workbook = new HSSFWorkbook();
			CreationHelper createHelper = workbook.getCreationHelper();
			Sheet sheet = workbook.createSheet();
			// Create a Font for styling header cells
			Font headerFont = workbook.createFont();
			headerFont.setFontHeightInPoints((short) 14);
			headerFont.setColor(IndexedColors.RED.getIndex());
			// Create Cell Style for formatting Date
			CellStyle dateCellStyle = workbook.createCellStyle();
			dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));
			// Create a CellStyle with the font
			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			int rowNum = 1;

			// creating headers
			Row headerRow = sheet.createRow(0);
			Cell cell = headerRow.createCell(0);
			cell.setCellValue("Customer Code");
			cell.setCellStyle(headerCellStyle);

			Cell cell1 = headerRow.createCell(1);
			cell1.setCellValue("Account Number");
			cell1.setCellStyle(headerCellStyle);

			Cell cell3 = headerRow.createCell(2);
			cell3.setCellValue("Customer Name");
			cell3.setCellStyle(headerCellStyle);

			Cell cell4 = headerRow.createCell(3);
			cell4.setCellValue("Estatement (YES / NO)");
			cell4.setCellStyle(headerCellStyle);

			Cell cell5 = headerRow.createCell(4);
			cell5.setCellValue("Statement Frequency");
			cell5.setCellStyle(headerCellStyle);

			Cell cell6 = headerRow.createCell(5);
			cell6.setCellValue("Email");
			cell6.setCellStyle(headerCellStyle);

			Cell cell7 = headerRow.createCell(6);
			cell7.setCellValue("Next statement Date");
			cell7.setCellStyle(headerCellStyle);

			Cell cell8 = headerRow.createCell(7);
			cell8.setCellValue("Last Sent Date");
			cell8.setCellStyle(headerCellStyle);

//			Cell cell9 = headerRow.createCell(8);
//			cell9.setCellValue("Number of Failed Attemps");
//			cell9.setCellStyle(headerCellStyle);

			Cell cell10 = headerRow.createCell(8);
			cell10.setCellValue("SMS");
			cell10.setCellStyle(headerCellStyle);

			Cell cell11 = headerRow.createCell(9);
			cell11.setCellValue("Mobile");
			cell11.setCellStyle(headerCellStyle);

			Cell cell12 = headerRow.createCell(10);
			cell12.setCellValue("Status");
			cell12.setCellStyle(headerCellStyle);

			Cell cell13 = headerRow.createCell(11);
			cell13.setCellValue("Creation Date");
			cell13.setCellStyle(headerCellStyle);
//			HSSFDataFormat format = workbook.createDataFormat();
			for (Account account : accounts) {
				Row row = sheet.createRow(rowNum++);

				row.createCell(0).setCellValue(account.getCustomerCode());
				row.createCell(1).setCellValue(account.getAccountNumber());
				row.createCell(2).setCellValue(account.getCustomerName());
				row.createCell(3).setCellValue(account.getEstatementFlag() == 1 ? "YES" : "NO");

				row.createCell(4).setCellValue(Defines.findFrequencyString(account.getFrequancyStmt()));

				row.createCell(5).setCellValue(account.getEmail());

				row.createCell(6).setCellValue(account.getNextDate() == null ? "" : account.getNextDate().toString());
//				short dateFormat = createHelper.createDataFormat().getFormat("yyyy-dd-MM");
//				row.getCell(6).getCellStyle().setDataFormat(dateFormat);
				row.createCell(7)
						.setCellValue(account.getLastSentDate() == null ? "" : account.getLastSentDate().toString());
				// row.createCell(8).setCellValue(account.getFailureMail());
				row.createCell(8).setCellValue(account.getSmsFlag() == 1 ? "YES" : "NO");
				row.createCell(9).setCellValue(account.getMobile());
				row.createCell(10).setCellValue(account.getStatus());
				row.createCell(11)
						.setCellValue(account.getCreationDate() == null ? "" : account.getCreationDate().toString());

			}

			for (int i = 0; i < 12; i++) {
				sheet.autoSizeColumn(i);
			}
			fileOut = new FileOutputStream("report.xls");
			workbook.write(response.getOutputStream());
			fileOut.close();
			workbook.close();
		} catch (

		Exception e) {
			e.printStackTrace();
		}
		return fileOut;
	}

	public static <T> FileOutputStream exportToExcel_mainAccountAudits(List<AccountAudit> accountAudits,
			HttpServletResponse response) {
		FileOutputStream fileOut = null;
		try {
			HSSFWorkbook workbook = new HSSFWorkbook();
			CreationHelper createHelper = workbook.getCreationHelper();
			Sheet sheet = workbook.createSheet();
			// Create a Font for styling header cells
			Font headerFont = workbook.createFont();
			headerFont.setFontHeightInPoints((short) 14);
			headerFont.setColor(IndexedColors.RED.getIndex());
			// Create Cell Style for formatting Date
			CellStyle dateCellStyle = workbook.createCellStyle();
			dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));
			// Create a CellStyle with the font
			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			int rowNum = 1;

			// creating headers
			Row headerRow = sheet.createRow(0);
			Cell cell = headerRow.createCell(0);
			cell.setCellValue("Customer Code");
			cell.setCellStyle(headerCellStyle);

			Cell cell1 = headerRow.createCell(1);
			cell1.setCellValue("Account Number");
			cell1.setCellStyle(headerCellStyle);

			Cell cell3 = headerRow.createCell(2);
			cell3.setCellValue("Customer Name");
			cell3.setCellStyle(headerCellStyle);

			Cell cell4 = headerRow.createCell(3);
			cell4.setCellValue("Email");
			cell4.setCellStyle(headerCellStyle);

			Cell cell5 = headerRow.createCell(4);
			cell5.setCellValue("Branch");
			cell5.setCellStyle(headerCellStyle);

			Cell cell6 = headerRow.createCell(5);
			cell6.setCellValue("Next statement Date");
			cell6.setCellStyle(headerCellStyle);

			Cell cell7 = headerRow.createCell(6);
			cell7.setCellValue("Mobile");
			cell7.setCellStyle(headerCellStyle);

			Cell cell8 = headerRow.createCell(7);
			cell8.setCellValue("Statement Frequency");
			cell8.setCellStyle(headerCellStyle);

			Cell cell10 = headerRow.createCell(8);
			cell10.setCellValue("Created By");
			cell10.setCellStyle(headerCellStyle);

			Cell cell11 = headerRow.createCell(9);
			cell11.setCellValue("Created Date");
			cell11.setCellStyle(headerCellStyle);

			Cell cell12 = headerRow.createCell(10);
			cell12.setCellValue("Action");
			cell12.setCellStyle(headerCellStyle);

			for (AccountAudit accountAudit : accountAudits) {
				Row row = sheet.createRow(rowNum++);

				row.createCell(0).setCellValue(accountAudit.getCustomerCode());
				row.createCell(1).setCellValue(accountAudit.getAccountNumber());
				row.createCell(2).setCellValue(accountAudit.getCustomerName());
				row.createCell(3).setCellValue(accountAudit.getEmail() != null ? accountAudit.getEmail() : "");
				row.createCell(4).setCellValue(accountAudit.getBranchName());
				row.createCell(5).setCellValue(accountAudit.getNextDate() != null ? accountAudit.getNextDate() : "");
				row.createCell(6).setCellValue(accountAudit.getMobile() != null ? accountAudit.getMobile() : "");
				row.createCell(7).setCellValue(Defines.findFrequencyString(accountAudit.getFrequancyStmt()));
				row.createCell(8).setCellValue(accountAudit.getCreatedBy());
				row.createCell(9).setCellValue(accountAudit.getCreatedDate());
				row.createCell(10).setCellValue(accountAudit.getAction());
			}

			for (int i = 0; i < 10; i++) {
				sheet.autoSizeColumn(i);
			}
			fileOut = new FileOutputStream("report.xls");
			workbook.write(response.getOutputStream());
			fileOut.close();
			workbook.close();
		} catch (

		Exception e) {
			e.printStackTrace();
		}
		return fileOut;
	}

	public static <T> FileOutputStream exportToExcel(List<T> objects, HttpServletResponse response, String tableName) {
		FileOutputStream fileOut = null;
		try {
			HSSFWorkbook workbook = new HSSFWorkbook();
			CreationHelper createHelper = workbook.getCreationHelper();
			Sheet sheet = workbook.createSheet();
			// Create a Font for styling header cells
			Font headerFont = workbook.createFont();
			headerFont.setFontHeightInPoints((short) 14);
			headerFont.setColor(IndexedColors.RED.getIndex());
			// Create Cell Style for formatting Date
			CellStyle dateCellStyle = workbook.createCellStyle();
			dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));
			// Create a CellStyle with the font
			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			int rowNum = 1;
			for (Object object : objects) {
				Field[] fields = object.getClass().getDeclaredFields();
				Row headerRow = sheet.createRow(0);
				for (int i = 0; i < fields.length; i++) {
					if (i == 5 || i == 6 || i == 7 || i == 8 || i == 9 || i == 11 || i == 12 || i == 14 || i == 16
							|| i == 17 || i == 18)
						continue;
					Cell cell = null;
					if (i <= 4) {
						cell = headerRow.createCell(i);
					} else if (i == 10) {
						cell = headerRow.createCell(5);
					} else if (i == 13) {
						cell = headerRow.createCell(6);
					} else if (i == 15) {
						cell = headerRow.createCell(7);
					} else if (i == 19) {
						cell = headerRow.createCell(8);
					} else if (i == 20) {
						cell = headerRow.createCell(9);
					} else if (i == 21) {
						cell = headerRow.createCell(10);
					}

					switch (fields[i].getName()) {
					case "customerCode":
						cell.setCellValue("Customer Code");
						cell.setCellStyle(headerCellStyle);
						break;
					case "accountNumber":
						cell.setCellValue("Account Number");
						cell.setCellStyle(headerCellStyle);
						break;
					case "frequancyStmt":

						cell.setCellValue("Statement Frequency");
						cell.setCellStyle(headerCellStyle);
						break;
					case "email":
						cell.setCellValue("Email");
						cell.setCellStyle(headerCellStyle);
						break;
					case "nextDate":
						cell.setCellValue("Next statement Date");
						cell.setCellStyle(headerCellStyle);
						break;
					case "mobile":
						cell.setCellValue("Mobile");
						cell.setCellStyle(headerCellStyle);
						break;
					case "customerName":
						cell.setCellValue("Customer Name");
						cell.setCellStyle(headerCellStyle);
						break;
					case "branchName":
						cell.setCellValue("Branch Name");
						cell.setCellStyle(headerCellStyle);
						break;
					case "createdBy":
						cell.setCellValue("Created By");
						cell.setCellStyle(headerCellStyle);
						break;
					case "createdDate":
						cell.setCellValue("Created Date");
						cell.setCellStyle(headerCellStyle);
						break;
					case "action":
						cell.setCellValue("Action");
						cell.setCellStyle(headerCellStyle);
						break;

					}
				}

				Row row = sheet.createRow(rowNum++);

				for (int i = 0; i < fields.length; i++) {
					if (i == 5 || i == 6 || i == 7 || i == 8 || i == 9 || i == 11 || i == 12 || i == 14 || i == 16
							|| i == 17 || i == 18)
						continue;
					Field field = fields[i];
					field.setAccessible(true);
					if (i <= 4) {
						if (field.get(object) != null)
							row.createCell(i).setCellValue(String.valueOf(field.get(object)));
						else
							row.createCell(i).setCellValue("");
					} else if (i == 10) {
						if (field.get(object) != null)
							row.createCell(5).setCellValue(String.valueOf(field.get(object)));
						else
							row.createCell(i).setCellValue("");
					} else if (i == 13) {
						if (field.get(object) != null)
							row.createCell(6).setCellValue(String.valueOf(field.get(object)));
						else
							row.createCell(i).setCellValue("");
					} else if (i == 15) {
						if (field.get(object) != null)
							row.createCell(7).setCellValue(String.valueOf(field.get(object)));
						else
							row.createCell(i).setCellValue("");
					} else if (i == 19) {
						if (field.get(object) != null)
							row.createCell(8).setCellValue(String.valueOf(field.get(object)));
						else
							row.createCell(i).setCellValue("");
					} else if (i == 20) {
						if (field.get(object) != null)
							row.createCell(9).setCellValue(String.valueOf(field.get(object)));
						else
							row.createCell(i).setCellValue("");
					} else if (i == 21) {
						if (field.get(object) != null)
							row.createCell(10).setCellValue(String.valueOf(field.get(object)));
						else
							row.createCell(i).setCellValue("");
					}

					if (i == 2) {
						int frequancyStmt = Integer.valueOf(String.valueOf(field.get(object)));
						String freqString;
						switch (frequancyStmt) {
						case Defines.Daily:
							freqString = "Daily";
							break;
						case Defines.Weekly:
							freqString = "Weekly";
							break;
//							case Defines.FortNightly :
//								freqString = "Fort Nightly";
//								break;
						case Defines.Monthly:
							freqString = "Monthly";
							break;
//							case Defines.Bi_Monthly :
//								freqString = "Bi_Monthly";
//								break;
						case Defines.Quarterly:
							freqString = "Quarterly";
							break;
						case Defines.Half_Yearly:
							freqString = "Semi-Annually";
							break;
						case Defines.Yearly:
							freqString = "Annually";
							break;
						default:
							freqString = "None";
							break;
						}
						if (field.get(object) != null)
							row.createCell(i).setCellValue(freqString);
						else
							row.createCell(i).setCellValue("");
					}
				}

				for (int i = 0; i < fields.length; i++) {
					if (i == 11 || i == 12 || i == 13 || i == 14 || i == 16 || i == 17 || i == 18 || i == 19 || i == 20
							|| i == 24)
						continue;
					if (i <= 4) {
						sheet.autoSizeColumn(i);
					} else if (i == 10) {
						sheet.autoSizeColumn(5);
					} else if (i == 13) {
						sheet.autoSizeColumn(6);
					} else if (i == 15) {
						sheet.autoSizeColumn(7);
					} else if (i == 19) {
						sheet.autoSizeColumn(8);
					} else if (i == 20) {
						sheet.autoSizeColumn(9);
					} else if (i == 21) {
						sheet.autoSizeColumn(10);
					}
				}
			}
			fileOut = new FileOutputStream("report.xls");
			workbook.write(response.getOutputStream());
			fileOut.close();
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fileOut;
	}

}
