package com.bdc.estatement.service.Impl;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import com.bdc.estatement.dao.TransactionsDao;
import com.bdc.estatement.model.Account;
import com.bdc.estatement.model.EstatementHistory;
import com.bdc.estatement.service.TranscationService;
import com.bdc.usermanagement.ldab.AccountAudit;
import com.bdc.usermanagement.ldab.UserManagmentDBTransactions;

public class TransactionServiceImpl implements TranscationService {

	List<Account> accountsList = new ArrayList<Account>();
	List<Account> newRequestsList = TransactionsDao.getAccountTypeList();
	List<EstatementHistory> estamentHistories = TransactionsDao.getEstatementHistory();
	UserManagmentDBTransactions dbTransactions = new UserManagmentDBTransactions();
	List<AccountAudit> accountsAuditList = dbTransactions.findAllAccountAudits();

	@Override
	public List<Account> getAllNewRequests() {
		return TransactionsDao.getAccountTypeList();
	}

	@Override
	public List<Account> getAllAccounts() {
		return TransactionsDao.getAccounts();
	}

	@Override
	public List<EstatementHistory> getAllEstamentHistories() {
		return TransactionsDao.getEstatementHistory();
	}

	@Override
	public List<AccountAudit> getAllAccountAudit() {
		return dbTransactions.findAllAccountAudits();
	}

	@Override
	public List<Account> searchAccountsByName(String searchParam) {
		accountsList = getAllAccounts();
		Comparator<Account> groupByComparator = Comparator.comparing(Account::getCustomerName);
		List<Account> result = accountsList.stream().filter(e -> e.getCustomerName() != null)
				.filter(e -> e.getCustomerName().toUpperCase().startsWith(searchParam.toUpperCase()))
				.sorted(groupByComparator).collect(Collectors.toList());
		return result;
	}

	public List<Account> searchAccountsById(String searchParam) {
		accountsList = getAllAccounts();
		Comparator<Account> groupByComparator = Comparator.comparing(Account::getCustomerCode);
		List<Account> result = accountsList.stream().filter(e -> e.getCustomerCode() != null)
				.filter(e -> e.getCustomerCode().equalsIgnoreCase(searchParam)).sorted(groupByComparator)
				.collect(Collectors.toList());
		return result;
	}

	public List<Account> searchAccountsByAccountNo(String searchParam) {
		accountsList = getAllAccounts();
		Comparator<Account> groupByComparator = Comparator.comparing(Account::getAccountNumber);
		List<Account> result = accountsList.stream().filter(e -> e.getAccountNumber() != null)
				.filter(e -> e.getAccountNumber().trim().equalsIgnoreCase(searchParam)).sorted(groupByComparator)
				.collect(Collectors.toList());
		return result;
	}

	public List<Account> searchAccountByStatus(String searchParam) {
		accountsList = getAllAccounts();
		Comparator<Account> groupByComparator = Comparator.comparing(Account::getAccountNumber);
		List<Account> result = accountsList.stream().filter(e -> e.getIsApproved() == Integer.valueOf(searchParam))
				.sorted(groupByComparator).collect(Collectors.toList());
		return result;
	}
	public List<Account> searchAccountByFrequency(String searchParam) {
		// newRequestsList = getAllNewRequests();
		accountsList = getAllAccounts();
		Comparator<Account> groupByComparator = Comparator.comparing(Account::getFrequancyStmt);
		List<Account> result = accountsList.stream().filter(e -> e.getFrequancyStmt() != 0)
				.filter(e -> Integer.toString(e.getFrequancyStmt()).trim().equalsIgnoreCase(searchParam))
				.sorted(groupByComparator).collect(Collectors.toList());
		return result;
	}
	

	public List<Account> searchNewRequestsByName(String searchParam) {
		// newRequestsList = getAllNewRequests();
		Comparator<Account> groupByComparator = Comparator.comparing(Account::getCustomerName);
		List<Account> result = newRequestsList.stream().filter(e -> e.getCustomerName() != null)
				.filter(e -> e.getCustomerName().toUpperCase().startsWith(searchParam.toUpperCase()))
				.sorted(groupByComparator).collect(Collectors.toList());
		return result;
	}

	public List<Account> searchNewRequestsById(String searchParam) {
		// newRequestsList = getAllNewRequests();
		Comparator<Account> groupByComparator = Comparator.comparing(Account::getCustomerCode);
		List<Account> result = newRequestsList.stream().filter(e -> e.getCustomerCode() != null)
				.filter(e -> e.getCustomerCode().equalsIgnoreCase(searchParam)).sorted(groupByComparator)
				.collect(Collectors.toList());
		return result;
	}

	public List<Account> searchNewRequestsByAccountNo(String searchParam) {
		// newRequestsList = getAllNewRequests();
		Comparator<Account> groupByComparator = Comparator.comparing(Account::getAccountNumber);
		List<Account> result = newRequestsList.stream().filter(e -> e.getAccountNumber() != null)
				.filter(e -> e.getAccountNumber().trim().equalsIgnoreCase(searchParam)).sorted(groupByComparator)
				.collect(Collectors.toList());
		return result;
	}

	public List<EstatementHistory> searchEstatementHistoryByName(String searchParam) {
		// newRequestsList = getAllNewRequests();
		Comparator<EstatementHistory> groupByComparator = Comparator.comparing(EstatementHistory::getCustomerName);
		List<EstatementHistory> result = estamentHistories.stream().filter(e -> e.getCustomerName() != null)
				.filter(e -> e.getCustomerName().toUpperCase().startsWith(searchParam.toUpperCase()))
				.sorted(groupByComparator).collect(Collectors.toList());
		return result;
	}

	public List<EstatementHistory> searchEstatementHistoryById(String searchParam) {
		// newRequestsList = getAllNewRequests();
		Comparator<EstatementHistory> groupByComparator = Comparator.comparing(EstatementHistory::getCustomerCode);
		List<EstatementHistory> result = estamentHistories.stream().filter(e -> e.getCustomerCode() != null)
				.filter(e -> e.getCustomerCode().equalsIgnoreCase(searchParam)).sorted(groupByComparator)
				.collect(Collectors.toList());
		return result;
	}

	public List<EstatementHistory> searchEstatementHistoryByAccountNo(String searchParam) {
		// newRequestsList = getAllNewRequests();
		Comparator<EstatementHistory> groupByComparator = Comparator.comparing(EstatementHistory::getAccountNo);
		List<EstatementHistory> result = estamentHistories.stream().filter(e -> e.getAccountNo() != null)
				.filter(e -> e.getAccountNo().trim().equalsIgnoreCase(searchParam)).sorted(groupByComparator)
				.collect(Collectors.toList());
		return result;
	}

	public List<EstatementHistory> searchEstatementHistoryByFrequency(String searchParam) {
		// newRequestsList = getAllNewRequests();
		Comparator<EstatementHistory> groupByComparator = Comparator.comparing(EstatementHistory::getFreqStmt);
		List<EstatementHistory> result = estamentHistories.stream().filter(e -> e.getFreqStmt() != 0)
				.filter(e -> Integer.toString(e.getFreqStmt()).trim().equalsIgnoreCase(searchParam))
				.sorted(groupByComparator).collect(Collectors.toList());
		return result;
	}
	
	public List<AccountAudit> searchAccountAuditsByName(String searchParam) {
		// newRequestsList = getAllNewRequests();
		Comparator<AccountAudit> groupByComparator = Comparator.comparing(AccountAudit::getCustomerName);
		List<AccountAudit> result = accountsAuditList.stream().filter(e -> e.getCustomerName() != null)
				.filter(e -> e.getCustomerName().toUpperCase().startsWith(searchParam.toUpperCase()))
				.sorted(groupByComparator).collect(Collectors.toList());
		return result;
	}

	public List<AccountAudit> searchAccountAuditsById(String searchParam) {
		// newRequestsList = getAllNewRequests();
		Comparator<AccountAudit> groupByComparator = Comparator.comparing(AccountAudit::getCustomerCode);
		List<AccountAudit> result = accountsAuditList.stream().filter(e -> e.getCustomerCode() != null)
				.filter(e -> e.getCustomerCode().equalsIgnoreCase(searchParam)).sorted(groupByComparator)
				.collect(Collectors.toList());
		return result;
	}

	public List<AccountAudit> searchAccountAuditsByAccountNo(String searchParam) {
		// newRequestsList = getAllNewRequests();
		Comparator<AccountAudit> groupByComparator = Comparator.comparing(AccountAudit::getAccountNumber);
		List<AccountAudit> result = accountsAuditList.stream().filter(e -> e.getAccountNumber() != null)
				.filter(e -> e.getAccountNumber().trim().equalsIgnoreCase(searchParam)).sorted(groupByComparator)
				.collect(Collectors.toList());
		return result;
	}

	public List<AccountAudit> searchAccountAuditsByFrequency(String searchParam) {
		// newRequestsList = getAllNewRequests();
		Comparator<AccountAudit> groupByComparator = Comparator.comparing(AccountAudit::getFrequancyStmt);
		List<AccountAudit> result = accountsAuditList.stream().filter(e -> e.getFrequancyStmt() != 0)
				.filter(e -> Integer.toString(e.getFrequancyStmt()).trim().equalsIgnoreCase(searchParam))
				.sorted(groupByComparator).collect(Collectors.toList());
		return result;
	}

}
