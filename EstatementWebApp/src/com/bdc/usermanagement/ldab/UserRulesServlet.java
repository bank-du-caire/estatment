package com.bdc.usermanagement.ldab;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bdc.estatement.util.Utils;

/**
 * Servlet implementation class UserRulesServlet
 */
@WebServlet("/UserRulesServlet")
public class UserRulesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("USER_ID") == null || session.getAttribute("USER_ID").equals("")
				|| session.getAttribute("USER_ID").toString().isEmpty()) {
			response.sendRedirect(request.getContextPath() + "/main/pages/tables/Login.jsp");
			return;
		} else if (request.getParameter("export") != null) {
			// export
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename=User Roles.xls");
			UserManagmentDBTransactions dbQueries = new UserManagmentDBTransactions();
			Utils.exportToExcelUserRoles(dbQueries.findAllUserRules(), response);
			return;
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("USER_ID") == null || session.getAttribute("USER_ID").equals("")
				|| session.getAttribute("USER_ID").toString().isEmpty()) {
			response.sendRedirect(request.getContextPath() + "/main/pages/tables/Login.jsp");
			return;
		} else
			doAction(request, response);
	}

	protected void doAction(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		UserManagmentDBTransactions dbTransactions = new UserManagmentDBTransactions();
		int status = 0;
		UserRule userRule = new UserRule();

		if (request.getParameter("approveUserIDHid") != null && !request.getParameter("approveUserIDHid").isEmpty()
				&& request.getParameter("approveRoleIDHid") != null
				&& !request.getParameter("approveRoleIDHid").isEmpty()) {

			int userID = Integer.parseInt(request.getParameter("approveUserIDHid").toString());
			int roleID = Integer.parseInt(request.getParameter("approveRoleIDHid").toString());

			int approvedByUserID = Integer.parseInt(session.getAttribute("USER_ID").toString());
			int status_ = dbTransactions.approveUserRole(userID, roleID, approvedByUserID, 1);
			System.out.println("status [" + status_ + "]");
			response.sendRedirect(request.getContextPath() + "/main/pages/tables/UserManagement.jsp");
			return;
		}
		if (request.getParameter("userRuleDeleteFlag") != null
				&& !request.getParameter("userRuleDeleteFlag").toString().isEmpty()
				&& request.getParameter("userRuleDeleteFlag").toString().equals("1")) {

			// same user cannot delete his roles
			int ur_userIDHid = request.getParameter("ur_userIDHid") != null
					? Integer.valueOf(request.getParameter("ur_userIDHid").toString())
					: 0;
			int loggedInUserId = session.getAttribute("USER_ID") != null
					? Integer.valueOf(session.getAttribute("USER_ID").toString())
					: 0;
			if (session.getAttribute("USER_ID") != null && request.getParameter("ur_userIDHid") != null
					&& loggedInUserId == ur_userIDHid) {
				session.setAttribute("userRuleResult", "Sorry you Cannot Delete your Own Roles");
				response.sendRedirect(request.getContextPath() + "/main/pages/tables/UserManagement.jsp");
				return;
			}
			dbTransactions.editUserRoleWithoutTrigger(Integer.valueOf(request.getParameter("ur_userIDHid").toString()),
					Integer.valueOf(request.getParameter("ur_ruleIDHid").toString()),
					Integer.parseInt(session.getAttribute("USER_ID").toString()));

			status = dbTransactions.deleteUserRule(Integer.valueOf(request.getParameter("ur_userIDHid").toString()),
					Integer.valueOf(request.getParameter("ur_ruleIDHid").toString()));
			request.setAttribute("userRuleDeleteFlag", "0");
			if (status > 0) {
				session.setAttribute("userRuleResult", "Role Deleted Successfully");
				response.sendRedirect(request.getContextPath() + "/main/pages/tables/UserManagement.jsp");
				return;
			} else {
				session.setAttribute("userRuleResult", "Rule Did not Deleted Successfully");
				response.sendRedirect(request.getContextPath() + "/main/pages/tables/UserManagement.jsp");
				return;
			}
		}
		userRule.setUserID(Integer.valueOf(request.getParameter("userList").toString()));
		String[] roles = request.getParameterValues("ruleList");
		for (int i = 0; i < roles.length; i++) {
			System.out.println(roles[i]);
			userRule.setRuleID(Integer.valueOf(roles[i].toString()));
			userRule.setCreatedByUserID(Integer.valueOf(session.getAttribute("USER_ID").toString()));
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy-HH:mm:ss");
			Date date = new Date();
			userRule.setCreatedDate(dateFormat.format(date));

			if (request.getParameter("userRuleEditFlag") != null
					&& !request.getParameter("userRuleEditFlag").toString().isEmpty()
					&& request.getParameter("userRuleEditFlag").toString().equals("1")) {
				UserRule userRule_ = dbTransactions.findUserRules(userRule.getUserID(), userRule.getRuleID());
				if (userRule_.getUserID() == 0)
				{
					userRule.setDescription("Create Role");
					status = dbTransactions.createUserRule(userRule);
				}
				else
					status = dbTransactions.updateUserRule(userRule,
							Integer.valueOf(request.getParameter("ur_userIDHid").toString()),
							Integer.valueOf(roles[i]));
				request.setAttribute("ur_userIDHid", "0");
				request.setAttribute("ur_ruleIDHid", "0");
				if (status > 0) {
					if (i == roles.length - 1) {
						session.setAttribute("userRuleResult", "Updated Successfully");
						response.sendRedirect(request.getContextPath() + "/main/pages/tables/UserManagement.jsp");
						return;
					}
				} else {
					session.setAttribute("userRuleResult", "Did not Updated Successfully");
					response.sendRedirect(request.getContextPath() + "/main/pages/tables/UserManagement.jsp");
					return;
				}
			} else {
				userRule.setDescription("Create Role");
				status = dbTransactions.createUserRule(userRule);
				if (status > 0) {
					if (i == roles.length - 1) {
						session.setAttribute("userRuleResult", "Role Assigned to the User Successfully");
						response.sendRedirect(request.getContextPath() + "/main/pages/tables/UserManagement.jsp");
						return;
					}
				} else {
					session.setAttribute("userRuleResult", "Rule did not  Assigned to the User Successfully");
					response.sendRedirect(request.getContextPath() + "/main/pages/tables/UserManagement.jsp");
					return;
				}
			}
		}
	}

}
