package com.bdc.usermanagement.ldab;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class ScreenServlet
 */
@WebServlet("/ScreenServlet")
public class ScreenServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ScreenServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("USER_ID") == null || session.getAttribute("USER_ID").equals("")
				|| session.getAttribute("USER_ID").toString().isEmpty()) {
			response.sendRedirect(request.getContextPath() + "/main/pages/tables/Login.jsp");
		} else
			doAction(request, response);
	}

	private void doAction(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		Screen screen = new Screen();
		screen.setScreenName(request.getParameter("screenName").toString().trim());
		screen.setScreenDescription(request.getParameter("screenDescription"));
		screen.setApplicationName(request.getParameter("applicationName"));
		screen.setRuleID(Integer.valueOf(request.getParameter("ruleList").toString()));
		screen.setCreatedByUserID(Integer.valueOf(session.getAttribute("USER_ID").toString()));
		UserManagmentDBTransactions dbTransactions = new UserManagmentDBTransactions();
		int status = 0;

		if (request.getParameter("screenIDHidden") == null
				|| request.getParameter("screenIDHidden").toString().isEmpty()
				|| request.getParameter("screenIDHidden").toString().equals("0")) {

			status = dbTransactions.createScreen(screen);
			if (status > 0) {
				session.setAttribute("screenResult", "Rule Created Successfully");
				response.sendRedirect(request.getContextPath() + "/main/pages/tables/RulesAndScreens.jsp");
				return;
			} else {
				session.setAttribute("screenResult", "Rule did not Created Successfully");
				response.sendRedirect(request.getContextPath() + "/main/pages/tables/RulesAndScreens.jsp");
				return;
			}
		} else if (request.getParameter("screenIDHidden") != null
				&& !request.getParameter("screenIDHidden").toString().isEmpty()
				&& !request.getParameter("screenIDHidden").toString().equals("0")) {
			screen.setScreeID(Integer.valueOf(request.getParameter("screenIDHidden").toString()));
			request.setAttribute("screenIDHidden", "0");
			status = dbTransactions.updateScreen(screen);

			if (status > 0) {
				session.setAttribute("screenResult", "Rule Updated Successfully");
				response.sendRedirect(request.getContextPath() + "/main/pages/tables/RulesAndScreens.jsp");
				return;
			} else {
				session.setAttribute("screenResult", "Rule did not Updated Successfully");
				response.sendRedirect(request.getContextPath() + "/main/pages/tables/RulesAndScreens.jsp");
				return;
			}
		}
	}

}
