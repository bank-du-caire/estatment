package com.bdc.usermanagement.ldab;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.mail.Session;

import com.bdc.estatement.connection.ConnectionManager;
import com.bdc.estatement.util.LogUtil;

public class UserManagmentDBTransactions {

	public int createUser(User user) {
		int status = 0;
		try {
			Connection con = ConnectionManager.getBDCConnection();

			String sql = "INSERT INTO ESTATEMENT.USERS(USER_NAME,FLAG_LOCKED,FLAG_DISABLE"
					+ ",CREATEDDATE,CREATEDBYUSERID,ACTIONDESCRIPTION) values " + "( ?, ?, ?, ?, ?, ?)";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, user.getUserName());
//			ps.setString(2, user.getMail() );
//			ps.setString(3, user.getBranch() );
			ps.setInt(2, user.getFlagLocked());
			ps.setInt(3, user.getFlagDisabled());
			ps.setString(4, user.getCreatedDate());
			ps.setInt(5, user.getCreatedByUserID());
			ps.setString(6, "Create New User");

			status = ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	public void editUserWithoutTrigger(int userID, int newUserId) {
		int status = 0;
		Connection con = null;
		try {
			con = ConnectionManager.getBDCConnection();

			String[] queries = { "ALTER TRIGGER UPDATE_USER DISABLE",
					"update users u set u.CREATEDBYUSERID = " + newUserId + " where u.USER_ID = " + userID,
					"ALTER TRIGGER UPDATE_USER enable", };
			Statement statement = con.createStatement();
			for (String query : queries) {
				statement.addBatch(query);
			}
			int[] val = statement.executeBatch();

//			String sql = "ALTER TRIGGER UPDATE_USER DISABLE; " + "update users u set u.CREATEDBYUSERID = " + newUserId
//					+ " where u.USER_ID = " + userID + "; ALTER TRIGGER UPDATE_USER enable";

//			PreparedStatement ps = con.prepareStatement(sql);
//			status = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (con != null && !con.isClosed()) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
//		return status;
	}

	public void editUserRoleWithoutTrigger(int userID, int RoleID, int newUserId) {
		int status = 0;
		Connection con = null;
		try {
			con = ConnectionManager.getBDCConnection();

			String[] queries = { "ALTER TRIGGER UPDATE_USER_RULE_TRIGGER DISABLE",
					"UPDATE ESTATEMENT.USERS_RULES SET CREATED_BY_USERID = " + newUserId + " WHERE USER_ID = " + userID
							+ " AND RULE_ID = " + RoleID + "",
					"ALTER TRIGGER UPDATE_USER_RULE_TRIGGER enable", };
			Statement statement = con.createStatement();
			for (String query : queries) {
				statement.addBatch(query);
			}
			int[] val = statement.executeBatch();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (con != null && !con.isClosed()) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
//		return status;
	}

	public int editUser(User user) {
		int status = 0;
		try {
			Connection con = ConnectionManager.getBDCConnection();

			String sql = "update  ESTATEMENT.USERS set ACTIONDESCRIPTION = '" + user.getDescription()
					+ "',USER_NAME = '" + user.getUserName() + "'," + "FLAG_LOCKED=" + user.getFlagLocked() + ","
					+ "STATUSFLAG=" + user.getApproveFlag() + "," + "FLAG_DISABLE=" + user.getFlagDisabled()
					+ ",CREATEDBYUSERID= " + user.getCreatedByUserID() + " where USER_ID = " + user.getUserID() + "";
			PreparedStatement ps = con.prepareStatement(sql);
			status = ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	public int changeUserLockStatus(User user) {
		int status = 0;
		try {
			Connection con = ConnectionManager.getBDCConnection();

			String sql = "update  ESTATEMENT.USERS set ACTIONDESCRIPTION = '" + user.getDescription() + "',FLAG_LOCKED="
					+ user.getFlagLocked() + ",statusflag=1,CREATEDBYUSERID= " + user.getCreatedByUserID()
					+ " where USER_ID = " + user.getUserID();
			PreparedStatement ps = con.prepareStatement(sql);
			status = ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	public int rejectUserDeletion(User user) {
		int status = 0;
		try {
			Connection con = ConnectionManager.getBDCConnection();

			String sql = "update ESTATEMENT.USERS set ACTIONDESCRIPTION = '" + user.getDescription()
					+ "',FLAG_DELETED = 0,statusflag=1,CREATEDBYUSERID= " + user.getCreatedByUserID()
					+ " where USER_ID = " + user.getUserID();
			PreparedStatement ps = con.prepareStatement(sql);
			status = ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	public int updateLastLoginDate(User user) {
		int status = 0;
		try {
			Connection con = ConnectionManager.getBDCConnection();

			String sql = "update  ESTATEMENT.USERS set LAST_LOGIN_DATE = to_date('" + user.getLastLoginDate()
					+ "','dd/mm/yyyy-hh24:mi:ss') where USER_ID = " + user.getUserID();
			PreparedStatement ps = con.prepareStatement(sql);
			status = ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	public int editLockUser(User user) {
		int status = 0;
		try {
			Connection con = ConnectionManager.getBDCConnection();

			String sql = "update  ESTATEMENT.USERS set USER_NAME = '" + user.getUserName() + "'," + "FLAG_LOCKED="
					+ user.getFlagLocked() + " where USER_ID = " + user.getUserID() + "";
			PreparedStatement ps = con.prepareStatement(sql);
			status = ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	public int approveUser(int userID, int approvedByUserID, int statusFlag) {
		int status = 0;
		Connection con = null;
		try {
			con = ConnectionManager.getBDCConnection();

			String sql = "update  ESTATEMENT.USERS set ACTIONDESCRIPTION=' ', STATUSFLAG = '" + statusFlag
					+ "',APPROVEDBYUSERID = '" + approvedByUserID + "' where USER_ID = '" + userID + "'";
			PreparedStatement ps = con.prepareStatement(sql);
			status = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (con != null && !con.isClosed())
					con.close();
			} catch (SQLException e) {
				e.printStackTrace();
				System.out.println(e);
			}
		}
		return status;
	}

	public int approveUserRole(int userID, int roleID, int approvedByUserID, int statusFlag) {
		int status = 0;
		Connection con = null;
		try {
			con = ConnectionManager.getBDCConnection();
			String sql = "update  ESTATEMENT.USERS_RULES set ACTIONDESCRIPTION=' ',STATUSFLAG = '" + statusFlag
					+ "',APPROVEDBYUSERID = '" + approvedByUserID + "' where USER_ID = '" + userID + "' and RULE_ID = '"
					+ roleID + "'";
			PreparedStatement ps = con.prepareStatement(sql);
			status = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (con != null && !con.isClosed())
					con.close();
			} catch (SQLException e) {
				e.printStackTrace();
				System.out.println(e);
			}
		}
		return status;
	}

	public int deleteUser(User user) {
		int status = 0;
		try {
			Connection con = ConnectionManager.getBDCConnection();
			String sql = "delete from estatement.users_rules where user_id = '" + user.getUserID() + "'";
			PreparedStatement ps = con.prepareStatement(sql);
			status = ps.executeUpdate();
			sql = "delete  ESTATEMENT.USERS  where USER_ID = " + user.getUserID() + "";
			ps = con.prepareStatement(sql);
			status = ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	public int editDeleteFlag(User user, int deleteFlag) {
		int status = 0;
		try {
			Connection con = ConnectionManager.getBDCConnection();
			String sql = "update estatement.users set ACTIONDESCRIPTION = 'Delete User',FLAG_DELETED='" + deleteFlag
					+ "' where user_id = '" + user.getUserID() + "'";
			PreparedStatement ps = con.prepareStatement(sql);
			status = ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	public List<User> findAllUsers() {
		Connection connection = ConnectionManager.getBDCConnection();
//		String sqlStr = "select asd.USER_ID,asd.USER_NAME,asd.MAIL,asd.BRANCH,asd.FLAG_LOCKED,asd.FLAG_DISABLE	"
//				+ ",asd.CREATEDDATE,asd.CREATEDBYUSERID,uss.USER_NAME createdUserName" + " from ESTATEMENT.USERS asd "
//				+ " left outer join ESTATEMENT.USERS uss on uss.CREATEDBYUSERID = asd.USER_ID" + " order by USER_ID DESC";

		String sqlStr = "select users_.USER_ID,users_.USER_NAME,users_.MAIL,users_.BRANCH,users_.FLAG_LOCKED,users_.FLAG_DISABLE,users_.CREATEDDATE, "
				+ " users_.CREATEDBYUSERID,u.USER_NAME createdUserName,users_.STATUSFLAG , "
				+ " to_char( users_.LAST_LOGIN_DATE, 'dd-mon-yy hh24:mi:ss' ),users_.FLAG_DELETED from users u "
				+ " join users users_ on users_.Createdbyuserid = u.user_id order by users_.USER_ID DESC";

		// LogUtil.info("Get All Created User's");

		List<User> allUsers = new ArrayList<User>();
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sqlStr);
			if (resultSet.isBeforeFirst()) {
				// LogUtil.info("Data retrieved successfully and will deal with it.");
				while (resultSet.next()) {
					User user = new User();
					user.setUserID(resultSet.getInt(1));
					user.setUserName(resultSet.getString(2));
					user.setMail(resultSet.getString(3));
					user.setBranch(resultSet.getString(4));
					user.setFlagLocked(resultSet.getInt(5));
					user.setFlagDisabled(resultSet.getInt(6));
					user.setCreatedDate(resultSet.getString(7));
					user.setCreatedByUserID(resultSet.getInt(8));
					user.setCreatedByUserName(resultSet.getString(9));
					user.setApproveFlag(resultSet.getInt(10));
					String lastLoginString = resultSet.getString(11);
					if (lastLoginString != null && !lastLoginString.isEmpty()) {
						Date lastLoginDate = new SimpleDateFormat("dd-MMM-yy HH:mm:ss").parse(lastLoginString);
						SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
						// format.format(lastLoginDate);
						System.out.println(format.format(lastLoginDate).toString());
						user.setLastLoginDate(format.format(lastLoginDate));
					} else
						user.setLastLoginDate(null);
					user.setFlagDeleted(resultSet.getInt(12));
					allUsers.add(user);
				}
			} else {
				// LogUtil.info("There no data retrieved");
			}
			// LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		} catch (Exception e) {
			// LogUtil.error("SQL Error {}.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				// LogUtil.error("Error While close the connection. {}", e);
			}
		}
		// LogUtil.info("Number of transactions is: " + allUsers.size());
		return allUsers;
	}

	public List<User> findAllUsersWithoutLoggedInUser(int userID) {
		Connection connection = ConnectionManager.getBDCConnection();
//		String sqlStr = "select asd.USER_ID,asd.USER_NAME,asd.MAIL,asd.BRANCH,asd.FLAG_LOCKED,asd.FLAG_DISABLE	"
//				+ ",asd.CREATEDDATE,asd.CREATEDBYUSERID,uss.USER_NAME createdUserName" + " from ESTATEMENT.USERS asd "
//				+ " left outer join ESTATEMENT.USERS uss on uss.CREATEDBYUSERID = asd.USER_ID" + " order by USER_ID DESC";

		String sqlStr = "select users_.USER_ID,users_.USER_NAME,users_.MAIL,users_.BRANCH,users_.FLAG_LOCKED,users_.FLAG_DISABLE,users_.CREATEDDATE, "
				+ " users_.CREATEDBYUSERID,u.USER_NAME createdUserName,users_.STATUSFLAG , "
				+ " to_char( users_.LAST_LOGIN_DATE, 'dd-mon-yy hh24:mi:ss' ),users_.FLAG_DELETED,users_.ACTIONDESCRIPTION from users u "
				+ " join users users_ on users_.Createdbyuserid = u.user_id where users_.USER_ID != '" + userID
				+ "' order by users_.USER_ID DESC";

		// LogUtil.info("Get All Created User's");

		List<User> allUsers = new ArrayList<User>();
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sqlStr);
			if (resultSet.isBeforeFirst()) {
				// LogUtil.info("Data retrieved successfully and will deal with it.");
				while (resultSet.next()) {
					User user = new User();
					user.setUserID(resultSet.getInt(1));
					user.setUserName(resultSet.getString(2));
					user.setMail(resultSet.getString(3));
					user.setBranch(resultSet.getString(4));
					user.setFlagLocked(resultSet.getInt(5));
					user.setFlagDisabled(resultSet.getInt(6));
					user.setCreatedDate(resultSet.getString(7));
					user.setCreatedByUserID(resultSet.getInt(8));
					user.setCreatedByUserName(resultSet.getString(9));
					user.setApproveFlag(resultSet.getInt(10));
					String lastLoginString = resultSet.getString(11);
					user.setDescription(resultSet.getString(12));
					if (lastLoginString != null && !lastLoginString.isEmpty()) {
						Date lastLoginDate = new SimpleDateFormat("dd-MMM-yy HH:mm:ss").parse(lastLoginString);
						SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
						// format.format(lastLoginDate);
						System.out.println(format.format(lastLoginDate).toString());
						user.setLastLoginDate(format.format(lastLoginDate));
					} else
						user.setLastLoginDate(null);
					user.setFlagDeleted(resultSet.getInt(12));
					user.setDescription(resultSet.getString(13));
					allUsers.add(user);
				}
			} else {
				// LogUtil.info("There no data retrieved");
			}
			// LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		} catch (Exception e) {
			// LogUtil.error("SQL Error {}.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				// LogUtil.error("Error While close the connection. {}", e);
			}
		}
		// LogUtil.info("Number of transactions is: " + allUsers.size());
		return allUsers;
	}

	public User getUser(String userName) {
		Connection connection = ConnectionManager.getBDCConnection();
		String sqlStr = "select USER_ID,FLAG_LOCKED,FLAG_DISABLE,FLAG_DELETED,STATUSFLAG from ESTATEMENT.USERS where USER_NAME = '"
				+ userName + "'";
		// LogUtil.info("Get All Created User's");
		Statement statement = null;
		ResultSet resultSet = null;
		int count = 0;
		User user = new User();
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sqlStr);
			if (resultSet.isBeforeFirst()) {
				while (resultSet.next()) {

					user.setUserID(resultSet.getInt(1));
					user.setFlagLocked(resultSet.getInt(2));
					user.setFlagDisabled(resultSet.getInt(3));
					user.setFlagDeleted(resultSet.getInt(4));
					user.setApproveFlag(resultSet.getInt(5));
				}
			} else {
				// LogUtil.info("There no data retrieved");
			}
			// LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		} catch (SQLException e) {
			// LogUtil.error("SQL Error {}.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				// LogUtil.error("Error While close the connection. {}", e);
			}
		}
		// LogUtil.info("Number of transactions is: " + allUsers.size());
		return user;
	}

	public int createRule(Rule rule) {
		int status = 0;
		try {
			Connection con = ConnectionManager.getBDCConnection();

			String sql = "INSERT INTO ESTATEMENT.RULES ( RULE_NAME, RULE_DESCRIPTION,INSERT_FLAG,"
					+ " UPDATE_FLAG, DELETE_FLAG,VIEW_FLAG, CREATED_DATE, CREATED_BY_USERID,USER_CREATION_FLAG,VIEW_REPORT_FLAG)"
					+ " VALUES (?,?,?,?,?,?,?,?,?,? )";

			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, rule.getRuleName());
			ps.setString(2, rule.getRuleDescription());
			ps.setInt(3, rule.getInsertFlag());
			ps.setInt(4, rule.getUpdateFlag());
			ps.setInt(5, rule.getDeleteFlag());
			ps.setInt(6, rule.getViewFlag());
			ps.setString(7, rule.getCreatedDate());
			ps.setInt(8, rule.getCreatedByUserID());
			ps.setInt(9, rule.getUserCreationFlag());
			ps.setInt(10, rule.getViewReportRoleFlag());
			status = ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	public int updateRule(Rule rule) {

		int status = 0;
		try {
			Connection con = ConnectionManager.getBDCConnection();

			String sql = "UPDATE ESTATEMENT.RULES SET RULE_NAME = '" + rule.getRuleName() + "', RULE_DESCRIPTION = '"
					+ rule.getRuleDescription() + "',INSERT_FLAG = '" + rule.getInsertFlag() + "', UPDATE_FLAG = '"
					+ rule.getUpdateFlag() + "', DELETE_FLAG = '" + rule.getDeleteFlag() + "', VIEW_FLAG = '"
					+ rule.getViewFlag() + "', USER_CREATION_FLAG = '" + rule.getUserCreationFlag()
					+ "', VIEW_REPORT_FLAG = '" + rule.getViewReportRoleFlag() + "' WHERE RULE_ID= '" + rule.getRuleID()
					+ "'";

			PreparedStatement ps = con.prepareStatement(sql);
			status = ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	public List<Rule> findAllRules() {
		List<Rule> allRules = new ArrayList<Rule>();
		Connection connection = ConnectionManager.getBDCConnection();
//		String sqlStr = "select RULE_ID, RULE_NAME, RULE_DESCRIPTION, INSERT_FLAG, UPDATE_FLAG, DELETE_FLAG,"
//				+ " VIEW_FLAG, CREATED_DATE, CREATED_BY_USERID,USER_CREATION_FLAG from  ESTATEMENT.RULES order by RULE_ID Desc";

		String sqlStr = "select r.RULE_ID, r.RULE_NAME, r.RULE_DESCRIPTION, r.INSERT_FLAG, r.UPDATE_FLAG, r.DELETE_FLAG, "
				+ " r.VIEW_FLAG, r.CREATED_DATE, u.user_name,r.USER_CREATION_FLAG,r.VIEW_REPORT_FLAG from  ESTATEMENT.RULES r "
				+ " left outer join users u on u.user_id = r.created_by_userid "
				+ " where r.RULE_ID!=22 order by RULE_ID Desc";
		// LogUtil.info("Get All Created User's");
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sqlStr);
			if (resultSet.isBeforeFirst()) {
				// LogUtil.info("Data retrieved successfully and will deal with it.");
				while (resultSet.next()) {
					Rule rule = new Rule();
					rule.setRuleID(resultSet.getInt(1));
					rule.setRuleName(resultSet.getString(2));
					rule.setRuleDescription(resultSet.getString(3));
					rule.setInsertFlag(resultSet.getInt(4));
					rule.setUpdateFlag(resultSet.getInt(5));
					rule.setDeleteFlag(resultSet.getInt(6));
					rule.setViewFlag(resultSet.getInt(7));
					rule.setCreatedDate(resultSet.getString(8));
					rule.setCreatedByUserName(resultSet.getString(9));
					rule.setUserCreationFlag(resultSet.getInt(10));
					rule.setViewReportRoleFlag(resultSet.getInt(11));
					allRules.add(rule);
				}
			} else {
				// LogUtil.info("There no data retrieved");
			}
			// LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		} catch (SQLException e) {
			// LogUtil.error("SQL Error {}.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				// LogUtil.error("Error While close the connection. {}", e);
			}
		}
		// LogUtil.info("Number of transactions is: " + allUsers.size());

		return allRules;
	}

	public int createScreen(Screen screen) {
		int status = 0;
		try {
			Connection con = ConnectionManager.getBDCConnection();

			String sql = "INSERT INTO ESTATEMENT.SCREENS (SCREEN_NAME, SCREEN_DESCRIPTION,  "
					+ " APPLICATION_NAME, RULE_ID, CREATEDBYUSERID)VALUES " + "( ? , ? , ? , ? , ? )";

			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, screen.getScreenName());
			ps.setString(2, screen.getScreenDescription());
			ps.setString(3, screen.getApplicationName());
			ps.setInt(4, screen.getRuleID());
			ps.setInt(5, screen.getCreatedByUserID());
			status = ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	public int updateScreen(Screen screen) {

		int status = 0;
		try {
			Connection con = ConnectionManager.getBDCConnection();

			String sql = "UPDATE ESTATEMENT.SCREENS SET SCREEN_NAME = '" + screen.getScreenName()
					+ "',SCREEN_DESCRIPTION = '" + screen.getScreenDescription() + "', APPLICATION_NAME = '"
					+ screen.getApplicationName() + "', RULE_ID = '" + screen.getRuleID() + "' WHERE SCREEN_ID = '"
					+ screen.getScreeID() + "'";
			PreparedStatement ps = con.prepareStatement(sql);
			status = ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	public List<Screen> findAllScreens() {
		List<Screen> allScreens = new ArrayList<Screen>();
		Connection connection = ConnectionManager.getBDCConnection();
		String sqlStr = "Select sc.SCREEN_ID, sc.SCREEN_NAME, sc.SCREEN_DESCRIPTION, sc.APPLICATION_NAME,"
				+ " sc.RULE_ID, sc.CREATEDBYUSERID from ESTATEMENT.SCREENS sc   left outer join ESTATEMENT.RULES rl "
				+ "on rl.RULE_ID = sc.RULE_ID order by sc.SCREEN_ID Desc";
		// LogUtil.info("Get All Created User's");
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sqlStr);
			if (resultSet.isBeforeFirst()) {
				// LogUtil.info("Data retrieved successfully and will deal with it.");
				while (resultSet.next()) {
					Screen screen = new Screen();
					screen.setScreeID(resultSet.getInt(1));
					screen.setScreenName(resultSet.getString(2));
					screen.setScreenDescription(resultSet.getString(3));
					screen.setApplicationName(resultSet.getString(4));
					screen.setRuleID(resultSet.getInt(5));
					screen.setCreatedByUserID(resultSet.getInt(6));
					allScreens.add(screen);
				}
			} else {
				// LogUtil.info("There no data retrieved");
			}
			// LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		} catch (SQLException e) {
			// LogUtil.error("SQL Error {}.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				// LogUtil.error("Error While close the connection. {}", e);
			}
		}
		// LogUtil.info("Number of transactions is: " + allUsers.size());

		return allScreens;
	}

	public int createUserRule(UserRule userRule) {
		int status = 0;
		try {
			Connection con = ConnectionManager.getBDCConnection();

			String sql = "INSERT INTO ESTATEMENT.USERS_RULES (USER_ID, RULE_ID, CREATED_DATE,   CREATED_BY_USERID, ACTIONDESCRIPTION)"
					+ "VALUES ( ?, ? , ? , ? ,?)";

			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, userRule.getUserID());
			ps.setInt(2, userRule.getRuleID());
			ps.setString(3, userRule.getCreatedDate());
			ps.setInt(4, userRule.getCreatedByUserID());
			ps.setString(5, userRule.getDescription());
			status = ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	public int updateUserRule(UserRule userRule, int userID, int ruleID) {
		int status = 0;
		try {
			Connection con = ConnectionManager.getBDCConnection();

			String sql = "UPDATE ESTATEMENT.USERS_RULES SET USER_ID = '" + userRule.getUserID() + "', RULE_ID = '"
					+ userRule.getRuleID() + "' WHERE USER_ID = '" + userID + "' AND RULE_ID = '" + ruleID + "'";

			PreparedStatement ps = con.prepareStatement(sql);
			status = ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	public int deleteUserRule(int userID, int ruleID) {
		int status = 0;
		try {
			Connection con = ConnectionManager.getBDCConnection();

			String sql = "delete from ESTATEMENT.USERS_RULES WHERE USER_ID = '" + userID + "' AND RULE_ID = '" + ruleID
					+ "'";
			PreparedStatement ps = con.prepareStatement(sql);
			status = ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	public UserRule findUserRules(int userID, int roleID) {
		Connection connection = ConnectionManager.getBDCConnection();
		String sqlStr = "select userRule.USER_ID,userRule.RULE_ID,users_.USER_NAME,rules_.RULE_NAME,userRule.CREATED_DATE,createdUsers.USER_NAME"
				+ " createdByUser from ESTATEMENT.USERS_RULES userRule "
				+ " left outer join ESTATEMENT.USERS users_ on users_.USER_ID = userRule.USER_ID "
				+ " left outer join ESTATEMENT.RULES rules_ on rules_.RULE_ID = userRule.RULE_ID "
				+ " left outer join ESTATEMENT.USERS createdUsers on createdUsers.USER_ID = userRule.CREATED_BY_USERID "
				+ " where userRule.USER_ID ='" + userID + "' and userRule.RULE_ID = '" + roleID + "'";
		// LogUtil.info("Get All Created User's");
		UserRule userRule = new UserRule();
		Statement statement = null;
		ResultSet resultSet = null;
		// int running =start ;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sqlStr);
			if (resultSet.isBeforeFirst()) {
				// LogUtil.info("Data retrieved successfully and will deal with it.");
				while (resultSet.next()) {

					userRule.setUserID(resultSet.getInt(1));
					userRule.setRuleID(resultSet.getInt(2));
					userRule.setUserName(resultSet.getString(3));
					userRule.setRuleName(resultSet.getString(4));
					userRule.setCreatedDate(resultSet.getString(5));
					userRule.setCreatedByUserName(resultSet.getString(6));
				}
			} else {
				// LogUtil.info("There no data retrieved");
			}
			// LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		} catch (SQLException e) {
			// LogUtil.error("SQL Error {}.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				// LogUtil.error("Error While close the connection. {}", e);
			}
		}
		// LogUtil.info("Number of transactions is: " + allUsers.size());
		return userRule;
	}

	public List<UserRule> findAllUserRules() {
		Connection connection = ConnectionManager.getBDCConnection();
		String sqlStr = "select userRule.USER_ID,userRule.RULE_ID,users_.USER_NAME,rules_.RULE_NAME,userRule.CREATED_DATE,createdUsers.USER_NAME"
				+ " createdByUser,userRule.STATUSFLAG,to_char( users_.LAST_LOGIN_DATE, 'dd-mon-yy hh24:mi:ss' ),userRule.ACTIONDESCRIPTION from ESTATEMENT.USERS_RULES userRule "
				+ "left outer join ESTATEMENT.USERS users_ on users_.USER_ID = userRule.USER_ID "
				+ "left outer join ESTATEMENT.RULES rules_ on rules_.RULE_ID = userRule.RULE_ID "
				+ "left outer join ESTATEMENT.USERS createdUsers on createdUsers.USER_ID = userRule.CREATED_BY_USERID "
				+ "order by to_date( userRule.CREATED_DATE, 'DD-MM-YYYY HH24-MI-SS' ) desc";
		// LogUtil.info("Get All Created User's");
		List<UserRule> allUserRules = new ArrayList<UserRule>();
		Statement statement = null;
		ResultSet resultSet = null;
		// int running =start ;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sqlStr);
			if (resultSet.isBeforeFirst()) {
				// LogUtil.info("Data retrieved successfully and will deal with it.");
				while (resultSet.next()) {
					UserRule userRule = new UserRule();
					userRule.setUserID(resultSet.getInt(1));
					userRule.setRuleID(resultSet.getInt(2));
					userRule.setUserName(resultSet.getString(3));
					userRule.setRuleName(resultSet.getString(4));
					userRule.setCreatedDate(resultSet.getString(5));
					userRule.setCreatedByUserName(resultSet.getString(6));
					userRule.setApproveFlag(resultSet.getInt(7));

					String lastLoginString = resultSet.getString(8);
					if (lastLoginString != null && !lastLoginString.isEmpty()) {
						Date lastLoginDate = new SimpleDateFormat("dd-MMM-yy HH:mm:ss").parse(lastLoginString);
						SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
						// format.format(lastLoginDate);
						System.out.println(format.format(lastLoginDate).toString());
						userRule.setLastLoginDate(format.format(lastLoginDate));
					} else
						userRule.setLastLoginDate(null);
					userRule.setDescription(resultSet.getString(9));
					allUserRules.add(userRule);
				}
			} else {
				// LogUtil.info("There no data retrieved");
			}
			// LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		} catch (Exception e) {
			// LogUtil.error("SQL Error {}.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				// LogUtil.error("Error While close the connection. {}", e);
			}
		}
		// LogUtil.info("Number of transactions is: " + allUsers.size());
		return allUserRules;
	}

	public List<UserPrivilege> findUserPrivileges(int userID, String screenName) {
		List<UserPrivilege> allUserPrivileges = new ArrayList<UserPrivilege>();
		Connection connection = ConnectionManager.getBDCConnection();
//		String sqlStr = "select rules_.INSERT_FLAG,rules_.UPDATE_FLAG,rules_.DELETE_FLAG,rules_.VIEW_FLAG,rules_.USER_CREATION_FLAG,rules_.VIEW_REPORT_FLAG "
//				+ "from ESTATEMENT.USERS_RULES userRules ,ESTATEMENT.RULES rules_ "
//				+ "inner join ESTATEMENT.SCREENS screen on screen.RULE_ID = rules_.RULE_ID "
//				+ "where userRules.USER_ID='" + userID + "' and rules_.RULE_ID = userRules.RULE_ID and "
//				+ "screen.SCREEN_NAME like '%" + screenName + "%'";
		/*
		 * String sqlStr =
		 * "select rules_.INSERT_FLAG,rules_.UPDATE_FLAG,rules_.DELETE_FLAG,rules_.VIEW_FLAG,rules_.USER_CREATION_FLAG,rules_.VIEW_REPORT_FLAG"
		 * + " from ESTATEMENT.USERS_RULES userRules ,ESTATEMENT.RULES rules_" +
		 * " where userRules.USER_ID='" + userID +
		 * "' and rules_.RULE_ID = userRules.RULE_ID and userRules.STATUSFLAG = 1 ";
		 */
		String sqlStr = "select rules_.INSERT_FLAG,rules_.UPDATE_FLAG,rules_.DELETE_FLAG,rules_.VIEW_FLAG,rules_.USER_CREATION_FLAG,rules_.VIEW_REPORT_FLAG"
				+ " from ESTATMENT.USERS_RULES userRules ,ESTATMENT.RULES rules_" + " where userRules.USER_ID='"
				+ userID + "' and rules_.RULE_ID = userRules.RULE_ID and userRules.STATUSFLAG = 1 ";

		// LogUtil.info("Get All Created User's");
		Statement statement = null;
		ResultSet resultSet = null;
		// int running =start ;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sqlStr);
			if (resultSet.isBeforeFirst()) {
				// LogUtil.info("Data retrieved successfully and will deal with it.");
				while (resultSet.next()) {
					UserPrivilege userPrivilege = new UserPrivilege();
					userPrivilege.setInsertFlag(resultSet.getInt(1));
					userPrivilege.setUpdateFlag(resultSet.getInt(2));
					userPrivilege.setDeleteFlag(resultSet.getInt(3));
					userPrivilege.setViewFlag(resultSet.getInt(4));
					userPrivilege.setUserCreation(resultSet.getInt(5));
					userPrivilege.setReportView(resultSet.getInt(6));
					allUserPrivileges.add(userPrivilege);
				}
			} else {
				// LogUtil.info("There no data retrieved");
			}
			// LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		} catch (SQLException e) {
			// LogUtil.error("SQL Error {}.", e);
			System.out.println(e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				// LogUtil.error("Error While close the connection. {}", e);
			}
		}
		// LogUtil.info("Number of transactions is: " + allUsers.size());
		return allUserPrivileges;
	}

	public boolean checkIfHostToHost(int userID) {
		Connection connection = ConnectionManager.getBDCConnection();
		String sqlStr = "select * from users_rules ur where ur.USER_ID =" + userID + " and ur.RULE_ID=61";

		Statement statement = null;
		ResultSet resultSet = null;
		boolean res = false;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sqlStr);
			if (resultSet.isBeforeFirst()) {
				// LogUtil.info("Data retrieved successfully and will deal with it.");
				while (resultSet.next()) {
					res = true;
					break;
				}
			}
		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				System.out.println(e);
			}
		}
		return res;
	}

	public boolean checkIfMeezaPrepaid(String userID) {
		Connection connection = ConnectionManager.getBDCConnection();
		String sqlStr = "select * from users_rules ur where ur.USER_ID =" + userID + " and ur.RULE_ID=81";

		Statement statement = null;
		ResultSet resultSet = null;
		boolean res = false;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sqlStr);
			if (resultSet.isBeforeFirst()) {
				// LogUtil.info("Data retrieved successfully and will deal with it.");
				while (resultSet.next()) {
					res = true;
					break;
				}
			}
		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				System.out.println(e);
			}
		}
		return res;
	}

	public List<AccountAudit> findAllAccountAudits(String fromDate, String toDate) {
		Connection connection = ConnectionManager.getBDCConnection();

		String sqlStr = " SELECT a.COD_CUST," + "         a.COD_ACCT_NO," + "         a.FREQ_STMNT,"
				+ "         a.EMAIL," + "         a.STMT_NEXT_DATE," + "         a.MOBILE_NO,"
				+ "         a.BRANCH_NAME," + "         a.CUSTOMER_NAME," + "         a.CREATED_DATE,"
				+ "         a.ACTION," + "         u.USER_NAME," + "		  a.LAST_SENT_DATE"
				+ "    FROM ESTATEMENT.account_audit a"
				+ "         INNER JOIN ESTATEMENT.USERS u ON u.USER_ID = a.CREATED_BY_USER_ID"
				+ "   WHERE     a.LAST_SENT_DATE between '" + fromDate + "' AND '" + toDate + "'"
				+ " 	ORDER BY a.ACCOUNT_AUDIT_ID DESC";
		System.out.println(sqlStr);
		// LogUtil.info("Get All Created User's");

		List<AccountAudit> allAccountAudits = new ArrayList<AccountAudit>();
		Statement statement = null;
		ResultSet resultSet = null;
		// int running =start ;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sqlStr);
			if (resultSet.isBeforeFirst()) {
				// LogUtil.info("Data retrieved successfully and will deal with it.");
				while (resultSet.next()) {
					AccountAudit accountAudit = new AccountAudit();
					accountAudit.setCustomerCode(resultSet.getString(1));
					accountAudit.setAccountNumber(resultSet.getString(2));
					accountAudit.setFrequancyStmt(resultSet.getInt(3));
					accountAudit.setEmail(resultSet.getString(4));
					accountAudit.setNextDate(resultSet.getString(5));
					accountAudit.setMobile(resultSet.getString(6));
					accountAudit.setBranchName(resultSet.getString(7));
					accountAudit.setCustomerName(resultSet.getString(8));
					accountAudit.setCreatedDate(resultSet.getString(9));
					accountAudit.setAction(resultSet.getString(10));
					accountAudit.setCreatedBy(resultSet.getString(11));
					accountAudit.setLastSentDate(resultSet.getDate(12));
					allAccountAudits.add(accountAudit);
				}
			} else {
				// LogUtil.info("There no data retrieved");
			}
			// LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		} catch (SQLException e) {
//			 LogUtil.error("SQL Error {}.", e);
			e.printStackTrace();
			System.out.println(e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				// LogUtil.error("Error While close the connection. {}", e);
			}
		}
		// LogUtil.info("Number of transactions is: " + allUsers.size());
		return allAccountAudits;
	}

	public List<AccountAudit> findAllAccountAudits() {
		Connection connection = ConnectionManager.getBDCConnection();
		String sqlStr = "select a.COD_CUST, a.COD_ACCT_NO, a.FREQ_STMNT, a.EMAIL, a.STMT_NEXT_DATE,a.MOBILE_NO, a.BRANCH_NAME,"
				+ "				a.CUSTOMER_NAME, a.CREATED_DATE,a.ACTION ," + "				u.user_name  "
				+ "				from ESTATEMENT.account_audit a ,ESTATEMENT.users u where "
				+ "				(a.APPROVEDBYUSERID != 0 and u.user_id =  a.APPROVEDBYUSERID)  "
				+ "				or (a.APPROVEDBYUSERID = 0 and u.user_id = a.created_by_user_id) "
				+ "				order by a.ACCOUNT_AUDIT_ID desc";
		// LogUtil.info("Get All Created User's");

		List<AccountAudit> allAccountAudits = new ArrayList<AccountAudit>();
		Statement statement = null;
		ResultSet resultSet = null;
		// int running =start ;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sqlStr);
			if (resultSet.isBeforeFirst()) {
				// LogUtil.info("Data retrieved successfully and will deal with it.");
				while (resultSet.next()) {
					AccountAudit accountAudit = new AccountAudit();
					accountAudit.setCustomerCode(resultSet.getString(1));
					accountAudit.setAccountNumber(resultSet.getString(2));
					accountAudit.setFrequancyStmt(resultSet.getInt(3));
					accountAudit.setEmail(resultSet.getString(4));
					accountAudit.setNextDate(resultSet.getString(5));
					accountAudit.setMobile(resultSet.getString(6));
					accountAudit.setBranchName(resultSet.getString(7));
					accountAudit.setCustomerName(resultSet.getString(8));
					accountAudit.setCreatedDate(resultSet.getString(9));
					accountAudit.setAction(resultSet.getString(10));
					accountAudit.setCreatedBy(resultSet.getString(11));
//					accountAudit.setLastSentDate(resultSet.getDate(12));
					allAccountAudits.add(accountAudit);
				}
			} else {
				// LogUtil.info("There no data retrieved");
			}
			// LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		} catch (SQLException e) {
			// LogUtil.error("SQL Error {}.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				// LogUtil.error("Error While close the connection. {}", e);
			}
		}
		// LogUtil.info("Number of transactions is: " + allUsers.size());
		return allAccountAudits;
	}

	public User getUserInfo(String userName) {
		Connection connection = ConnectionManager.getBDCConnection();

		String sqlStr = "select users_.USER_ID,users_.USER_NAME,users_.MAIL,users_.BRANCH,users_.FLAG_LOCKED,users_.FLAG_DISABLE,users_.CREATEDDATE, "
				+ "users_.CREATEDBYUSERID,u.USER_NAME createdUserName " + "from users u "
				+ " join users users_ on users_.Createdbyuserid = u.user_id " + "where users_.USER_NAME= '" + userName
				+ "' order by users_.USER_ID DESC ";

		// LogUtil.info("Get All Created User's");

		User user = new User();
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sqlStr);
			if (resultSet.isBeforeFirst()) {
				// LogUtil.info("Data retrieved successfully and will deal with it.");
				while (resultSet.next()) {

					user.setUserID(resultSet.getInt(1));
					user.setUserName(resultSet.getString(2));
					user.setMail(resultSet.getString(3));
					user.setBranch(resultSet.getString(4));
					user.setFlagLocked(resultSet.getInt(5));
					user.setFlagDisabled(resultSet.getInt(6));
					user.setCreatedDate(resultSet.getString(7));
					user.setCreatedByUserID(resultSet.getInt(8));
					user.setCreatedByUserName(resultSet.getString(9));

				}
			} else {
				// LogUtil.info("There no data retrieved");
			}
			// LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		} catch (SQLException e) {
			// LogUtil.error("SQL Error {}.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				// LogUtil.error("Error While close the connection. {}", e);
			}
		}
		// LogUtil.info("Number of transactions is: " + allUsers.size());
		return user;
	}

	public List<UserRule> findAllUserRulesByUserName(String userName) {
		Connection connection = ConnectionManager.getBDCConnection();
		String sqlStr = "select userRule.USER_ID,userRule.RULE_ID,users_.USER_NAME,rules_.RULE_NAME,userRule.CREATED_DATE,createdUsers.USER_NAME"
				+ " createdByUser from ESTATEMENT.USERS_RULES userRule "
				+ "left outer join ESTATEMENT.USERS users_ on users_.USER_ID = userRule.USER_ID "
				+ "left outer join ESTATEMENT.RULES rules_ on rules_.RULE_ID = userRule.RULE_ID "
				+ "left outer join ESTATEMENT.USERS createdUsers on createdUsers.USER_ID = userRule.CREATED_BY_USERID "
				+ "where users_.USER_NAME='" + userName + "' order by userRule.CREATED_DATE desc";
		// LogUtil.info("Get All Created User's");

		List<UserRule> allUserRules = new ArrayList<UserRule>();
		Statement statement = null;
		ResultSet resultSet = null;
		// int running =start ;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sqlStr);
			if (resultSet.isBeforeFirst()) {
				// LogUtil.info("Data retrieved successfully and will deal with it.");
				while (resultSet.next()) {
					UserRule userRule = new UserRule();
					userRule.setUserID(resultSet.getInt(1));
					userRule.setRuleID(resultSet.getInt(2));
					userRule.setUserName(resultSet.getString(3));
					userRule.setRuleName(resultSet.getString(4));
					userRule.setCreatedDate(resultSet.getString(5));
					userRule.setCreatedByUserName(resultSet.getString(6));
					allUserRules.add(userRule);
				}
			} else {
				// LogUtil.info("There no data retrieved");
			}
			// LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		} catch (SQLException e) {
			// LogUtil.error("SQL Error {}.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				// LogUtil.error("Error While close the connection. {}", e);
			}
		}
		// LogUtil.info("Number of transactions is: " + allUsers.size());
		return allUserRules;
	}

	public List<UserAudit> findAllUserAutids() {
		Connection connection = ConnectionManager.getBDCConnection();
		String sqlStr = "SELECT u_audit.USER_NAME, u_audit.FLAG_LOCKED, u_audit.FLAG_DISABLE, "
				+ " u_audit.ACTION, u_audit.CREATEDATE,u.USER_NAME createdByUserName "
				+ "FROM ESTATEMENT.USER_AUDIT u_audit left outer join users u on u.USER_ID = u_audit.CREATEDBYUSERID"
				+ " order by u_audit.CREATEDATE desc";

		List<UserAudit> allUserAudit = new ArrayList<UserAudit>();
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sqlStr);
			if (resultSet.isBeforeFirst()) {
//				LogUtil.info("Data retrieved successfully and will deal with it.");
				while (resultSet.next()) {
					UserAudit userAudit = new UserAudit();
					userAudit.setUserName(resultSet.getString(1));
					userAudit.setFlagLocked(resultSet.getInt(2));
					userAudit.setFlagDisable(resultSet.getInt(3));
					userAudit.setAction(resultSet.getString(4));
					userAudit.setCreatedDate(resultSet.getDate(5));
					userAudit.setCreatedByUserName(resultSet.getString(6));
					allUserAudit.add(userAudit);
				}
			} else {
//				LogUtil.info("There no data retrieved");
			}
//			LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		} catch (SQLException e) {
			System.out.println(e);
//			LogUtil.error("SQL Error {}.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				// LogUtil.error("Error While close the connection. {}", e);
			}
		}
		// LogUtil.info("Number of transactions is: " + allUsers.size());
		return allUserAudit;
	}

	public List<UserAudit> findAllUserAutids(String searchUserName) {
		Connection connection = ConnectionManager.getBDCConnection();
		String sqlStr = "SELECT u_audit.USER_NAME, u_audit.FLAG_LOCKED, u_audit.FLAG_DISABLE, "
				+ " u_audit.ACTION, u_audit.CREATEDATE,u.USER_NAME createdByUserName"
				+ " FROM ESTATEMENT.USER_AUDIT u_audit left outer join users u on u.USER_ID = u_audit.CREATEDBYUSERID"
				+ " where upper(u_audit.USER_NAME) = upper('" + searchUserName + "') or upper(u.USER_NAME) = upper('"
				+ searchUserName + "')" + " order by u_audit.CREATEDATE desc";

		List<UserAudit> allUserAudit = new ArrayList<UserAudit>();
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sqlStr);
			if (resultSet.isBeforeFirst()) {
//				LogUtil.info("Data retrieved successfully and will deal with it.");
				while (resultSet.next()) {
					UserAudit userAudit = new UserAudit();
					userAudit.setUserName(resultSet.getString(1));
					userAudit.setFlagLocked(resultSet.getInt(2));
					userAudit.setFlagDisable(resultSet.getInt(3));
					userAudit.setAction(resultSet.getString(4));
					userAudit.setCreatedDate(resultSet.getDate(5));
					userAudit.setCreatedByUserName(resultSet.getString(6));
					allUserAudit.add(userAudit);
				}
			} else {
//				LogUtil.info("There no data retrieved");
			}
//			LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		} catch (SQLException e) {
			System.out.println(e);
//			LogUtil.error("SQL Error {}.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				// LogUtil.error("Error While close the connection. {}", e);
			}
		}
		// LogUtil.info("Number of transactions is: " + allUsers.size());
		return allUserAudit;
	}

	public List<ScreenAudit> findAllScreenAudits() {
		Connection connection = ConnectionManager.getBDCConnection();
		String sqlStr = "SELECT screenAudit.SCREEN_NAME, screenAudit.SCREEN_DESCRIPTION, screenAudit.APPLICATION_NAME, "
				+ "     rules_.rule_name, u.USER_NAME, screenAudit.CREATIONDATE, screenAudit.ACTION "
				+ "FROM ESTATEMENT.SCREEN_AUDIT screenAudit,rules rules_,users u "
				+ "    where rules_.rule_id = screenAudit.RULE_ID and u.USER_ID = screenAudit.CREATEDBYUSERID "
				+ "order by CREATIONDATE desc";

		List<ScreenAudit> allScreenAudits = new ArrayList<ScreenAudit>();
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sqlStr);
			if (resultSet.isBeforeFirst()) {
//				LogUtil.info("Data retrieved successfully and will deal with it.");
				while (resultSet.next()) {
					ScreenAudit screenAudit = new ScreenAudit();
					screenAudit.setScreenName(resultSet.getString(1));
					screenAudit.setScreenDescription(resultSet.getString(2));
					screenAudit.setApplicationName(resultSet.getString(3));
					screenAudit.setRoleName(resultSet.getString(4));
					screenAudit.setCreatedByUserName(resultSet.getString(5));
					screenAudit.setCreatedDate(resultSet.getDate(6));
					screenAudit.setAction(resultSet.getString(7));
					allScreenAudits.add(screenAudit);
				}
			} else {
//				LogUtil.info("There no data retrieved");
			}
//			LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		} catch (SQLException e) {
			System.out.println(e);
//			LogUtil.error("SQL Error {}.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				System.out.println(e);
				// LogUtil.error("Error While close the connection. {}", e);
			}
		}
		// LogUtil.info("Number of transactions is: " + allUsers.size());
		return allScreenAudits;
	}

	public List<ScreenAudit> findAllScreenAudits(String searchUserName) {
		Connection connection = ConnectionManager.getBDCConnection();
		String sqlStr = "SELECT screenAudit.SCREEN_NAME, screenAudit.SCREEN_DESCRIPTION, screenAudit.APPLICATION_NAME, "
				+ " rules_.rule_name, u.USER_NAME, screenAudit.CREATIONDATE, screenAudit.ACTION "
				+ " FROM ESTATEMENT.SCREEN_AUDIT screenAudit,rules rules_,users u "
				+ " where rules_.rule_id = screenAudit.RULE_ID and u.USER_ID = screenAudit.CREATEDBYUSERID "
				+ " and upper(u.USER_NAME) = upper('" + searchUserName + "')" + " order by CREATIONDATE desc";

		List<ScreenAudit> allScreenAudits = new ArrayList<ScreenAudit>();
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sqlStr);
			if (resultSet.isBeforeFirst()) {
//				LogUtil.info("Data retrieved successfully and will deal with it.");
				while (resultSet.next()) {
					ScreenAudit screenAudit = new ScreenAudit();
					screenAudit.setScreenName(resultSet.getString(1));
					screenAudit.setScreenDescription(resultSet.getString(2));
					screenAudit.setApplicationName(resultSet.getString(3));
					screenAudit.setRoleName(resultSet.getString(4));
					screenAudit.setCreatedByUserName(resultSet.getString(5));
					screenAudit.setCreatedDate(resultSet.getDate(6));
					screenAudit.setAction(resultSet.getString(7));
					allScreenAudits.add(screenAudit);
				}
			} else {
//				LogUtil.info("There no data retrieved");
			}
//			LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		} catch (SQLException e) {
			System.out.println(e);
//			LogUtil.error("SQL Error {}.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				System.out.println(e);
				// LogUtil.error("Error While close the connection. {}", e);
			}
		}
		// LogUtil.info("Number of transactions is: " + allUsers.size());
		return allScreenAudits;
	}

	public List<RoleAudit> findAllroleAudits() {
		Connection connection = ConnectionManager.getBDCConnection();
		String sqlStr = "SELECT rulesAudit.RULE_NAME, rulesAudit.RULE_DESCRIPTION, rulesAudit.INSERT_FLAG, "
				+ "   rulesAudit.UPDATE_FLAG, rulesAudit.DELETE_FLAG, rulesAudit.VIEW_FLAG,  rulesAudit.USER_CREATION_FLAG, "
				+ "   user_.USER_NAME, rulesAudit.CREATION_DATE, rulesAudit.ACTION "
				+ "FROM ESTATEMENT.RULES_AUDIT rulesAudit,users user_ where user_.USER_ID = rulesAudit.CREATED_BY_USERID "
				+ "order by rulesAudit.CREATION_DATE desc";

		List<RoleAudit> allRoleAudits = new ArrayList<RoleAudit>();
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sqlStr);
			if (resultSet.isBeforeFirst()) {
//				LogUtil.info("Data retrieved successfully and will deal with it.");
				while (resultSet.next()) {
					RoleAudit roleAudit = new RoleAudit();
					roleAudit.setRoleName(resultSet.getString(1));
					roleAudit.setRoleDescription(resultSet.getString(2));
					roleAudit.setInsertFlag(resultSet.getInt(3));
					roleAudit.setUpdateFlag(resultSet.getInt(4));
					roleAudit.setDeleteFlag(resultSet.getInt(5));
					roleAudit.setViewFlag(resultSet.getInt(6));
					roleAudit.setUserCreationFlag(resultSet.getInt(7));
					roleAudit.setUserName(resultSet.getString(8));
					roleAudit.setCreatedDate(resultSet.getDate(9));
					roleAudit.setAction(resultSet.getString(10));
					allRoleAudits.add(roleAudit);
				}
			} else {
//				LogUtil.info("There no data retrieved");
			}
//			LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		} catch (SQLException e) {
			System.out.println(e);
//			LogUtil.error("SQL Error {}.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				System.out.println(e);
				// LogUtil.error("Error While close the connection. {}", e);
			}
		}
		// LogUtil.info("Number of transactions is: " + allUsers.size());
		return allRoleAudits;
	}

	public List<RoleAudit> findAllroleAudits(String searchByUserName) {
		Connection connection = ConnectionManager.getBDCConnection();
		String sqlStr = "SELECT rulesAudit.RULE_NAME, rulesAudit.RULE_DESCRIPTION, rulesAudit.INSERT_FLAG, "
				+ " rulesAudit.UPDATE_FLAG, rulesAudit.DELETE_FLAG, rulesAudit.VIEW_FLAG,  rulesAudit.USER_CREATION_FLAG, "
				+ " user_.USER_NAME, rulesAudit.CREATION_DATE, rulesAudit.ACTION "
				+ " FROM ESTATEMENT.RULES_AUDIT rulesAudit,users user_ where user_.USER_ID = rulesAudit.CREATED_BY_USERID "
				+ " and upper(user_.USER_NAME) = upper('" + searchByUserName + "')"
				+ " order by rulesAudit.CREATION_DATE desc";

		List<RoleAudit> allRoleAudits = new ArrayList<RoleAudit>();
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sqlStr);
			if (resultSet.isBeforeFirst()) {
//				LogUtil.info("Data retrieved successfully and will deal with it.");
				while (resultSet.next()) {
					RoleAudit roleAudit = new RoleAudit();
					roleAudit.setRoleName(resultSet.getString(1));
					roleAudit.setRoleDescription(resultSet.getString(2));
					roleAudit.setInsertFlag(resultSet.getInt(3));
					roleAudit.setUpdateFlag(resultSet.getInt(4));
					roleAudit.setDeleteFlag(resultSet.getInt(5));
					roleAudit.setViewFlag(resultSet.getInt(6));
					roleAudit.setUserCreationFlag(resultSet.getInt(7));
					roleAudit.setUserName(resultSet.getString(8));
					roleAudit.setCreatedDate(resultSet.getDate(9));
					roleAudit.setAction(resultSet.getString(10));
					allRoleAudits.add(roleAudit);
				}
			} else {
//				LogUtil.info("There no data retrieved");
			}
//			LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		} catch (SQLException e) {
			System.out.println(e);
//			LogUtil.error("SQL Error {}.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				System.out.println(e);
				// LogUtil.error("Error While close the connection. {}", e);
			}
		}
		// LogUtil.info("Number of transactions is: " + allUsers.size());
		return allRoleAudits;
	}

	public List<UserRoleAudit> findAllUserRoleAudits() {
		Connection connection = ConnectionManager.getBDCConnection();
		String sqlStr = "SELECT r.RULE_NAME , u.USER_NAME, userRoleAudit.CREATIONDATE, cu.USER_NAME, userRoleAudit.ACTION "
				+ "FROM ESTATEMENT.USER_ROLES_AUDIT userRoleAudit,rules r, users u,users cu "
				+ "where r.RULE_ID = userRoleAudit.RULE_ID and u.USER_ID = userRoleAudit.USER_ID "
				+ "and cu.USER_ID = userRoleAudit.CREATED_BY_USERID " + "order by userRoleAudit.CREATIONDATE desc ";

		List<UserRoleAudit> allUserRoleAudits = new ArrayList<UserRoleAudit>();
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sqlStr);
			if (resultSet.isBeforeFirst()) {
//				LogUtil.info("Data retrieved successfully and will deal with it.");
				while (resultSet.next()) {
					UserRoleAudit userRoleAudit = new UserRoleAudit();
					userRoleAudit.setRoleName(resultSet.getString(1));
					userRoleAudit.setUserName(resultSet.getString(2));
					userRoleAudit.setCreationDate(resultSet.getDate(3));
					userRoleAudit.setCreatedByUserName(resultSet.getString(4));
					userRoleAudit.setAction(resultSet.getString(5));
					allUserRoleAudits.add(userRoleAudit);
				}
			} else {
//				LogUtil.info("There no data retrieved");
			}
//			LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		} catch (SQLException e) {
			System.out.println(e);
//			LogUtil.error("SQL Error {}.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				System.out.println(e);
				// LogUtil.error("Error While close the connection. {}", e);
			}
		}
		// LogUtil.info("Number of transactions is: " + allUsers.size());
		return allUserRoleAudits;
	}

	public List<UserRoleAudit> findAllUserRoleAudits(String searchUserName) {
		Connection connection = ConnectionManager.getBDCConnection();
		String sqlStr = "SELECT r.RULE_NAME , u.USER_NAME, userRoleAudit.CREATIONDATE, cu.USER_NAME, userRoleAudit.ACTION "
				+ " FROM ESTATEMENT.USER_ROLES_AUDIT userRoleAudit,rules r, users u,users cu "
				+ " where r.RULE_ID = userRoleAudit.RULE_ID and u.USER_ID = userRoleAudit.USER_ID "
				+ " and cu.USER_ID = userRoleAudit.CREATED_BY_USERID and  \r\n" + " (upper(cu.USER_NAME) = upper('"
				+ searchUserName + "') or upper(u.USER_NAME) = upper('" + searchUserName + "'))  "
				+ " order by userRoleAudit.CREATIONDATE desc ";

		List<UserRoleAudit> allUserRoleAudits = new ArrayList<UserRoleAudit>();
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sqlStr);
			if (resultSet.isBeforeFirst()) {
//				LogUtil.info("Data retrieved successfully and will deal with it.");
				while (resultSet.next()) {
					UserRoleAudit userRoleAudit = new UserRoleAudit();
					userRoleAudit.setRoleName(resultSet.getString(1));
					userRoleAudit.setUserName(resultSet.getString(2));
					userRoleAudit.setCreationDate(resultSet.getDate(3));
					userRoleAudit.setCreatedByUserName(resultSet.getString(4));
					userRoleAudit.setAction(resultSet.getString(5));
					allUserRoleAudits.add(userRoleAudit);
				}
			} else {
//				LogUtil.info("There no data retrieved");
			}
//			LogUtil.debug("SQL Statement: " + sqlStr.replace("'", ""));
		} catch (SQLException e) {
			System.out.println(e);
//			LogUtil.error("SQL Error {}.", e);
		} finally {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				System.out.println(e);
				// LogUtil.error("Error While close the connection. {}", e);
			}
		}
		// LogUtil.info("Number of transactions is: " + allUsers.size());
		return allUserRoleAudits;
	}
}
