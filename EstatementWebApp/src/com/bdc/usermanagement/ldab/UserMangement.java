package com.bdc.usermanagement.ldab;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bdc.estatement.util.Utils;

/**
 * Servlet implementation class UserMangement
 */
@WebServlet("/UserMangement")
public class UserMangement extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		session.removeAttribute("allUser");
		session.removeAttribute("allUserRules");
		if (request.getParameter("approveUserIDHidden") != null
				&& !request.getParameter("approveUserIDHidden").isEmpty()) {
			int userID = Integer.parseInt(request.getParameter("approveUserIDHidden").toString());
			int approvedByUserID = Integer.parseInt(session.getAttribute("USER_Name").toString());
			UserManagmentDBTransactions dbTransactions = new UserManagmentDBTransactions();
			int status = dbTransactions.approveUser(userID, approvedByUserID, 1);
			System.out.println("status [" + status + "]");
			response.sendRedirect(request.getContextPath() + "/main/pages/tables/UserManagement.jsp");
			return;
		}

		if (session.getAttribute("USER_ID") == null || session.getAttribute("USER_ID").equals("")
				|| session.getAttribute("USER_ID").toString().isEmpty()) {
			response.sendRedirect(request.getContextPath() + "/main/pages/tables/Login.jsp");
			return;
		} else if (request.getParameter("export") != null) {
			// export
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename=Users.xls");
			UserManagmentDBTransactions dbQueries = new UserManagmentDBTransactions();
			Utils.exportToExcelUsers(dbQueries.findAllUsers(), response);
			return;
		} else {
			response.sendRedirect(request.getContextPath() + "/main/pages/tables/UserManagement.jsp");
			return;
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("USER_ID") == null || session.getAttribute("USER_ID").equals("")
				|| session.getAttribute("USER_ID").toString().isEmpty()) {
//			response.sendRedirect(request.getContextPath() + "/JSPs/Login.jsp");
			response.sendRedirect(request.getContextPath() + "/main/pages/tables/Login.jsp");
			return;
		} else if (request.getParameter("reset") != null && request.getParameter("reset").equals("1")) {

			session.removeAttribute("allUser");
			session.removeAttribute("allUserRules");
//			response.sendRedirect(request.getContextPath() + "/JSPs/UserManagement.jsp");
			response.sendRedirect(request.getContextPath() + "/main/pages/tables/UserManagement.jsp");
			return;
		} else
			doAction(request, response);
	}

	public void doAction(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("testing do action");
		HttpSession session = request.getSession();
		User user = new User();
		UserManagmentDBTransactions dbTransactions = new UserManagmentDBTransactions();

		if (request.getParameter("approveUserIDHidden") != null && request.getParameter("actionDescription") != null
				&& !request.getParameter("actionDescription").toString().isEmpty()) {

			user.setUserID(Integer.valueOf(request.getParameter("approveUserIDHidden").toString()));
			user.setCreatedByUserID(Integer.valueOf(session.getAttribute("USER_ID").toString()));
			user.setDescription("");
			if (request.getParameter("actionDescription").toString().equals("Mark as Locked"))
				user.setFlagLocked(0);
			else if (request.getParameter("actionDescription").toString().equals("Remove Lock"))
				user.setFlagLocked(1);
			if (request.getParameter("actionDescription").toString().equals("Mark as Locked")
					|| request.getParameter("actionDescription").toString().equals("Remove Lock")) {

				int status = dbTransactions.changeUserLockStatus(user);
				if (status > 0) {
					session.setAttribute("result", "Request Rejected Successfully");
					response.sendRedirect(request.getContextPath() + "/main/pages/tables/UserManagement.jsp");
					return;
				} else {
					session.setAttribute("result", "Request dose not Rejected");
					response.sendRedirect(request.getContextPath() + "/main/pages/tables/UserManagement.jsp");
					return;
				}

			} else if (request.getParameter("actionDescription").toString().equals("Delete User")) {
				int status = dbTransactions.rejectUserDeletion(user);
				if (status > 0) {
					session.setAttribute("result", "Request Rejected Successfully");
					response.sendRedirect(request.getContextPath() + "/main/pages/tables/UserManagement.jsp");
					return;
				} else {
					session.setAttribute("result", "Request dose not Rejected");
					response.sendRedirect(request.getContextPath() + "/main/pages/tables/UserManagement.jsp");
					return;
				}
			} else if (request.getParameter("actionDescription").toString().equals("Create New User")) {
				int status = dbTransactions.deleteUser(user);
				if (status > 0) {
					session.setAttribute("result", "Request Rejected Successfully");
					response.sendRedirect(request.getContextPath() + "/main/pages/tables/UserManagement.jsp");
					return;
				} else {
					session.setAttribute("result", "Request dose not Rejected");
					response.sendRedirect(request.getContextPath() + "/main/pages/tables/UserManagement.jsp");
					return;
				}
			}
		}

		if (request.getParameter("approveUserIDHidden") != null
				&& !request.getParameter("approveUserIDHidden").isEmpty()
				&& request.getParameter("flagImmediateDelete") != null
				&& !request.getParameter("flagImmediateDelete").isEmpty()
				&& request.getParameter("flagImmediateDelete").equals("1")) {

			user.setUserID(Integer.valueOf(request.getParameter("approveUserIDHidden").toString()));

			dbTransactions.editUserWithoutTrigger(user.getUserID(), user.getCreatedByUserID());
			int status = dbTransactions.deleteUser(user);
			request.setAttribute("flagImmediateDelete", "0");
			request.setAttribute("approveUserIDHidden", "1");
			if (status > 0) {
				session.setAttribute("result", "User Deleted Successfully");
				response.sendRedirect(request.getContextPath() + "/main/pages/tables/UserManagement.jsp");
				return;
			} else {
				session.setAttribute("result", "User dose not Deleted");
				response.sendRedirect(request.getContextPath() + "/main/pages/tables/UserManagement.jsp");
				return;
			}

		} else if (request.getParameter("approveUserIDHidden") != null
				&& !request.getParameter("approveUserIDHidden").isEmpty()) {
			int userID = Integer.parseInt(request.getParameter("approveUserIDHidden").toString());

			int approvedByUserID = Integer.parseInt(session.getAttribute("USER_ID").toString());
			int status = dbTransactions.approveUser(userID, approvedByUserID, 1);
			System.out.println("status [" + status + "]");
			response.sendRedirect(request.getContextPath() + "/main/pages/tables/UserManagement.jsp");
			return;
		}

		session.removeAttribute("allUser");
		session.removeAttribute("allUserRules");

		if ((request.getParameter("deleteFlag") == null || request.getParameter("deleteFlag").equals("0"))
				&& request.getParameter("userName").isEmpty()) {
			session.setAttribute("result", "User Name Is Required");
			response.sendRedirect(request.getContextPath() + "/main/pages/tables/UserManagement.jsp");
			return;
		}
		user.setUserName(request.getParameter("userName"));
//		user.setMail(request.getParameter("mail"));
//		user.setBranch(request.getParameter("branch"));
		System.out.println(request.getParameter("locked"));
		if (request.getParameter("locked") != null && request.getParameter("locked").equals("on")) {
			user.setFlagLocked(1);
			user.setDescription("Mark as Locked");
		}
		if (request.getParameter("disabled") != null && request.getParameter("disabled").equals("on")) {
			user.setFlagDisabled(1);
			user.setDescription("Mark as Disabled");
		}
		if (request.getParameter("disabled") != null && request.getParameter("disabled").equals("on")
				&& request.getParameter("locked") != null && request.getParameter("locked").equals("on"))
			user.setDescription("Mark as Locked And Disabled");
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy-HH:mm:ss");
		Date date = new Date();

		user.setCreatedDate(dateFormat.format(date));
		// here i need to edit it later after i do the login page
		System.out.println("user ID: " + session.getAttribute("USER_ID").toString());
		user.setCreatedByUserID(Integer.valueOf(session.getAttribute("USER_ID").toString()));
		int status;

		if (request.getParameter("deleteFlag") != null && request.getParameter("deleteFlag").equals("1")) {
			user.setUserID(Integer.valueOf(request.getParameter("hid_del_userID").toString()));

			// same user cannot delete his roles
			if (session.getAttribute("USER_ID") != null && user.getUserID() != 0
					&& Integer.valueOf(session.getAttribute("USER_ID").toString()) == user.getUserID()) {
				session.setAttribute("result", "Sorry You Cannot Delete Yourself");
				response.sendRedirect(request.getContextPath() + "/main/pages/tables/UserManagement.jsp");
				return;
			}

			dbTransactions.editUserWithoutTrigger(user.getUserID(), user.getCreatedByUserID());
			status = dbTransactions.editDeleteFlag(user, 1);
			// status = dbTransactions.deleteUser(user);
			request.setAttribute("userIDHidden", "0");
			request.setAttribute("deleteFlag", "0");
			if (status > 0) {
				session.setAttribute("result", "User Deleted Successfully");
				response.sendRedirect(request.getContextPath() + "/main/pages/tables/UserManagement.jsp");
				return;
			} else {
				session.setAttribute("result", "User dose not Deleted");
				response.sendRedirect(request.getContextPath() + "/main/pages/tables/UserManagement.jsp");
				return;
			}

		} else if (request.getParameter("userIDHidden") == null || request.getParameter("userIDHidden").isEmpty()
				|| request.getParameter("userIDHidden").equals("0")) {
			User existUser = dbTransactions.getUser(user.getUserName());
			if (existUser.getUserID() != 0) {
				session.setAttribute("result", "User Alread Exist");
				response.sendRedirect(request.getContextPath() + "/main/pages/tables/UserManagement.jsp");
				return;
			}
			try {
				System.out.println(session.getAttribute("USER_Name"));
				boolean ldapFlag = checkLdapUser(session.getAttribute("USER_Name").toString(),
						session.getAttribute("Password").toString(), user.getUserName());
				if (!ldapFlag) {
					session.setAttribute("result", "User : " + user.getUserName() + " Dose not exists in ldap Users");
					response.sendRedirect(request.getContextPath() + "/main/pages/tables/UserManagement.jsp");
					return;
				}
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println(e);
			}

			status = dbTransactions.createUser(user);
			request.setAttribute("userIDHidden", "0");

			if (status > 0) {
				session.setAttribute("result", "User Created Successfully");
				response.sendRedirect(request.getContextPath() + "/main/pages/tables/UserManagement.jsp");
				return;
			} else {
				session.setAttribute("result", "User dose not Created");
				response.sendRedirect(request.getContextPath() + "/main/pages/tables/UserManagement.jsp");
				return;
			}

		} else {

			try {
				System.out.println(session.getAttribute("USER_Name"));
				boolean ldapFlag = checkLdapUser(session.getAttribute("USER_Name").toString(),
						session.getAttribute("Password").toString(), user.getUserName());
				if (!ldapFlag) {
					session.setAttribute("result", "User : " + user.getUserName() + " Dose not exists in ldap Users");
					response.sendRedirect(request.getContextPath() + "/main/pages/tables/UserManagement.jsp");
					return;
				}
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println(e);
			}

			user.setUserID(Integer.valueOf(request.getParameter("userIDHidden")));
			user.setApproveFlag(0);
			user.setCreatedByUserID(Integer.valueOf(session.getAttribute("USER_ID").toString()));
			if (user.getDescription() == null && (user.getFlagDisabled() == 0 || user.getFlagLocked() == 0))
				user.setDescription("Remove Lock");
			status = dbTransactions.editUser(user);
			request.setAttribute("userIDHidden", "0");
			if (status > 0) {
				session.setAttribute("result", "User Updated Successfully");
				response.sendRedirect(request.getContextPath() + "/main/pages/tables/UserManagement.jsp");
				return;
			} else {
				session.setAttribute("result", "User dose not Updated");
				response.sendRedirect(request.getContextPath() + "/main/pages/tables/UserManagement.jsp");
				return;
			}
		}
	}

	///////////////////////////////////////

	private boolean checkLdapUser(String ldapUsername, String ldapPassword, String ldapAccountToLookup)
			throws NamingException {

		String ldapAdServer = "ldap://172.17.25.3:389";
		final String ladpDomain = "@bdc.bank.local";
		final String ldapSearchBase = "DC=BDC,DC=bank,DC=local";

		Hashtable<String, Object> env = new Hashtable<String, Object>();
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		if (ldapUsername != null) {
			env.put(Context.SECURITY_PRINCIPAL, ldapUsername);
		}
		if (ldapPassword != null) {
			env.put(Context.SECURITY_CREDENTIALS, ldapPassword);
		}
		env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL, ldapAdServer);

		// ensures that objectSID attribute values
		// will be returned as a byte[] instead of a String
		env.put("java.naming.ldap.attributes.binary", "objectSID");

		// the following is helpful in debugging errors
		// env.put("com.sun.jndi.ldap.trace.ber", System.err);

		LdapContext ctx = new InitialLdapContext();
		// 1) lookup the ldap account
		SearchResult srLdapUser = findAccountByAccountName(ctx, ldapSearchBase, ldapAccountToLookup, ladpDomain,
				ldapAdServer, ldapUsername, ldapPassword);
		System.out.println(srLdapUser);
		if (srLdapUser == null) {
			return false;
		}
		return true;
	}

	public SearchResult findAccountByAccountName(DirContext ctx, String ldapSearchBase, String accountName,
			String ladpDomain, String ldapAdServer, String userName, String Password) throws NamingException {

		String searchFilter = "(&(objectClass=user)(sAMAccountName=" + accountName + "))";
		SearchControls searchControls = new SearchControls();
		searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		ctx = getLdapContext(userName, Password, ladpDomain, ldapAdServer);
		NamingEnumeration<SearchResult> results = ctx.search(ldapSearchBase, searchFilter, searchControls);

		SearchResult searchResult = null;
		if (results.hasMoreElements()) {
			searchResult = results.nextElement();

			// make sure there is not another item available, there should be only 1 match
			if (results.hasMoreElements()) {
				System.err.println("Matched multiple users for the accountName: " + accountName);
				return null;
			}
		}

		return searchResult;
	}

	public LdapContext getLdapContext(String username, String password, String ladpDomain, String ldapAdServer) {
		LdapContext ctx = null;
		try {
			// Log.info(getClass(),"Username:"+username);

			Hashtable<String, Object> env = new Hashtable<String, Object>();
			env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
			env.put(Context.SECURITY_AUTHENTICATION, "Simple");

			// NOTE: replace user@domain.com with a User that is present in your
			// Active Directory/LDAP
			env.put(Context.SECURITY_PRINCIPAL, username + ladpDomain);
			// NOTE: replace userpass with passwd of this user.
			env.put(Context.SECURITY_CREDENTIALS, password);
			// NOTE: replace ADorLDAPHost with your Active Directory/LDAP
			// Hostname or IP.
			env.put(Context.PROVIDER_URL, ldapAdServer);

			ctx = new InitialLdapContext(env, null);
			// Log.info(getClass(),"Connection done");
		} catch (NamingException nex) {
			// Log.info(getClass(),"LDAP Connection: FAILED");
			nex.printStackTrace();
		}
		return ctx;
	}

}
