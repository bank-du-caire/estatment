package com.bdc.usermanagement.ldab;

import java.util.Date;

public class ScreenAudit {
	private String screenName;
	private String screenDescription;
	private String applicationName;
	private String roleName;
	private String CreatedByUserName;
	private Date createdDate;
	private String action;

	public String getScreenName() {
		return screenName;
	}

	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

	public String getScreenDescription() {
		return screenDescription;
	}

	public void setScreenDescription(String screenDescription) {
		this.screenDescription = screenDescription;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getCreatedByUserName() {
		return CreatedByUserName;
	}

	public void setCreatedByUserName(String createdByUserName) {
		CreatedByUserName = createdByUserName;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

}
