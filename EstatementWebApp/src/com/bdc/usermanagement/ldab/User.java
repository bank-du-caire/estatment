package com.bdc.usermanagement.ldab;

import java.util.Date;

public class User {

	private int userID;
	private String userName;
	private String mail;
	private String branch;
	private String lastLoginDate;
	private int noFailedLogin;
	private Date lastFailedLogin;
	private int flagLocked;
	private int flagDisabled;
	private int flagDeleted;
	private String createdDate;
	private int createdByUserID;
	private String CreatedByUserName;
	private int approveFlag;
	private String description;

	public User() {
		userID = 0;
		userName = "";
		mail = "";
		branch = "";
		lastLoginDate = null;
		noFailedLogin = 0;
		lastFailedLogin = null;
		flagLocked = 0;
		flagDisabled = 0;
		flagDeleted = 0;
		createdDate = "";
		createdByUserID = 0;
	}

	public int getUserID() {
		return userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

	public int getNoFailedLogin() {
		return noFailedLogin;
	}

	public void setNoFailedLogin(int noFailedLogin) {
		this.noFailedLogin = noFailedLogin;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getLastLoginDate() {
		return lastLoginDate;
	}

	public void setLastLoginDate(String lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}

	public Date getLastFailedLogin() {
		return lastFailedLogin;
	}

	public void setLastFailedLogin(Date lastFailedLogin) {
		this.lastFailedLogin = lastFailedLogin;
	}

	public int getFlagLocked() {
		return flagLocked;
	}

	public void setFlagLocked(int flagLocked) {
		this.flagLocked = flagLocked;
	}

	public int getFlagDisabled() {
		return flagDisabled;
	}

	public void setFlagDisabled(int flagDisabled) {
		this.flagDisabled = flagDisabled;
	}

	public int getFlagDeleted() {
		return flagDeleted;
	}

	public void setFlagDeleted(int flagDeleted) {
		this.flagDeleted = flagDeleted;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public int getCreatedByUserID() {
		return createdByUserID;
	}

	public void setCreatedByUserID(int createdByUserID) {
		this.createdByUserID = createdByUserID;
	}

	public String getCreatedByUserName() {
		return CreatedByUserName;
	}

	public void setCreatedByUserName(String createdByUserName) {
		CreatedByUserName = createdByUserName;
	}

	public int getApproveFlag() {
		return approveFlag;
	}

	public void setApproveFlag(int approveFlag) {
		this.approveFlag = approveFlag;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
