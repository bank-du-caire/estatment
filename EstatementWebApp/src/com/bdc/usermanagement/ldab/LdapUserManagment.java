package com.bdc.usermanagement.ldab;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

public class LdapUserManagment {

	private static final long serialVersionUID = 1L;

	private static final String ladpURL = "ldap://172.17.25.3:389";

	private static final String ladpDC = "DC=BDC,DC=bank,DC=local";

	private static final String ladpDomain = "@bdc.bank.local";

	LdapUserInfo user = null;
	Attribute attr = null;
	LdapContext ctx = null;

	LdapUserInfo getUser(String username, String password)

	{
		try {
			user = new LdapUserInfo();

			SearchControls constraints = new SearchControls();
			constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
			// NOTE: The attributes mentioned in array below are the ones that
			// will be retrieved, you can add more.
			String[] attrIDs = { "distinguishedName", "sn", "givenname",
					"mail", "telephonenumber", "canonicalName",
					"userAccountControl", "accountExpires", "whenCreated",
					"co", "countryCode" };

			constraints.setReturningAttributes(attrIDs);

			// NOTE: replace DC=domain,DC=com below with your domain info. It is
			// essentially the Base Node for Search.

			ctx = getLdapContext(username, password);

			if (ctx == null) {

				return null;
			}

			NamingEnumeration<SearchResult> answer = ctx.search(ladpDC,
					"sAMAccountName=" + username, constraints);

			if (answer.hasMore()) {
				Attributes attrs = ((SearchResult) answer.next())
						.getAttributes();

				attr = attrs.get("givenname");
				if (attr != null) {
					user.setGiveName(attr.get().toString());
				}

				attr = attrs.get("sn");
				if (attr != null) {
					user.setSurName(attr.get().toString());
				}

				attr = attrs.get("mail");
				if (attr != null) {
					user.setEmail(attr.get().toString());
				}

			} else {
				return null;
			}

		} catch (Exception e) {
		}

		finally {
			try {
				ctx.close();
			} catch (Exception e) {
			}
		}

		return user;

	}

	public LdapContext getLdapContext(String username, String password) {
		LdapContext ctx = null;
		try {
			// Log.info(getClass(),"Username:"+username);

			Hashtable<String, Object> env = new Hashtable<String, Object>();
			env.put(Context.INITIAL_CONTEXT_FACTORY,
					"com.sun.jndi.ldap.LdapCtxFactory");
			env.put(Context.SECURITY_AUTHENTICATION, "Simple");

			// NOTE: replace user@domain.com with a User that is present in your
			// Active Directory/LDAP
			env.put(Context.SECURITY_PRINCIPAL, username + ladpDomain);
			// NOTE: replace userpass with passwd of this user.
			env.put(Context.SECURITY_CREDENTIALS, password);
			// NOTE: replace ADorLDAPHost with your Active Directory/LDAP
			// Hostname or IP.
			env.put(Context.PROVIDER_URL, ladpURL);

			ctx = new InitialLdapContext(env, null);
			// Log.info(getClass(),"Connection done");
		} catch (NamingException nex) {
			// Log.info(getClass(),"LDAP Connection: FAILED");
			nex.printStackTrace();
		}
		return ctx;
	}

	public boolean isLDAPUsser(String username, String password) {
		LdapContext ctx = null;
		boolean isFnd = false;
		try {
			// Log.info(getClass(),"Username:"+username);

			Hashtable<String, Object> env = new Hashtable<String, Object>();
			env.put(Context.INITIAL_CONTEXT_FACTORY,
					"com.sun.jndi.ldap.LdapCtxFactory");
			env.put(Context.SECURITY_AUTHENTICATION, "Simple");

			// NOTE: replace user@domain.com with a User that is present in your
			// Active Directory/LDAP
			env.put(Context.SECURITY_PRINCIPAL, username + ladpDomain);
			// NOTE: replace userpass with passwd of this user.
			env.put(Context.SECURITY_CREDENTIALS, password);
			// NOTE: replace ADorLDAPHost with your Active Directory/LDAP
			// Hostname or IP.
			env.put(Context.PROVIDER_URL, ladpURL);

			ctx = new InitialLdapContext(env, null);
			// Log.info(getClass(),"Connection done");
			isFnd = true;
		} catch (NamingException nex) {
			isFnd = false;
			// Log.info(getClass(),"LDAP Connection: FAILED");
			nex.printStackTrace();
		} finally {
			try {
				if (ctx != null) {
					ctx.close();
					isFnd = true;
				}
			} catch (Exception e) {
			}
		}
		return isFnd;
	}

}
