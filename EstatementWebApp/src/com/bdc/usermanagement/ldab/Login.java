package com.bdc.usermanagement.ldab;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public Login() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doAction(request, response);
	}

	public void doAction(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String userName = request.getParameter("userName");
		String passsword = request.getParameter("password");
//		LdapUserManagment ldapUserManagment = new LdapUserManagment();
//		boolean result = ldapUserManagment.isLDAPUsser(userName, passsword);
		boolean result = true;
		HttpSession session = request.getSession();
		session.setMaxInactiveInterval(30 * 60);
		System.out.println("result:  " + result);
		UserManagmentDBTransactions dbTransactions = new UserManagmentDBTransactions();
		if (result) {
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy-HH:mm:ss");
			Date todayDate = new Date();

			session.removeAttribute("loginAttemptsCount");
			System.out.println("LDAP check is correct");
//			User logedInUser = dbTransactions.getUser(userName);
//			logedInUser.setLastLoginDate(dateFormat.format(todayDate));
//			dbTransactions.updateLastLoginDate(logedInUser);
//			if (logedInUser.getUserID() == 0) {
//				session.setAttribute("result", "Wrong User Name or Password");
//				response.sendRedirect(request.getContextPath() + "/main/pages/tables/Login.jsp");
//			} else if (logedInUser.getFlagDeleted() == 1) {
//				session.setAttribute("result", "This User Name was deleted!!");
//				response.sendRedirect(request.getContextPath() + "/main/pages/tables/Login.jsp");
//			} else if (logedInUser.getApproveFlag() == 0) {
//				session.setAttribute("result", "This User Not Approved Yet");
//				response.sendRedirect(request.getContextPath() + "/main/pages/tables/Login.jsp");
//			} else if (logedInUser.getFlagDisabled() == 1) {
//				session.setAttribute("result", "This User Name was disabled!!");
//				response.sendRedirect(request.getContextPath() + "/main/pages/tables/Login.jsp");
//			} else if (logedInUser.getFlagLocked() == 1) {
//				session.setAttribute("result", "This User Name was Locked!!");
//				response.sendRedirect(request.getContextPath() + "/main/pages/tables/Login.jsp");
//			}
//
//			else {
				session.setAttribute("USER_ID", 223);
				session.setAttribute("USER_Name", request.getParameter("userName"));
				session.setAttribute("Password", request.getParameter("password"));
//				UserPrivilege userPrivilege = UserPrivilege.findAllUserPrivileges("view_accounts.jsp", request);
				UserPrivilege userPrivilege = new UserPrivilege();
				userPrivilege.setInsertFlag(1);
				userPrivilege.setReportView(1);
				userPrivilege.setUpdateFlag(1);
				userPrivilege.setUserCreation(1);
				userPrivilege.setViewFlag(1);

//				boolean hostToHost = false;//dbTransactions.checkIfHostToHost(logedInUser.getUserID());
//				boolean meezaPrepaid = false;//dbTransactions.checkIfMeezaPrepaid(String.valueOf(logedInUser.getUserID()));
//				if (meezaPrepaid) {
//					response.sendRedirect(request.getContextPath() + "/main/pages/tables/Mezza_Reg_Customers.jsp");
//				} else if (hostToHost) {
//					response.sendRedirect(request.getContextPath() + "/main/pages/tables/HosttoHost.jsp");
//				} else if (userPrivilege.getUserCreation() == 1) {
//					response.sendRedirect(request.getContextPath() + "/main/pages/tables/UserManagement.jsp");
//				} else if (userPrivilege.getReportView() == 1) {
//					response.sendRedirect(request.getContextPath() + "/main/pages/tables/AccountsAudit.jsp");
//				} else
//					
					response.sendRedirect(request.getContextPath() + "/main/pages/tables/view_accounts.jsp");
//			}

		}
//		else {
//			int loginAttemptsCount = session.getAttribute("loginAttemptsCount") != null
//					? Integer.valueOf(session.getAttribute("loginAttemptsCount").toString())
//					: 0;
//			session.setAttribute("loginAttemptsCount", ++loginAttemptsCount);
//			if (loginAttemptsCount >= 3) {
//				User user = dbTransactions.getUser(userName);
//				user.setUserName(userName);
//				user.setFlagLocked(1);
//				dbTransactions.editLockUser(user);
//			}
//			System.out.println("LDAP check is not correct");
//
//			session.setAttribute("result", "Wrong User Name or Password");
//			response.sendRedirect(request.getContextPath() + "/main/pages/tables/Login.jsp");
//		}
	}
}
