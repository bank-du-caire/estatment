package com.bdc.usermanagement.ldab;

public class Screen {
	private int screeID;
	private String screenName;
	private String screenDescription;
	private String applicationName;
	private int ruleID;
	private int createdByUserID;
	
	public void Screen() {
		screenName = "";
		screenDescription = "";
		applicationName = "";
	}

	public int getScreeID() {
		return screeID;
	}

	public void setScreeID(int screeID) {
		this.screeID = screeID;
	}

	public String getScreenName() {
		return screenName;
	}

	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

	public String getScreenDescription() {
		return screenDescription;
	}

	public void setScreenDescription(String screenDescription) {
		this.screenDescription = screenDescription;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public int getRuleID() {
		return ruleID;
	}

	public void setRuleID(int ruleID) {
		this.ruleID = ruleID;
	}

	public int getCreatedByUserID() {
		return createdByUserID;
	}

	public void setCreatedByUserID(int createdByUserID) {
		this.createdByUserID = createdByUserID;
	}

}
