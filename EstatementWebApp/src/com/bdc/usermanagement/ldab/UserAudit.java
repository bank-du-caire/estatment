package com.bdc.usermanagement.ldab;

import java.util.Date;

public class UserAudit {

	private String userName;
	private int flagLocked;
	private int flagDisable;
	private String action;
	private Date createdDate;
	private String createdByUserName;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getFlagLocked() {
		return flagLocked;
	}

	public void setFlagLocked(int flagLocked) {
		this.flagLocked = flagLocked;
	}

	public int getFlagDisable() {
		return flagDisable;
	}

	public void setFlagDisable(int flagDisable) {
		this.flagDisable = flagDisable;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedByUserName() {
		return createdByUserName;
	}

	public void setCreatedByUserName(String createdByUserName) {
		this.createdByUserName = createdByUserName;
	}

}
