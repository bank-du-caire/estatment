package com.bdc.usermanagement.ldab;

public class Rule {

	private int ruleID;
	private String ruleName;
	private String ruleDescription;
	private int insertFlag;
	private int updateFlag;
	private int deleteFlag;
	private int viewFlag;
	private int userCreationFlag;
	private int viewReportRoleFlag;
	private String createdDate;
	private int createdByUserID;
	private String createdByUserName;

	public void Rule() {

		ruleName = "";
		ruleDescription = "";
		createdDate = "";
	}

	public int getRuleID() {
		return ruleID;
	}

	public void setRuleID(int ruleID) {
		this.ruleID = ruleID;
	}

	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

	public String getRuleDescription() {
		return ruleDescription;
	}

	public void setRuleDescription(String ruleDescription) {
		this.ruleDescription = ruleDescription;
	}

	public int getInsertFlag() {
		return insertFlag;
	}

	public void setInsertFlag(int insertFlag) {
		this.insertFlag = insertFlag;
	}

	public int getUpdateFlag() {
		return updateFlag;
	}

	public void setUpdateFlag(int updateFlag) {
		this.updateFlag = updateFlag;
	}

	public int getDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(int deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public int getViewFlag() {
		return viewFlag;
	}

	public void setViewFlag(int viewFlag) {
		this.viewFlag = viewFlag;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedByUserName() {
		return createdByUserName;
	}

	public void setCreatedByUserName(String createdByUserName) {
		this.createdByUserName = createdByUserName;
	}

	public int getUserCreationFlag() {
		return userCreationFlag;
	}

	public void setUserCreationFlag(int userCreationFlag) {
		this.userCreationFlag = userCreationFlag;
	}

	public int getCreatedByUserID() {
		return createdByUserID;
	}

	public void setCreatedByUserID(int createdByUserID) {
		this.createdByUserID = createdByUserID;
	}

	public int getViewReportRoleFlag() {
		return viewReportRoleFlag;
	}

	public void setViewReportRoleFlag(int viewReportRoleFlag) {
		this.viewReportRoleFlag = viewReportRoleFlag;
	}

}
