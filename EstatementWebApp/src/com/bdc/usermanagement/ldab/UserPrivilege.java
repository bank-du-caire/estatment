package com.bdc.usermanagement.ldab;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class UserPrivilege {
	private int insertFlag;
	private int updateFlag;
	private int viewFlag;
	private int deleteFlag;
	private int userCreation;
	private int reportView;

	public static UserPrivilege findAllUserPrivileges(String screenName, HttpServletRequest request) {
		HttpSession session = request.getSession();
		UserPrivilege userPrivilege = new UserPrivilege();
		if (session.getAttribute("USER_ID") != null) {

			UserManagmentDBTransactions dbTransactions = new UserManagmentDBTransactions();
			List<UserPrivilege> allUserPrivileges = dbTransactions
					.findUserPrivileges(Integer.valueOf(session.getAttribute("USER_ID").toString()), screenName);
			// stupid problem

			for (UserPrivilege privilege : allUserPrivileges) {
				if (privilege.getDeleteFlag() == 1)
					userPrivilege.setDeleteFlag(1);
				if (privilege.getInsertFlag() == 1)
					userPrivilege.setInsertFlag(1);
				if (privilege.getUpdateFlag() == 1)
					userPrivilege.setUpdateFlag(1);
				if (privilege.getViewFlag() == 1)
					userPrivilege.setViewFlag(1);
				if (privilege.getUserCreation() == 1)
					userPrivilege.setUserCreation(1);
				if (privilege.getReportView() == 1)
					userPrivilege.setReportView(1);
			}
		}
		return userPrivilege;
	}

	public static boolean checkIfHostToHostPrivilege(HttpServletRequest request) {
		HttpSession session = request.getSession();
		if (session.getAttribute("USER_ID") != null) {
			UserManagmentDBTransactions dbTransactions = new UserManagmentDBTransactions();
			boolean res = dbTransactions.checkIfHostToHost(Integer.valueOf(session.getAttribute("USER_ID").toString()));
			return res;
		}
		return false;
	}

	public int getInsertFlag() {
		return insertFlag;
	}

	public void setInsertFlag(int insertFlag) {
		this.insertFlag = insertFlag;
	}

	public int getUpdateFlag() {
		return updateFlag;
	}

	public void setUpdateFlag(int updateFlag) {
		this.updateFlag = updateFlag;
	}

	public int getViewFlag() {
		return viewFlag;
	}

	public void setViewFlag(int viewFlag) {
		this.viewFlag = viewFlag;
	}

	public int getDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(int deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public int getUserCreation() {
		return userCreation;
	}

	public void setUserCreation(int userCreation) {
		this.userCreation = userCreation;
	}

	public int getReportView() {
		return reportView;
	}

	public void setReportView(int reportView) {
		this.reportView = reportView;
	}

}
