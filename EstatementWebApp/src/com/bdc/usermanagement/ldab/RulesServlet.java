package com.bdc.usermanagement.ldab;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class RulesAndScreens
 */
@WebServlet("/RulesServlet")
public class RulesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public RulesServlet() {
		super();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("USER_ID") == null || session.getAttribute("USER_ID").equals("")
				|| session.getAttribute("USER_ID").toString().isEmpty()) {
			response.sendRedirect(request.getContextPath() + "/main/pages/tables/Login.jsp");
		} else
			doAction(request, response);
	}

	private void doAction(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Rule rule = new Rule();
		HttpSession session = request.getSession();
		rule.setRuleName(request.getParameter("ruleName"));
		rule.setRuleDescription(request.getParameter("ruleDescription"));
		if (request.getParameter("insertFlag") != null && request.getParameter("insertFlag").equals("on"))
			rule.setInsertFlag(1);
		if (request.getParameter("updateFlag") != null && request.getParameter("updateFlag").equals("on"))
			rule.setUpdateFlag(1);
		if (request.getParameter("deleteFlag") != null && request.getParameter("deleteFlag").equals("on"))
			rule.setDeleteFlag(1);
		if (request.getParameter("viewFlag") != null && request.getParameter("viewFlag").equals("on"))
			rule.setViewFlag(1);
		if (request.getParameter("userCreationFlag") != null && request.getParameter("userCreationFlag").equals("on"))
			rule.setUserCreationFlag(1);
		if (request.getParameter("viewReportRole") != null && request.getParameter("viewReportRole").equals("on"))
			rule.setViewReportRoleFlag(1);
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy-HH:mm:ss");
		Date date = new Date();
		rule.setCreatedDate(dateFormat.format(date));

		rule.setCreatedByUserID(Integer.valueOf(session.getAttribute("USER_ID").toString()));

		UserManagmentDBTransactions dbTransactions = new UserManagmentDBTransactions();
		int status = 0;
		if (request.getParameter("ruleIDHidden") == null || request.getParameter("ruleIDHidden").toString().equals("0")
				|| request.getParameter("ruleIDHidden").toString().isEmpty()) {
			status = dbTransactions.createRule(rule);
			if (status > 0) {
				session.setAttribute("ruleResult", "Rule Created Successfully");
				response.sendRedirect(request.getContextPath() + "/main/pages/tables/RulesAndScreens.jsp");
				return;
			} else {
				session.setAttribute("ruleResult", "Rule did not Created Successfully");
				response.sendRedirect(request.getContextPath() + "/main/pages/tables/RulesAndScreens.jsp");
				return;
			}
		} else if (request.getParameter("ruleIDHidden") != null
				&& !request.getParameter("ruleIDHidden").toString().equals("0")
				&& !request.getParameter("ruleIDHidden").toString().isEmpty()) {
			rule.setRuleID(Integer.valueOf(request.getParameter("ruleIDHidden").toString()));
			request.setAttribute("ruleIDHidden", null);
			status = dbTransactions.updateRule(rule);
			if (status > 0) {
				session.setAttribute("ruleResult", "Rule Updated Successfully");
				response.sendRedirect(request.getContextPath() + "/main/pages/tables/RulesAndScreens.jsp");
				return;
			} else {
				session.setAttribute("ruleResult", "Rule did not Updated Successfully");
				response.sendRedirect(request.getContextPath() + "/main/pages/tables/RulesAndScreens.jsp");
				return;
			}
		}
	}
}
