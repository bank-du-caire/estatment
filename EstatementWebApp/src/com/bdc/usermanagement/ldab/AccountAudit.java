package com.bdc.usermanagement.ldab;

import java.util.Date;

public class AccountAudit {

	private String customerCode;
	private String accountNumber;
	private int frequancyStmt;
	private String email;
	private String nextDate;
	private String description;
	private Date lastSentDate;
	private int noOfFaildAttemps;
	private String password;
	private String status;
	private String mobile;
	private String address;
	private String accountType;
	private String branchName;
	private String currencyName;
	private String customerName;
	private String fromDate;
	private String toDate;
	private String accountTypeFlag;

	private String createdBy;
	private String createdDate;
	private String action;

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public int getFrequancyStmt() {
		return frequancyStmt;
	}

	public void setFrequancyStmt(int frequancyStmt) {
		this.frequancyStmt = frequancyStmt;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNextDate() {
		return nextDate;
	}

	public void setNextDate(String nextDate) {
		this.nextDate = nextDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getLastSentDate() {
		return lastSentDate;
	}

	public void setLastSentDate(Date lastSentDate) {
		this.lastSentDate = lastSentDate;
	}

	public int getNoOfFaildAttemps() {
		return noOfFaildAttemps;
	}

	public void setNoOfFaildAttemps(int noOfFaildAttemps) {
		this.noOfFaildAttemps = noOfFaildAttemps;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getCurrencyName() {
		return currencyName;
	}

	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getAccountTypeFlag() {
		return accountTypeFlag;
	}

	public void setAccountTypeFlag(String accountTypeFlag) {
		this.accountTypeFlag = accountTypeFlag;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

}
