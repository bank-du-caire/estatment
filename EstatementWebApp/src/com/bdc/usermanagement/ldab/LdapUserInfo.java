package com.bdc.usermanagement.ldab;

public class LdapUserInfo {
	
	
	private String giveName=null;
	private String surName=null;
	private String email=null;
	
	public String getGiveName() {
		return giveName;
	}
	public void setGiveName(String giveName) {
		this.giveName = giveName;
	}
	public String getSurName() {
		return surName;
	}
	public void setSurName(String surName) {
		this.surName = surName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	

}
