package com.bdc.usermanagement.ldab;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bdc.estatement.util.Utils;

@WebServlet("/UserAuditServlet")
public class UserAuditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		UserManagmentDBTransactions dbTransactions = new UserManagmentDBTransactions();
		if (request.getParameter("exportUserAudit") != null) {
			List<UserAudit> allUserAudits = new ArrayList<UserAudit>();
			allUserAudits = dbTransactions.findAllUserAutids();
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename=User Audits.xls");
			Utils.exportToExcelUserAudits(allUserAudits, response);
		} else if (request.getParameter("exportScreenAudit") != null) {
			List<ScreenAudit> allScreenAudits = new ArrayList<ScreenAudit>();
			allScreenAudits = dbTransactions.findAllScreenAudits();
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename=Screen Audits.xls");
			Utils.exportToExcelScreenAudits(allScreenAudits, response);
		} else if (request.getParameter("exportRoleAudit") != null) {
			List<RoleAudit> allRoleAudit = new ArrayList<RoleAudit>();
			allRoleAudit = dbTransactions.findAllroleAudits();
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename=Role Audits.xls");
			Utils.exportToExcelRoleAudits(allRoleAudit, response);
		} else if (request.getParameter("exportUserRoleAudit") != null) {
			List<UserRoleAudit> allUserRoleAudits = new ArrayList<UserRoleAudit>();
			allUserRoleAudits = dbTransactions.findAllUserRoleAudits();
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename=User Role Audits.xls");
			Utils.exportToExcelUserRoleAudits(allUserRoleAudits, response);
		} else if (request.getParameter("userNameSearch") != null && !request.getParameter("userNameSearch").isEmpty())// search
		{
			String searchUserName = request.getParameter("userNameSearch");

			List<UserAudit> allUserAudits = dbTransactions.findAllUserAutids(searchUserName);
			List<ScreenAudit> allScreenAudits = dbTransactions.findAllScreenAudits(searchUserName);
			List<RoleAudit> allRoleAudit = dbTransactions.findAllroleAudits(searchUserName);
			List<UserRoleAudit> allUserRoleAudits = dbTransactions.findAllUserRoleAudits(searchUserName);
			response.sendRedirect(request.getContextPath() + "/main/pages/tables/UsersAudit.jsp");
			HttpSession session = request.getSession();
			session.setAttribute("allUserAudits", allUserAudits);
			session.setAttribute("allScreenAudits", allScreenAudits);
			session.setAttribute("allRoleAudit", allRoleAudit);
			session.setAttribute("allUserRoleAudits", allUserRoleAudits);
		} else {
			List<UserAudit> allUserAudits = new ArrayList<UserAudit>();
			List<ScreenAudit> allScreenAudits = new ArrayList<ScreenAudit>();
			List<RoleAudit> allRoleAudit = new ArrayList<RoleAudit>();
			List<UserRoleAudit> allUserRoleAudits = new ArrayList<UserRoleAudit>();

			allUserAudits = dbTransactions.findAllUserAutids();
			allScreenAudits = dbTransactions.findAllScreenAudits();
			allRoleAudit = dbTransactions.findAllroleAudits();
			allUserRoleAudits = dbTransactions.findAllUserRoleAudits();

			HttpSession session = request.getSession();
			session.setAttribute("allUserAudits", allUserAudits);
			session.setAttribute("allScreenAudits", allScreenAudits);
			session.setAttribute("allRoleAudit", allRoleAudit);
			session.setAttribute("allUserRoleAudits", allUserRoleAudits);

			response.sendRedirect(request.getContextPath() + "/main/pages/tables/UsersAudit.jsp");
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		UserManagmentDBTransactions dbTransactions = new UserManagmentDBTransactions();

		if (request.getParameter("exportUserAudit") != null) {
			List<UserAudit> allUserAudits = new ArrayList<UserAudit>();
			allUserAudits = dbTransactions.findAllUserAutids();
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename=User Audits.xls");
			Utils.exportToExcelUserAudits(allUserAudits, response);
		} else if (request.getParameter("exportScreenAudit") != null) {
			List<ScreenAudit> allScreenAudits = new ArrayList<ScreenAudit>();
			allScreenAudits = dbTransactions.findAllScreenAudits();
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename=Screen Audits.xls");
			Utils.exportToExcelScreenAudits(allScreenAudits, response);
		} else if (request.getParameter("exportRoleAudit") != null) {
			List<RoleAudit> allRoleAudit = new ArrayList<RoleAudit>();
			allRoleAudit = dbTransactions.findAllroleAudits();
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename=Role Audits.xls");
			Utils.exportToExcelRoleAudits(allRoleAudit, response);
		} else if (request.getParameter("exportUserRoleAudit") != null) {
			List<UserRoleAudit> allUserRoleAudits = new ArrayList<UserRoleAudit>();
			allUserRoleAudits = dbTransactions.findAllUserRoleAudits();
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename=User Role Audits.xls");
			Utils.exportToExcelUserRoleAudits(allUserRoleAudits, response);
		} else// search
		{
			String searchUserName = request.getParameter("userNameSearch");

			List<UserAudit> allUserAudits = dbTransactions.findAllUserAutids(searchUserName);
			List<ScreenAudit> allScreenAudits = dbTransactions.findAllScreenAudits(searchUserName);
			List<RoleAudit> allRoleAudit = dbTransactions.findAllroleAudits(searchUserName);
			List<UserRoleAudit> allUserRoleAudits = dbTransactions.findAllUserRoleAudits(searchUserName);
			response.sendRedirect(request.getContextPath() + "/main/pages/tables/UsersAudit.jsp");
			HttpSession session = request.getSession();
			session.setAttribute("allUserAudits", allUserAudits);
			session.setAttribute("allScreenAudits", allScreenAudits);
			session.setAttribute("allRoleAudit", allRoleAudit);
			session.setAttribute("allUserRoleAudits", allUserRoleAudits);
		}

	}

}
