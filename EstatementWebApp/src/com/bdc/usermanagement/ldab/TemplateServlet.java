package com.bdc.usermanagement.ldab;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;

/**
 * Servlet implementation class TemplateServlet
 */
@WebServlet("/TemplateServlet")
public class TemplateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public TemplateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		session.removeAttribute("USER_ID");
		session.removeAttribute("USER_Name");
		session.removeAttribute("FLAG_SUPER_USER");
		response.sendRedirect(request.getContextPath()
				+ "/JSPs/RulesAndScreens.jsp");
		return;
	}

}
